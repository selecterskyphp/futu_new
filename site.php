<?php
/**
 * 分类算法
 * 1级分类开始  10000
 * 2级分类开始  10100
 * 3级分类开始  10101
 */
return array(
    //'IMAGES_SOURCE_PATH'=>'E:/php/ftnew/upload/', //原始图保存路径 必须是绝对路径 必须/结束
    'IMAGES_SOURCE_PATH'=>'d:/web/futu_new/upload/', //原始图保存路径 必须是绝对路径 必须/结束
    //'IMAGES_SOURCE_HTTP'=>'http://imgadmin.photochina.com.cn:81/', //原始图网址
    'IMAGES_SOURCE_HTTP'=>'./', //原始图网址
    'IMAGES_THUMB_PATH'=>'./Uploads/thumb/',//缩略图保存路径 必须是相对路径
    //'IMAGES_THUMB_HTTP'=>'http://img.photochina.com.cn:82/',//缩略图网址
    'IMAGES_THUMB_HTTP'=>'./Uploads/thumb/',//缩略图网址
    'THUMB_SIZE' =>array(
        array('prefix'=>'s_','width'=>160,'height'=>160),
        array('prefix'=>'m_','width'=>560,'height'=>560)
    ),//缩略图前缀和尺寸
    'IMAGES_HEAD_PATH'=>'./Uploads/headimage/',//头像图片保存路径
    'IMAGES_HOT_PATH'=>'./Uploads/hot/',//首页热图图片保存路径
    'IMAGES_ALLOW_EXT'=>'*.jpg,*.jpeg,*.psd',//允许上传的图片格式
    'IMAGES_PREFIX'=>'p',//图片前缀 图片文件名为 前缀+图片自增ID号的MD5码
    'USER_TYPE'=>array(1=>'下载用户',2=>'摄影师'),//用户类型
    'USER_SUBTYPE'=>array(
        1=>array(
            1=>'包年用户',2=>'预付款用户',3=>'后结算用户',4=>'专线用户',5=>'下载合作机构',6=>'浏览用户'
        ),
        2=>array(1=>'摄影师',2=>'机构')
     ),
    //图组状态
    'IMAGE_STATUS'=>array(1=>'待审核',2=>'部分通过',3=>'全部通过',4=>'未通过',100=>'未上传图片',200=>'删除'),//0白色 1黄色 2绿色 3红色
    'IMAGE_STATUS_IMG'=>array(1=>'./Public/Images/write.jpg',2=>'./Public/Images/yellow.jpg',3=>'./Public/Images/green.jpg',4=>'./Public/Images/red.jpg',100=>'./Public/Images/write.jpg',200=>'./Public/Images/write.jpg'),//1白色 2黄色 3绿色 4红色
    'GROUP_C_TYPE'=>array(1=>'资料图片',2=>'时事图片'),
     //会员状态
    'USER_STATUS'=>array(0=>'待审核',1=>'正常',2=>'锁定',3=>'过期',200=>'已删除'),
     //支付类型
    'PAY_TYPES'=>array(1=>'<font color=green>充值</font>',2=>'<font color=red>消费</font>',3=>'<font color=yellow>结算</font>',4=>'<font color=green>销售</font>'),
    'BUY_TYPES'=>array(1=>'点数支付',2=>'张数支付',3=>'免费购买'),
    'IMAGE_STATUS_COLOR'=>array(1=>'qiu_white',2=>'qiu_yellow',3=>'qiu_gree',4=>'qiu_red',100=>'qiu_white',200=>'qiu_white',10=>'qiu_white'),
    
);