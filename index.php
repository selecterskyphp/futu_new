<?php
if (!is_file('./config.php')) header("location: ./Install");
header("Content-type: text/html; charset=utf-8");
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', true);
define('UPLOAD_PATH', './Uploads/');
define('VERSION', 'v1.0');
define('UPDATETIME', '20110806');
define('THINK_PATH', './Core');
define('APP_NAME', 'PhotoChina');
define('APP_PATH', './PhotoChina');
define('NO_CACHE_RUNTIME',true);

require(THINK_PATH."/Core.php");

App::run();
?>