﻿function showLogin()
{
	var login = 
'<div class="loginDiv">' +
'<h2>请登录或注册</h2>' +
'<span><a href="javascript:;" onclick="$.jBox.close(true)"><img src="'+PUB+'/images/close.gif" width="20" height="20" /></a></span>' +
'<em>为了购买图片或成为富图中国的一部分，您需要登录或注册。</em>' +
'<div class="loginContent">' +
'<form action="' + LOGIN_URL + '" method="post">' +
'  <input name="account" type="text" id="textfield2" style="padding-left:10px; width:333px; height:26px; line-height:26px; background:url('+PUB+'/images/loginInput.jpg) no-repeat; border:none" value="会员账号或电子邮箱" onfocus="cjx.chkfocusValue(this)" onblur="cjx.chkblurValue(this)" />' +
'  <input name="password" type="password" id="textfield2" style="padding-left:10px; width:333px; height:26px; line-height:26px; background:url('+PUB+'/images/loginInput.jpg) no-repeat; border:none" value="密码" " onfocus="cjx.chkfocusValue(this)" onblur="cjx.chkblurValue(this)" />' +
'  <div class="loginSelect"><input type="checkbox" name="checkbox" id="checkbox" /> 自动登录</div>' +
'  <div class="loginDl">' +
'    <input type="submit" name="button2" id="button2" value="" />' +
'  </div>' +
'</form>' +
'</div>' +
'<div class="tipLeft">还不是会员？<a href="javascript:;"  onclick="showregist()">免费注册！</a></div' +
'<div class="tipRight"><a href="javascript:;">忘记密码</a></div>' +
'' +
'</div>';

	var jboxConifg = {
		showIcon:false,
		showClose:false,
		draggable:false,
		title:null,
		width:379,
		opacity:0.5,
		buttons:{}
	};
	$.jBox(login,jboxConifg);
}

function showregist()
{
	$.jBox.close(true);
	var login = 
'<div class="loginDiv">' +
'<h2>用户注册</h2>' +
'<span><a href="javascript:;" onclick="$.jBox.close(true)"><img src="'+PUB+'/images/close.gif" width="20" height="20" /></a></span>' +
'<em>请选择你要注册的用户类型</em>' +
'<div class="loginContent">' +
'<a href="'+REG_PHOTO_URL+'"><img src="'+PUB+'/images/default_register_1.jpg" alt="注册成为签约摄影师" title="注册成为签约摄影师" /></a>让更多的人欣赏到您的佳作...<br />' +
'  <a href="'+REG_DOWN_URL+'"><img src="'+PUB+'/images/default_register_2.jpg" alt="注册成为图片下载用户" title="注册成为图片下载用户" /></a>超大容量的共享图片让您一饱眼福...' +
'  </div>' +
'<div class="tipLeft">已经是会员 ？</div' +
'<div class="tipRight"><a href="javascript:;"  onclick="showLogin()">返回登录！</a></div>' +
'' +
'</div>';
	var jboxConifg = {
		showIcon:false,
		showClose:false,
		draggable:false,
		title:null,
		width:379,
		opacity:0.5,
		buttons:{}
	};
	$.jBox(login,jboxConifg);
}

function showSearch()
{
	var head = document.getElementById('Menu');
	var headSeach = document.getElementById("gjSearch_div");
	if(head.style.display == '')
	{
		head.style.display = 'none';
		headSeach.style.display='';
	}
	else
	{
		head.style.display = '';
		headSeach.style.display='none';
	}
}
/**
 * 显示图片
 * @param id 图片ID
 * @param gid 图片所在图组
 * @param self 是否是摄影自己的图片 0不是 1是
 */
function showImages(id,gid,self)
{
	var url = 'index.php?m=Images&a=show&id='+id+'&gid='+gid+'&self='+self
	var jboxConifg = {
			showIcon:false,
			showClose:false,
			draggable:false,
			title:null,
			width:880,
			height:588,
			opacity:0.5,
			top:'5%',
			showScrolling:false,
			iframeScrolling:'no',
			buttons:{}
		};
		$.jBox('iframe:'+url,jboxConifg);
}
