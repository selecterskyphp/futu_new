<?php
$database = require ('./config.php');//数据库配置
$site = require './site.php';//自定义的网站相关配置
//框架相关配置
$config	= array(
		'DEFAULT_THEME' => 'Default',
		'DEFAULT_CHARSET' => 'utf-8',
		'APP_GROUP_LIST' => 'Home,Admin,Down,Photo',
        'APP_DEBUG'=>false,
        'URL_MODEL'=>0,//普通模式
		'DEFAULT_GROUP' =>'Home',
		'TMPL_FILE_DEPR' => '_',
		'TMPL_CACHE_ON' => false,//模版不使用缓存
		'DB_FIELDS_CACHE' => false,
		'DB_FIELDTYPE_CHECK' => true,
		'URL_CASE_INSENSITIVE'=>true,
		'URL_ROUTER_ON' => true,
		'URL_AUTO_REDIRECT' => false,
		'DEFAULT_LANG'   =>    'zh-cn',//默认语言
		'LANG_SWITCH_ON' => true, //多语言
		'TAG_EXTEND_PARSE' =>array(
			'if' => 'template_if',
			'else' =>'template_else',
			'elseif' => 'template_elseif'
		),
		'APP_AUTOLOAD_PATH'=> 'Think.Util.',
		'TAGLIB_LOAD' => true,
		//'TAGLIB_PRE_LOAD' => 'html,yp',
);
return array_merge($database, $config, $site);
?>