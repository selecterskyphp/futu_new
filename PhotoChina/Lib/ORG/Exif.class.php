<?php
/**
 * 
 * 获取图片的exif信息
 * @author Administrator
 *
 */
class Exif extends Think
{
    public $img=null;
    
    /*构造函数，检测exif和mbstring模块是否开启*/
    function __construct($img=null)
    {
        extension_loaded('exif')&&extension_loaded('mbstring') or
            die('exif module was not loaded,please check it in php.ini<br>NOTICE:On Windows,php_mbstring.dll must be before php_exif.dll');
        $this->img = $img;
    }
    function is_utf8($liehuo_net) 
    { 
        if (preg_match("/^([".chr(228)."-".chr(233)."]{1}[".chr(128)."-".chr(191)."]{1}[".chr(128)."-".chr(191)."]{1}){1}/",$liehuo_net) == true || preg_match("/([".chr(228)."-".chr(233)."]{1}[".chr(128)."-".chr(191)."]{1}[".chr(128)."-".chr(191)."]{1}){1}$/",$liehuo_net) == true || preg_match("/([".chr(228)."-".chr(233)."]{1}[".chr(128)."-".chr(191)."]{1}[".chr(128)."-".chr(191)."]{1}){2,}/",$liehuo_net) == true) 
        { 
            return true; 
        } 
        else 
        { 
            return false; 
        } 
    }
    //获取iptc信息
    public function getIptc($img = null)
    {
        if(!$img)
        {
            $img = $this->img;
        }
        else 
        {
            $this->img = $img;
        }
        //获取Iptc信息
        $size = GetImageSize($img,&$info);
        $iptc = null;
        $checkarr = array('2#020','2#005','2#025','2#120','2#080','2#105','2#015','2#101','2#095','2#090');
        if (isset($info["APP13"])) 
        {
            
          $iptc = iptcparse($info["APP13"]);
          //查询是否是utf8编码
          foreach ($checkarr as $k)
          {
              if(!$this->is_utf8($iptc[$k][0]))
              {
                  $iptc[$k][0] = trim(iconv('gb2312','utf-8',$iptc[$k][0]));
              }
              else 
              {
                  $iptc[$k][0] = trim($iptc[$k][0]);
              }
              if(!$iptc[$k][0])
              {
                  $iptc[$k][0] = '';
              }
          }
        }
        return $iptc;
        /*
         iptc 数组
         [2#005][0] 标题  放入图片说明 如果标题没有则放入大标题
         [2#025][0] 关键字
         [2#040][0] 简单说明(不要的)
         [2#055][0] 创建时间 如：20111221 表示2011-12-21 用strtotime转换成时间整数
         [2#080][0] 创建作者
         [2#085][0] 职务
         [2#090][0] 城市 (原稿) 
         [2#092][0] 子位置
         [2#095][0] 州  （原稿 放入省份）
         [2#100][0] 国家地区代码
         [2#101][0] 国家 （原稿）
         [2#103][0] 作业标识
         [2#105][0] 大标题 放图组标题 
         [2#110][0] 来源附注
         [2#115][0] 源
         [2#116][0] 版权公告
         [2#120][0] 详细说明  放入图组说明
         [2#122][0] 说明作者
         [2#020][0] 类别
         */
    }
    /*
    * 一个全面获取图象信息EXIF的函数
    *
    * @access public
    * @param string $img 图片路径
    * @return array
    */
    function GetImageInfoVal($ImageInfo,$val_arr) {
        $InfoVal    =    "未知";
        foreach($val_arr as $name=>$val) {
            if ($name==$ImageInfo) {
                $InfoVal    =    &$val;
                break;
            }
        }
        return $InfoVal;
    }
    function getInfo($type='basic',$img=null) 
    {
        if(!$img)
        {
            $img = $this->img;
        }
        else
        {
            $this->img = $img;
        }
        $imgtype            =    array("", "GIF", "JPG", "PNG", "SWF", "PSD", "BMP", "TIFF(intel byte order)", "TIFF(motorola byte order)", "JPC", "JP2", "JPX", "JB2", "SWC", "IFF", "WBMP", "XBM");
        $Orientation        =    array("", "top left side", "top right side", "bottom right side", "bottom left side", "left side top", "right side top", "right side bottom", "left side bottom");
        $ResolutionUnit        =    array("", "", "英寸", "厘米");
        $YCbCrPositioning    =    array("", "the center of pixel array", "the datum point");
        $ExposureProgram    =    array("未定义", "手动", "标准程序", "光圈先决", "快门先决", "景深先决", "运动模式", "肖像模式", "风景模式");
        $MeteringMode_arr    =    array(
            "0"        =>    "未知",
            "1"        =>    "平均",
            "2"        =>    "中央重点平均测光",
            "3"        =>    "点测",
            "4"        =>    "分区",
            "5"        =>    "评估",
            "6"        =>    "局部",
            "255"    =>    "其他"
            );
        $Lightsource_arr    =    array(
            "0"        =>    "未知",
            "1"        =>    "日光",
            "2"        =>    "荧光灯",
            "3"        =>    "钨丝灯",
            "10"    =>    "闪光灯",
            "17"    =>    "标准灯光A",
            "18"    =>    "标准灯光B",
            "19"    =>    "标准灯光C",
            "20"    =>    "D55",
            "21"    =>    "D65",
            "22"    =>    "D75",
            "255"    =>    "其他"
            );
        $Flash_arr            =    array(
            "0"        =>    "flash did not fire",
            "1"        =>    "flash fired",
            "5"        =>    "flash fired but strobe return light not detected",
            "7"        =>    "flash fired and strobe return light detected",
            );
     
        $exif = exif_read_data ($img,"IFD0");
        if (!$exif) 
        {
            return null;
        }
        $exif = exif_read_data ($img,0,true);
        $iptc = $this->getIptc();
        if('basic' === $type)
        {
            $data=array (
                //文件信息-----------------------------,
                'FileName'=>array("文件名",trim($exif[FILE][FileName])),
                'FileType' =>array("文件类型",$imgtype[$exif[FILE][FileType]]),
                'MimeType' =>array("文件格式",$exif[FILE][MimeType]),
                'FileSize' => array("文件大小",$exif[FILE][FileSize]),
                'FileDateTime' => array("时间戳",$exif[FILE][FileDateTime]),
            	'DateTimeOriginal'=>array("拍摄时间",$exif[EXIF][DateTimeOriginal]),
            	'Height'=>array("拍摄分辨率高",$exif[COMPUTED][Height]),
                'Width'=>array("拍摄分辨率宽",$exif[COMPUTED][Width]),
                //图像信息-----------------------------
                'Artist'=>array("作者",isset($exif[IFD0][Artist])?trim($exif[IFD0][Artist]):''),
                
                'iptcTitle' =>array('图片说明',$iptc['2#005'][0]),//标题 如果标题没有则放入大标题
                'iptcKeyword'=>array('关键字',$iptc['2#025'][0]),
                'iptcDesc'=>array('图组说明 ',$iptc['2#120'][0]),//详细说明  放入图组说明
                'iptcAuth'=>array('作者',$iptc['2#080'][0]),//创建作者
                'iptcBigTitle'=>array('图组标题', $iptc['2#105'][0]),//大标题   
                'iptcCate'=>array('类别',$iptc['2#020'][0]),
                'iptcCountry'=>array('国家/地区',$iptc['2#101'][0]),
                'iptcProvince'=>array('省份/州',$iptc['2#095'][0]),//州  （原稿 放入省份）
                'iptcCity'=>array('城市',$iptc['2#090'][0]),
            );
        }
        else 
        {
            $data=array (
                //文件信息-----------------------------,
                'FileName'=>array("文件名",$exif[FILE][FileName]),
                'FileType' =>array("文件类型",$imgtype[$exif[FILE][FileType]]),
                'MimeType' =>array("文件格式",$exif[FILE][MimeType]),
                'FileSize' => array("文件大小",$exif[FILE][FileSize]),
                'FileDateTime' => array("时间戳",$exif[FILE][FileDateTime]),
                //图像信息-----------------------------
                'ImageDescription' => array("图片说明",$exif[IFD0][ImageDescription]),
                'Make'=>array("制造商",$exif[IFD0][Make]),
                'Model'=>array("型号",$exif[IFD0][Model]),
                'Orientation'=>array("方向",$Orientation[$exif[IFD0][Orientation]]),
                'ResolutionUnit'=>array("水平分辨率",$exif[IFD0][XResolution].$ResolutionUnit[$exif[IFD0][ResolutionUnit]]),
                'ResolutionUnit'=>array("垂直分辨率",$exif[IFD0][YResolution].$ResolutionUnit[$exif[IFD0][ResolutionUnit]]),
                'Software' => array("创建软件",$exif[IFD0][Software]),
                'DateTime'=>array("修改时间",$exif[IFD0][DateTime]),
                'Artist'=>array("作者",$exif[IFD0][Artist]),
                'YCbCrPositioning'=>array("YCbCr位置控制",$YCbCrPositioning[$exif[IFD0][YCbCrPositioning]]),
                'Copyright'=>array("版权",$exif[IFD0][Copyright]),
                'Photographer'=>array("摄影版权",$exif[COMPUTED][Copyright.Photographer]),
                'Editor'=>array("编辑版权",$exif[COMPUTED][Copyright.Editor]),
                //拍摄信息-----------------------------
                'ExifVersion'=>array("Exif版本",$exif[EXIF][ExifVersion]),
                'FlashPixVersion'=>array("FlashPix版本","Ver. ".number_format($exif[EXIF][FlashPixVersion]/100,2)),
                'DateTimeOriginal'=>array("拍摄时间",$exif[EXIF][DateTimeOriginal]),
                'DateTimeDigitized'=>array("数字化时间",$exif[EXIF][DateTimeDigitized]),
                'Height'=>array("拍摄分辨率高",$exif[COMPUTED][Height]),
                'Width'=>array("拍摄分辨率宽",$exif[COMPUTED][Width]),
                /*
                The actual aperture value of lens when the image was taken.
                Unit is APEX.
                To convert this value to ordinary F-number(F-stop),
                calculate this value's power of root 2 (=1.4142).
                For example, if the ApertureValue is '5', F-number is pow(1.41425,5) = F5.6.
                */
                'ApertureValue'=>array("光圈",$exif[EXIF][ApertureValue]),
                'ShutterSpeedValue'=>array("快门速度",$exif[EXIF][ShutterSpeedValue]),
                'ApertureFNumber'=>array("快门光圈",$exif[COMPUTED][ApertureFNumber]),
                'MaxApertureValue'=>array("最大光圈值","F".$exif[EXIF][MaxApertureValue]),
                'ExposureTime'=>array("曝光时间",$exif[EXIF][ExposureTime]),
                'FNumber'=>array("F-Number",$exif[EXIF][FNumber]),
                'MeteringMode'=>array("测光模式",$this->GetImageInfoVal($exif[EXIF][MeteringMode],$MeteringMode_arr)),
                'LightSource'=>array("光源",$this->GetImageInfoVal($exif[EXIF][LightSource], $Lightsource_arr)),
                'Flash'=>array("闪光灯",$this->GetImageInfoVal($exif[EXIF][Flash], $Flash_arr)),
                'ExposureMode'=>array("曝光模式",($exif[EXIF][ExposureMode]==1?"手动":"自动")),
                'WhiteBalance'=>array("白平衡",($exif[EXIF][WhiteBalance]==1?"手动":"自动")),
                'ExposureProgram'=>array("曝光程序",$ExposureProgram[$exif[EXIF][ExposureProgram]]),
                /*
                Brightness of taken subject, unit is APEX. To calculate Exposure(Ev) from BrigtnessValue(Bv), you must add SensitivityValue(Sv).
                Ev=Bv+Sv   Sv=log((ISOSpeedRating/3.125),2)
                ISO100:Sv=5, ISO200:Sv=6, ISO400:Sv=7, ISO125:Sv=5.32.
                */
                'ExposureBiasValue'=>array("曝光补偿",$exif[EXIF][ExposureBiasValue]."EV"),
                'ISOSpeedRatings'=>array("ISO感光度",$exif[EXIF][ISOSpeedRatings]),
                'ComponentsConfiguration'=>array("分量配置",(bin2hex($exif[EXIF][ComponentsConfiguration])=="01020300"?"YCbCr":"RGB")),//'0x04,0x05,0x06,0x00'="RGB" '0x01,0x02,0x03,0x00'="YCbCr"
                'CompressedBitsPerPixel'=>array("图像压缩率",$exif[EXIF][CompressedBitsPerPixel]."Bits/Pixel"),
                'FocusDistance'=>array("对焦距离",$exif[COMPUTED][FocusDistance]."m"),
                'FocalLength'=>array("焦距",$exif[EXIF][FocalLength]."mm"),
                'FocalLengthIn35mmFilm'=>array("等价35mm焦距",$exif[EXIF][FocalLengthIn35mmFilm]."mm"),
                /*
                Stores user comment. This tag allows to use two-byte character code or unicode. First 8 bytes describe the character code. 'JIS' is a Japanese character code (known as Kanji).
                '0x41,0x53,0x43,0x49,0x49,0x00,0x00,0x00':ASCII
                '0x4a,0x49,0x53,0x00,0x00,0x00,0x00,0x00':JIS
                '0x55,0x4e,0x49,0x43,0x4f,0x44,0x45,0x00':Unicode
                '0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00':Undefined
                */
                'UserCommentEncoding'=>array("用户注释编码",$exif[COMPUTED][UserCommentEncoding]),
                'UserComment'=>array("用户注释",$exif[COMPUTED][UserComment]),
                'ColorSpace'=>array("色彩空间",($exif[EXIF][ColorSpace]==1?"sRGB":"Uncalibrated")),
                'ExifImageLength'=>array("Exif图像宽度",$exif[EXIF][ExifImageLength]),
                'ExifImageWidth'=>array("Exif图像高度",$exif[EXIF][ExifImageWidth]),
                'FileSource'=>array("文件来源",(bin2hex($exif[EXIF][FileSource])==0x03?"digital still camera":"unknown")),
                'SceneType'=>array("场景类型",(bin2hex($exif[EXIF][SceneType])==0x01?"A directly photographed image":"unknown")),
                'FileType'=>array("缩略图文件格式",$exif[COMPUTED][Thumbnail.FileType]),
                'MimeType'=>array("缩略图Mime格式",$exif[COMPUTED][Thumbnail.MimeType]),
                'iptcTitle' =>array('标题',$iptc['2#005'][0]),
                'iptcKeyword'=>array('关键字',$iptc['2#025'][0]),
                'iptcMemo'=>array('说明 ',$iptc['2#040'][0]),//简单说明
                'iptcDesc'=>array('说明 ',$iptc['2#120'][0]),//详细说明
                'iptcAuth'=>array('作者',$iptc['2#122'][0]),//创建说明的作者
                'iptcBigTitle'=>array('大标题',$iptc['2#105'][0]),
                'iptcCate'=>array('类列',$iptc['2#015'][0])
            );
        }
        return $data;
    }
}
