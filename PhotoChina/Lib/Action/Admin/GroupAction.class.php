<?php

class GroupAction extends AdminbaseAction {
	private  $dao,$edit_id,$groupid;
	private $orderfield,$order,$username;
	function _initialize()
	{

		parent::_initialize();
		$this->username = $_SESSION['username'];
		$edit_group_msg = '';
		$this->edit_id = isset($_REQUEST['edit_id'])?intval($_REQUEST['edit_id']):0;
		$this->groupid = isset($_REQUEST['groupid'])?intval($_REQUEST['groupid']):0;
		$this->orderfield = isset($_REQUEST['orderfield'])?trim($_REQUEST['orderfield']):'id';
		$this->order = isset($_REQUEST['order'])?trim($_REQUEST['order']):'desc';
		$mod = M('GrouponDetail');
		if($this->edit_id)
		{
		    $result = $mod->where('id='.$this->edit_id)->field('title')->find();
		    if($result)
		    {
		        $edit_group_msg = '<a href="'.U('Group/edit',array('edit_id'=>$this->edit_id)).'"><font color=red>当前编辑图组:'.$result['title'].'</font></a>';
		    }
		}
		else 
		{
		    $result = $mod->where('state=\'100\' and check_oper=\''.$_SESSION['username'].'\'')->field('id,title')->find();
		    if($result)
		    {
		        $edit_group_msg = '<font color=red>当前编辑图组:'.$result['title'].'</font>';
		        $this->edit_id = $result['id'];
		    }
		    else 
		    {
		        $edit_group_msg = '<font color=red>当前没有正在编辑的图组!</font>';
		    }
		}
		$this->assign('edit_id',$this->edit_id);
		$this->assign('groupid',$this->groupid);
		$this->assign('orderfield',$this->orderfield);
		$this->assign('order',$this->order);
		$this->assign('edit_group_msg',$edit_group_msg);
		//$this->dao=M('userrequire');
	}
	
	public function index() {

		$columnM = M('column');
		$list = $columnM->where('parentid=0')->order('sort')->select();
		$column = array();
		foreach($list as $v)
		{
		    $v['sub'] = $columnM->where('parentid='.$v['groupid'])->order('sort')->select();
		    $column[] = $v;
		}
		$this->assign('column',$column);
		import ('@.ORG.Page');
		//查询
		$type=isset($_GET['type'])?intval($_GET['type']):0;
		$username=isset($_GET['username'])?trim($_GET['username']):'';
		$author=isset($_GET['author'])?trim($_GET['author']):'';
		$state=isset($_GET['state'])?intval($_GET['state']):1;
		$start=isset($_GET['start_query'])?strtotime($_GET['start_query']):'';
		$end=isset($_GET['end_query'])?strtotime($_GET['end_query']):'';
		$name=isset($_GET['group_name'])?trim($_GET['group_name']):'';
		//$where = 'state=\'1\'';
		$where='';
		$this->assign($_GET);
		if(0 !== $state)
		{
		    $where .= ' state=\''.$state.'\'';
		}
		else 
		{
		    $where .= ' state<>\'0\'';
		}
		
		$this->assign('state',$state);
		
		if($start>0 && $end>0)
	    {
	        $where .= ' and up_time between '.$start.' and ' . $end;
	    }
	    elseif($start>0)
	    {
	        $end = $start + 60*60*24*3;//3天内
	        $where .= ' and up_time between '.$start.' and ' . $end;
	    }
	    elseif($end>0)
	    {
	        $start = $end - 60*60*24*3;
	        $where .= ' and up_time between '.$start.' and ' . $end;
	    }
	    else 
	    {
	        $end = time();
	        $start = $end - 60*60*24*3;
	        $where .= ' and up_time between '.$start.' and ' . $end;
	    }
	    $this->assign('start',$start);
	    $this->assign('end',$end);
		$group=M('group_detail');
		if($type > 0)
		{
		    $max = $type+10000;
			$where .= " and type_one>=".$type." and type_one<".$max;
		}
		if( strlen($name)>0 )
		{
			$where .=" and title like '%".$name."%'";
		}
	    if( strlen($username)>0 )
		{
			$where .= " and account='".$username."'";
		}
	    if( $author )
		{
			$where .= " and author='".$author."'";
		}
		$count=$group->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		$this->assign("page",$show);

		$orderinfo=$this->orderfield." ".$this->order;
		
		$list=$group->order($orderinfo)->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
		//var_dump($group->getlastsql(),$list);
		//exit;
		$this->assign('mlist',$list);
		
		$this->display();
	}
	
	//图组编辑
	public function edit() {
		
		//刷新当前用户正在编辑的图组
		$gid = $this->edit_id;
		$edGroup=M('GrouponDetail');
		if($gid<=0)
		{
    		$this->error('没有正在编辑的图组');
		}
	    $where=" id=".$gid;
	    $vo=$edGroup->where($where)->find();
	    //var_dump($where);
	    //exit;
		if(!$vo)
		{
			//$this->display('PhotoerImageGroup_groupnull');
			$this->error('没有正在编辑的图组');
			//return;
		}
		$gid = $vo['id'];
		$where=" group_id=".$gid;
		$remarkM = M('GrouponRemark');
		$vo['remark'] = $remarkM->where($where)->getField('remark');
		
		//找出3种类型的名字
		$col=M('column');
		if(!empty($vo['type_one']) && $vo['type_one']>0 )
		{
			$where=" groupid=".$vo['type_one'];
			$nameone=$col->where($where)->find();
			$vo['type_one_name']=$nameone['name'];
		}
		if(!empty($vo['type_two']) && $vo['type_two']>0 )
		{
			$where=" groupid=".$vo['type_two'];
			$nametwo=$col->where($where)->find();
			$vo['type_two_name']=$nametwo['name'];
		}
		if(!empty($vo['type_three']) && $vo['type_three']>0 )
		{
			$where=" groupid=".$vo['type_three'];
			$namethree=$col->where($where)->find();
			$vo['type_three_name']=$namethree['name'];
		}
		
		//关键字分类
		$keytypeMod=M('keyword_type');
        $keytype=$keytypeMod->select();
        $this->assign('keytype',$keytype);
        $keyMod=M('keyword');
        $keylist=$keyMod->select();
        $this->assign('keylist',$keylist);
		$gdetail=M('GrouponImages');
		$sql = 'select b.*,c.remark from __TABLE__ a inner join '.C('DB_PREFIX').'images_detail b on a.images_id = b.id left join '.C('DB_PREFIX').'images_remark c on a.images_id=c.images_id where a.group_id='.$gid;
		$imagelist=$gdetail->query($sql);
		//var_dump($gdetail->getLastSql(),$imagelist);
		//exit;
		
		$this->assign('vo',$vo);
		$this->assign('imagelist',$imagelist);
		
		$this->display();
	}
   
	//审核和删除所选图组
	function checkwithdel()
	{
		$ids=$_POST['ids'];
		$g=M('group_detail');
		if($_POST['notchecked']) //审核不通过
		{ 
			for($i=0;$i<count($ids);$i++)
			{
				$data['state']='4';
				$g->where(' id='.$ids[$i])->save($data);
			}
		}
		else if($_POST['delchecked']) //删除
		{
			for($i=0;$i<count($ids);$i++)
			{
				$data['state']='200'; //状态10为已删除
				$g->where(' id='.$ids[$i])->save($data);
			}
		}
	    else if($_POST['redelchecked']) //还原
		{
			for($i=0;$i<count($ids);$i++)
			{
				$data['state']='1'; //状态10为已删除
				$g->where(' id='.$ids[$i])->save($data);
			}
		}
		$this->redirect('Group/index',array('edit_id'=>$this->edit_id));
		//$this->query();
	}
	
	//点开图组
	function images()
	{
	    $username=$_SESSION['username'];
		$groupId=$this->groupid;
		
		//图组分类，创建时间，编辑时间
		$upGroup=M('group_detail');
		$vo=$upGroup->getById($groupId);

		//判断该图组是否是在编辑状态
		if( $vo['edit_state']=='1' && $vo['check_oper'] != $username && strlen($vo['check_oper'])>1)
		{
			$this->error('该图组已经是编辑状态');
		}
		$remarkM = M('GroupRemark');
		$vo['remark']= $remarkM->where('group_id='.$groupId)->getField('remark');
		$up=M('images_detail');
		$sql = "select * from __TABLE__ a left join ".C('DB_PREFIX').'images_remark b on a.id=b.images_id where a.old_group_id='.$groupId;
		$imagelist=$up->query($sql);
		
		//把该图组置于编辑状态
		$data = array();
		$data['edit_state']='1';
		$data['check_oper']=$username;
		$upGroup->where(' id='.$groupId)->save($data);
		//var_dump($imagelist);
		//exit;
		$this->assign('vo',$vo);
		$this->assign('imagelist',$imagelist);

		$this->display();
	}
	
	//新建编辑图组
	function addgroup()
	{
		//创建该编辑的 编辑图组
		$username=$_SESSION['username'];
		$upgroupid=$this->groupid;
		
		//清空之前的编辑图组
		//1.找出属于这个用户的GROUPid
		$eg=M('GrouponDetail');
		$where =" (check_oper='".$username."') and state='100'";
		$list=$eg->where($where)->select();
		//$this->ajaxReturn('',$eg->getLastSql(),1);
		if($list)
		{
		    $this->ajaxReturn('','您有编辑未完成的图组，不能创建新图组',1);
		   //$this->error('您有编辑未完成的图组，不能创建新图组');
		}
	
		//取原图组的上传时间个上传人
		$ug=M('GroupDetail');
		$ulist=$ug->where(' id='.$upgroupid)->find();
			
		//创建该用户的图组
		$data['image_count']=0;
		$data['up_time']=time();
		$data['update_time']=time();
		$data['check_oper']=$username;
		$data['state']='100';
		//$data['main_url']=$ulist['main_url'];
		$data['c_type']=$_POST['c_type'];
		
		
		$data['city']=$_POST['city'];
		$data['group_key']=checkstr($_POST['gkey'],0,128);
		$data['title']=$_POST['gtitle'];
		$data['province']=$ulist['province'];
		$data['photo_date']=$ulist['photo_date'];
		$data['author']=$ulist['author'];
		$result = $eg->add($data);
		if($result)
		{
		    $groupid = $eg->getLastInsID();
		    $data =array();
		    $data['group_id']=$groupid;
		    $data['remark'] = $_POST['gremark'];
		    $mod = M('GrouponRemark');
		    $result = $mod->data($data)->add();
		    
		    if($result)
		    {
		        $this->ajaxReturn('','成功',0);
		    }
		    else
		    {
		        $this->ajaxReturn('','添加描述失败',1);
		    }
		}
		else
		{
		    $this->ajaxReturn('','操作数据库失败',1);
		}
	}
	
	//复制单张或多张图片
	function copy()
	{
		$username=$_SESSION['username'];
		$ids=$_GET['ids'];
		$idarr = explode(',', $ids);
		//$this->ajaxReturn('',$ids,1);
		if(empty($idarr))
		{
		    $this->ajaxReturn('','没有选择要操作的图片',1);
		}
		
		$old_group_id=$_GET['groupid'];
		$old_groupM = M('GroupDetail');
		//摄影师的图组是否存在 
	    $where = 'id='.$old_group_id;
		$oldGroupData = $old_groupM->where($where)->find();
		if(!$oldGroupData)
		{
		    $this->ajaxReturn('','编辑图组不存在',1);
		}
		//判断该用户是否有编辑图组
		$eg=M('GrouponDetail');
		if($this->edit_id<=0)
		{
		    $this->ajaxReturn('','当前没有正在编辑的图组',1);
		}
	    $where = 'id='.$this->edit_id;
		$glist=$eg->where($where)->find();
		if(!$glist)
		{
			$this->ajaxReturn('','编辑图组不存在',1);
		}
		
		$image=M('images_detail');
		//如果没有设置封图  设置新图组的封面图
        if(strlen($glist['main_url'])<5)
        {
            for($i=0;$i<count($idarr);$i++)
            {
                $result = $image->where('id='.$idarr[$i])->find();
                if($result)
                {
                    $data = array();
                    $data['main_url']=$result['url'];
                    $data['main_url_date']=$result['up_time'];
                    $data['main_url_userid']=$result['userid'];
                    $where = 'id='.$this->edit_id;
                    $eg->where($where)->save($data);
                    break;
                }
            }
        }
		//拷贝该图片信息到编辑图组
		$values = '';
		$update_sql = '';
		$gimages = M('GrouponImages');
		$gremark = M('ImagesRemark');
		//循环检查产品是否已经在编辑图组中存在 如不存在则新增
		for($i=0;$i<count($idarr);$i++)
		{
		    $id = $idarr[$i];
		    $result = $gimages->where('group_id='.$this->edit_id.' and images_id='.$id)->find();
		    if(!$result)
		    {
		        //$this->ajaxReturn('',$result.'='.$id,1);
		        if('' == $values)
		        {
		            $values = '('.$this->edit_id.','.$id.')';
		            $update_sql = 'id='.$id;
		        }
		        else
		        {
		            $values .= ',('.$glist['id'].','.$id.')';
		            $update_sql .= ' or id='.$id;
		        }
		        $sql = "update __TABLE__ set remark=old_remark where images_id=".$id;
		        $gremark->execute($sql);
		        $sql = "update __TABLE__ set state='3',check_time='".time()."',check_oper='".$username."' where state<>'3' and id=".$id;
		        $result = $image->execute($sql);
		        if($result)
		        {
		            $this->updateGroupState($old_group_id);
		        }
		        //$data = array();
		        //$data['remark'] = $remark;
		        //$gremark->where('iamges_id='.$id)->save($data);
		        //$this->ajaxReturn('',$gremark->getlastsql(),1);
		    }
		}
		if('' !== $values)
		{
		    $mod = M('GrouponImages');
    		$sql = 'insert into __TABLE__ (group_id,images_id)values'.$values;
    		$mod->execute($sql);
    		//重新计算编辑图组的图片数量
    	    $count = $image->where("group_id=".$this->edit_id)->count();
    	    $mod = M('GrouponDetail');
    	    $data = array();
    	    $data['image_count'] = $count;
    	    $mod->where('id='.$this->edit_id)->save($data);
    		$this->ajaxReturn('','',0);
		}
		else
		{
		    $this->ajaxReturn('','图片已经在当前编辑的图组中存在',1);
		}
	}
	//单张或多张图片审核不成功
	function checknotpass()
	{
	    $username=$_SESSION['username'];
		$ids=$_GET['ids'];
		$idsarr = explode(',', $ids);
	    if(empty($idsarr))
		{
		    $this->ajaxReturn('','没有选择要操作的图片',1);
		}
	    $old_group_id=$this->groupid;
		$old_groupM = M('GroupDetail');
		//摄影师的图组是否存在 
		$where = 'id='.$old_group_id;
		$oldGroupData = $old_groupM->where($where)->find();
		if(!$oldGroupData)
		{
		    $this->ajaxReturn('123','编辑图组不存在',0);
		}
		
		//审核不通过
		//$gimages = M('GrouponImages');
		//$result = $gimages->where
		$image=M('images_detail');
		$where = str_replace(',', ' or id=', $ids);
		$where = 'state<>\'4\' and (id='.$where.')';
		$data = array();
	    $data['state'] = '4';
        $data['check_time']=time();//
	    $data['check_oper']=$username;//
	    $result = $image->where($where)->save($data);
	    if($result)
	    {
	        //统计已经审核的图片数量，并设置摄影师整个图组的状态
    		$this->updateGroupState($old_group_id);
    		$this->ajaxReturn('','',0);
	    }
	    else
	    {
	        $this->ajaxReturn('','当前图片已经是不通过状态',1);
	    }
	    
	}
	
	//移除编辑图组的图片，单张或多张
	function removeeditimg()
	{
	    if(0 == $this->edit_id)
	    {
	        $this->error('当前没有正在编辑的图组');
	    }
		$ids=$_GET['ids'];
		$idsarr = explode(',', $ids);
	    if(empty($idsarr))
		{
		    $this->error('没有选择要操作的图片');
		}
		$image=M('GrouponImages');
		foreach($idsarr as $v)
		{
		    $where = 'group_id='.$this->edit_id.' and images_id='.$v;
		    $image->where($where)->delete();
		}
	    //重新计算编辑图组的图片数量
	    $count = $image->where("group_id=".$this->edit_id)->count();
	    $mod = M('GrouponDetail');
	    $data = array();
	    $data['image_count'] = $count;
	    $mod->where('id='.$this->edit_id)->save($data);
	    $this->redirect('Group/edit',array('edit_id'=>$this->edit_id));
	}
	
	function delgroup()
	{
	    $gid = $this->groupid;
	    if(!$gid)
	    {
	        $this->error('参数错误');
	    }
	    $data = array();
	    $data['state'] = '200';
	    $data['edit_state'] = '0';
	    $data['check_time'] = time();
	    $data['check_oper'] = $this->username;
	    $mod = M('GroupDetail');
	    $result = $mod->where('id='.$gid)->save($data);
	    if($result)
	    {
	        $this->assign('jumpUrl',U('Group/index',array('edit_id'=>$this->edit_id)));
	        $this->success('删除成功，即将跳转摄影师图组');
	    }
	    else
	    {
	        $this->error('删除失败');
	    }
	}
	
	//移除该编辑正在编辑的图组
	//$type 0 外部URL调用移除  1内部调用 有返回值 
	function deleditgroup()
	{
	    if(0 == $this->edit_id)
	    {
	        $this->error('当前没有正在编辑的图组');
	    }
		//找出编辑图组
		$gid = $this->edit_id;
		$mod=M('GrouponDetail');
		$glist=$mod->getById($gid);
		if(!$glist)
		{
		    $this->error('图组不存在');
		}
		if($glist['state'] == '100')
		{
		    //当前图组在编辑状态，直接删除
		    $image=M('GrouponImages');
    		$where='group_id='.$gid;
    		//删除该编辑组的图片
    		$image->where($where)->delete();
    		//删除该编辑组
    		$result = $mod->where('id='.$gid)->delete();
		}
		else
		{
		    $data = array();
	        $data['state'] = '200';
	        $where = 'id='.$gid;
	        $result = $mod->where($where)->save($data);
		}
	    if($result)
	    {
	        $this->assign('jumpUrl',U('Group/index'));
	        $this->success('删除成功，即将跳转到摄影师图组');
	    }
		else 
		{
		    $this->error('删除失败!');
		}
	}
	
	//只保存组信息
	function savegroupinfo()
	{
		$typeone=$_POST['typeoneid'];
		$typetwo=$_POST['typetwoid'];
		$typethree=$_POST['typethreeid'];
		
		//通过ID找到菜单的名字
		$typeonename="";
		$typetwoname="";
		$typethreename="";
		
		$m=M('column');
		$is_set_cate = false;
		if( intval($typeone)>0 )
		{
			$t1=$m->where(' groupid='.$typeone)->find();
			$typeonename=$t1['name'];
			$is_set_cate = true;
		}
		
		if( intval($typetwo)>0 )
		{
			$t2=$m->where(' groupid='.$typetwo)->find();
			$typetwoname=$t2['name'];
			$is_set_cate = true;
		}
		
		if( intval($typethree)>0 )
		{
			$t3=$m->where(' groupid='.$typethree)->find();
			$typethreename=$t3['name'];
			$is_set_cate = true;
		}
		if(!$is_set_cate)
		{
		    $this->errmsg = '没有设置图组分类';
		    return false;
		}
		//发生时间，发生地点
		$time=strtotime($_POST['gtime']);
		if($time<=0)
		{
		    $this->errmsg = '没有设置拍摄时间';
		    return false;
		}
		
		$country=checkstr($_POST['country'],0,20);
		if(!$country)
		{
		    $country = '';
		    //$this->errmsg = '没有选择国家';
		    //return false;
		}
		$city=checkstr($_POST['city']);
	    $province=checkstr($_POST['province'],0,20);
		if(!$city)
		{
		    $city=checkstr($_POST['city1'],0,20);
		}
		if(!$province)
		{
		    $province=checkstr($_POST['province1'],0,20);
		}
	    if(!$province)
		{
		    
		    $province = '';
		    //$this->errmsg = '没有选择省份';
		    //return false;
		}
	    if(!$city)
		{
		    //$this->errmsg = '没有选择城市';
		    //return false;
		}
		//var_dump($_POST);
		//exit;
		//关键词
		$key=checkstr($_POST['gkey'],0,128);
		if(!$key)
		{
		    //$this->errmsg = '没有填写图组关键字';
		    //return false;
		}
		
		//标题
		$title=checkstr($_POST['gtitle'],0,128);
	    if(!$title)
		{
		    $this->errmsg = '没有填写图组标题';
		    return false;
		}
		$remark=checkstr($_POST['gremark'],0,4000);
	    if(!$remark)
		{
		    $this->errmsg = '没有填写图组说明';
		    return false;
		}
		
		//更新信息
		$group=M('GrouponDetail');
		$data['type_one']=$typeone;
		$data['type_two']=$typetwo;
		$data['type_three']=$typethree;
		
		$data['type_one_name']=$typeonename;
		$data['type_two_name']=$typetwoname;
		$data['type_three_name']=$typethreename;
		
		$data['photo_date']=$time;
		$data['update_time']=time();
	    $data['city']=$city;
	    $data['province']=$province;
	    $data['country']=$country;
	    if($key)
	    {
		    $data['group_key']=$key;
	    }
		$data['title']=$title;
		//$data['remark']=$remark;
		//$where=" check_oper='".$username."' and state=100";
		$where = 'id='.$this->edit_id;
		$group->where($where)->save($data);
		//var_dump($group->getlastsql(),$_POST);
		//exit;
		$where = 'group_id='.$this->edit_id;;
	    $data = array();
	    $data['remark'] = $remark;
	    $mod = M('GrouponRemark');
	    $mod->where($where)->save($data);
	    return true;
	}
	
	//保存图组中图片信息
	public function saveeditgroup()
	{
		$type=$_POST['subtype'];
		
		//找出该编辑图组中的图片信息
		$group=M('GrouponDetail');
		//$where=" check_oper='".$username."' and state=100";
		//var_dump($_POST,$this->edit_id);
		//exit;
		$where = 'id='.$this->edit_id;
		$glist=$group->where($where)->find();
		if(!$glist)
		{
		    $this->error('编辑的图组不存在');
		}
		if(!$this->savegroupinfo())
		{
		    $this->error('保存图组信息失败：'.$this->getLastError());
		}
		$groupId=$glist['id'];
		
		$image=M('ImagesDetail');
		$gimage=M('GrouponImages');
		
		$where=" group_id=".$groupId;
		$imagelist=$gimage->where($where)->select();
		$st = '';
		
		$typeone=$_POST['typeoneid'];
		$typetwo=$_POST['typetwoid'];
		$typethree=$_POST['typethreeid'];
		
		
		for($i=0;$i<count($imagelist);$i++)
		{
			$id=$imagelist[$i]['images_id'];
			//地区
			$imgcountry=checkstr($_POST['imgcountry'.$id],0,20);
			$imgprovince=checkstr($_POST['imgprovince'.$id],0,20);
			$imgcity=checkstr($_POST['imgcity'.$id],0,20);
			
			//图片说明
			$remark=checkstr($_POST['imageremark'.$id],0,4000);
			
			//key
			$key=checkstr($_POST['imagekey'.$id],0,128);
			
			//点数
			$point=intval($_POST['point'.$id]);
			
			//人物姓名
			$rwname=checkstr($_POST['rwname'.$id],0,20);
			
			//艺名绰号
			$ymnick=checkstr($_POST['ymnick'.$id],0,20);
			
			//特写 半身 全身,0--全没选，1--特写 2--半身 3--全身
			$texie=isset($_POST['texie'.$id])?intval($_POST['texie'.$id]):0;
			
			
			//1人 2人 3人 合影，，0 1 2 3 4
			$ren=isset($_POST['ren'.$id])?intval($_POST['ren'.$id]):0;
			
			//横图 竖图 方图  0,1,2,3
			$position=isset($_POST['position'.$id])?intval($_POST['position'.$id]):0;
			
			//保存
			$where=" id=".$id;
			$data['type_one']=$typeone;
			$data['type_two']=$typetwo;
			$data['type_three']=$typethree;
			$data['images_key']=$key;
			$data['renwu_name']=$rwname;
			//$data['yiming']=$ymnick;
			$data['texie']=trim($texie);
			$data['ren']=trim($ren);
			$data['position']=trim($position);
			$data['value']=$point;
			$data['country']=$imgcountry;
			$data['province']=$imgprovince;
			$data['city']=$imgcity;
			$image->where($where)->save($data);
			//var_dump($image->getlastsql());
			//exit;
			$where = 'images_id='.$id;
			$data = array();
			$data['remark']=$remark;
			$mod = M('ImagesRemark');
			$mod->where($where)->save($data);
			//var_dump($mod->getlastsql());
			//exit;
		}
	    
		if( $type==1 ) //保存后提交
		{
			$gdata = array();
			//$gdata['check_time']=time();
			//$gdata['check_oper']=$username;
			$gdata['state']='1';//1待审核 2通过 3未通过 100编辑状态 200已删除
			$where=" state<>'2' and id=".$this->edit_id;
			$group->where($where)->save($gdata);//意思是除编辑已经发布图组外，其它只有是编辑过了，就改变图组状态为待审核
			$gimages = M("GrouponImages");
			$count = $gimages->where('group_id='.$this->edit_id)->count();
    		$data = array();
    		$data['image_count'] = $count;
    		$group->where('id='.$this->edit_id)->save($data);
    		
			$this->assign('jumpUrl',U('Groupon/index'));
			$this->success('操作成功');
		}
		
		//刷新页面
		$this->redirect('Group/edit',array('edit_id'=>$this->edit_id));
	}
	
	//设置封面图片
	function seteditface()
	{
		$id=$_GET['mainfaceid'];
		
		$group=M('GrouponDetail');
		
		$image=M('ImagesDetail');
		$where=" id=".$id;//" and group_id=".$glist['id'];
		$ilist=$image->where($where)->find();
		
		//$where=" id=".$glist['id'];
		$where=" id=".$this->edit_id;
		$data['main_url']=$ilist['url'];
		$data['main_url_date']=$ilist['up_time'];
		$data['main_url_userid']=$ilist['userid'];
		$result = $group->where($where)->save($data);
		if($result)
		{
		    $this->ajaxReturn('','',0);
		}
		else
		{
		    //var_dump($group->getlastsql());
		    //exit;
		    $this->ajaxReturn('','当前图片已经是封面图',1);
		}
        //$this->redirect('Photoerimagegroup/')
	}
	
	
	//旋转图片
	function imgRotation()
	{
		$id=$_GET['imgid'];
		$type=$_GET['rationtype'];
	
		$img=M('ImagesDetail');
		$list=$img->where(' id='.$id)->find();
	
		if( $type==0 ) $degrees=90;
		else $degrees=-90;
		$filename=getPicUrl($list['url'], $list['userid'], $list['up_time'],100,$list['bigurl']);
		$source = imagecreatefromjpeg($filename);
		if(!$source)
			$this->ajaxReturn('','源图不存在',1);
	
		// Rotate
		$rotate = imagerotate($source, $degrees,0);
	
		// Output
		//unlink($filename);
		if(!imagejpeg($rotate,$filename))
			$this->ajaxReturn($filename,'源图旋转失败',1);
	
		$filename=getPicUrl($list['url'], $list['userid'], $list['up_time'],0,'',true);
		//$this->ajaxReturn('','11-'.$filename,1);
		$source = imagecreatefromjpeg($filename);
		if(!$source)
			$this->ajaxReturn('','小图不存在',1);
	
		// Rotate
		$rotate = imagerotate($source, $degrees,0);
	
		// Output
		//unlink($filename);
		if(!imagejpeg($rotate,$filename))
			$this->ajaxReturn($filename,'小图旋转失败',1);
	
		$filename=getPicUrl($list['url'], $list['userid'], $list['up_time'],1,'',true);;
		$source = imagecreatefromjpeg($filename);
		if(!$source)
			$this->ajaxReturn($filename,'中图不存在',1);
	
		// Rotate
		$rotate = imagerotate($source, $degrees,0);
	
		// Output
		//unlink($filename);
		if(!imagejpeg($rotate,$filename))
			$this->ajaxReturn($filename,'中图旋转失败',1);
	
		$this->ajaxReturn('','',0);
	}

	//取消图组在线编辑状态
	function canceledit()
	{
		$id=$this->groupid;

		$g=M('group_detail');
		$data['edit_state']='0';
		if($g->where(' id='.$id)->save($data))
		{
			$this->success('取消编辑状态成功');
		}
		else
		{
			$this->success('取消编辑状态失败');
		}
		
		$this->success('取消编辑状态失败');
	}

	//返回
	function returngroup()
	{
		$g=M('group_detail');
		$data['edit_state']='0';
		$g->where(' id='.$this->groupid)->save($data);
		$this->redirect('Group/index',array('edit_id'=>$this->edit_id));
	}

	//图片替换
	function replaceimage()
	{
		$id=$_GET['id'];
		$vo['id']=$id;
		$this->assign('vo',$vo);
		$this->display();
	}
	
	function doreplaceimage()
	{
		$id=$_POST['rimageid'];

		//获取图片的url
		$img=M('ImagesDetail');
		$ilist=$img->where('id='.$id)->find();
		$userid = $ilist['userid'];
		
		
        $source_path = getPicUrl($ilist['url'], $ilist['userid'], $ilist['up_time'],100,$ilist['bigurl']);
        $pathinfo = pathinfo($source_path);
		
		import("@.ORG.UploadFile");
		import("@.ORG.Image");
		$upload = new UploadFile(); // 实例化上传类
		$upload->maxSize  = 3145728 ; // 设置附件上传大小
		$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg'); // 设置附件上传类型
		$upload->savePath =  $pathinfo['dirname'].'/'; // 设置附件上传目录
		$upload->$uploadReplace = true;
		$upload->thumb = true; 
        
		$thumb_path = getPicUrl($ilist['url'], $ilist['userid'], $ilist['up_time'],0,'',true);;//读取缩略图保存的路径
		$pathinfo=pathinfo($thumb_path);
        $thumb_size = C('THUMB_SIZE');
        $upload->thumbPrefix = $thumb_size[0]['prefix'].','.$thumb_size[1]['prefix'];  //生产2张缩略图
         
        $upload->thumbPath = $pathinfo['dirname'].'/';  //缩略图 保存路径
        //设置缩略图最大宽度 
        $upload->thumbMaxWidth = $thumb_size[0]['width'].','.$thumb_size[1]['width']; 
        //设置缩略图最大高度 
        $upload->thumbMaxHeight = $thumb_size[0]['height'].','.$thumb_size[1]['height']; 
			
		if(!$upload->upload('',$ilist['url'])) { // 上传错误 提示错误信息
			$this->error($upload->getErrorMsg());
		}else{ // 上传成功 获取上传文件信息
		   // var_dump($ilist['url'],$upload->getUploadFileInfo());
		   // exit;
		   $data = array();
		   $data['bigurl'] = '';
		   $img->where('id='.$id)->save($data);
			$this->success('up success');
		}
	}
	function copygrouptext()
	{
	    $gmod = M('GroupDetail');
	    $rmod = M('GroupRemark');
	    $gonmod = M('GrouponDetail');
	    $gonrmod = M('GrouponRemark');
	    if(!$this->edit_id)
	    {
	        $this->ajaxReturn(null,'当前没有正在编辑的图组',1);
	    }
	    $gdata = $gmod->where('id='.$this->groupid)->find();
	    
	    if(!$gdata)
	    {
	        $this->ajaxReturn(null,'当前组不存在',1);
	    }
	    $data = array();
	    $data['title'] = $gdata['title'];
	    $data['group_key'] = $gdata['group_key'];
	    $data['photo_date'] = $gdata['photo_date'];
	    $data['country'] = $gdata['country'];
	    $data['province'] = $gdata['province'];
	    $data['city'] = $gdata['city'];
	    $gonmod->where('id='.$this->edit_id)->save($data);
	    $remark = $rmod->where('group_id='.$this->groupid)->getField('remark');
	    if($remark)
	    {
    	    $data = array();
    	    $data['remark'] = $remark;
    	    $gonrmod->where('group_id='.$this->edit_id)->save($data);
	    }
	    $this->ajaxReturn(null,'',0);
	}
}
?>