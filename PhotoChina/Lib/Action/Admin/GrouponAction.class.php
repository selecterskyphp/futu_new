<?php
class GrouponAction extends AdminbaseAction {
    private  $dao,$edit_id,$groupid;
	function _initialize() {
		parent::_initialize();
		$edit_group_msg = '';
		$this->edit_id = isset($_REQUEST['edit_id'])?intval($_REQUEST['edit_id']):0;
		$this->groupid = isset($_REQUEST['groupid'])?intval($_REQUEST['groupid']):0;
		$mod = M('GrouponDetail');
		if($this->edit_id)
		{
		    $result = $mod->where('id='.$this->edit_id)->field('title')->find();
		    if($result)
		    {
		        $edit_group_msg = '<a href="'.U('Group/edit',array('edit_id'=>$this->edit_id)).'"><font color=red>当前编辑图组:'.$result['title'].'</font></a>';
		    }
		}
		else 
		{
		    $edit_group_msg = '<font color=red>当前没有正在编辑的图组!</font>';
		}
		$this->assign('edit_id',$this->edit_id);
		$this->assign('groupid',$this->groupid);
		$this->assign('edit_group_msg',$edit_group_msg);
	}
	
	function index() {
		
		$columnM = M('column');
		$list = $columnM->where('parentid=0')->order('sort')->select();
		$column = array();
		foreach($list as $v)
		{
		    $v['sub'] = $columnM->where('parentid='.$v['groupid'])->order('sort')->select();
		    $column[] = $v;
		}
		$this->assign('column',$column);
		import ('@.ORG.Page');

		//查询审核通过 但未发布图组信息
		$typeid=$_GET['typeid'];
		$user=$_GET['username'];
		$author=$_GET['author'];
		$start=isset($_GET['start_query'])?strtotime($_GET['start_query']):0;
		$end=isset($_GET['end_query'])?strtotime($_GET['end_query']):0;
		$groupname=$_GET['group_name'];
		$type=$_GET['type'];
		$state=isset($_GET['state'])?intval($_GET['state']):1;
		//$where="id>0";
		if(0 !== $state)
		{
		    if(1 == $state)
		    {
		        //$where .=" and (state='1' or state='4')";
		        $where =" state='1'";
		    }
		    else
		    {
		        $where =" state='".$state.'\'';
		    }
		}
		else 
		{
		    $where = " state<>'0'";
		}
		$this->assign('state',$state);
		
		if($start>0 && $end>0)
	    {
	        $where .= ' and update_time between '.$start.' and ' . $end;
	    }
	    elseif($start>0)
	    {
	        $end = $start + 60*60*24*3;//3天内
	        $where .= ' and update_time between '.$start.' and ' . $end;
	    }
	    elseif($end>0)
	    {
	        $start = $end - 60*60*24*3;
	        $where .= ' and update_time between '.$start.' and ' . $end;
	    }
	    else 
	    {
	        $end = time();
	        $start = $end - 60*60*24*3;
	        $where .= ' and update_time between '.$start.' and ' . $end;
	    }
	    $this->assign('start',$start);
	    $this->assign('end',$end);
		
		//指出按哪个字段排序且是升序还是降序
		$orderfiled=$_POST['orderfiled'];
		$order=$_POST['order'];
		if(empty($orderfiled)||strlen($orderfiled)<=0)
		{
			$orderinfo=' update_time desc';
		}
		else 
		{
    		$vo['filed']=$orderfiled;
    		$vo['order']=$order;
    		$orderinfo=$orderfiled." ".$order;
		}

		$group=M('groupon_detail');
		
		if( !empty($type)  )
		{
			$twhere=sprintf(" and type_one>=%s and type_one<%s",
			$type,$type+10000);
			$where=$where.$twhere;
		}
		if( !empty($user) )
			$where=$where." and check_oper='".$user."'";
			
		if($author)
		{
		    $where=$where." and author='".$author."'";
		}
	    
		
		if ( !empty($groupname) )
			$where=$where.$and." and title like '%".$groupname."%'";
		
		

		$count=$group->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		$this->assign("page",$show);
		
		
		$glist=$group->order($orderinfo)->where($where)->limit($page->firstRow.','.$page->listRows)->select();
        //var_dump($group->getLastSql());
        //exit;
		//找出真实姓名
		/*for($i=0;$i<count($glist);$i++)
		{
			//用户的真实姓名
			$acct=$glist[$i]['up_account'];

			$p=M('photoer');
			$acct=$p->where("account='".$acct."'")->find();
			$glist[$i]['up_account']=$acct['name'];
		}*/

		$this->assign('glist',$glist);
		$this->assign('vo',$vo);
		$this->assign($_POST);

		$this->display();
	}
    //点开图组
	function images()
	{
		//刷新当前用户正在编辑的图组
		$gid = $this->groupid;
		$edGroup=M('GrouponDetail');
	    $where=" id=".$gid;
	    $vo=$edGroup->where($where)->find();
	    //var_dump($where);
	    //exit;
		if(!$vo)
		{
			//$this->display('PhotoerImageGroup_groupnull');
			$this->error('图组不存在');
			//return;
		}
		$gid = $vo['id'];
		$where=" group_id=".$gid;
		$remarkM = M('GrouponRemark');
		$vo['remark'] = $remarkM->where($where)->getField('remark');
		
		//找出3种类型的名字
		$col=M('column');
		if(!empty($vo['type_one']) && $vo['type_one']>0 )
		{
			$where=" groupid=".$vo['type_one'];
			$nameone=$col->where($where)->find();
			$vo['type_one_name']=$nameone['name'];
		}
		if(!empty($vo['type_two']) && $vo['type_two']>0 )
		{
			$where=" groupid=".$vo['type_two'];
			$nametwo=$col->where($where)->find();
			$vo['type_two_name']=$nametwo['name'];
		}
		if(!empty($vo['type_three']) && $vo['type_three']>0 )
		{
			$where=" groupid=".$vo['type_three'];
			$namethree=$col->where($where)->find();
			$vo['type_three_name']=$namethree['name'];
		}
		
		$gdetail=M('GrouponImages');
		$sql = 'select b.*,c.remark from __TABLE__ a inner join '.C('DB_PREFIX').'images_detail b on a.images_id = b.id left join '.C('DB_PREFIX').'images_remark c on a.images_id=c.images_id where a.group_id='.$gid;
		$imagelist=$gdetail->query($sql);
		//var_dump($gdetail->getLastSql(),$imagelist);
		//exit;
		
		$this->assign('vo',$vo);
		$this->assign('imagelist',$imagelist);
		
		$this->display();
	}
//复制单张或多张图片
	function copy()
	{
		$ids=$_GET['ids'];
		$idarr = explode(',', $ids);
		if(empty($idarr))
		{
		    $this->ajaxReturn('','没有选择要操作的图片',1);
		}
	    if($this->edit_id<=0)
		{
		    $this->ajaxReturn('','当前没有正在编辑的图组',1);
		}
		$old_group_id=$_GET['groupid'];
		$old_groupM = M('GrouponDetail');
		//摄影师的图组是否存在 
	    $where = 'id='.$old_group_id;
		$oldGroupData = $old_groupM->where($where)->find();
		if(!$oldGroupData)
		{
		    $this->ajaxReturn('','图组不存在',1);
		}
		//判断该用户是否有编辑图组
		$eg=M('GrouponDetail');
		
	    $where = 'id='.$this->edit_id;
		$glist=$eg->where($where)->find();
		if(!$glist)
		{
			$this->ajaxReturn('','编辑图组不存在',1);
		}
		
		$image=M('images_detail');
		//如果没有设置封图  设置新图组的封面图
        if(strlen($glist['main_url'])<5)
        {
            for($i=0;$i<count($idarr);$i++)
            {
                $result = $image->where('id='.$idarr[$i])->find();
                if($result)
                {
                    $data = array();
                    $data['main_url']=$result['url'];
                    $data['main_url_date']=$result['up_time'];
                    $data['main_url_userid']=$result['userid'];
                    $where = 'id='.$this->edit_id;
                    $eg->where($where)->save($data);
                    break;
                }
            }
        }
		//拷贝该图片信息到编辑图组
		$values = '';
		$update_sql = '';
		$gimages = M('GrouponImages');
		//循环检查产品是否已经在编辑图组中存在 如不存在则新增
		for($i=0;$i<count($idarr);$i++)
		{
		    $result = $gimages->where('group_id='.$this->edit_id.' and images_id='.$idarr[$i])->find();
		    if(!$result)
		    {
		        if('' == $values)
		        {
		            $values = '('.$this->edit_id.','.$idarr[$i].')';
		            $update_sql = 'id='.$idarr[$i];
		        }
		        else
		        {
		            $values .= ',('.$glist['id'].','.$idarr[$i].')';
		            $update_sql .= ' or id='.$idarr[$i];
		        }
		    }
		}
		if('' !== $values)
		{
		    $mod = M('GrouponImages');
    		$sql = 'insert into __TABLE__ (group_id,images_id)values'.$values;
    		$mod->execute($sql);
    		//$this->ajaxReturn('',$eg->getlastsql(),1);
    		//更新当前图片的状态
    		$data = array();
    		$data['state']='3';//审核通过
    		$data['check_time']=time();//
    		$data['check_oper']=$username;//
    		$result = $image->where($update_sql)->save($data);
    		$this->ajaxReturn('','',0);
		}
		else
		{
		    $this->ajaxReturn('','图片已经在当前编辑的图组中存在',1);
		}
	}
	//提交发布图组
	function submit()
	{
		$id=$_GET['id'];
		if(!empty($id))
		{
		    $this->setGroupState($id, 3);
			$this->success('发布成功');
		}
		else
		{
			$this->error('请选择发布图组');
		}
	}
	
	//审核不通过及删除
	function checkwithdel()
	{
		$ids=$_POST['ids'];
		$g=M('groupon_detail');
		
		$data = array();
		if($_POST['notchecked']) //审核不通过
		{
		    $msg = checkstr($_POST['fail_msg'],0,128);
		    if(strlen($msg)<1)
		    {
		        $this->error('没有填写不通过原因');
		    }
			for($i=0;$i<count($ids);$i++)
			{
			    $id = $ids[$i];
			    $data = array();
    		    $data['fail_msg'] = checkstr($msg,0,128);
    		    $g->where('id='.$id)->save($data);
			    $this->setGroupState($id, 4);
		    }
		    $this->assign('jumpUrl',U('Groupon/index',array('edit_id'=>$this->edit_id)));
		    $this->success('操作成功');
		}
		else if($_POST['delchecked']) //删除
		{
			for($i=0;$i<count($ids);$i++)
			{
			    $id = $ids[$i];
			    $this->setGroupState($id, 200);
			}
			$this->assign('jumpUrl',U('Groupon/index',array('edit_id'=>$this->edit_id)));
		    $this->success('操作成功');
			//$this->redirect('Groupon/index',array('edit_id'=>$this->edit_id,'state'=>200));
		}
	    else if($_POST['checked24']) //删除
		{
			for($i=0;$i<count($ids);$i++)
			{
			    $id = $ids[$i];
			    $data = array();
			    $data['is_24hot']='1';
			    $g->where('id='.$id)->save($data);
			    //$data['is']
			    //$this->setGroupState($id, 200);
			}
			$this->assign('jumpUrl',U('Groupon/index',array('edit_id'=>$this->edit_id,'state'=>3)));
		    $this->success('操作成功');
		}
	}
}
?>