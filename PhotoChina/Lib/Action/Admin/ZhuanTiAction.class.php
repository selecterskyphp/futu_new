<?php
class ZhuantiAction extends AdminbaseAction {
    private $groupid,$ids;
	function _initialize() {
		parent::_initialize();
		$this->groupid = isset($_REQUEST['groupid'])?intval($_REQUEST['groupid']):0;
		$this->ids = isset($_REQUEST['ids'])?$_REQUEST['ids']:0;
	}

	function index() {

		//查询审核通过 但未发布图组信息
		import ('@.ORG.Page');
		$typeid=$_GET['typeid'];
		$user=$_GET['username'];
		$author=$_GET['author'];
		$start=isset($_GET['start_query'])?strtotime($_GET['start_query']):0;
		$end=isset($_GET['end_query'])?strtotime($_GET['end_query']):0;
		$groupname=$_GET['group_name'];
		$type=$_GET['type'];
		$state=isset($_GET['state'])?intval($_GET['state']):1;
		//$where="id>0";
		if(0 !== $state)
		{
		    if(1 == $state)
		    {
		        //$where .=" and (state='1' or state='4')";
		        $where =" state='1'";
		    }
		    else
		    {
		        $where =" state='".$state.'\'';
		    }
		}
		else 
		{
		    $where = " state<>'0'";
		}
		$this->assign('state',$state);
		
		if($start>0 && $end>0)
	    {
	        $where .= ' and update_time between '.$start.' and ' . $end;
	    }
	    elseif($start>0)
	    {
	        $end = $start + 60*60*24*3;//3天内
	        $where .= ' and update_time between '.$start.' and ' . $end;
	    }
	    elseif($end>0)
	    {
	        $start = $end - 60*60*24*3;
	        $where .= ' and update_time between '.$start.' and ' . $end;
	    }
	    else 
	    {
	        $end = time();
	        $start = $end - 60*60*24*3;
	        $where .= ' and update_time between '.$start.' and ' . $end;
	    }
	    $this->assign('start',$start);
	    $this->assign('end',$end);
		$where .= ' and is_group=1';
		//指出按哪个字段排序且是升序还是降序
		$orderfiled=$_POST['orderfiled'];
		$order=$_POST['order'];
		if(empty($orderfiled)||strlen($orderfiled)<=0)
		{
			$orderinfo=' update_time desc';
		}
		else 
		{
    		$vo['filed']=$orderfiled;
    		$vo['order']=$order;
    		$orderinfo=$orderfiled." ".$order;
		}

		$group=M('groupon_detail');
		
		if( !empty($type)  )
		{
			$twhere=sprintf(" and type_one>=%s and type_one<%s",
			$type,$type+10000);
			$where=$where.$twhere;
		}
		if( !empty($user) )
			$where=$where." and check_oper='".$user."'";
			
		if($author)
		{
		    $where=$where." and author='".$author."'";
		}
	    
		
		if ( !empty($groupname) )
			$where=$where.$and." and title like '%".$groupname."%'";
		
		

		$count=$group->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		$this->assign("page",$show);
		
		
		$glist=$group->order($orderinfo)->where($where)->limit($page->firstRow.','.$page->listRows)->select();
        //var_dump($group->getLastSql());
        //exit;
		//找出真实姓名
		/*for($i=0;$i<count($glist);$i++)
		{
			//用户的真实姓名
			$acct=$glist[$i]['up_account'];

			$p=M('photoer');
			$acct=$p->where("account='".$acct."'")->find();
			$glist[$i]['up_account']=$acct['name'];
		}*/

		$this->assign('glist',$glist);
		$this->assign('vo',$vo);
		$this->assign($_POST);

		$this->display();
	}
	
	//专题图组详细
	function edit()
	{
		$gid = $this->groupid;
		$edGroup=M('GrouponDetail');
	    $where=" id=".$gid;
	    $vo=$edGroup->where($where)->find();
	    //var_dump($where);
	    //exit;
		if(!$vo)
		{
			//$this->display('PhotoerImageGroup_groupnull');
			$this->error('图组不存在');
			//return;
		}
		$where=" group_id=".$gid;
		$remarkM = M('GrouponRemark');
		$vo['remark'] = $remarkM->where($where)->getField('remark');
		
		//找出3种类型的名字
		$col=M('column');
		if(!empty($vo['type_one']) && $vo['type_one']>0 )
		{
			$where=" groupid=".$vo['type_one'];
			$nameone=$col->where($where)->find();
			$vo['type_one_name']=$nameone['name'];
		}
		if(!empty($vo['type_two']) && $vo['type_two']>0 )
		{
			$where=" groupid=".$vo['type_two'];
			$nametwo=$col->where($where)->find();
			$vo['type_two_name']=$nametwo['name'];
		}
		if(!empty($vo['type_three']) && $vo['type_three']>0 )
		{
			$where=" groupid=".$vo['type_three'];
			$namethree=$col->where($where)->find();
			$vo['type_three_name']=$namethree['name'];
		}
		
		$gdetail=M('GrouponGroup');
		$sql = 'select b.title,b.main_url,b.main_url_userid,b.main_url_date,b.image_count from __TABLE__ a inner join '.C('DB_PREFIX').'groupon_detail b on a.images_id = b.id where a.group_id='.$gid;
		$imagelist=$gdetail->query($sql);
		//var_dump($gdetail->getLastSql(),$imagelist);
		//exit;
		
		$this->assign('vo',$vo);
		$this->assign('imagelist',$imagelist);
		
		$this->display();
	}
	
	
	//删除图组
	function deletegroup()
	{
		$ids=$_POST['ids'];
		$id=$_GET['id'];
		
		$t=M('group_detail');
		for($i=0;$i<count($ids);$i++)
		{
			$gid=$ids[$i];
			$data['topic_id']=0;
			$t->where(' id='.$gid)->save($data);
		}
		
		$this->success('删除成功');
	}
	//删除专题
	function del(){
		
		$groupid = $this->groupid;
		$data = array();
		$data['state'] = '200';
		$mod = M('GrouponDetail');
		$where = 'id='.$groupid;

		$result = $mod->where($where)->data($data)->save();
		$this->assign('jumpUrl',U('ZhuanTi/index'));
		if($result)
		{
		    $this->success('删除成功');
		}
		else
		{
		    $this->error('删除失败');
		}
	}

	//设置封面
	function setface()
	{
		$groupid = $this->groupid;
		$ids = $this->ids;
        $mod = M('GrouponDetail');
        $where = 'id='.$ids;
        $data = $mod->where($where)->getField('main_url,main_userid,main_date');
        $where = 'id='.$groupid;
        $result = $mod->where($where)->data($data)->save();
        if($result)
        {
            $this->success('操作成功');
        }
        else
        {
            $this->error('操作失败');
        }
	}
	
	function send()
	{
	    $groupid = $this->groupid;
	    $data = array();
	    $data['state'] = '3';
	    $mod = M('GrouponDetail');
	    $result = $mod->where('id='.$groupid)->data($data)->save();
	    $this->assign('jumpUrl',U('ZhuanTi/index'));
	    if($result)
	    {
	        $this->success('操作成功');
	    }
	    else
	    {
	        $this->error('操作失败');
	    }
	    
	}
	function update()
	{
	    $groupid = $this->groupid;
	    if(0 === $groupid)
	    {
	        $this->error('参数错误');
	    }
	    $typeone=$_POST['typeoneid'];
		$typetwo=$_POST['typetwoid'];
		$typethree=$_POST['typethreeid'];
		
		//通过ID找到菜单的名字
		$typeonename="";
		$typetwoname="";
		$typethreename="";
		
		$m=M('column');
		$is_set_cate = false;
		if( intval($typeone)>0 )
		{
			$t1=$m->where(' groupid='.$typeone)->find();
			$typeonename=$t1['name'];
			$is_set_cate = true;
		}
		
		if( intval($typetwo)>0 )
		{
			$t2=$m->where(' groupid='.$typetwo)->find();
			$typetwoname=$t2['name'];
			$is_set_cate = true;
		}
		
		if( intval($typethree)>0 )
		{
			$t3=$m->where(' groupid='.$typethree)->find();
			$typethreename=$t3['name'];
			$is_set_cate = true;
		}
		if(!$is_set_cate)
		{
		    $this->errmsg = '没有设置图组分类';
		    return false;
		}
		//标题
		$title=checkstr($_POST['gtitle'],0,128);
	    if(!$title)
		{
		    $this->errmsg = '没有填写图组标题';
		    return false;
		}
		$remark=checkstr($_POST['gremark'],0,4000);
	    if(!$remark)
		{
		    $this->errmsg = '没有填写图组说明';
		    return false;
		}
		
		//更新信息
		$group=M('GrouponDetail');
		$data['type_one']=$typeone;
		$data['type_two']=$typetwo;
		$data['type_three']=$typethree;
		
		$data['type_one_name']=$typeonename;
		$data['type_two_name']=$typetwoname;
		$data['type_three_name']=$typethreename;
		
		$data['update_time']=time();
		$data['title']=$title;
		$where = 'id='.$groupid;
		$group->where($where)->save($data);
		//var_dump($group->getlastsql(),$_POST);
		//exit;
		$where = 'group_id='.$this->edit_id;;
	    $data = array();
	    $data['remark'] = $remark;
	    $mod = M('GrouponRemark');
	    $mod->where($where)->save($data);
	    $this->success('操作成功');
	}
//审核和删除所选图组
	function checkwithdel()
	{
		$ids=$this->ids;
		$g=M('GrouponDetail');
		if($_POST['delchecked']) //删除
		{
			for($i=0;$i<count($ids);$i++)
			{
				$data['state']='200'; //状态10为已删除
				$g->where(' id='.$ids[$i])->save($data);
			}
		}
	    else if($_POST['redelchecked']) //还原
		{
			for($i=0;$i<count($ids);$i++)
			{
				$data['state']='1'; //状态10为已删除
				$g->where(' id='.$ids[$i])->save($data);
			}
		}
		$this->redirect('ZhuanTi/index');
		//$this->query();
	}
	
	function save()
	{
	    $typeone=$_POST['typeoneid'];
		$typetwo=$_POST['typetwoid'];
		$typethree=$_POST['typethreeid'];
		
		//通过ID找到菜单的名字
		$typeonename="";
		$typetwoname="";
		$typethreename="";
		
		$m=M('column');
		$is_set_cate = false;
		if( intval($typeone)>0 )
		{
			$t1=$m->where(' groupid='.$typeone)->find();
			$typeonename=$t1['name'];
			$is_set_cate = true;
		}
		
		if( intval($typetwo)>0 )
		{
			$t2=$m->where(' groupid='.$typetwo)->find();
			$typetwoname=$t2['name'];
			$is_set_cate = true;
		}
		
		if( intval($typethree)>0 )
		{
			$t3=$m->where(' groupid='.$typethree)->find();
			$typethreename=$t3['name'];
			$is_set_cate = true;
		}
		if(!$is_set_cate)
		{
		    $this->errmsg = '没有设置图组分类';
		    return false;
		}
		//标题
		$title=checkstr($_POST['gtitle'],0,128);
	    if(!$title)
		{
		    $this->errmsg = '没有填写图组标题';
		    return false;
		}
		$remark=checkstr($_POST['gremark'],0,4000);
	    if(!$remark)
		{
		    $this->errmsg = '没有填写图组说明';
		    return false;
		}
		
		//更新信息
		$username=$_SESSION['username'];
		$group=M('GrouponDetail');
		$data['type_one']=$typeone;
		$data['type_two']=$typetwo;
		$data['type_three']=$typethree;
		
		$data['type_one_name']=$typeonename;
		$data['type_two_name']=$typetwoname;
		$data['type_three_name']=$typethreename;
		
		$data['update_time']=time();
		$data['title']=$title;
	    $data['image_count']=0;
		$data['up_time']=time();
		$data['check_oper']=$username;
		$data['state']='1';
		//$data['main_url']=$ulist['main_url'];
		$data['c_type']='1';
		
		$result = $group->add($data);
		if($result)
		{
		    $groupid = $eg->getLastInsID();
		    $data =array();
		    $data['group_id']=$groupid;
		    $data['remark'] = $_POST['gremark'];
		    $mod = M('GrouponRemark');
		    $result = $mod->data($data)->add();
		    
		    if($result)
		    {
		        $this->ajaxReturn('','成功',0);
		    }
		    else
		    {
		        $this->ajaxReturn('','添加描述失败',1);
		    }
		}
		else
		{
		    $this->ajaxReturn('','操作数据库失败',1);
		}
	}
	public function addgroup()
	{
	    $gid = $this->groupid;
	    $_SESSION['ZhuanTiGid'] = $gid;
	    $this->redirect('Groupon/index');
	}
}

?>