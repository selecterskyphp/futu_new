<?php
class MainpictureAction extends AdminbaseAction {

	function _initialize() {
		parent::_initialize();
		
	}
	
	function index() {
		$picture=M('mainpicture');
		$curtime=time();
		$picture->where('endtime<'.$curtime)->delete();
		$where=" endtime >= ".$curtime;	
		$info=$picture->where($where)->select();
		$this->assign('info',$info);
		$this->display('Mainpicture_index');
	}
	
	public function delete(){
		
		$ids=$_POST['ids'];
		if(!empty($ids))
		{
			$m=M('mainpicture');
			
			for($i=0;$i<count($ids);$i++)
			{
				$m->where('id='.$ids[$i])->delete();
			}
			
			$this->success('删除成功');
		}
	
	}
		
	public function update(){
		$id=$_GET['id'];
		$gid=isset($_GET['gid'])?intval($_GET['gid']):0;
		if(!empty($id))
		{
			$user=M('Mainpicture');
			$where="id = ".$id;
			$vo=$user->where($where)->find();
		
			$this->assign('vo',$vo);	
		}
		else
		{
		    $vo['number'] = 0;
		    $vo['begintime'] = time();
		    $vo['endtime'] = time()+(60*60*24);
		    if($gid>0)
		    {
		        $path = C("IMAGES_HOT_PATH");
		        $mod = M('GrouponDetail');
		        $data = $mod->where('id='.$gid)->find();
		        if($data)
		        {
		            $old_pic = getPicUrl($data['main_url'], $data['main_url_userid'], $data['main_url_date'],100);
		            if(is_readable($old_pic))
		            {
    		            $pathinfo = pathinfo($old_pic);
    		            $save = $path . $pathinfo['basename'];
    		            import("@.ORG.Image");
    		            $result = Image::thumb($old_pic, $save,'',960,500,true);
    		            if(!$result)
    		            {
    		                $this->error('生成缩略图失败');
    		            }
    		            $vo['imgurl'] = $save;
		            }
    		        $vo['remark'] = $data['title'];
    		        $vo['url'] = U('Home-Images/index',array('gid'=>$data['id']));
		        }
		    }
		    $this->assign('vo',$vo);
		}
		$this->display();
	}
		
	public function save(){
			$date['id']=$_POST['id'];

			import("@.ORG.UploadFile");
			$path = C("IMAGES_HOT_PATH");
			$upload = new UploadFile(); // 实例化上传类
			$upload->maxSize  = 3145728 ; // 设置附件上传大小
			$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg'); // 设置附件上传类型
			$upload->savePath =  $path; // 设置附件上传目录
			$upload->$uploadReplace = true;
			
			$time=sysmd5(date('Y-m-d H:i:s',time()));
			
			if(!$upload->upload('',$time.".jpg")) { // 上传错误 提示错误信息
				$date['imgurl'] = isset($_POST['old_imgurl'])?trim($_POST['old_imgurl']):'';
			}else
			{ // 上传成功 获取上传文件信息
				$info =  $upload->getUploadFileInfo();
				$uploadfilename=$info[0]["savename"];
				$date['imgurl']=$path.$uploadfilename;
			}
			
			
			$date['remark']=$_POST['remark'];
			$date['begintime']=strtotime($_POST['begintime']);
			$date['endtime']=strtotime($_POST['endtime']);
			$date['number']=$_POST['number'];
			$date['url']=$_POST['url'];
			$user=M('mainpicture');
			if(strlen($_POST['id'])>0)
			{
				if(false==$user->save($date))
					$this->error("保存失败");
				else
					$this->success('操作成功');
			}
			else
			{
				//新增
				if(false==$user->add($date))
					$this->error('保存失败');
				else
					$this->success('操作成功');
			}
		}	
}