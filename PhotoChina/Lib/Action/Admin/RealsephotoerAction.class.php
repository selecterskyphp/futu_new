<?php
class realsephotoerAction extends AdminbaseAction {
    private  $dao,$edit_id,$groupid;
	function _initialize() {
		parent::_initialize();
        $edit_group_msg = '';
		$this->edit_id = isset($_REQUEST['edit_id'])?intval($_REQUEST['edit_id']):0;
		$this->groupid = isset($_REQUEST['groupid'])?intval($_REQUEST['groupid']):0;
		$mod = M('GrouponDetail');
		if($this->edit_id)
		{
		    $result = $mod->where('id='.$this->edit_id)->field('title')->find();
		    if($result)
		    {
		        $edit_group_msg = '<a href="'.U('Photoerimagegroup/editgroup',array('edit_id'=>$this->edit_id)).'"><font color=red>当前编辑图组:'.$result['title'].'</font></a>';
		    }
		}
		else 
		{
		    $edit_group_msg = '<font color=red>当前没有正在编辑的图组!</font>';
		}
		$this->assign('edit_id',$this->edit_id);
		$this->assign('groupid',$this->groupid);
		$this->assign('edit_group_msg',$edit_group_msg);
	}

	function index() {
		
		
		import ('@.ORG.Page');

		//��ѯ���ͨ�� ��δ����ͼ����Ϣ
		$typeid=$_POST['typeid'];
		$user=$_POST['username'];
		$start=$_POST['start_query'];
		$end=$_POST['end_query'];
		$groupname=$_POST['group_name'];
		$type=$_POST['type'];
		$state=$_POST['state'];
		
		if(empty($state)) $state=3;
		
		//指出按哪个字段排序且是升序还是降序
		$orderfiled=$_POST['orderfiled'];
		$order=$_POST['order'];
		if(empty($orderfiled)||strlen($orderfiled)<=0)
		{
			$orderfiled="id";
			$order="desc";
		}
		
		$vo['filed']=$orderfiled;
		$vo['order']=$order;

		$group=M('groupon_detail');
		
		$where="state=".$state;
		
		if( !empty($type) && $type>100 )
		{
			$twhere=sprintf(" and ( (type_one>=%s and type_one<%s) or (type_two>=%s and type_two<=%s) or (type_three>=%s and type_three<=%s) )",
										$type,$type+9999,
										$type,$type+9999,
										$type,$type+9999);
			$where=$where.$twhere;
		}
		if( !empty($user) )
			$where=$where." and check_oper='".$user."'";
	
		if( !empty($start) )
			$where=$where." and photo_time>='".strtotime($start)."'";

		if( !empty($end) )
			$where=$where." and photo_time<='".strtotime($end)."'";
		if( !empty($groupname) )
			$where=$where." and title like '%".trim($groupname)."%'";

		//$where=$where." and is_topic=0";

		$count=$group->where($where)->count();
		//var_dump($group->getlastsql());
		//exit;
		$page=new Page($count,20);
		$show=$page->show();
		$this->assign("page",$show);
		
		$orderinfo=$orderfiled." ".$order;
		$glist=$group->order($orderinfo)->where($where)->limit($page->firstRow.','.$page->listRows)->select();



		

		$this->assign('glist',$glist);
		$this->assign('vo',$vo);
		$this->assign('state',$state);

		$this->display('Realsephotoer_index');
	}
	
	
	//审核通过不通过
	function checkimage()
	{
		$type=$_POST['checktype'];
		$id=$_POST['checkid'];
		
		$img=M('images_detail');
		$where=" id=".$id;
		$data['state']=$type;
		if($img->where($where)->save($data))
			$this->success('审核成功');
		else
			$this->error('审核失败，id='.$type);	
	}
	
	//添加热门图组
	function addhot()
	{
		$id=$_GET['id'];
		
		$g=M('group_detail');
		$where=" id=".$id;
		$glist=$g->where($where)->find();
		
		//找出封面图片的高宽
		$img=M('images_detail');
		$where=" group_id=".$id;
		$ilist=$img->where($where)->select();
		for($i=0;$i<count($ilist);$i++)
		{
			if( $ilist[$i]['small_url']==$glist['main_url'] )
			{
				$width=$ilist[$i]['width'];
				$height=$ilist[$i]['height'];
				break;
			}
		}
		
		$h=M('hot_picture');
		
		$data['group_id']=$id;
		$data['picturename']=$glist['title'];
		$data['url']=$glist['main_url'];
		$data['begintime']=time();
		$data['endtime']=time()+7*24*3600;
		$data['number']=1;
		$data['place']="热门图片展示";
		$data['width']=$width;
		$data['height']=$height;
		
		//序号先加一
		$sql="update pc_hot_picture set number=number+1";
		$h->execute($sql);
		
		$h->add($data);
		
		$this->success('添加成功');
	}
	
	//添加TOP图组
	function addtop()
	{
		$id=$_GET['id'];
		
		$g=M('group_detail');
		//$glist=$g->where(' id='.$id)->find();
		
		$data['is_top']=1;
		$g->where(' id='.$id)->save($data);
		
		$this->success('添加成功');
	}
	
	//添加图片TOP
	function setimagetop()
	{
		$id=$_POST['topimageid'];
		
		$img=M('images_detail');
		
		//查找Top
		$ilist=$img->where('id='.$id)->find();
		
		if( $ilist['is_top']==1 )
			$data['is_top']=0;
		else
			$data['is_top']=1;
		
		if($img->where('id='.$id)->save($data))
			$this->success('添加成功');
		else
			$this->success('添加失败');
	}
	
	//24小时热图
	function add24hot()
	{
		$id=$_GET['id'];
		$g=M('groupon_detail');
		$list=$g->where(' id='.$id)->find();
		if( $list['is_24hot']==0 )
			$data['is_24hot']=1;
		else
			$data['is_24hot']=0;
		
		if($g->where(' id='.$id)->save($data))
			$this->success('设置成功');
		else
			$this->success('设置失败');
	}
    //广告热图
	function settop()
	{
		$id=$_GET['id'];
		$g=M('groupon_detail');
		$list=$g->where(' id='.$id)->find();
		if( $list['is_top']==0 )
			$data['is_top']=1;
		else
			$data['is_top']=0;
		
		if($g->where(' id='.$id)->save($data))
			$this->success('设置成功');
		else
			$this->success('设置失败');
	}
	//删除整个图组
	function delgroup()
	{
		$id=$_GET['id'];
		
		$g=M('group_detail');
		$sql="update pc_group_detail set state=200 where id=".$id;
		$g->execute($sql);
		
		$this->ajaxReturn('123','成功',1);
	}
//审核和删除所选图组
	function checkwithdel()
	{
		$ids=$_POST['ids'];
		$g=M('groupon_detail');
		$m=M('images_detail');
		if($_POST['notchecked']) //审核不通过
		{
			for($i=0;$i<count($ids);$i++)
			{
				$data['state']=1;
				$g->where(' id='.$ids[$i])->save($data);

				$mdata['state']=1;
				$m->where(' group_id='.$ids[$i])->save($mdata);
			}
		}
		else if($_POST['delchecked']) //删除
		{
			for($i=0;$i<count($ids);$i++)
			{
			$data['check_state']=10; //状态10为已删除
			$g->where(' id='.$ids[$i])->save($data);
			}
		}
		else if( $_POST['checked'] ) //审核通过
		{
			for($i=0;$i<count($ids);$i++)
			{
				$data['state']=1;
				$g->where(' id='.$ids[$i])->save($data);
			
				$mdata['state']=1;
				$m->where(' group_id='.$ids[$i])->save($mdata);
			}
		}
			
	    $this->redirect('Realsephotoer/index',array('edit_id'=>$this->edit_id));
	}
	
}
?>