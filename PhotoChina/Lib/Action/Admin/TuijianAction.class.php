<?php
class TuijianAction extends AdminbaseAction {
	public $dao;
	function _initialize() {
		parent::_initialize();
		$this->dao=D('tuijian');
		if( empty($this->dao) )
			echo "photoer is null";
	}
	
	function index() {
		

		import ('@.ORG.Page');
	
		$position=$_POST['positoin'];
		$this->assign($_POST);
	
		if ($position=L("全部"))
			;
		else
			$where="position='".$position."'";
		
		$photoer=$this->dao;
		$count=$photoer->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
	
		$this->assign("page",$show);
		$list=$photoer->order('id')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
	
		$this->assign('mlist',$list);
		$this->display();
	}
	
	function insert(){
		$tuijian=$this->dao;
		if($data=$tuijian->create()){
			if(false!==$tuijian->add()){
				/*$uid=$photoer->getLastInsID();
				 $ru['role_id']=$_POST['groupid'];
				$ru['user_id']=$uid;
				$roleuser=M('RoleUser');
				$roleuser->add($ru);*/
				$this->success(L('add_ok'));
			}else{
				$this->error(L('add_error'));
			}
		}else{
			$this->error($tuijian->getError());
		}
	}
	
	function update(){
		$tuijian=$this->dao;
		if($data=$tuijian->create()){
			if(!empty($data['id'])){
				if(false!==$tuijian->save()){
					$this->success(L('edit_ok'));
				}else{
					$this->error(L('edit_error').$photoer->getDbError());
				}
			}else{
				$this->error(L('do_error'));
			}
		}else{
			$this->error($photoer->getError());
		}
	}
	
	function _before_add(){
		$this->assign('rlist',$this->usergroup);
	}
	
	function _before_edit(){
		$this->assign('rlist',$this->usergroup);
	}
	
	function delete(){
		$id=$_GET['id'];
		$photoer=$this->dao;
		if(false!==$photoer->delete($id)){
			$roleuser=M('RoleUser');
			$roleuser->where('user_id ='.$id)->delete();
			delattach(array('moduleid'=>0,'catid'=>0,'id'=>0,'userid'=>$id));
			$this->success(L('delete_ok'));
		}else{
			$this->error(L('delete_error').$photoer->getDbError());
		}
	}
	
	function deleteall(){
		$ids=$_POST['ids'];
		if(!empty($ids) && is_array($ids)){
			$photoer=$this->dao;
			$id=implode(',',$ids);
			if(false!==$photoer->delete($id)){
				/*$roleuser=M('RoleUser');
				 $roleuser->where('user_id in('.$id.')')->delete();
				delattach("moduleid=0 and catid=0 and id=0 and userid in($id)");
				*/
				$this->success(L('delete_ok'));
			}else{
				$this->error(L('delete_error'));
			}
		}else{
			$this->error(L('do_empty'));
		}
	}
}



?>