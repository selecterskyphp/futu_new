<?php
/**
 * 
 * Attachment(��������)
 *
 * @package      	YOURPHP
 * @author          liuxun QQ:147613338 <admin@yourphp.cn>
 * @copyright     	Copyright (c) 2008-2011  (http://www.yourphp.cn)
 * @license         http://www.yourphp.cn/license.txt
 * @version        	yourphp��ҵ��վϵͳ v2.0 2011-03-01 yourphp.cn $
 */
class AttachmentAction extends  Action {

	protected $dao,$Config,$sysConfig,$isadmin=0,$userid=0,$groupid=0;
    function _initialize()
    {	
		$this->Config=F('Config');
		$this->sysConfig = F('sys.config');
		if($_POST['PHPSESSID'] && $_POST['swf_auth_key'] && ($_POST['swf_auth_key']==sysmd5($_POST['PHPSESSID'].$_POST['userid'],$this->sysConfig['ADMIN_ACCESS']))){
			$this->userid = $_POST['userid'];
		}
		
		$this->isadmin= $_REQUEST['isadmin'] ? $_REQUEST['isadmin'] : 0;
		
		if(!$this->userid){
			if($this->isadmin){

				import('@.Action.Adminbase');
				$Adminbase=new AdminbaseAction();
				$Adminbase->_initialize();
				$this->userid=  $_SESSION[C('USER_AUTH_KEY')];
				$this->groupid=  $_SESSION['groupid'];

			}else{
			
				C('ADMIN_ACCESS',$this->sysConfig['ADMIN_ACCESS']);
				if($_COOKIE['YP_auth']){
					if(!strstr($_SERVER['HTTP_USER_AGENT'],'Flash'))cookie('YP_cookie',$_SERVER['HTTP_USER_AGENT']);
					$HTTP_USER_AGENT = strstr($_SERVER['HTTP_USER_AGENT'],'Flash') ? $_COOKIE['YP_cookie'] : $_SERVER['HTTP_USER_AGENT'];
					$yourphp_auth_key = sysmd5($this->sysConfig['ADMIN_ACCESS'].$HTTP_USER_AGENT);
					list($userid, $groupid ,$password) = explode("-", authcode($_COOKIE['YP_auth'], 'DECODE', $yourphp_auth_key));
					$this->userid = $userid;
					$this->groupid = $groupid; 
					//cookie('YP_cookie',null);
				}
		
				if(!$this->userid){
					$this->assign('jumpUrl',U('User-Login/index'));
					$this->error(L('no_login'));
				}
			
			}
		}
		$this->dao=M('Attachment');
    }
	public function index(){

		$auth = str_replace(' ','+',$_REQUEST['auth']) ;
		unset($_REQUEST['auth']);
		$upsetup = implode('-',$_REQUEST);
		$yourphp_auth_key = sysmd5(C('ADMIN_ACCESS').$_SERVER['HTTP_USER_AGENT']);
		$enupsetup = authcode($auth, 'DECODE', $yourphp_auth_key);
		if(!$enupsetup || $upsetup!=$enupsetup)  $this->error (L('do_empty'));

		$sessid = time();

		$count = $this->dao->where('status=0 and userid ='.$this->userid)->count();
		$this->assign('no_use_files',$count);


		$types = '*.'.str_replace(",",";*.",$_REQUEST['file_types']); ;
		$this->assign('moduleid',$_REQUEST['moduleid']);
		$this->assign('file_size',$_REQUEST['file_size']);
		$this->assign('file_limit',$_REQUEST['file_limit']);
		$this->assign('file_types',$types);
		$this->assign('isthumb',$_REQUEST['isthumb']);
		$this->assign('isadmin',$this->isadmin);
		$this->assign('sessid',$sessid);
		$this->assign('userid',$this->userid);
		$swf_auth_key = sysmd5($sessid.$this->userid);
 
		$this->assign('swf_auth_key',$swf_auth_key);
		$this->assign('more',$_GET['more']);		
		$this->display();
	}

	public function upload(){
		//if($_POST['swf_auth_key']!= sysmd5($_POST['PHPSESSID'].$this->userid)) $this->ajaxReturn(0,L('do_empty'),0);
 
		import("@.ORG.UploadFile"); 
        $upload = new UploadFile(); 
		//$upload->supportMulti = false;
        //�����ϴ��ļ���С 
        $upload->maxSize = $this->Config['attach_maxsize']; 
		$upload->autoSub = true; 
		$upload->subType = 'date';
		$upload->dateFormat = 'Ym';
        //�����ϴ��ļ����� 
        $upload->allowExts = explode(',', $this->Config['attach_allowext']); 
        //���ø����ϴ�Ŀ¼ 
        $upload->savePath = UPLOAD_PATH; 
		 //�����ϴ��ļ����� 
        $upload->saveRule = uniqid; 

       
        //ɾ��ԭͼ 
        $upload->thumbRemoveOrigin = true; 
        if (!$upload->upload()) { 
			$this->ajaxReturn(0,$upload->getErrorMsg(),0);
        } else { 
            //ȡ�óɹ��ϴ����ļ���Ϣ 
            $uploadList = $upload->getUploadFileInfo(); 
			
			if($_POST['addwater']){
				import("@.ORG.Image");  
				Image::watermark($uploadList[0]['savepath'].$uploadList[0]['savename'],'',$this->Config);
			}
			
			$imagearr = explode(',', 'jpg,gif,png,jpeg,bmp,ttf,tif'); 
			$data=array();
			$model = M('Attachment');
			//���浱ǰ��ݶ���
			$data['moduleid'] = $_REQUEST['moduleid'];
			$data['catid'] = 0;
			$data['userid'] = $_REQUEST['userid'];
			$data['filename'] = $uploadList[0]['name'];
			$data['filepath'] = $uploadList[0]['savepath'].$uploadList[0]['savename'];
			$data['filesize'] = $uploadList[0]['size']; 
			$data['fileext'] = $uploadList[0]['extension']; 
			$data['isimage'] = in_array($uploadList[0]['extension'],$imagearr) ? 1 : 0;
			$data['isthumb'] = intval($_REQUEST['isthumb']);
			$data['createtime'] = time();
			$data['uploadip'] = get_client_ip();
			$aid = $model->add($data); 
			$returndata['aid']		= $aid;
			$returndata['filepath'] = $data['filepath'];
			$returndata['fileext']  = $data['fileext'];
			$returndata['isimage']  = $data['isimage'];
			$returndata['filename'] = $data['filename'];
			$returndata['filesize'] = $data['filesize']; 

			$this->ajaxReturn($returndata,L('upload_ok'), '1');
			//print_r($uploadList[0]['savepath'].$uploadList[0]['savename']);
        }	
	}

	public function filelist(){

		$where= $_REQUEST['typeid'] ?  " status=1 " : " status=0 ";
		$where .=" and userid = ".$this->userid ;
		import ( '@.ORG.Page' );
		$count = $this->dao->where($where)->count();
		$page=new Page($count,12); 
		$imagearr = explode(',', 'jpg,gif,png,jpeg,bmp,ttf,tif'); 

		$page->urlrule = 'javascript:ajaxload('.$_REQUEST['typeid'].',{$page},\''.$_REQUEST['inputid'].'\','.$this->isadmin.');';
		$show = $page->show(); 
		$this->assign("page",$show);
		$list=$this->dao->order('aid desc')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
		foreach((array)$list as $key=>$r){
		$list[$key]['thumb']=in_array($r['fileext'],$imagearr) ? $r['filepath'] : __ROOT__.'/Public/Images/ext/'.$r['fileext'].'.png'; 
		}
		$this->assign('list',$list);
		$this->display();
	}

	function delfile($aid){
		if(empty($aid)){
		$aid=$_REQUEST['aid'];
		}
		$r = delattach(array('aid'=>$aid,'userid'=>$this->userid));
		if($r){		 
			$this->success ( L ( 'delete_ok' ) );
		}else{
			$this->error ( L ( 'delete_error' ) );
		}
	
	}
	function cleanfile(){

		$r = delattach(array('status'=>0,'userid'=>$this->userid));
		if($r){		 
			$this->success ( L ( 'delete_ok' ) );
		}else{
			$this->error ( L ( 'delete_error' ) );
		}
	}
	
}
?>