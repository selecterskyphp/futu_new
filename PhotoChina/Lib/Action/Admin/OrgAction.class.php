<?php
/**
 * 
 * Role(��Ա�����)
 *
 * @package      	YOURPHP
 * @author          liuxun QQ:147613338 <admin@yourphp.cn>
 * @copyright     	Copyright (c) 2008-2011  (http://www.yourphp.cn)
 * @license         http://www.yourphp.cn/license.txt
 * @version        	yourphp��ҵ��վϵͳ v2.0 2011-03-01 yourphp.cn $
 */
class OrgAction extends AdminbaseAction {

    function _initialize()
    {	
		parent::_initialize();		
	
    }
    
    public function update()
    {
        $username = $_POST['username'];
        $mod = M('photoer');
        $where = array();
        $where['account'] = $username;
        $id = $mod->where($where)->getField('id');
        if($id)
        {
            $_POST['userid'] = $id;
        }
        else
        {
            $this->error('用户不存在');
        }
        $from_path = $_POST['from_path'];
        if(!is_readable($from_path))
        {
            $this->error('图片源路径不正确,需要填写绝对路径');
        }
        $name = MODULE_NAME;
		$model = M ( $name );
		if (false === $model->create ($_POST)) {
			$this->error ( $model->getError () );
		}
		if (false !== $model->save ()) {
		    $this->assign ( 'jumpUrl', U(MODULE_NAME.'/index') );
			$this->success (L('edit_ok'));
		} else {
			$this->success (L('edit_error').': '.$model->getDbError());
		}
    }
    
public function insert()
    {
        $username = $_POST['username'];
        
        $mod = M('photoer');
        $where = array();
        $where['account'] = $username;
        $id = $mod->where($where)->getField('id');
        if($id)
        {
            $_POST['userid'] = $id;
        }
        else
        {
            $this->error('用户不存在');
        }
        $from_path = $_POST['from_path'];
        if(!is_readable($from_path))
        {
            $this->error('图片源路径不正确,需要填写绝对路径');
        }
        $name = MODULE_NAME;
		$model = M ( $name );
		$count = $model->count();
		if($count>10)
		{
		    $this->error('最多只能支持10个机构');
		}
		if (false === $model->create ($_POST)) {
			$this->error ( $model->getError () );
		}
		if (false !== $model->add ()) {
		    $this->assign ( 'jumpUrl', U(MODULE_NAME.'/index') );
			$this->success (L('add_ok'));
		} else {
			$this->success (L('add_error').': '.$model->getDbError());
		}
    }
    
}
?>