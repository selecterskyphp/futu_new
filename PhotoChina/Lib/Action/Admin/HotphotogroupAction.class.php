<?php
class HotphotogroupAction extends AdminbaseAction
{
	public function index()
	{
		$hot=M('hot_picture');
		$curtime=time();
		$hot->where('endtime<'.$curtime)->delete();//删除过期图片
		$where="begintime <= ".$curtime." and endtime >= ".$curtime;	
		$info=$hot->where($where)->select();
		$this->assign('info',$info);
	
		$this->display('Hotphotogroup_index');
	}
	public function delete(){
		
		$id=$_GET['id'];
		$user=M('hot_picture');
		if(false!==$user->delete($id)){
			$this->success(L('delete_ok'));
		}else{
			$this->error(L('delete_error').$photoer->getDbError());
			}
		}
	
	public function update(){
		$id=$_GET['id'];
		$user=M('hot_picture');
		$where="id=".$id;
		$vo=$user->where($where)->find();
		$this->assign('vo',$vo);
		$this->assign('id',$id);
		$this->display('Hotphotogroup_add');
	}
		//new
		public function add()
		{
		    $gid = isset($_GET['gid'])?intval($_GET['gid']):0;
		    $vo['number'] = 0;
		    $vo['begintime'] = time();
		    $vo['endtime'] = time()+(60*60*24);
		    if($gid>0)
		    {
		        $group = M('GrouponDetail');
		        $data = $group->where('id='.$gid)->find();
		        if($data)
		        {
		            $old_pic = getPicUrl($data['main_url'], $data['main_url_userid'], $data['main_url_date'],100);
		            if(is_readable($old_pic))
		            {
    		            $pathinfo = pathinfo($old_pic);
    		            $save = $path . $pathinfo['basename'];
    		            import("@.ORG.Image");
    		            $result = Image::thumb($old_pic, $save,'',184,120,true);
    		            if(!$result)
    		            {
    		                $this->error('生成缩略图失败');
    		            }
    		            $vo['imgurl'] = $save;
		            }
		            $vo['picturename'] = $data['title'];
		            $vo['url'] = U('Home-Images/index',array('gid'=>$data['id']));
		        }
		    }
		    $this->assign('vo',$vo);
			$this->display();
		}
		
		public function addsave()
		{
			$date['id']=$_POST['id'];
			
			import("@.ORG.UploadFile");
			$path = C("IMAGES_HOT_PATH");
			$upload = new UploadFile(); // 实例化上传类
			$upload->maxSize  = 3145728 ; // 设置附件上传大小
			$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg'); // 设置附件上传类型
			$upload->savePath =  $path; // 设置附件上传目录
			$upload->$uploadReplace = true;
			$gid = isset($_POST['gid'])?intval($_POST['gid']):0;
			$gpic = isset($_POST['gpic'])?trim($_POST['gpic']):'';
			$old_imgurl = isset($_POST['old_imgurl'])?trim($_POST['old_imgurl']):'';
			
			$time=sysmd5(date('Y-m-d H:i:s',time()));
			
			if(!$upload->upload('',$time.".jpg")) { // 上传错误 提示错误信息
				if(0 !== $gid)
				{
				    $pathinfo = pathinfo($gpic);
				    $dest = $path . $pathinfo['basename']; 
				    if(!copy($gpic, $dest))
				    {
				        $this->error('拷贝原图组图片失败');
				    }
				    $date['imgurl'] =$dest;
				}
				else 
				{
				    if(!is_readable($old_imgurl))
				    {
				        $this->error('请选择你要上传的图片');
				    }
				    else 
				    {
				        $date['imgurl'] =$old_imgurl;
				    }
				}
				
			}
			else 
			{
			    $info =  $upload->getUploadFileInfo();
				$uploadfilename=$info[0]["savename"];
				$date['imgurl']=$path.$uploadfilename;
			}
			
			
			//$date['remark']=$_POST['remark'];
			$date['begintime']=strtotime($_POST['begintime']);
			$date['endtime']=strtotime($_POST['endtime']);
			$date['number']=$_POST['number'];
			$date['url']=$_POST['url'];
			$date['picturename']=$_POST['picturename'];
			$user=M('hot_picture');
			//echo $_POST['id'];
			if(strlen($_POST['id'])>0)
			{
				if(false==$user->save($date))
				{
					$this->error("保存失败,没有记录被更新");
				}
			}
			else
			{
				//新增
				$date['place']="热门图组推荐";
				if(false==$user->add($date))
					$this->error('新增失败');
			}
			$this->success('操作成功');
		}
}
?>