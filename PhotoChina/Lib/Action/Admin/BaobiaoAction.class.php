<?php
class BaobiaoAction extends AdminbaseAction
{
    public function free_down()
    {
        $this->down(5);
    }
    public function pay_down()
    {
        $this->down(2);
    }
    public function zhuan_down()
    {
        $this->down(4);
    }
    public function upload()
    {
    import ('@.ORG.Page');
		$mAccount=isset($_GET['account'])?trim($_GET['account']):'';
		$mName=isset($_GET['name'])?trim($_GET['name']):'';
		$mCompany=isset($_GET['company'])?trim($_GET['company']):'';
		$start_query=isset($_GET['start_query'])?strtotime($_GET['start_query']):0;
		$end_query=isset($_GET['end_query'])?strtotime($_GET['end_query']):0;
		$images_id=isset($_GET['images_id'])?intval($_GET['images_id']):0;
		
		$where = 'b.id>0 ';
		if($subtype>0)
		{
		    $where .= ' and a.subtype='.$subtype;
		}
		$this->assign('type',$subtype);
		if( !empty($mAccount) ){
			$where .=" and a.account like '".$mAccount."%'";
		}
		if( !empty($mName)){
		
			$where .= " and a.name like '".$mName."%'";
		}
		
		if( !empty($mCompany)){
			$where .= " and a.company_name like '".$mCompany."%'";
		}
		
		if($start_query>0 ){
			$where .= " and b.up_time>=".$start_query;
		}
		
	    if($end_query>0 ){
	        $end = $end_query + 60*60*24;
			$where .= " and b.up_time<=".$end;
		}
		$this->assign($_GET);
		$sql = "select count(*) as tt from __TABLE__ b left join ".C('DB_PREFIX').'photoer a on a.id=b.userid where '.$where;
		$photoer=M('ImagesDetail');
		$result=$photoer->query($sql);
		$count = $result[0]['tt'];
		$page=new Page($count,20);
		$show=$page->show();
		$this->assign("page",$show);
		$this->assign('count',$count);
		//var_dump($photoer->getlastsql(),$count,$result[0]['tt2']);
		//exit;
		$sql = "select a.account,a.name,a.company_name,b.id,b.up_time,b.url,b.userid,c.title from __TABLE__ b left join ".C('DB_PREFIX').'photoer a on a.id=b.userid left join '.C('DB_PREFIX').'group_detail c on b.old_group_id=c.id where '.$where.' order by up_time desc limit '.$page->firstRow.','.$page->listRows;
		$list=$photoer->query($sql);
//var_dump($photoer->getlastsql());
		$this->assign('mlist',$list);
		
		$this->display();
    }
	public function index()
    {
    	
		import ('@.ORG.Page');
		$mAccount=$_GET['account'];
		$mName=$_GET['name'];
		$mCompany=$_GET['company'];
		$mProvince=$_GET['province'];
		$mCity=$_GET['city'];
		$mRegStart=$_GET['start_query'];
		$mRegEnd=$_GET['end_query'];
		$mTitle=$_GET['title'];
		$mPhotoid=$_GET['photoid'];
		
		$and="and";
		$where = 'a.id>0 ';
		if( !empty($mAccount) ){
			$where=$where.$and." b.account like '%".$mAccount."%' ";
			$and=" and ";
		}
		if( !empty($mName)){
			$where=$where.$and." b.name like '%".$mName."%'";
			$and=" and ";
		}
		
		if( !empty($mCompany)){
			$where=$where.$and." b.company_name like '%".$mCompany."%'";
			$and=" and ";
		}
		
		if( !empty($mProvince) ){
			$where=$where.$and." b.province='".$mProvince."'";
			$and=" and ";
		}
		
		if( !empty($mCity) ){
			$where=$where.$and." b.city='".$mCity."'";
			$and=" and ";
		}
		
		if( !empty($mRegStart) ){
			$begintime=strtotime($mRegStart);
			$where = $where.$and."buytime>='".$begintime."'";
			$and=" and ";
		}
		
		if( !empty($mRegEnd) ){
			$endtime=strtotime($mRegEnd);
			$where = $where.$and."buytime<='".$endtime."'";
			$and=" and ";
		}
		if(!empty($mPhotoid)){
			$where = $where.$and."images_id='".$mPhotoid."'";
			$and=" and ";
			
			}
		$this->assign($_GET);
		$photoer=M('buy_images');
		$sql = "select count(*) as tt from __TABLE__ a left join ".C('DB_PREFIX').'photoer b on a.userid=b.id where '.$where;
		$result = $photoer->query($sql);
		$count =$result[0]['tt'];
		$page=new Page($count,20);
		$show=$page->show();
		$this->assign("page",$show);
		$sql = "select a.*,b.account,b.company_name,b.name,b.province,b.city from __TABLE__ a left join ".C('DB_PREFIX').'photoer b on a.userid=b.id where '.$where;
		$sql .= ' order by a.buytime desc limit '.$page->firstRow.','.$page->listRows;
		$list=$photoer->query($sql);
		//var_dump($photoer->getlastsql());
		
		//今日注册人数	昨天注册人数	前天注册人数	前七天注册人数
		//前一月注册人数	总注册人数	总激活账号
		$curDate=date('Y-m-d 00:00:01',time());
		$where=" buytime>=".strtotime($curDate);
		$curCount=$photoer->where($where)->count(); //当天下载次数
		
		$yesDate=strtotime($curDate)-24*3600;
		$where=" buytime>=".$yesDate;
		$yesCount=$photoer->where($where)->count(); //昨天下载次数
		$yesCount=$yesCount-$curCount;
		
		$before7Date=strtotime($curDate)-24*3600*7;
		$where=" buytime>=".$before7Date;
		$before7count=$photoer->where($where)->count(); //一周下载次数
		
		
		$monthDay=strtotime("-1 month");
		$where=" buytime>=".$monthDay;
		$monthcount=$photoer->where($where)->count(); //月度下载次数
		
		
		$yearDay=strtotime("-1 year");
		$where=" buytime>=".$yearDay;
		$yearcount=$photoer->where($where)->count(); //年度下载次数
		
		//支付点数
		
		$totalValue=$photoer->sum('buy_values');
		
		$vo['today_reg_num']=$curCount;
		$vo['yer_reg_num']=$yesCount;
		$vo['before7_reg_num']=$before7count;
		$vo['month_reg_num']=$monthcount;
		$vo['year_reg_num']=$yearcount;
		$vo['total_reg_num']=$totalValue;
		//剩余点数
		$user=M('photoer');
		$uservalue=$user->where('type=1')->sum('pvalue');	
		$vo['user_reg_num']=$uservalue;	
		$this->assign('vo',$vo);
		//查询用户表中的信息 单位名称  用户类型 用户状态
		$this->assign('mlist',$list);
		
		$this->display();
    }
	//下载用户账号管理
	private function down($subtype=0){


		import ('@.ORG.Page');
		$mAccount=isset($_GET['account'])?trim($_GET['account']):'';
		$mName=isset($_GET['name'])?trim($_GET['name']):'';
		$mCompany=isset($_GET['company'])?trim($_GET['company']):'';
		$start_query=isset($_GET['start_query'])?strtotime($_GET['start_query']):0;
		$end_query=isset($_GET['end_query'])?strtotime($_GET['end_query']):0;
		$images_id=isset($_GET['images_id'])?intval($_GET['images_id']):0;
		
		$where = 'b.id>0 ';
		if($subtype>0)
		{
		    $where .= ' and a.subtype='.$subtype;
		}
		$this->assign('type',$subtype);
		if( !empty($mAccount) ){
			$where .=" and a.account like '".$mAccount."%'";
		}
		if( !empty($mName)){
		
			$where .= " and a.name like '".$mName."%'";
		}
		
		if( !empty($mCompany)){
			$where .= " and a.company_name like '".$mCompany."%'";
		}
		
		if($start_query>0 ){
			$where .= " and b.buytime>=".$start_query;
		}
		
	    if($end_query>0 ){
	        $end = $end_query + 60*60*24;
			$where .= " and b.buytime<=".$end;
		}
	    if($images_id>0 ){
			$where .= " and b.images_id=".$images_id;
		}
		$this->assign($_GET);
		$sql = "select count(*) as tt,sum(b.buy_values) as tt2 from __TABLE__ b left join ".C('DB_PREFIX').'photoer a on a.id=b.userid where '.$where;
		$photoer=M('BuyImages');
		$result=$photoer->query($sql);
		$count = $result[0]['tt'];
		$page=new Page($count,20);
		$show=$page->show();
		$this->assign("page",$show);
		$this->assign('count',$count);
		$this->assign('buy_count',$result[0]['tt2']);
		//var_dump($photoer->getlastsql(),$count,$result[0]['tt2']);
		//exit;
		$sql = "select b.id,a.account,a.name,a.company_name,b.images_id,b.up_time,b.buy_values,b.buytime,b.ip,b.type,b.up_userid,b.sell_values,b.url,c.title from __TABLE__ b left join ".C('DB_PREFIX').'photoer a on a.id=b.userid left join '.C('DB_PREFIX').'groupon_detail c on b.group_id=c.id where '.$where.' order by buytime desc limit '.$page->firstRow.','.$page->listRows;
		$list=$photoer->query($sql);
//var_dump($photoer->getlastsql());
		$this->assign('mlist',$list);
		
		$this->display('Baobiao_down');
		}
	//下载用户充值查询				
	public function supplement(){


		import ('@.ORG.Page');
		$mAccount=$_POST['account'];
		$mName=$_POST['name'];
		$mCompany=$_POST['company'];
		$mProvince=$_POST['province'];
		$mCity=$_POST['city'];
		$mRegStart=$_POST['start_query'];
		$mRegEnd=$_POST['end_query'];
		$mTitle=$_POST['title'];
		$mPhotoid=$_POST['photoid'];
		
		$and="and";
		$where = 'a.id>0 ';
		if( !empty($mAccount) ){
			$where=$where.$and." b.account like '%".$mAccount."%' ";
			$and=" and ";
		}
		if( !empty($mName)){
			$where=$where.$and." b.name like '%".$mName."%'";
			$and=" and ";
		}
		
		if( !empty($mCompany)){
			$where=$where.$and." b.company_name like '%".$mCompany."%'";
			$and=" and ";
		}
		
		if( !empty($mProvince) ){
			$where=$where.$and." b.province='".$mProvince."'";
			$and=" and ";
		}
		
		if( !empty($mCity) ){
			$where=$where.$and." b.city='".$mCity."'";
			$and=" and ";
		}
		
		if( !empty($mRegStart) ){
			$begintime=strtotime($mRegStart);
			$where = $where.$and."buytime>='".$begintime."'";
			$and=" and ";
		}
		
		if( !empty($mRegEnd) ){
			$endtime=strtotime($mRegEnd);
			$where = $where.$and."buytime<='".$endtime."'";
			$and=" and ";
		}
		if(!empty($mPhotoid)){
			$where = $where.$and."images_id='".$mPhotoid."'";
			$and=" and ";
			
			}
		$this->assign($_POST);
		$photoer=M('buy_images');
		$sql = "select count(*) as tt from __TABLE__ a left join ".C('DB_PREFIX').'photoer b on a.up_userid=b.id where '.$where;
		$result = $photoer->query($sql);
		$count =$result[0]['tt'];
		$page=new Page($count,20);
		$show=$page->show();
		$this->assign("page",$show);
		$sql = "select a.*,b.account,b.company_name,b.name,b.province,b.city from __TABLE__ a left join ".C('DB_PREFIX').'photoer b on a.up_userid=b.id where '.$where;
		$sql .= ' order by a.buytime desc limit '.$page->firstRow.','.$page->listRows;
		$list=$photoer->query($sql);
		//var_dump($photoer->getlastsql());
		
		//查询用户表中的信息 单位名称  用户类型 用户状态
		$this->assign('mlist',$list);
		
		$this->display();
	}
	//摄影师图片统计
	public function phototj(){


		import ('@.ORG.Page');
		$mAccount=$_POST['account'];
		$mName=$_POST['name'];
		$mCompany=$_POST['company'];
		$mProvince=$_POST['province'];
		$mCity=$_POST['city'];
		$mRegStart=$_POST['start_query'];
		$mRegEnd=$_POST['end_query'];
		
		$and="";
		if( !empty($mAccount) ){
			$where=$where.$and."account = '".$mAccount."'";
			$and=" and ";
		}
		if( !empty($mName)){
			
			$where=$where.$and."name ='".$mName."'";
			$and=" and ";
		}
		
		if( !empty($mCompany)){			
			$where=$where.$and."company_name like '%".$mCompany."%' ";
			$and=" and ";
		}
		
		if( !empty($mRegStart) ){
			$begintime=strtotime($mRegStart);
			$where = $where.$and."createtime>='".$begintime."'";
			$and=" and ";
		}
		
		if( !empty($mRegEnd) ){
			$endtime=strtotime($mRegEnd);
			$where = $where.$and."createtime<='".$endtime."'";
			$and=" and ";
		}
		if( !empty($mProvince) ){
			$where=$where.$and."province='".$mProvince."'";
			$and=" and ";
		}
		
		if( !empty($mCity) ){
			$where=$where.$and."city='".$mCity."'";
			$and=" and ";
		}
		$photoer=M('photoer');
		$count=$photoer->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		
		$this->assign("page",$show);
		$list=$photoer->order('id')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
	
		$this->assign("page",$show);

	   for($i=0;$i<count($list);$i++){
		 $group=M('upload_group_detail');
		 $images=M('upload_images_detail');
		 $simages=M('images_detail');
		 $buy=M('buy_images');
		$whereinfo="up_account = '".$list[$i]['account']."'";
		$groupinfo=$group->where($whereinfo)->count();
		$imagesinfo=$images->where($whereinfo)->count();
		$simagesinfo=$simages->where($whereinfo)->count();
		$buyinfo=$buy->where($whereinfo)->count();	
		
		$mlist[$i]['id']=$list[$i]['id'];
		$mlist[$i]['role']=$list[$i]['role'];		
		$mlist[$i]['account']=$list[$i]['account'];
		$mlist[$i]['name']=$list[$i]['name'];	
		$mlist[$i]['company_name']=$list[$i]['company_name'];	
		$mlist[$i]['city']=$list[$i]['city'];
		$mlist[$i]['phone']=$list[$i]['phone'];
			
		$mlist[$i]['group']=$groupinfo;
		$mlist[$i]['images']=$imagesinfo;
		
		$mlist[$i]['simages']=$simagesinfo;	
			
		$mlist[$i]['sellnum']=$buyinfo;
		$mlist[$i]['values']=$list[$i]['values'];
		//已结算
		$mlist[$i]['remark']=$list[$i]['values'];
		//状态
		if($list[$i]['state']==0)
			$states="正常";
		else
			$states="未激活";
		$mlist[$i]['state']=$states;	
	   }
		$this->assign('mlist',$mlist);	
			
		$this->display();
	}
	//摄影师结算管理
	public function photojs(){

		import ('@.ORG.Page');
		$mAccount=$_POST['account'];
		$mRegStart=$_POST['start_query'];
		$mRegEnd=$_POST['end_query'];
		
		$and="";
		if( !empty($mAccount) ){
			$where=$where.$and."account = '".$mAccount."'";
			$and=" and ";
		}
		if( !empty($mRegStart) ){
			$begintime=strtotime($mRegStart);
			$where = $where.$and."createtime>='".$begintime."'";
			$and=" and ";
		}
		
		if( !empty($mRegEnd) ){
			$endtime=strtotime($mRegEnd);
			$where = $where.$and."createtime<='".$endtime."'";
			$and=" and ";
		}
		$photoer=M('application');
		$count=$photoer->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		
		$this->assign("page",$show);
		$list=$photoer->order('id')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
	
		$this->assign("page",$show);
		$this->assign('mlist',$list);
	

	$this->display();
	}
	function correctstate(){

		if($_POST['0submit']){
			$ids=$_POST['ids'];
			if(!empty($ids) && is_array($ids)){
				$photoer=M('downloader');
				$id=implode(',',$ids);
				$date['id']=$id;
				$date['state']=0;
				
				if(false!==$photoer->save($date)){
					$this->success(L('激活成功'));
				}else{
					$this->error(L('delete_error'));
				}
			}else{
				$this->error(L('do_empty'));
			
			}
		}
		else if($_POST['1submit']){
			$ids=$_POST['ids'];
			if(!empty($ids) && is_array($ids)){
				$photoer=M('downloader');
				$id=implode(',',$ids);
				$date['id']=$id;
				$date['state']=1;
				
				if(false!==$photoer->save($date)){
					$this->success(L('锁定成功'));
				}else{
					$this->error(L('delete_error'));
				}
			}else{
				$this->error(L('do_empty'));
			
			}	
		}
		else{
			$ids=$_POST['ids'];
			if(!empty($ids) && is_array($ids)){
				$photoer=M('downloader');
				$id=implode(',',$ids);
				$date['id']=$id;
				$date['state']=2;
				
				if(false!==$photoer->save($date)){
					$this->success(L('封杀成功'));
				}else{
					$this->error(L('delete_error'));
				}
			}else{
				$this->error(L('do_empty'));
			
			}
			
			
			}
	}

}
