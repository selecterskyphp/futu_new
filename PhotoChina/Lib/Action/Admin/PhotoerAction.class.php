<?php
class PhotoerAction extends AdminbaseAction {

	public $dao;
	function _initialize() {
		parent::_initialize();
		$this->dao=D('Admin.photoer');
		if( empty($this->dao) )
			echo "photoer is null";
	}
	function index() {	
		
		import ('@.ORG.Page');
		
		$mAccount=$_GET['account'];
		$company_name=$_GET['company_name'];
		$mName=$_GET['name'];
		$mEmail=$_GET['email'];
		$mPhone=$_GET['phone'];
		$money=$_GET['money'];
		$valid=$_GET['valid'];
		$mProvince=$_GET['province'];
		$mCity=$_GET['city'];
		$mRegStart=$_GET['start_query'];
		$mRegEnd=$_GET['end_query'];
		$mState=$_GET['state'];
		$type = isset($_GET['type'])?intval($_GET['type']):2;
		$subtype = isset($_GET['subtype'])?intval($_GET['subtype']):0;
		$no_pvalue = isset($_GET['no_pvalue'])?intval($_GET['no_pvalue']):0;
		
		$orderfield = isset($_REQUEST['orderfield'])?trim($_REQUEST['orderfield']):'id';
		$order = isset($_REQUEST['order'])?trim($_REQUEST['order']):'desc';
		$orderinfo=$orderfield." ".$order;
		
		$this->assign($_GET);
		$this->assign('orderfield',$orderfield);
		$this->assign('order',$order);
		$this->assign('type',$type);
		$and="";
		if($no_pvalue===1)
		{
		    $where=$where.$and." no_pvalue>0";
			$and=" and ";
		}
	    if($subtype>0)
		{
		    $where=$where.$and." subtype=".$subtype;
			$and=" and ";
		}
		if(isset($money) && '1' == $money)
		{
		    $where=$where.$and." pvalue<300";
			$and=" and ";
		}
		if(isset($valid) && '1' == $valid)
		{
		    $where=$where.$and." end_time>".strtotime('-3 day');
			$and=" and ";
		}
		if( !empty($mAccount) ){
			$where=$where.$and."account like '%".$mAccount."%' ";
			$and=" and ";
		}
		if( !empty($company_name) ){
			$where=$where.$and."company_name like '%".$company_name."%' ";
			$and=" and ";
		}
	    if( !empty($type) ){
			$where=$where.$and."type='".$type."' ";
			$and=" and ";
		}
		if( !empty($mName)){
			$where=$where.$and."name like '%".$mName."%' ";
			$and=" and ";
		}
		
		if( !empty($mEmail)){
			$where=$where.$and."email='".$mEmail."'";
			$and=" and ";
		}
		
		if( !empty($mPhone) ){
			$where=$where.$and."phone='".$mPhone."'";
			$and=" and ";
		}
		
		
		
		if( !empty($mCity) ){
			$where=$where.$and." city like '".$mCity."%'";
			$and=" and ";
		}
	    if( !empty($mProvince) ){
			$where=$where.$and."province like '".$mProvince."%'";
			$and=" and ";
		}
		
		if( !empty($mRegStart) ){
			$where = $where.$and."createtime>=".strtotime($mRegStart)."";
			$and=" and ";
		}
		
		if( !empty($mRegEnd) ){
			$where = $where.$and."createtime<".(strtotime($mRegEnd)+60*60*24)."";
			$and=" and ";
		}
		elseif(!empty($mRegStart))
		{
		    $where = $where.$and."createtime<".(strtotime($mRegStart)+60*60*24)."";
			$and=" and ";
		}
		if( !empty($mState) &&$mState!="全部" ){
			if($mState=="正常")
				$state=0;
			elseif($mState=="等待审核")
				$state=1;
			elseif($mState=="锁定")
				$state=2;
			elseif($mState=="已删除")
				$state=3;
			$where = $where.$and."state='".$state."'";
			$and=" and ";
		}
		
		$photoer=$this->dao;
		$count=$photoer->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		
		$this->assign("page",$show);
		$list=$photoer->order($orderinfo)->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
		//var_dump($photoer->getlastsql());
		//exit;
		
		//今日注册人数	昨天注册人数	前天注册人数	前七天注册人数	
		//前一月注册人数	总注册人数	总激活账号
		$curDate=date('Y-m-d 00:00:01',time());
		$where=" type={$type} and createtime>=".strtotime($curDate);
		$curCount=$photoer->where($where)->count(); //当天注册人数
		
		$yesDate=strtotime($curDate)-24*3600;
		$where=" type={$type} and createtime>=".$yesDate;
		$yesCount=$photoer->where($where)->count(); //昨天注册人数
		$yesCount=$yesCount-$curCount;
		
		$beforeDate=strtotime($curDate)-24*3600*2;
		$where=" type={$type} and createtime>=".$beforeDate;
		$beforeCount=$photoer->where($where)->count(); //前天注册人数
		$beforeCount=$beforeCount-$yesCount-$curCount;
		
		$before7Date=strtotime($curDate)-24*3600*7;
		$where=" type={$type} and createtime>=".$beforeDate;
		$before7count=$photoer->where($where)->count(); //前7天注册人数
		
		//前一个月注册人数
		$monthDay=strtotime("-1 month");
		$where=" type={$type} and createtime>=".$monthDay;
		$monthcount=$photoer->where($where)->count(); //前一个月注册人数
		
		//总注册人数
		$totalCount=$photoer->where('type='.$type)->count();
		
		if($type == 1)
		{
		    $need_money = $photoer->where('type=1 and pvalue<300')->count();
		    $will_valid = $photoer->where('type=1 and end_time>'.strtotime('-3 day'))->count();
		    $vo['need_money'] = $need_money;
		    $vo['will_valid'] = $will_valid;
		}
		
		//总激活账号？？什么才叫激活账号?暂时全部激活
		$totalActive=$totalCount;
		
		$vo['today_reg_num']=$curCount;
		$vo['yer_reg_num']=$yesCount;
		$vo['before_reg_num']=$beforeCount;
		$vo['before7_reg_num']=$before7count;
		$vo['month_reg_num']=$monthcount;
		$vo['total_reg_num']=$totalCount;
		$vo['total_active_num']=$totalActive;
		
		$this->assign('vo',$vo);
		$this->assign('mlist',$list);
		$this->display();
	}
    function point()
	{
	    $this->assign($_GET);
	    $this->display();
	}
    function point2()
	{
	    $this->assign($_GET);
	    $id = intval($_GET['id']);
	    $vo = $this->dao->getById($id);
	    $this->assign('vo',$vo);
	    $this->display();
	}
	function she2()
	{
	    $this->assign($_GET);
	    $id = intval($_GET['id']);
	    $vo = $this->dao->getById($id);
	    if(!isset($vo['zheke']) || floatval($vo['zheke']) === 0)
	    {
	        $vo['zheke'] = 0.5;
	    }
	    $this->assign('vo',$vo);
	    $this->display();
	}
	function she()
	{
	   $type = $_GET['type'];
	   $this->assign('jumpUrl',U('Photoer/index'));
	   if(2 == $type)
	   {
	       $where = array();
	       $where['id'] = $_GET['id'];
	       $where['type'] = 2;
	       $where['state'] = 0;
	       $data = array();
	       $data['start_time'] = time();
	       $data['state'] = 1;
	       $result = $this->dao->where($where)->save($data);
	       if($result)
	       {
	           $this->success('审核成功');
	       }
	       else 
	       {
	           $this->error('审核失败');
	       }
	   }
	   else
	   {
	       $this->assign($_GET);
	       $where = array();
	       $where['id'] = array('eq',$_GET['id']);
	       $vo = $this->dao->where($where)->find();
	       if($vo['start_time'] <= 0)
	       {
	           $vo['start_time'] = time();
	       }
	       $this->assign('vo',$vo);
	       $this->display();
	   }
	}
	function topay()
	{
	    $id = $_GET['id'];
	    $vo = $this->dao->getById($id);
	    $this->assign('vo',$vo);
	    $this->display();
	}
	function dotopay()
	{
	    $id = $_POST['id'];
	    $username= $_SESSION['username'];
	    $vo = $this->dao->getById($id);
	    if(!$vo)
	    {
	        $this->error('ID参数不正确');
	    }
	    $yes = isset($_POST['yesorno'])?intval($_POST['yesorno']):-1;
	    if(-1 === $yes)
	    {
	        $this->error('选项参数不正确');
	    }
	    $no_pvalue = intval($vo['no_pvalue']);
	    $cur_pvalue = intval($vo['pvalue']);
	    if($no_pvalue<=0)
	    {
	        $this->error('没有需要审核的申请结算');
	    }
	    if(0 === $yes)
	    {
	        //审核 不通过 
	        $pvalue = $cur_pvalue + $no_pvalue;
    	    $data = array();
    	    $data['pvalue'] = $pvalue;
    	    $data['no_pvalue'] = 0;
    	    $result = $this->dao->where('id='.$id)->save($data);
	        if(!$result)
	        {
	            $this->error('更新记录失败');
	        }
	        else
	        {
	            $this->success('操作成功 ');
	        }
	    }
	    //审核通过要加入消费记录
	    $data = array();
	    $data['no_pvalue'] = 0;
	    $result = $this->dao->where('id='.$id)->save($data);
	    if(!$result)
	    {
	        $this->error('更新记录失败');
	    }
	    $data = array();
        $data['type'] = '3';
        $data['userid'] = $id;
        $data['account'] = $vo['account'];
        $data['name'] = $vo['name'];
        $data['oper'] = $username;
        $data['points'] = $no_pvalue;
        $data['use_points'] = $vo['pvalue'];
        $data['createtime'] = time();
        $data['pid'] = 0;
        $data['group_id'] = 0;
        $data['url'] ='';
        $data['up_time'] =time();
        $mod = M('pay');
        $result = $mod->data($data)->add();
        if(!$result)
        {
            $this->error('增加摄影师点数结算明细记录失败');
        }
        else
        {
            $this->success('操作成功');
        }
	}
	function pay()
	{
	    $id = $_GET['id'];
	    $vo = $this->dao->getById($id);
	    $this->assign('vo',$vo);
	    $this->display();
	}
	function dopay()
	{
	    $id = $_POST['id'];
	    $username= $_SESSION['username'];
	    $vo = $this->dao->getById($id);
	    if(!$vo)
	    {
	        $this->error('参数不正确');
	    }
	    $pvalue = isset($_POST['pvalue'])?intval($_POST['pvalue']):0;
	    if($pvalue<=0)
	    {
	        $this->error('参数不正确');
	    }
	    $cur_pvalue = intval($vo['pvalue']);
	    if($pvalue>$cur_pvalue)
	    {
	        $this->error('结算的点数大于可用点数');
	    }
	    $use_pvalue = $cur_pvalue - $pvalue;
	    $data = array();
	    $data['pvalue'] = $use_pvalue;
	    $result = $this->dao->where('id='.$id)->save($data);
	    if(!$result)
	    {
	        $this->error('更新记录失败');
	    }
	    $data = array();
        $data['type'] = '3';
        $data['userid'] = $id;
        $data['account'] = $vo['account'];
        $data['name'] = $vo['name'];
        $data['oper'] = $username;
        $data['points'] = $pvalue;
        $data['use_points'] = $use_pvalue;
        $data['createtime'] = time();
        $data['pid'] = 0;
        $data['group_id'] = 0;
        $data['url'] ='';
        $data['up_time'] =time();
        $mod = M('pay');
        $result = $mod->data($data)->add();
        if(!$result)
        {
            $this->error('增加摄影师点数结算明细记录失败');
        }
        else
        {
            $this->success('操作成功');
        }
	}
	function doshe()
	{
	    $user_type = intval($_POST['user_type']);
	    $user_num = intval($_POST['user_num']);
	    $user_numed = intval($_POST['user_numed']);
	    $user_zheke = floatval($_POST['user_zheke']);
	    $user_zq = intval($_POST['user_zq']);
	    $nodown = isset($_POST['nodown'])?intval($_POST['nodown']):0;
	    $allowdown_type = isset($_POST['allowdown_type'])?trim($_POST['allowdown_type']):'';
	    $id = intval($_POST['id']);
	    $type = intval($_POST['type']);
	    
	    if((1==$user_type || 4==$user_type)&&$user_num<=0)
	    {
	        $this->error('请输入正确的最大下载图片数量');
	    }
	    if(($user_type != 5 && $user_type != 6)&&$user_zheke<=0)
	    {
	        $this->error('请输入正确的图片折扣');
	    }
	    if(($user_type == 3)&&$user_zq<=0)
	    {
	        $this->error('请输入正确的图片结算周期');
	    }
	    $where = array();
	    $where['id'] = $id;
	    $where['type'] = $type;
	    //$where['state'] = 0;
	    $data = array();
	    $data['start_time'] = strtotime($_POST['start_time']);
	    if($data['start_time']<=0)
	    {
	        $data['start_time'] = time();
	    }
	    if(isset($_POST['end_time']))
	    {
	        $data['end_time'] = strtotime($_POST['end_time']);
	    }
	    if(3 == $user_type)
	    {
	        $data['every_pay_day'] = $user_zq;
	    }
	    $data['zheke'] = $user_zheke;
	    $data['nodown'] = $nodown;
	    $data['allow_down_num'] = $user_num;
	    $data['allowdown_type'] = $allowdown_type;
	    $data['downed_num'] = $user_numed;
	    $data['state'] = 1;
	    $data['subtype'] = $user_type;
	    $result = $this->dao->where($where)->save($data);
	    if($result)
	    {
	        $this->success('操作成功');
	    }
	    else 
	    {
	        $this->error('操作失败,没有记录被更新');
	    }
	}
	function doshe2()
	{
	    $id = intval($_POST['id']);
	    $zheke = isset($_POST['zheke'])?floatval($_POST['zheke']):0.5;
	    $data = array();
        $data['state'] = 1;
        $data['zheke'] = $zheke;    
        $this->assign('jumpUrl',U('Photoer/she2',array('id'=>$id)));
        $result = $this->dao->where('id='.$id)->save($data);
        if($result)
        {
            //var_dump($this->dao->getLastSql());
            //exit;
            $this->success('操作成功');
        }
        else
        {
            $this->error('操作失败，没有记录被更新');
        }
	}
    function dopoint()
	{
	    $id = intval($_POST['id']);
	    $username = $_SESSION['username'];
	    $point = isset($_POST['point'])?intval($_POST['point']):0;
	    $remark = isset($_POST['remark'])?trim($_POST['remark']):'';
	    
        $this->assign('jumpUrl',U('Photoer/point',array('id'=>$id)));
        if($point<=0)
        {
            $this->error('点数输入不正确');
        }
	    $result = $this->dao->setInc('pvalue','id='.$id,$point);
	    if(!$result)
	    {
	        $this->error('操作点数失败');
	    }
        $vo = $this->dao->getById($id);
        $data = array();
        $data['type']=1;
        $data['userid']=$id;
        $data['account']=$vo['account'];
        $data['name']=$vo['name'];
        $data['oper']=$username;
        $data['points']=$point;
        $data['use_points']=$vo['pvalue'];
        $data['createtime']=time();
        $data['remark']=$remark;
        $data['pid']=0;
        $data['ptitle']='';
        $mod = M('pay');
        $result = $mod->data($data)->add();
        if($result )
        {
            $this->success('操作成功');
        }
        else
        {
            
            $this->success('操作失败，加入消费记录失败');
        }
	}
    function dopoint2()
	{
	    $id = intval($_POST['id']);
	    $username = $_SESSION['username'];
	    $point = isset($_POST['point'])?intval($_POST['point']):0;
	    $remark = isset($_POST['remark'])?trim($_POST['remark']):'';
	    if($point<=0)
	    {
	        $this->error('点数输入不正确 ');
	    }
	    $vo = $this->dao->getById($id);
	    $this->assign('jumpUrl',U('Photoer/point2',array('id'=>$id)));
	    if($point > $vo['pvalue'])
	    {
	        $this->error('当前余额不足');
	    }
	    
	    $result = $this->dao->setDec('pvalue','id='.$id,$point);
        if($result)
        {
            $vo = $this->dao->getById($id);
            $data = array();
            $data['type']=2;
            $data['userid']=$id;
            $data['account']=$vo['account'];
            $data['name']=$vo['name'];
            $data['oper']=$username;
            $data['points']=$point;
            $data['use_points']=$vo['pvalue'];
            $data['createtime']=time();
            $data['remark']=$remark;
            $data['pid']=0;
            $data['ptitle']='';
            $mod = M('pay');
            $result = $mod->data($data)->add();
            if($result )
            {
                $this->success('操作成功');
            }
            else
            {
                $this->success('操作失败，加入消费记录失败');
            }
        }
        else
        {
            $this->error('操作失败，没有记录被更新');
        }
	}
	function insert(){
		$photoer=$this->dao;
		if($data=$photoer->create()){
			if(false!==$photoer->add()){
				/*$uid=$photoer->getLastInsID();
				$ru['role_id']=$_POST['groupid'];
				$ru['user_id']=$uid;
				$roleuser=M('RoleUser');
				$roleuser->add($ru);*/
				$this->success(L('add_ok'));
			}else{
				$this->error(L('add_error'));
			}
		}else{
			$this->error($photoer->getError());
		}
	}
	
	function update(){
		//修改资料
		$uploadfilename="";

		import("@.ORG.UploadFile");    
		$upload = new UploadFile(); // 实例化上传类    
		$upload->maxSize  = 3145728 ; // 设置附件上传大小    
		$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg'); // 设置附件上传类型    
		$upload->savePath =  './Uploads/headimage/'; // 设置附件上传目录    
		$upload->$uploadReplace = true;

		if(!$upload->upload('',$_POST['head_image_path'])) { // 上传错误 提示错误信息    
			//$this->error($upload->getErrorMsg());    
		}else{ // 上传成功 获取上传文件信息    
			$info =  $upload->getUploadFileInfo(); 
			$uploadfilename=$info[0]["savename"];
			$_POST['head_image_path'] = $upload->savePath . $uploadfilename;
		}    
		
		$photoer=$this->dao;
	    if(false!==$photoer->save($_POST)){
					$this->success(L('edit_ok'));
				}else{
				    
					$this->error(L('edit_error').$photoer->getDbError());
				}
	}
	
	function _before_add(){
		$this->assign('rlist',$this->usergroup);
	}
	
	function _before_edit(){
		$this->assign('rlist',$this->usergroup);
	}
	
	function delete(){
		$id=$_GET['id'];
		$photoer=$this->dao;
		if(false!==$photoer->delete($id)){
			$roleuser=M('RoleUser');
			$roleuser->where('user_id ='.$id)->delete();
			delattach(array('moduleid'=>0,'catid'=>0,'id'=>0,'userid'=>$id));
			$this->success(L('delete_ok'));
		}else{
			$this->error(L('delete_error').$photoer->getDbError());
		}
	}
	
	function deleteall(){
		if($_POST['deletesubmit'])
		{
			$ids=$_POST['ids'];
			if(!empty($ids) && is_array($ids))
			{
				$photoer=M('Photoer');
				//$id=implode(',',$ids);
				$where = array();
				for($i=0;$i<count($ids);$i++)
				{
					/*$date['id']=$ids[$i];
					$date['state']=200;
					//dump($date);
					if(false==$photoer->save($date))
						$this->error(L('delete_error'));*/
				    $where['id'] = $ids[$i];
				    $photoer->where($where)->delete();
				}
				$this->success(L('delete_ok'));
			}
			else{
				$this->error(L('do_empty'));
				}
			}
		
		elseif($_POST['2submit']){
			$ids=$_POST['ids'];
			if(!empty($ids) && is_array($ids)){
				$photoer=M('Photoer');
				$id=implode(',',$ids);		
				for($i=0;$i<count($ids);$i++)
				{
					$date['id']=$ids[$i];
					$date['state']=2;
					if(false==$photoer->save($date))
						$this->error(L('锁定失败'));
				}
				$this->success(L('锁定成功'));
			}else{
				$this->error(L('do_empty'));
			
			}	
			
		}
		elseif($_POST['0submit']){
			$ids=$_POST['ids'];
			if(!empty($ids) && is_array($ids)){
				$photoer=M('Photoer');
				$id=implode(',',$ids);
				for($i=0;$i<count($ids);$i++)
				{
					$date['id']=$ids[$i];
					$date['state']=1;
					if(false==$photoer->save($date))
						$this->error(L('激活失败'));
				}
				$this->success(L('激活成功'));
			}
			else
			{
				$this->error(L('do_empty'));	
			}
		}
	}
	
	function reset()
	{
	    $id = intval($_GET['id']);
	    $data['password'] = sysmd5('123456');
	    $result = $this->dao->where('id='.$id)->save($data);
	    if($result)
	    {
	        $this->success('操作成功');
	    }
	    else
	    {
	        $this->error('操作失败');
	    }
	}
	
}
?>