<?php
class PhototypeAction extends AdminbaseAction {
	
    private $dao;
    public function _initialize()
    {
        $this->dao=M('column');
    }
	function index() {
		
		
		//��ѯ���е�һ���Ͷ�����
		$pid = isset($_GET['pid'])?intval($_GET['pid']):0;
		$secid = isset($_GET['secid'])?intval($_GET['secid']):0;
		if($secid>0)
		{
		    $where=" secondid=".$secid;
		}
		elseif($pid>0)
		{
		    $where=" parentid=".$pid.' and secondid=0';
		}
		else 
		{
		    $where=" parentid=0";
		}
		
		$list=$this->dao->where($where)->order('sort asc,groupid desc')->select();
		$this->assign('pid',$pid);
		$this->assign('sec',$secid);
		$this->assign('list',$list);
		
		$this->display();
	}
	public function edit()
	{
	    $id = $_GET['id'];
	    $vo = $this->dao->getByGroupid($id);
	    $parentname = '';
	    if($vo['secondid']>0)
	    {
	        $parentname = $this->dao->where('groupid='.$vo['secondid'])->getField('name');
	    }
	    else if($vo['parentid']>0)
	    {
	        $parentname = $this->dao->where('groupid='.$vo['parentid'])->getField('name');
	    }
	    $this->assign('vo',$vo);
	    $this->assign($_GET);
	    $this->assign('parentname',$parentname);
	    $this->display();
	}
    public function add()
	{
	    $pid = isset($_GET['pid'])?intval($_GET['pid']):0;
	    $secid = isset($_GET['secid'])?intval($_GET['secid']):0;
	    
	    $parentname = '';
	    if($secid>0)
	    {
	        $parentname = $this->dao->where('groupid='.$secid)->getField('name');
	    }
	    elseif($pid>0)
	    {
	        $parentname = $this->dao->where('groupid='.$pid)->getField('name');
	    }
	    $this->assign($_GET);
	    $this->assign('parentname',$parentname);
	    $this->display('Phototype_edit');
	}
	public function update()
	{
	    $sort = isset($_POST['sort'])?intval($_POST['sort']):0;
	    $name = isset($_POST['name'])?trim($_POST['name']):0;
	    $allowlist = isset($_POST['allowlist'])?trim($_POST['allowlist']):'';
	    $pid = isset($_POST['pid'])?intval($_POST['pid']):0;
	    $secid = isset($_POST['secid'])?intval($_POST['secid']):0;
	    $id = isset($_POST['id'])?intval($_POST['id']):0;
	    
	    $data = array();
	    $data['sort'] = $sort;
	    $data['name'] = $name;
	    $data['allowlist'] = $allowlist;
	    $result = $this->dao->where('groupid='.$id)->save($data);
	    if($result)
	    {
	        $this->assign('jumpUrl',U("Phototype/index",array('pid'=>$pid,'secid'=>$secid)));
	        $this->success('操作成功');
	    }
	    else
	    {
	        $this->error('操作失败，没有记录被更新');
	    }
	}
	public function insert()
	{
	    $sort = isset($_POST['sort'])?intval($_POST['sort']):0;
	    $name = isset($_POST['name'])?trim($_POST['name']):0;
	    $allowlist = isset($_POST['allowlist'])?trim($_POST['allowlist']):'';
	    $pid = isset($_POST['pid'])?intval($_POST['pid']):0;
	    $secid = isset($_POST['secid'])?intval($_POST['secid']):0;
	    $data = array();
	    $data['sort'] = $sort;
	    $data['name'] = $name;
	    $data['allowlist'] = $allowlist;
	    if($secid>0)
	    {
	        $count = $this->dao->where('secondid='.$secid)->count();
	        $secdata = $this->dao->where('groupid='.$secid)->find();
	        
	        $count++;
	        $groupid = $secid + $count;
	        $data['parentid'] = $secdata['parentid'];
	        $data['secondid'] = $secid;
	        $data['groupid'] = $groupid;
	        $data['state'] = 1;
	    }
	    else if($pid>0)
	    {
	        $count = $this->dao->where('parentid='.$pid)->count();
	        $pdata = $this->dao->where('groupid='.$pid)->find();
	        
	        $count++;
	        $groupid = $pid + ($count*100);
	        $data['parentid'] = $pid;
	        $data['secondid'] = 0;
	        $data['groupid'] = $groupid;
	        $data['state'] = 1;
	    }
	    else
	    {
	        $count = $this->dao->where('parentid=0')->count();
	        
	        $count++;
	        $groupid = $count*10000;
	        $data['parentid'] = 0;
	        $data['secondid'] = 0;
	        $data['groupid'] = $groupid;
	        $data['state'] = 1;
	    }
	    $result = $this->dao->data($data)->add();
	    if($result)
	    {
	        $this->assign('jumpUrl',U("Phototype/index",array('pid'=>$pid,'secid'=>$secid)));
	        $this->success('操作成功');
	    }
	    else
	    {
	        $this->error('操作失败，没有记录被增加');
	    }
	}
	
	public function hide()
	{
	    $pid = isset($_GET['pid'])?intval($_GET['pid']):0;
	    $secid = isset($_GET['secid'])?intval($_GET['secid']):0;
	    $id = isset($_GET['id'])?intval($_GET['id']):0;
	    $state = isset($_GET['state'])?intval($_GET['state']):0;
	    $state = (0 == $state)?1:0;
	    $data = array();
	    $data['state']=$state;
	    $this->dao->where('groupid='.$id)->save($data);
	    $this->redirect("Phototype/index",array('pid'=>$pid,'secid'=>$secid));
	}
	public function sort()
	{
	    $pid = isset($_GET['pid'])?intval($_GET['pid']):0;
	    $secid = isset($_GET['secid'])?intval($_GET['secid']):0;
	    $id = isset($_GET['id'])?intval($_GET['id']):0;
	    $sort = isset($_GET['sort'])?intval($_GET['sort']):0;
	    if($sort>=0)
	    {
    	    $data = array();
    	    $data['sort']=$sort;
    	    $this->dao->where('groupid='.$id)->save($data);
    	    $this->redirect("Phototype/index",array('pid'=>$pid,'secid'=>$secid));
	    }
	    else
	    {
	        $this->redirect("Phototype/index",array('pid'=>$pid,'secid'=>$secid),3,'已经到了最顶部');
	    }
	}
}
?>