<?php
/**
 * 
 * Role(��Ա�����)
 *
 * @package      	YOURPHP
 * @author          liuxun QQ:147613338 <admin@yourphp.cn>
 * @copyright     	Copyright (c) 2008-2011  (http://www.yourphp.cn)
 * @license         http://www.yourphp.cn/license.txt
 * @version        	yourphp��ҵ��վϵͳ v2.0 2011-03-01 yourphp.cn $
 */
class RoleAction extends AdminbaseAction {

	protected $dao;
    function _initialize()
    {	
		parent::_initialize();		
	
    }

	public function _before_insert()
    {
		$_POST['allowpost'] = $_POST['allowpost'] ? 1 : 0 ;
		$_POST['allowpostverify'] = $_POST['allowpostverify'] ? 1 : 0 ;
		$_POST['allowupgrade'] = $_POST['allowupgrade'] ? 1 : 0 ;
		$_POST['allowsendmessage'] = $_POST['allowsendmessage'] ? 1 : 0 ;
		$_POST['allowattachment'] = $_POST['allowattachment'] ? 1 : 0 ;
		$_POST['allowsearch'] = $_POST['allowsearch'] ? 1 : 0 ;
	}
	
	public function index() {
		$model = M ('Role');
		$list = $model->select();
		$this->assign('list', $list);
		$this->display();
	}


	public function _before_update()
    {
		$_POST['allowpost'] = $_POST['allowpost'] ? 1 : 0 ;
		$_POST['allowpostverify'] = $_POST['allowpostverify'] ? 1 : 0 ;
		$_POST['allowupgrade'] = $_POST['allowupgrade'] ? 1 : 0 ;
		$_POST['allowsendmessage'] = $_POST['allowsendmessage'] ? 1 : 0 ;
		$_POST['allowattachment'] = $_POST['allowattachment'] ? 1 : 0 ;
		$_POST['allowsearch'] = $_POST['allowsearch'] ? 1 : 0 ;
	}
	
	//�û�Ȩ�޹���
	public function auth()
	{
		$model = M ('Role');
		$list = $model->select();
		$this->assign('list', $list);
		$this->display();
	}
	
	//��ѯ
	public function query()
	{
		$id=$_GET['id'];
		$vo['roleid']=$id;
		
		$m=M('role_auth');
		$sql="select menu_id as id from pc_role_auth where roleid=".$id;
		$vids=$m->query($sql);
		for($i=0;$i<count($vids);$i++)
			$vlist[$i]=$vids[$i]['id'];
		
		$this->assign('vo',$vo);
		$this->assign('vlist',$vlist);
		
		$this->display();
	}
	
	//����Ȩ��
	public function update()
	{
		$rid=$_POST['roleid'];
		$ids=$_POST['ids'];

		if(!empty($ids) && is_array($ids))
		{
			//$id=implode(',',$ids);
			$auth=M('role_auth');
			
			$sql="delete from pc_role_auth where roleid=".$rid;
			$auth->execute($sql);
			for($i=0;$i<count($ids);$i++)
			{
				$data['roleid']=$rid;
				$data['menu_id']=$ids[$i];
				
				$auth->add($data);
			}
			
			$this->success('权限修改成功');
		}
		
		$this->error('请选择权限记录');
	}

	public function update2()
	{
		$id=$_POST['id'];

		if(!empty($id))
		{
			//$id=implode(',',$ids);
			$r=M('role');
			
			$data['name']=$_POST['name'];
			$data['status']=$_POST['status'];
			$data['remark']=$_POST['remark'];

			$r->where('id='.$id)->save($data);
			
			
			$this->success('修改成功');
		}
		
		$this->error('修改失败');
	}

}
?>