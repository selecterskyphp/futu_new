<?php
class gonggaoAction extends AdminbaseAction {

	function index()
	{
		$start=$_POST['start'];
		$end=$_POST['end'];

		if( !empty($start) )
		{
			$where=" time>=".strtotime($start);
			$and=" and";
		}

		if( !empty($end) )
			$where=$where.$and." time<=".strtotime($end);

		$g=M('gonggao');
		$glist=$g->where($where)->select();

		$this->assign('glist',$glist);

		$this->display();
	}

	//删除
	function del()
	{
		$id=$_GET['id'];
		if( !empty($id) )
		{
			$sql="delete from pc_gonggao where id=".$id;
			echo $sql;
			$g=M('gonggao');
			if($g->execute($sql))
				$this->success('删除成功');
			else
				$this->error('删除失败');
		}
	}

	//公告修改/新增
	function add()
	{
		$id=$_GET['id'];

		if(!empty($id))
		{
			$g=M('gonggao');
			$where=" id=".$id;

			$glist=$g->where($where)->find();

			$this->assign('glist',$glist);
		}
		
		$this->display();
	}

	function subadd()
	{
		$id=$_POST['id'];

		if( !empty($id) && $id>0 )
		{
			//修改
			$data['title']=$_POST['title'];
			$data['content']=$_POST['context'];

			$g=M('gonggao');
			if($g->where(' id='.$id)->save($data))
				$this->success('修改成功');
			else
				$this->error('修改失败');
		}
		else
		{
			//新增
			$data['title']=$_POST['title'];
			$data['content']=$_POST['context'];
			$data['time']=time();

			//dump($data);

			$g=M('gonggao');
			if($g->add($data))
				$this->success('新增成功');
			else
				$this->error('新增失败');
		}
	}

	function show()
	{
		$id=$_GET['id'];

		if(!empty($id))
		{
			$g=M('gonggao');

			$glist=$g->where('id='.$id)->find();

			$this->assign('glist',$glist);
			$this->display();
		}
	}
}
?>