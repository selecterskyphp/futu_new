<?php
class MemberAction extends AdminbaseAction {

    public $dao,$usergroup;
	function _initialize()
	{
		parent::_initialize();
		$this->dao = D('Admin.Member');
		//$this->usergroup=F('Role');
		//$this->assign('usergroup',$this->usergroup);
	}


	function index(){
		import ( '@.ORG.Page' );
		
		$mAccount=$_POST['account'];
		$mName=$_POST['name'];
		$mEmail=$_POST['email'];
		$mPhone=$_POST['phone'];
		$mProvince=$_POST['province'];
		$mCity=$_POST['city'];
		$mRegStart=$_POST['start_query'];
		$mRegEnd=$_POST['end_query'];
		$mState=$_POST['state'];

		$this->assign($_POST);
		
		$and="";
		if( !empty($mAccount) ){
			$where=$where.$and."account like '%".$mAccount."%' ";
			$and=" and ";
		}
		if( !empty($mName)){
			$where=$where.$and."name like '%".$mName."%' ";
			$and=" and ";
		}
		
		if( !empty($mEmail)){
			$where=$where.$and."email='".$mEmail."'";
			$and=" and ";
		}
		
		if( !empty($mPhone) ){
			$where=$where.$and."phone='".$mPhone."'";
			$and=" and ";
		}
		
		if( !empty($mProvince) ){
			$where=$where.$and."province='".$mProvince."'";
			$and=" and ";
		}
			
		if( !empty($mCity) ){
			$where=$where.$and."city='".$mCity."'";
			$and=" and ";
		}
		
		if( !empty($mRegStart) ){
			$where = $where.$and."createtime>='".$mRegStart."'";
			$and=" and ";
		}
		
		if( !empty($mRegEnd) ){
			$where = $where.$and."createtime<='".$mRegEnd."'";
			$and=" and ";
		}
		
		//echo "where:".$where;
		
		$member=$this->dao;
		$count=$member->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();

		$this->assign("page",$show);
		$list=$member->order('id')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();

		$this->assign('mlist',$list);
		$this->display();
	}

	function insert(){
		$member=$this->dao;
		if($data=$member->create()){
			if(false!==$member->add()){
				$uid=$member->getLastInsID();
				$ru['role_id']=$_POST['groupid'];
				$ru['user_id']=$uid;
				$roleuser=M('RoleUser');
				$roleuser->add($ru);			
				$this->success(L('add_ok'));
			}else{
				$this->error(L('add_error'));
			}
		}else{
			$this->error($user->getError());
		}
	}

	function update(){
		$member=$this->dao;
		if($data=$user->create()){
			if(!empty($data['id'])){
				$opwd=$_POST['opwd'];
				$user->password=empty($user->password)?$opwd:sysmd5($user->password);
				if(false!==$user->save()){
					$ru['role_id']=$_POST['groupid'];
					$roleuser=M('RoleUser');
					$roleuser->where('user_id='.$user->id)->save($ru);
					$this->success(L('edit_ok'));
				}else{
					$this->error(L('edit_error').$user->getDbError());
				}
			}else{
				$this->error(L('do_error'));
			}
		}else{
			$this->error($user->getError());
		}
	}
 

	function _before_add(){
		$this->assign('rlist',$this->usergroup);	
	}

	function _before_edit(){
		$this->assign('rlist',$this->usergroup);	
	}


	function delete(){
		$id=$_GET['id'];
		$user=$this->dao;
		if(false!==$user->delete($id)){
			$roleuser=M('RoleUser');
			$roleuser->where('user_id ='.$id)->delete();
			delattach(array('moduleid'=>0,'catid'=>0,'id'=>0,'userid'=>$id));
			$this->success(L('delete_ok'));
		}else{
			$this->error(L('delete_error').$user->getDbError());
		}
	}

	function deleteall(){		
		$ids=$_POST['ids'];
		if(!empty($ids) && is_array($ids)){
			$user=$this->dao;
			$id=implode(',',$ids);
			if(false!==$user->delete($id)){
				/*$roleuser=M('RoleUser');
				$roleuser->where('user_id in('.$id.')')->delete();
				delattach("moduleid=0 and catid=0 and id=0 and userid in($id)");
				*/
				$this->success(L('delete_ok'));
			}else{
				$this->error(L('delete_error'));
			}
		}else{
			$this->error(L('do_empty'));
		}
	}
}
?>