<?php
class keywordAction extends AdminbaseAction {

	protected $dao;
	function _initialize()
	{
		parent::_initialize();

	}
	
	function index()
	{
		//查找已经有的关键词
		$type=$_POST['type'];
		
		if(!empty($type) && $type!=="全部")
			$where=" type_name='".$type."'";
		$k=M('keyword');
		$klist=$k->where($where)->select();
		
		$this->assign('klist',$klist);
		
		//加载类别
		$t=M('keyword_type');
		$tlist=$t->select();
		$this->assign('tlist',$tlist);
		
		$this->display();
	}
	
	//类别管理
	function type()
	{
		//加载类别
		$t=M('keyword_type');
		$tlist=$t->select();
		$this->assign('tlist',$tlist);
		
		$this->display();
	}
	
	//添加关键词
	function addtype()
	{
		$id=$_GET['id'];
		if(!empty($id))
		{
			$k=M('keyword_type');
			$vo=$k->where(' id='.$id)->find();
			
			$this->assign('vo',$vo);
		}
		$this->display();	
	}
	
	//添加类别
	function subaddtype()
	{
		$name=$_POST['typename'];
		$parentid=$_POST['parentid'];
		$parentname=$_POST['parentname'];
		
		//判断是否已经有该关键字
		if(!empty($name))
		{
			$kt=M('keyword_type');
			
			//$count=$kt->where(" name='".$name."'")->count();
			//if( $count>0 )
				//$this->error('已经有该关键字');
			
			$data['name']=$name;
			$data['parent_id']=$parentid;
			$data['parent_name']=$parentname;
			$kt->add($data);
			
			$this->success('添加成功');
		}
	}
	
	//删除类别
	function del()
	{
		$id=$_GET['id'];
		
		$kt=M('keyword_type');
		$kt->where('id='.$id)->delete();
		
		$kt->success('删除成功');
	}
	
	//添加关键字
	function addkey()
	{
		$vo['id']=$_GET['id'];
		$vo['name']=$_GET['name'];
		
		$this->assign('vo',$vo);
		
		$this->display();
	}
	
	//提交添加关键字
	function subaddkey()
	{
		$tid=$_POST['tid'];
		$key=$_POST['key'];
		$tname=$_POST['tname'];
		
		if(strlen($key)>0)
		{
			$k=M('keyword');
			$data['name']=$key;
			$data['type_id']=$tid;
			$data['type_name']=$tname;
			$k->add($data);
		}
		else
		{
			$this->success('请输入关键字');
		}
		
		$this->success('添加成功');
	}
	
	//删除关键字
	function delkey()
	{
		$id=$_GET['id'];
		
		$k=M('keyword');
		$k->where(' id='.$id)->delete();
		
		$k->success('成功删除关键字');
	}
	
	//修改关键字
	function edittype()
	{
		$id=$_GET['id'];
		
		$k=M('keyword_type');
		$vo=$k->where('id='.$id)->find();

		$this->assign('vo',$vo);
		$this->display();
	}
	
	function subedittype()
	{
		$id=$_POST['id'];
		$vo['name']=$_POST['name'];
		
		$k=M('keyword_type');
		$k->where('id='.$id)->save($vo);
		$this->success('修改成功');
	}
}

?>