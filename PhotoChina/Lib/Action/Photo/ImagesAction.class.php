<?php
class ImagesAction extends UserAction
{
    private $dao;
	function _initialize()
    {
        parent::_initialize();
        $this->dao = M('ImagesDetail');
    }
    public function index()
    {
        $id = intval($_GET['id']);
        import ('@.ORG.Page');
        $where = 'old_group_id='.$id;
        $count=$this->dao->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		
		$this->assign("page",$show);
		$list=$this->dao->order('id desc')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
		//$list = $mod->where($where)->order('buytime desc')->select();
		$this->assign('glist',$list);
		$color = C('IMAGE_STATUS_COLOR');
		$this->assign('color',$color);
        $this->display();
    }
}	
?>