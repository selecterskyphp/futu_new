<?php
class MessageAction extends UserAction
{
	function _initialize()
    {
        parent::_initialize();
    }
    public function index()
    {
        $this->display();
    }
    public function dosend()
    {
        $data = $_POST;
        $mod = M('userrequire');
        if(is_array($data['type']))
        {
            $data['type'] = implode(',', $data['type']);
        }
        $data['createtime'] = time();
        $data['time'] = strtotime($data['time']);
        $data['starttime'] = strtotime($data['starttime']);
        $data['endtime'] = strtotime($data['endtime']);
        $data['username'] = $this->login['account'];
        $mod->add($data);
        $this->assign('jumpUrl',U("Index/index"));
        $this->success('操作成功');
    }
}
 	
?>