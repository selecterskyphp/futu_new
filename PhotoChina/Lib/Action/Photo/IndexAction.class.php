<?php
class IndexAction extends UserAction
{
	function _initialize()
    {
        parent::_initialize();
    }
    public function index()
    {
        $this->display();
    }
    public function check()
    {
        $mod = M('buy_images');
        $mod2 = M('Pay');
        $list = $mod->where('buy_values=0 and sell_values=0')->select();
        $i = 0;
        foreach($list as $v)
        {
            $where = 'type=4 and pid='.$v['images_id'].' and userid='.$v['up_userid'];
            $points = $mod2->where($where)->getField('userid');
            if(!$points)
            {
                $data = array();
                $data['type'] = '4';
                $data['userid'] = $v['up_userid'];
                $data['account'] = $v['up_account'];
                $data['name'] = '';
                $data['points'] = 0;
                $data['use_points'] = 0;
                $data['createtime'] = $v['buytime'];
                $data['pid'] = $v['images_id'];
                $data['group_id'] = $v['group_id'];
                $data['url'] =$v['url'];
                $data['up_time'] =$v['up_time'];
                $result = $mod2->data($data)->add();
                if($result)
                {
                    $i++;
                }
            }
        }
        echo '共成功操作了'.$i.'条记录';
    }
    public function check2()
    {
        $mod = M('Pay');
        $user = M('Photoer');
        $list = $mod->select();
        $i = 0;
        foreach($list as $v)
        {
            if(strlen($v['account'])<1)
            {
                $account = $user->where('id='.$v['userid'])->getField('account');
                if($account)
                {
                    $data = array();
                    $data['account'] = $account;
                    $result = $mod->where('id='.$v['id'])->save($data);
                    if($result)
                    {
                        $i++;
                    }
                }
            }
        }
        echo '共成功操作了'.$i.'条记录';
    }
    public function basic()
    {
        $this->display();
    }
    public function pwd()
    {
        $this->display();
    }
	public function dobasic()
	{
	    $data = $_POST;
	    $mod = M('Photoer');
	    $where = array();
	    $result = $mod->where('id <> '.$this->login['id'].' and email=\''.$data['email'].'\'')->find();
	    if($result)
	    {
	        $this->error('邮箱已经存在');
	    }
	    $mod->where('id='.$this->login['id'])->data($data)->save();
	    $data = $mod->getByid($this->login['id']);
	    $this->setLogin($data);
	    $this->assign('jumpUrl',U("Index/basic"));
	    $this->success('操作成功');
	}
    public function dopwd()
    {
        $data = $_POST;
        $data['old_password'] = sysmd5($data['old_password']);
        $data['password'] = sysmd5($data['password']);
        $data['password2'] = sysmd5($data['password2']);
        if($data['old_password'] != $this->login['password'])
        {
            $this->error('旧密码输入不正确');
        }
        if($data['password'] != $data['password2'])
        {
            $this->error('两次密码输入不一样');
        }
	    $mod = M('Photoer');
	    $mod->data($data)->where('id='.$this->login['id'])->save();
	    $this->updateLogin($data);
	    $this->assign('jumpUrl',U("Index/basic"));
	    $this->success('操作成功');
    }
}
 	
?>