<?php
class UpAction extends UserAction
{
	function _initialize()
    {
        parent::_initialize();
    }
    public function index()
    {
        //$_SESSION[photoid]=4;
		$this->display();
    }
    public function one()
	{
		$user=$this->login['account'];
		//删除没有上传完成的图组
		$mod = M('GroupDetail');
		$modRemark = M('GroupRemark');
		$modDetail = M('ImagesDetail');
		$modDetailRemark = M('ImagesRemark');
		$where = array();
		$where['userid'] = array('eq',$this->login['id']);
		$where['state'] = '100';
		$list = $mod->where($where)->select();
		foreach ($list as $v)
		{
		    //循环删除图组说明
		    $where2 = array();
		    $where2['group_id']=array('eq',$v['id']);
		    $modRemark->where($where2)->delete();
		    //查找是否有图片要删除
		    $where2 = array();
		    $where2['old_group_id']=array('eq',$v['id']);
		    $list2 = $modDetail->where($where2)->select();
		    foreach ($list2 as $vv)
		    {
		        //循环删除图片说明
		        $where3 = array();
		        $where3['images_id'] = $vv['id'];
		        $modDetailRemark->where($where3)->delete();
		    }
		    $modDetail->where($where2)->delete();
		}
		$mod->where($where)->delete();
		
		//插入组信息
	//$data['title']=;
	    $data = array();
	    $data['title'] = $_POST['title'];
		$data['userid']=$this->login['id'];
		$data['account']=$this->login['account'];
		$data['up_time']=time();
		$data['photo_date']=time();
		$data['author']=$this->login['name'];
		$data['state']='100';
		$type = isset($_POST['type'])?intval($_POST['type']):1;
		$data['c_type']= trim($type);
		$mode = isset($_POST['mode'])?intval($_POST['mode']):0;
		if(0 == $mode)
		{
		    //手工/批量上传
    		$data['group_key']=$_POST['key'];
    		//$data['remark']=$_POST['remark'];
    		$data['photo_date']=strtotime($_POST['date']);
    		//将国家省份存入session中，方便添加图片时使用
    		$_SESSION['country'] = isset($_POST['country'])?trim($_POST['country']):'';
    		$_SESSION['province'] = isset($_POST['province'])?trim($_POST['province']):'';
    		$_SESSION['city'] = isset($_POST['city'])?trim($_POST['city']):'';
    		$_SESSION['photo_time'] = isset($_POST['photo_time'])?strtotime($_POST['photo_time']):time();
    		if('中国' != trim($_POST['country']))
    		{
    		    $_SESSION['country'] = isset($_POST['country1'])?trim($_POST['country1']):'';
        		$_SESSION['province'] = isset($_POST['province1'])?trim($_POST['province1']):'';
        		$_SESSION['city'] = isset($_POST['city1'])?trim($_POST['city1']):'';
    		}
		}
		else 
		{
		    //自动上传则清用上次用的国家省份
		    unset($_SESSION['country'],$_SESSION['province'],$_SESSION['city']);
		}
		
		$mod->add($data);
		$groupId=$mod->getLastInsID() ;
		if(0 == $mode)
		{
    		//手工/批量上传 插入组描述
    		$remark = $_POST['remark'];
    		if(!$this->addGroupRemark($groupId,$remark))
    		{
    		    $this->error('增加图组描述失败');
    		}
		}
		$this->assign("groupid",$groupId);
		$this->assign("mode",$mode);
		$this->assign("type",$type);
		$this->display();
	}
	//只负责文件的上传
	public function doup()
	{	
		$userid=$this->login['id'];
	    
		$groupId=isset($_GET['groupid'])?intval($_GET['groupid']):0;
		$type = isset($_GET['type'])?intval($_GET['type']):1;
		$mode = isset($_GET['mode'])?intval($_GET['mode']):0;
		
		$photoer=M('Photoer');
		
	    //先查询组信息
		$tGroup=M('GroupDetail');
		$tGroupinfo = $tGroup->getById($groupId);
		if(!$tGroupinfo)
		{
		    $this->ajaxReturn('','获取组信息失败',1);
		}
		
		import("@.ORG.UploadFile");
		import("@.ORG.Image");
		$upload = new UploadFile(); // 实例化上传类
		$upload->maxSize  = 31457280 ; // 设置附件上传大小
		$upload->allowExts  = array('jpg', 'jpeg', 'psd'); // 设置附件上传类型
		$source_path = C('IMAGES_SOURCE_PATH');
		//$this->ajaxReturn('',$source_path,1);
		$curTime = time();//记录当前的时间
		$curDate = date('Ymd',$curTime);
		$upload->savePath =  $source_path.$userid.'/'.$curDate.'/'; // 设置附件上传目录
		//var_dump($upload->savePath);
		//exit;
		@mkdir($source_path.$userid);
		@mkdir($source_path.$userid.'/'.$curDate);
		$upload->$uploadReplace = true;
		 //设置需要生成缩略图，仅对图像文件有效 
        $upload->thumb = true; 
		
		 //设置需要生成缩略图的文件后缀 
        $thumb_path = C('IMAGES_THUMB_PATH');//读取缩略图保存的路径
        $thumb_size = C('THUMB_SIZE');
        $upload->thumbPrefix = $thumb_size[0]['prefix'].','.$thumb_size[1]['prefix'];  //生产2张缩略图
         
        $upload->thumbPath = $thumb_path.$userid.'/'.$curDate.'/';  //缩略图 保存路径
        @mkdir($thumb_path);
        @mkdir($thumb_path.$userid);
		@mkdir($thumb_path.$userid.'/'.$curDate);
        //设置缩略图最大宽度 
        $upload->thumbMaxWidth = $thumb_size[0]['width'].','.$thumb_size[1]['width']; 
        //设置缩略图最大高度 
        $upload->thumbMaxHeight = $thumb_size[0]['height'].','.$thumb_size[1]['height']; 
	
		if(!$upload->uploadmany($userid,$curTime)) { // 上传错误 提示错误信息
		    $this->ajaxReturn('',$upload->getErrorMsg(),1);
		}else
		{ // 上传成功 获取上传文件信息
			$info =  $upload->getUploadFileInfo();
			//var_dump($info);
			if(1 == $mode)
			{
			    //如果是自动 上传，则自动 添加组的描述信息
			    if(strlen($tGroupinfo['title'])<1)
			    {
			        //找一条$gtitle,$gremark都不为空的记录
    		        $gtitle = isset($info[0]['exif']['iptcBigTitle'][1])?$info[0]['exif']['iptcBigTitle'][1]:'';
    			    $gremark = isset($info[0]['exif']['iptcDesc'][1])?$info[0]['exif']['iptcDesc'][1]:'';
    			    $gcity = isset($info[0]['exif']['iptcCity'][1])?$info[0]['exif']['iptcCity'][1]:'';
    			    $province = isset($info[0]['exif']['iptcProvince'][1])?$info[0]['exif']['iptcProvince'][1]:'';
    			    $group_key = isset($info[0]['exif']['iptcKeyword'][1])?$info[0]['exif']['iptcKeyword'][1]:'';
    			    $photo_time = isset($info[$i]['exif']['DateTimeOriginal'][1])?strtotime($info[$i]['exif']['DateTimeOriginal'][1]):0;
    			    if(strlen($gtitle)>1)
    			    {
    			        $dataG = array();
        			    $dataG['title'] = str_replace('\'', '', $gtitle);
        			    if($photo_time && $photo_time>0)
        			    {
        			        $dataG['photo_date'] = $photo_time;
        			    }
        			    $dataG['province'] = $province;
        			    $remark = str_replace('\'', '', $gremark);
        			    $dataG['city'] = str_replace('\'', '', $gcity);
        			    $dataG['group_key'] = str_replace('\'', '', $group_key);
        			    $tGroup->where('id='.$groupId)->save($dataG);
        			    $this->addGroupRemark($groupId, $remark);
    			    }
			    }
			    //$this->ajaxReturn('',$tGroup->getLastSql(),1);
			    //var_dump($tGroup->getLastSql());
			    //exit;
			}
			else 
			{
			    //手工/批量上传 读取 session里面的省份城市
			    $data['country'] = isset($_SESSION['country'])?$_SESSION['country']:'';
			    $data['province'] = isset($_SESSION['province'])?$_SESSION['province']:'';
			    $data['city'] = isset($_SESSION['city'])?$_SESSION['city']:'';
			    $data['photo_time'] = isset($_SESSION['photo_time'])?$_SESSION['photo_time']:time();
			}
			//文件上传成功后进行入库
			$upfiles=M('ImagesDetail');
			$data['old_group_id']=$groupId;
			$data['state']='1';
			$data['userid']=$userid;
			$data['up_time']=$curTime;
			$data['c_type'] = $type;
			$i=0;
			$data['url']=$info[$i]["savename"];
			$imgInfo=Image::getImageInfo($info[$i]['savepath'].$info[$i]["savename"]);
			
			$data['width']=$imgInfo['width'];
			$data['height']=$imgInfo['height'];
			$data['filesize']=ceil($imgInfo['size']/1024);
			
			if(1 == $mode)
			{
			    //自动 上传
			    $data['country'] = isset($info[$i]['exif']['iptcCountry'][1])?str_replace('\'', '', $info[$i]['exif']['iptcCountry'][1]):'';
			    $data['province'] = isset($info[$i]['exif']['iptcProvince'][1])?str_replace('\'', '', $info[$i]['exif']['iptcProvince'][1]):'';
			    $data['city'] = isset($info[$i]['exif']['iptcCity'][1])?str_replace('\'', '', $info[$i]['exif']['iptcCity'][1]):'';
			    //分类 
			    $cate = isset($info[$i]['exif']['iptcCate'][1])?str_replace('\'', '', $info[$i]['exif']['iptcCate'][1]):'';
			    
			    $remark = isset($info[$i]['exif']['iptcBigTitle'][1])?str_replace('\'', '',$info[$i]['exif']['iptcBigTitle'][1]):'';
			    if(strlen($remark)<2)
			    {
			        //如果描述信息 获取失败 则获取大标题的信息$_SESSION['photo_time']
			        $remark = isset($info[0]['exif']['iptcTitle'][1])?str_replace('\'', '', $info[0]['exif']['iptcTitle'][1]):'';
			    }
			    $data['photo_time'] = isset($info[$i]['exif']['DateTimeOriginal'][1])?strtotime($info[$i]['exif']['DateTimeOriginal'][1]):0;
			    if(!$data['photo_time'])
			    {
			        $data['photo_time'] = $curTime;
			    }
			    $key = isset($info[$i]['exif']['iptcKeyword'][1])?str_replace('\'', '', $info[$i]['exif']['iptcKeyword'][1]):'';
			    $data['images_key'] = $key;
			    $author = isset($info[$i]['exif']['iptcAuth'][1])?str_replace('\'', '', $info[$i]['exif']['iptcAuth'][1]):'';
			    if(strlen($author)<2)
			    {
			        //如果IPTC的作者获取失败，则获取exif的作者
			        $author = isset($info[$i]['exif']['Artist'][1])?str_replace('\'', '', $info[$i]['exif']['Artist'][1]):'';
			    }
				//var_dump($data,$info[$i]['exif']);
				//exit;
				$id=$upfiles->add($data);
				if(!$id)
				{
					$this->ajaxReturn('',$upfiles->getLastSql(),2);
				}
				else 
				{
				    $id = $upfiles->getLastInsID();
				    $title= '';
				    $this->addImagesRemark($id, $remark,0,$title,$key,$cate,$author);
				}
					
			}
		}
        $this->ajaxReturn('','11',0);
        exit;
	}
	
//下一步
	function two()
	{
		$user=$this->login['id'];
		$groupid=isset($_GET['groupid'])?intval($_GET['groupid']):0;
		$mod = M('ImagesDetail');
		$modg = M('GroupDetail');
		$modgR = M('GroupRemark');
		$sql = 'select * from __TABLE__ a left join '.C('DB_PREFIX').'images_remark b on a.id=b.images_id where a.old_group_id='.$groupid;
		$vlist = $mod->query($sql);
		$ginfo = $modg->getById($groupid);
		$ginfo['remark'] = $modgR->where('group_id='.$groupid)->getField('remark');
		//var_dump($vlist,$mod->getLastSql());
		//exit;
		$this->assign('vlist',$vlist);
		$this->assign('ginfo',$ginfo);
		$this->assign('groupid',$groupid);
		$this->display();
	}
	
	//保存图片说明
	public function save()
	{
		$ids = $_POST['ids'];
		$groupid=isset($_GET['groupid'])?intval($_GET['groupid']):0;
		$upImage=M('ImagesDetail');
		if(is_array($ids))
		{
		    foreach ($ids as $v)
		    {
		        $data = array();
		        $remark=isset($_POST['remark'.$v])?str_replace('\'', '', trim($_POST['remark'.$v])):'';
		        $keyword=isset($_POST['keyword'.$v])?str_replace('\'', '', trim($_POST['keyword'.$v])):'';
		        //$data['photo_time']=isset($_POST['photo_time'.$v])?strtotime(trim($_POST['photo_time'.$v])):time();
		        //$data['city']=isset($_POST['city'.$v])?str_replace('\'', '', trim($_POST['city'.$v])):'';
		        $data['point']=isset($_POST['point'.$v])?intval($_POST['point']):100;
		        $data['images_key']=$keyword;
		        //$data['point'] = 100;
		        unset($where);
		        $where['id']=$v;
		        $upImage->where($where)->save($data);
		        $this->addImagesRemark($v, $remark,0,'',$keyword);
		    }
		}
		$count = count($ids);
		$v = isset($_POST['main'])?intval($_POST['main']):$ids[0];
		$main_url = isset($_POST['img'.$v])?trim($_POST['img'.$v]):'';
		$main_url_date = isset($_POST['img_date'.$v])?intval($_POST['img_date'.$v]):0;
		$city = isset($_POST['city'.$v])?trim($_POST['city'.$v]):'';
		$gedit = isset($_POST['gedit'])?intval($_POST['gedit']):0;
		unset($gdata);
		//if(1 === $gedit && isset($_POST['gtitle']) && isset($_POST['gremark']))
		//{
		    $gdata['title']=str_replace('\'', '', $_POST['gtitle']);
		    $remark=str_replace('\'', '', $_POST['gremark']);
		//}
		//保存图片说明后 直接修改图组的状态
	
		$upGroup=M('GroupDetail');
		$gdata['state']='1';
		$gdata['image_count']=$count;
		$gdata['main_url']=$main_url;
		$gdata['main_url_date']=$main_url_date;
		if(strlen($city)>1)
		{
		    $gdata['city']=$city;
		}
		$upGroup->where(' id='.$groupid)->save($gdata);
		$this->addGroupRemark($groupid, $remark);
		
		//设置缓存所有已经上传的图片总数
		$sql = 'select sum(image_count) as tt from __TABLE__ where userid='.$this->login['id'];
		$result = $upGroup->query($sql);
		//var_dump($result);
		$data = array();
		$data['count'] = $result[0]['tt'];
		$this->updateLogin($data);
		$this->assign('jumpUrl',U('Up/index'));
		$this->success('操作成功');
	}
	
}
 	
?>