<?php
class ToPayAction extends UserAction
{
	function _initialize()
    {
        parent::_initialize();
    }
    public function index()
    {
        $this->display();
    }
    public function save()
    {
        $curValue = intval($this->login['pvalue']);
        $pvalue = isset($_POST['pvalue'])?intval($_POST['pvalue']):0;
        if($pvalue<=1 || $pvalue>$curValue)
        {
            $this->error('点数填写不正确');
        }
        if(intval($this->login['no_pvalue'])>0)
        {
            $this->error('当前您有申请结算的点数正在审核，不能重复申请');
        }
        $value = $curValue - $pvalue;
        $mod = M('Photoer');
        $data = array();
        $data['pvalue'] = $value;
        $data['no_pvalue'] = $pvalue;
        $result = $mod->where('id='.$this->login['id'])->save($data);
        if($result)
        {
            $this->updateLogin($data);
            $this->assign('jumpUrl',U('Index/index'));
            $this->success('申请成功，请耐心等待我们的申请');
        }
        else
        {
            $this->error('申请失败，请确认您的操作是否正确 ');
        }
    }
    public function cancel()
    {
        if($this->login['no_pvalue']<=0)
        {
            $this->error('操作失败');
        }
        $mod = M('Photoer');
        $pvalue = intval($this->login['pvalue']);
        $no_pvalue = intval($this->login['no_pvalue']);
        $pvalue = $pvalue + $no_pvalue;
        $data = array();
        $data['pvalue'] = $pvalue;
        $data['no_pvalue'] = 0;
        $result = $mod->where('id='.$this->login['id'])->save($data);
        if($result)
        {
            $this->updateLogin($data);
            $this->assign('jumpUrl',U('Index/index'));
            $this->success('取消申请成功');
        }
        else
        {
            $this->error('取消申请失败，请确认您的操作是否正确 ');
        }
    }
}
 	
?>