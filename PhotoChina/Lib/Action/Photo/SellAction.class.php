<?php
class SellAction extends UserAction
{
	function _initialize()
    {
        parent::_initialize();
    }
    public function index()
    {
        import ('@.ORG.Page');
        $mod = M('pay');
        $where = 'points>0 and userid='.$this->login['id'].' and type=4';
        $images_id = isset($_GET['images_id'])?intval($_GET['images_id']):0;
        $starttime = isset($_GET['starttime'])?strtotime($_GET['starttime']):0;
        $endtime = isset($_GET['endtime'])?strtotime($_GET['endtime']):0;
        if($images_id>0)
        {
            $where .= ' and pid='.$images_id;
        }
        if($starttime>0)
        {
            $where .= ' and createtime>='.$starttime;
            $this->assign('starttime',date('Y-m-d',$starttime));
        }
        if($endtime>0)
        {
            $end = $endtime + 60*60*24;
            $where .= ' and createtime<='.$end;
            $this->assign('endtime',date('Y-m-d',$endtime));
        }
        $count=$mod->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		
		$this->assign("page",$show);
		$list=$mod->order('createtime desc,use_points desc')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
		//$list = $mod->where($where)->order('buytime desc')->select();
		$this->assign('list',$list);
        $this->display();
    }
}
 	
?>