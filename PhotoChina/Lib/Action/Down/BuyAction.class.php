<?php
class BuyAction extends UserAction
{
	function _initialize()
    {
        parent::_initialize();
    }
    public function index()
    {
        import ('@.ORG.Page');
        $mod = M('BuyImages');
        $where = 'userid='.$this->login['id'];
        $type = isset($_GET['type'])?intval($_GET['type']):0;
        $starttime = isset($_GET['starttime'])?strtotime($_GET['starttime']):0;
        $endtime = isset($_GET['endtime'])?strtotime($_GET['endtime']):0;
        $up_userid = isset($_GET['up_userid'])?intval($_GET['up_userid']):0;
        $images_id = isset($_GET['images_id'])?intval($_GET['images_id']):0;
        $this->assign('type',$type);
       // var_dump($starttime,$endtime);
        if($type>0)
        {
            $where.=' and type='.$type;
        }
        if($starttime>0)
        {
            $where .= ' and buytime>='.$starttime;
            $this->assign('starttime',date('Y-m-d',$starttime));
        }
        if($endtime>0)
        {
            $end = $endtime + 60*60*24;
            $where .= ' and buytime<='.$end;
            $this->assign('endtime',date('Y-m-d',$endtime));
        }
        if($up_userid>0)
        {
            $where .= ' and up_userid='.$up_userid;
            $this->assign('up_userid',$up_userid);
        }
        if($images_id>0)
        {
            $where .= ' and images_id='.$images_id;
            $this->assign('images_id',$images_id);
        }
        $count=$mod->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		
		$this->assign("page",$show);
		$list=$mod->order('buytime desc')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
		//$list = $mod->where($where)->order('buytime desc')->select();
        $tmp = array();
		foreach ($list as $v)
		{
		    $v['is_buy'] = $this->chkBuyHistory($v['images_id']);
		    $tmp[] = $v;
		}
		$this->assign('list',$tmp);
        $this->display();
    }
}
 	
?>