<?php
class IndexAction extends UserAction
{
    private $dao;
	function _initialize()
    {
        parent::_initialize();
        $this->dao = M('Photoer');
    }
    public function index()
    {
        $mod = M('BuyImages');
        $count = $mod->where('userid='.$this->login['id'])->count();
        $this->assign('total_buy_num',$count);
        $this->display();
    }
    
    public function basic()
    {
        $this->display();
    }
    public function pwd()
    {
        $this->display();
    }
	public function dobasic()
	{
	    $data = $_POST;
	    $mod = $this->dao;
	    $where = array();
	    $result = $mod->where('id <> '.$this->login['id'].' and email=\''.$data['email'].'\'')->find();
	    if($result)
	    {
	        $this->error('邮箱已经存在');
	    }
	    $mod->where('id='.$this->login['id'])->data($data)->save();
	    $data = $mod->getByid($this->login['id']);
	    $this->setLogin($data);
	    $this->assign('jumpUrl',U("Index/basic"));
	    $this->success('操作成功');
	}
    public function dopwd()
    {
        $data = $_POST;
        $data['old_password'] = sysmd5($data['old_password']);
        $data['password'] = sysmd5($data['password']);
        $data['password2'] = sysmd5($data['password2']);
        if($data['old_password'] != $this->login['password'])
        {
            $this->error('旧密码输入不正确');
        }
        if($data['password'] != $data['password2'])
        {
            $this->error('两次密码输入不一样');
        }
	    $mod = $this->dao;
	    $mod->data($data)->where('id='.$this->login['id'])->save();
	    $this->updateLogin($data);
	    $this->assign('jumpUrl',U("Index/basic"));
	    $this->success('操作成功');
    }
}
 	
?>