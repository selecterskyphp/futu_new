<?php

class PayAction extends UserAction
{
	function _initialize()
    {
        parent::_initialize();
    }
    public function index()
    {
        import ('@.ORG.Page');
        $mod = M('Pay');
        $where = 'userid='.$this->login['id'];
        $type = isset($_GET['type'])?intval($_GET['type']):0;
        $starttime = isset($_GET['starttime'])?strtotime($_GET['starttime']):0;
        $endtime = isset($_GET['endtime'])?strtotime($_GET['endtime']):0;
        $this->assign('type',$type);
        if($type>0)
        {
            $where.=' and type='.$type;
        }
        if($starttime>0)
        {
            $where .= ' and createtime>='.$starttime;
            $this->assign('starttime',date('Y-m-d',$starttime));
        }
        if($endtime>0)
        {
            $end = $endtime + 60*60*24;
            $where .= ' and createtime<='.$end;
            $this->assign('endtime',date('Y-m-d',$endtime));
        }
        $count=$mod->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		
		$this->assign("page",$show);
		$list=$mod->order('createtime desc')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
		//$list = $mod->where($where)->order('buytime desc')->select();
		$this->assign('list',$list);
        $this->display();
    }
}
 	
?>