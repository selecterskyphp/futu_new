<?php
class CartAction extends UserAction
{
    protected $dao;
	function _initialize()
    {
        parent::_initialize();
        $this->dao = M('cart');
    }
    public function index()
    {
        //import ('@.ORG.Page');
        $where = array();
        $where['userid'] = $this->login['id'];
		$list = $this->dao->where($where)->order('createtime desc')->select();
		$tmp = array();
		foreach ($list as $v)
		{
		    $v['is_buy'] = $this->chkBuyHistory($v['pid']);
		    $tmp[] = $v;
		}
		
		$this->assign('list',$tmp);
        $this->display();
    }

	public function del()
    {	 
		$id = isset($_GET['pid'])?intval($_GET['pid']):0;
		$where = array();
		$where['pid'] = $id;
		$where['userid'] = $this->login['id'];
		$result = $this->dao->where($where)->delete();
		$this->assign('jumpUrl',U('Cart/index'));
		if(false !== $result)
		{
		    $count = $this->dao->where("userid=".$this->login['id'])->count();
	        $data = array('cart_num'=>$count);
	        $this->updateLogin($data);
			$this->success(L('删除购物车图片成功'));
		}
		else
		{
		    $this->error(L('delete_error'));
		}	
    }
    
    public function add()
    {
        $id = isset($_GET['id'])?intval($_GET['id']):0;
        $gid = isset($_GET['gid'])?intval($_GET['gid']):0;
        $where = 'id='.$id;
        $imageMod = M('ImagesDetail');
        $groupMod = M('GrouponDetail');
        $image = $imageMod->where($where)->find();
        if(!$image)
        {
            $this->error('图片不存在');
        }
        $where = array();
		$where['pid'] = $id;
		$where['userid'] = $this->login['id'];
		$result = $this->dao->where($where)->find();
		if($result)
		{
		    $this->error('该图片已经在购物车中存在');
		}
		else 
		{
		    $group = $groupMod->where('id='.$gid)->field('title,type_one,type_one_name')->find();
		    if(!$group)
		    {
		        $this->error('图片图不存在');
		    }
		    $data = array();
		    $data['pid'] = $id;
		    $data['group_id'] = $gid;
		    $data['url'] = $image['url'];
		    $data['up_time'] = $image['up_time'];
		    $data['up_userid'] = $image['userid'];
		    $data['title'] = $group['title'];
		    $data['type_one'] = $group['type_one'];
		    $data['type_one_name'] = $group['type_one_name'];
		    $data['userid'] = $this->login['id'];
		    $data['createtime'] = time();
		    $result = $this->dao->data($data)->add();
		    if($result)
		    {
		        $count = $this->dao->where("userid=".$this->login['id'])->count();
		        $data = array('cart_num'=>$count);
		        $this->updateLogin($data);
		        $this->success('加入购物车成功');
		    }
		    else 
		    {
		        //var_dump($this->dao->getlastsql());
		        //exit;
		        $this->error('加入购物车失败');
		    }
		}
		
    }
}
 	
?>