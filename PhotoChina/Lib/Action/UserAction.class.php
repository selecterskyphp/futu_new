<?php
class UserAction extends BaseAction
{
    public function _initialize() {
        parent::_initialize();
        if(!$this->islogin)
        {
            $this->error('您还没有登录或者登录超时，无法进行此操作');
        }
    }
    /**
     * 
     * 检查下载多个图片点数和最允许下载的数量允许
     * @param int/array $ids_
     * @param int $type 1为下载数组
     */
    private function chkBuyAll($ids_,$type=0)
    {
        $buytype = 0;//0为以最大下载图片数量下载 1为以点数下载 2免费下载
        if(1 == $this->login['subtype'] || 4 == $this->login['subtype'])
        {
            //包年用户、专线用户
            $buytype = 0;
        }
        else if(2 == $this->login['subtype'] || 3 == $this->login['subtype'])
        {
            //预付款用户、后结算用户
            $buytype = 1;
        }
        else if(5 == $this->login['subtype'])
        {
            //合作机构
            $buytype = 2;
        }
        else 
        {
            $this->errmsg = '当前用户没有下载的权限';
            return false;
        }
        $mod = M('ImagesDetail');
        $sum = (0 === $buy_type)?'count(*) as rr':'sum(value) as rr';
        if(1 === $type)
        {
            $gimgMod = M('GrouponImages');
            $imglist = $gimgMod->where('group_id='.$ids_)->select();
            if(!$imglist)
            {
                $this->errmsg = '组不存在或组下没有任何图片';
                return false;
            }
            $where = '';
            foreach($imglist as $v)
            {
                $where .= ('' === $s)?'id='.$v['images_id']:' or id='.$v['images_id'];
            }
        }
        else
        {
            if(is_array($ids_) && !empty($ids_))
            {
                $where = 'id=';
                $where += implode(' and id=', $ids_);
            }
            else 
            {
                $where = 'id='.$ids_;
            }
        }
        $sql = 'select '.$sum.' from __TABLE__ where '.$where;
        $result = $mod->query($sql);
        $num = $result['rr'];
        if(0 === $num)
        {
            $this->errmsg = '图片不存在';
            return false;
        }
        $allow_down_num = intval($this->login['allow_down_num']);
        $downed_num = intval($this->login['downed_num']);
        $use_num = $allow_down_num - $downed_num;
        
        $zheke = floatval($this->login['zheke']);//折扣
        $pvalue = intval($this->login['pvalue']);//可用点数
        
        if(0 === $buy_type)
        {
            //以图片张数购买
            if($use_num<$num)
            {
                //如果可用张数小于要下载的张数，则要检查点数
                if($pvalue<=0)
                {
                    //如果点为0，则直接退出
                    $this->errmsg = '用户点数余额为0';
                    return false;
                }
                $sql = 'select sum(value) as rr from __TABLE__ where '.$where.' limit '.$use_num.','.$num;
                $result = $mod->query($sql);
                if($num == $result['rr'])
                {
                    $num = $zheke * $num;
                    if($num>$pvalue)
                    {
                        $this->errmsg = '下载限额张数不足并且点数余额不足';
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            else
            {
                return true;
            }
        }
        else
        {
            //检查点数
            $num = $zheke * $num;
            if($num>$pvalue)
            {
                $this->errmsg = '点数余额不足';
                return false;
            }
            else
            {
                return true;
            }
        }
    }
    /**
     * 
     * 按图片最大允许下载数量购买
     * @param array $pdata 要购买的产品记录数组
     */
    private function buyByNum($pdata)
    {
        $umod = M('Photoer');
        $udata = $this->getUserInfo();
        $allow_down_num = intval($udata['allow_down_num']);
        $downed_num = intval($udata['downed_num']);
        if($downed_num >= $allow_down_num)
        {
            $this->errmsg = '已经超过最多下载图片数量';
            return false;
        }
        //在最大下载的范围内
        $downed_num++;
        //添加下载次数
        $where = array();
        $where['id'] = $this->login['id'];
        $data = array();
        $data['downed_num'] = $downed_num;
        
        $result = $umod->where($where)->save($data);
        if(!$result)
        {
            //添加下载次数失败
            $this->errmsg = '添加下载次数失败';
            return false;
        }
        else 
        {
            $data = array();
            $data['downed_num'] = $downed_num;
            $data['use_down_num'] = $this->login['allow_down_num']-$downed_num;
            $this->updateLogin($data);
            //$udata = $this->getUserInfo();
            //$this->setLogin($udata);//重新设置登录缓存
            return true;
        }
    }
    /**
     * 
     * 使用点数购买
     * @param array $pdata 要购买的产品记录数组 自动区别后结算用户
     */
    private function buyByPoint($pdata)
    {
        $point = $pdata['value'];
        $udata = $this->getUserInfo();
        $zheke = floatval($udata['zheke']);
        
        $pvalue = intval($udata['pvalue']);//可用点数
        $need_pay = intval($udata['need_pay']);//累计点数  只针对后结算用户
    
        $pay = intval($point * $zheke);
        if(($pvalue < $pay) && $this->login['type'] != 3)
        {
            //点数不够并且会员不是后结算用户
            $this->errmsg = '您的点数不足以购买此图片，请充值';
            return false;
        }
        else 
        {
            $where = array();
            $where['id'] = $this->login['id'];
            $data = array();
            if(3 == $this->login['subtype'])
            {
                //后结算用户 增加累计点数
                $value = $need_pay + $pay;
                $data['need_pay'] = $value;
            }
            else
            {
                $value = $pvalue - $pay;
                $data['pvalue'] = $value;
            }
            
            //操作点数
            $umod = M('Photoer');
            $result = $umod->where($where)->save($data);
            if(!$result && $pay>0)
            {
                $this->errmsg = '操作点数失败';
                return false;
            }
            else 
            {
                $udata = $this->getUserInfo();
                $this->setLogin($udata);//重新设置登录缓存
                return true;
            }
        }
    }
    /**
     * 
     * 检查是否是24小时内下载的图片 60*60*24=3600*24=86400
     * @param int $pid 下载的图片ID
     */
    public function chkBuyHistory($pid)
    {
        $mod = M('BuyImages');
        //type 为3时为免费购买
        $where = 'images_id = '.$pid.' and '.time().'-buytime<86400 and userid='.$this->login['id'].' and type<>3';
        $result = $mod->where($where)->find();
        if($result)
        {
            return true;
        }
        else 
        {
            return false;
        }
    }
    
    //购买图片
    public function buy($pdata,$gid)
    {
        $umod = M('Photoer');
        $where = array();
        $where['id'] = $pdata['userid'];
        $udata = $umod->where($where)->find();
        if(!$udata)
        {
            $this->errmsg = '摄影师不存在';
            return false;
        }
        if($pdata['userid'] == $this->login['id'])
        {
            return true;//摄影师下载自己的图片
        }
        $type = 1;//1以点数支付  2以图片张数支付 3 免费下载
        if($this->chkBuyHistory($pdata['id']))
        {
            //如果是24小时内下载的图片 可以直接下载
            //$pdata['remark'] = '不记点';
            //$type = 3;
            //if(false === $this->add_buy($pdata,$udata['zheke'],$type))
            //{
            //    //加入购买记录失败
            //    $this->errmsg = '24小时内下载加入购买记录失败';
            //    return false;
            //}
            return true;
        }
        //var_dump($this->login['subtype']);
        //exit;
        if(1 == $this->login['subtype'] || 4 == $this->login['subtype'])
        {
            //包年用户、专线用户
            $result = $this->buyByNum($pdata);
            $type = 2;
            
            if(false === $result)
            {
                //不在有效的最大下载范围内 则以点数购买
                $result = $this->buyByPoint($pdata);
                $type = 1;
                if(false === $result)
                {
                    return false;
                }
                //使用点数购买成功，需要增加消费记录
                if(false === $this->add_pay($pdata,$gid))
                {
                    $this->errmsg = '加入点数消费记录失败';
                    return false;
                }
            }
            $type=3;//专线用户不加入摄影师消费点数，为免费取图 2012-07-26
        }
        else if(2 == $this->login['subtype'] || 3 == $this->login['subtype'])
        {
            //预付款用户、后结算用户
            $type = 1;
            if(false === $this->buyByPoint($pdata))
            {
                return false;
            }
            //使用点数购买成功，需要增加消费记录
            if(false === $this->add_pay($pdata,$gid))
            {
                $this->errmsg = '加入点数消费记录失败';
                return false;
            }
        }
        else if(5 == $this->login['subtype'])
        {
            //合作机构
            $pdata['remark'] = '合作机构下载';
            $type = 3;
            //return true;
        }
        else
        {
            $this->errmsg = '您没有权限下载图片';
            return false;
        }
    
        if(false === $this->add_buy($pdata,$udata['zheke'],$gid,$type))
        {
            //加入购买记录失败
            $this->errmsg = '加入购买记录失败';
            return false;
        }
        return $this->add_sell($pdata,$gid,$type);
    }
    
    /**
     * 
     * 插入消费记录
     * @param array $pdata 产品信息数组
     */
    private function add_pay($pdata,$gid)
    {
        $mod = M("pay");
        $data = array();
        $data['type'] = '2';
        $data['userid'] = $this->login['id'];
        $data['account'] = $this->login['account'];
        $data['name'] = $this->login['name'];
        $data['points'] = intval($pdata['value']*$this->login['zheke']);
        $data['use_points'] = $this->login['pvalue'];
        $data['createtime'] = time();
        $data['pid'] = $pdata['id'];
        $data['group_id'] = $gid;
        $data['url'] = $pdata['url'];
        $data['up_time'] = $pdata['up_time'];
        $result = $mod->data($data)->add();
        //var_dump($mod->getlastsql());
        //exit;
        return ($result)?true:false;
    }
    
     /**
     * 
     * 插入销售记录
     * @param array $pdata 产品
     * @param int $type 1为点数支付 2张数支付 3免费下载
     */
    private function add_sell($pdata,$gid,$type)
    {
        $mod = M("pay");
        $umod = M("Photoer");
        $where = array();
        $where['id'] = $pdata['userid'];
        $udata = $umod->where($where)->find();
        if(!$gid && 0 !== $gid)
        {
            $this->errmsg = '图组不存在';
            return false;
        }
        if(!$udata)
        {
            $this->errmsg = '摄影师不存在';
            return false;
        }
        $points = ($udata['pvalue'])?intval($udata['pvalue']):0;//原来的点数
        $cur_zheke = $this->login['zheke'];//当前登录用户购买的折扣
        $pvalue = intval($pdata['value'] * $cur_zheke);
        $zheke = ($udata['zheke'])?floatval($udata['zheke']):0.5;
        //按分成增加摄影师的点数
        $get_points = intval($pvalue * $zheke);
        //
        if($type === 1 || $type === 2)
        {
            $points += $get_points;
            $data = array();
            $data['pvalue'] = $points;
            $result = $umod->where('id='.$udata['id'])->save($data);
            if(!$result && $get_points>0)
            {
                $this->errmsg = '增加摄影师点数失败';
                return false;
            }
        }
        else
        {
            //免费下载 
            $get_points = 0;
        }
        $data = array();
        $data['type'] = '4';
        $data['userid'] = $pdata['userid'];
        $data['account'] = $udata['account'];
        $data['name'] = $udata['name'];
        $data['points'] = $get_points;
        $data['use_points'] = $points;
        $data['createtime'] = time();
        $data['pid'] = $pdata['id'];
        $data['group_id'] = $gid;
        $data['url'] =$pdata['url'];
        $data['up_time'] =$pdata['up_time'];
        $result = $mod->data($data)->add();
        if(!$result)
        {
            $this->errmsg = '增加摄影师点数明细记录失败';
            return false;
        }
        
        return true;
    }
    
    /**
     * 
     * 加入购买产品历史记录
     * @param array $pdata 购买的产品数据
     * @param float $zheke 摄影师的分成折扣
     * @param int $type 1为点数支付 2张数支付 3免费下载
     */
    private function add_buy($pdata,$zheke,$gid,$type=1)
    {
        $mod = M('BuyImages');
        $data = array();
        $data['userid'] = $this->login['id'];
        $data['account'] = $this->login['account'];
        $data['url'] = $pdata['url'];
        $data['up_time'] = $pdata['up_time'];
        $data['buytime'] = time();
        $data['type'] = trim($type);
        $data['buy_values'] = (1 === $type)?($pdata['value']*$this->login['zheke']):0;
        $data['sell_values'] = (3 !== $type)?intval($data['buy_values']*$zheke):0;
        $data['images_id'] = $pdata['id'];
        $data['group_id'] = $gid;
        $data['up_userid'] = $pdata['userid'];
        $data['ip'] = get_client_ip(); 
        $data['remark'] = isset($pdata['remark'])?trim($pdata['remark']):'';//购买说明
        $result = $mod->data($data)->add();
        return ($result)?true:false;
    }
    /**
     * 
     * 生成图片的描述文本文件
     * @param array $pdata 
     * @param int $gid 
     * @return 生成后的文本文件路径
     */
    private function writePicTxt($pdata,$gid)
    {
        //获取图片的描述
        $mod = M('ImagesRemark');
        $where = array();
        $where['images_id']=array('eq',$pdata['id']);
        if(!$gid)
        {
            $this->errmsg = '写入文件文件时，参数错误';
            return false;
        }
        if($this->login['id'] == $pdata['userid'])
        {
            //摄影师自行下载
            $remark = $mod->where($where)->getField('old_remark');
            if(null === $remark)
            {
                $this->errmsg = '图片描述获取失败';
                return false;
            }
            $gmod = M('GroupDetail');
            $gremarkMod = M('GroupRemark');
        }
        else
        {
            $remark = $mod->where($where)->getField('remark');
            if(null === $remark)
            {
                $this->errmsg = '图片描述获取失败';
                return false;
            }
            $gmod = M('GrouponDetail');
            $gremarkMod = M('GrouponRemark');
        }
        //获取图组的标题
        $where = array();
        $where['id'] = $gid;
        $gtitle = $gmod->where($where)->getField('title');
        if(null === $gtitle)
        {
            $this->errmsg = '图组标题获取失败';
            return false;
        }
        //获取组组的描述
        $where = array();
        $where['group_id'] = $gid;
        $gremark = $gremarkMod->where($where)->getField('remark');
        if(null === $gremark)
        {
            $this->errmsg = '图组描述获取失败';
            return false;
        }
        $tmppath = basename($pdata['url']);
        $txtfile = TEMP_PATH .$tmppath . '.txt';
        $content = '';
        //$content .= '下载时间:'.date('Y-m-d H:i:s',time())."\r\n";
        $content .= '图片编号:'.$pdata['id']."\r\n";
        $content .= '图片大小:'.getFileSize($pdata['filesize'])."\r\n";
        $content .= '图片尺寸:'.$pdata['width'].'px×'.$pdata['height']."px\r\n";
        $content .= '拍摄时间:'.date('Y-m-d H:i:s',$pdata['photo_time'])."\r\n";
        $content .= '拍摄城市:'.$pdata['city']."\r\n";
        $content .= '图片描述:' . $remark."\r\n";
        $content .= "\r\n";
        $content .= "\r\n";
        $content .= "================================\r\n";
        $content .= '图组标题:'.$gtitle."\r\n";
        $content .= '图组描述:'.$gremark."\r\n";
        $result = file_put_contents($txtfile, iconv('utf-8', 'gb2312', $content));
        if(!$result)
        {
            $this->errmsg = '写入文本文件失败';
            return false;
        }
        return $txtfile;
    }
    
    public function downText()
    {
        $id = isset($_REQUEST['id'])?intval($_REQUEST['id']):0;
        $gid = isset($_REQUEST['gid'])?intval($_REQUEST['gid']):0;
        if(!$id || !$gid)
        {
            $this->error('参数不正确');
        }
        $mod = M('ImagesDetail');
        $pdata = $mod->getById($id);
        if(!$pdata)
        {
            $this->error('图片不存在');
        }
        $txt = $this->writePicTxt($pdata, $gid);
        if(!$txt)
        {
            $this->error($this->getLastError());
        }
        import('@.ORG.Http');
        HTTP::download($txt);
    }
    /**
     * 
     * 下载图片
     */
    public function down()
    {
        $ids = isset($_REQUEST['ids'])?$_REQUEST['ids']:'';
        $gid = isset($_REQUEST['gid'])?intval($_REQUEST['gid']):0;
        $type = isset($_REQUEST['type'])?intval($_REQUEST['type']):0;//$type为1时表示下载图组内的所有图片
        $buy_type = isset($_REQUEST['buy_type'])?intval($_REQUEST['buy_type']):1;//为1购买并下载 2只购买图片
        $pmod = M('ImagesDetail');
        if($this->login['nodown'] == 1)
        {
            if(!is_numeric($ids) || $type == 1 || !$this->login['allowdown_type'])
            {
                $this->error('您没有权限进行此操作，如有疑问请咨询我们 的客服人员');
            }
            $type_one = $pmod->where('id='.$ids)->getField('type_one');
            if(!$type_one)
            {
                $this->error('图片不存在');
            }
            $tmp = explode(',', $this->login['allowdown_type']);
            if(!in_array($type_one, $tmp))
            {
                $this->error('您没有权限进行此操作，如有疑问请咨询我们 的客服人员');
            }
        }
        //if(false === $this->chkBuyAll($ids_,$type))
        //{
        //    $this->error($this->getLastError());
        //}
        $picArray = array();
        $num = 0;
        $sucnum = 0;
        $sucids = '';
        if(is_array($ids))
        {
            //
            $num = count($ids);
            foreach($ids as $v)
            {
                $tmpgid = (0 === $gid)?$_REQUEST['gid'.$v]:$gid;
                if(!isset($tmpgid)||!$tmpgid)
                {
                    $this->error('参数错误');
                }
                $pdata = $pmod->getById($v);//获取产品信息
                if(!$pdata)
                {
                    //图片在数据库不存在
                    //$this->error('参数错误');
                    continue;
                }
                $result = $this->buy($pdata,$tmpgid);//如果此购买成功
                if(true === $result)
                {
                    //获取源图路径
                    if(1 === $buy_type)
                    {
                        $pic = getPicUrl($pdata['url'],$pdata['userid'], $pdata['up_time'],100,$pdata['bigurl']);
                        if(is_readable($pic))
                        {
                            $picArray[] = $pic;
                            $txt = $this->writePicTxt($pdata,$tmpgid);
                            if(false !== $txt)
                            {
                                if(is_readable($txt))
                                {
                                    $picArray[] = $txt;
                                    $sucnum++;
                                    $sucids .= ',' . $pdata['id'];
                                }
                            }
                        }
                    }
                    else
                    {
                        $sucnum++;
                        $sucids .= ',' . $pdata['id'];
                    }
                }
            }
        }
        else 
        {
            if(1 === $type)
            {
                //如果是图组
                $where = array();
                $where['group_id'] = $ids;
                $list = $pmod->where($where)->select();
                $num = count($list);
                foreach ($list as $v)
                {
                    $pdata = $v;
                    $result = $this->buy($pdata,$ids);//如果此购买成功
                    if(true === $result)
                    {
                        if(1 === $buy_type)
                        {
                            //获取源图路径
                            $pic = getPicUrl($pdata['url'],$pdata['userid'], $pdata['up_time'],100,$pdata['bigurl']);
                            if(is_readable($pic))
                            {
                                $picArray[] = $pic;
                                $txt = $this->writePicTxt($pdata,$ids);
                                if(false !== $txt)
                                {
                                    if(is_readable($txt))
                                    {
                                        $picArray[] = $txt;
                                        $sucnum++;
                                        $sucids .= ',' . $pdata['id'];
                                    }
                                }
                            }
                        }
                        else 
                        {
                            $sucnum++;
                            $sucids .= ',' . $pdata['id'];
                        }
                    }
                }
            }
            else 
            {
                $num = 1;
                if(!$gid)
                {
                    $this->error('图组不存在');
                }
                $pdata = $pmod->getById($ids);//获取图片信息
                if(!$pdata)
                {
                    $this->error('图片不存在');
                }
                $result = $this->buy($pdata,$gid);
                if(!$result)
                {
                    $this->error($this->getLastError());
                }
                if(1 == $buy_type)
                {
                    $pic = getPicUrl($pdata['url'],$pdata['userid'], $pdata['up_time'],100,$pdata['bigurl']);
                    //var_dump($pic);
                    //exit;
                    if(is_readable($pic))
                    {
                        $picArray[] = $pic;
                        $txt = $this->writePicTxt($pdata,$gid);
                        if(false !== $txt)
                        {
                            $picArray[] = $txt;
                            $sucnum++;
                            $sucids .= ',' . $pdata['id'];
                        }
                        else
                        {
                            $this->error('写入备注文件失败');
                        }
                    }
                    else
                    {
                        $this->error('源图文件不存在');
                    }
                }
                else 
                {
                    $sucnum++;
                    $sucids .= ',' . $pdata['id'];
                }
            }
        }
        if(0 == $sucnum)
        {
            $this->error('操作失败 '.$this->getLastError());
        }
        $sucids =trim($sucids,',');
        $mod = M('cart');
        $mod->where('pid in('.$sucids.')')->delete();//删除购物车的记录
        //var_dump($pdata,$picArray);
        //exit;
        if(1 == $buy_type)
        {
            import('@.ORG.Phpzip');
            $zip = new Phpzip();
            $zip->ZipAndDownload($picArray);//下载
        }
        else 
        {
            if(isset($_REQUEST['refer_url']) && $_REQUEST['refer_url'])
            {
                $this->assign('jumpUrl',$_REQUEST['refer_url']);
            }
            $str = ($num == $sucnum)?'':'，由于您的余额不足或其它情况有部分图片没有购买成功';
            $this->success('操作成功'.$str);
        }
    }
    
    /**
     * 
     * 添加组描述 
     * @param int $groupid 组ID
     * @param string $remark 组描述
     * @param int $type 0表示添加摄影师组描述(默认) 1表示添加后台审核发布的组描述
     * @param int $newgroup_id 新组ID $type为1时必须填写
     * @return 操作结果
     */
    public function addGroupRemark($groupid,$remark)
    {
        $mod = M('GroupRemark');
        $where = array();
        $data = array();
        //查询条件
        $userid = $this->login['id'];
        //$where['userid'] = $userid;
        $where['group_id'] = $groupid;//以原组ID为准 新的组ID和原组ID在一条记录
        $data['remark'] = $remark;
        $result = $mod->where($where)->find();
        if($result)
        {
            //存在记录则更新
            $result = $mod->where($where)->save($data);
        }
        else 
        {
            //不存在则新建
            $data['userid'] = $userid;
            $data['group_id'] = $groupid;
            $result = $mod->data($data)->add();
        }
       return $result;
    }
    
    /**
     * 
     * 插入图片描述
     * @param int $imagesid 图片ID
     * @param string $remark 图片描述
     * @param int $type 操作类型 0表示添加摄影师图片描述 1表示添加后台审核发布的图片描述(默认)
     * @param unknown_type $old_title 摄影师图片标题
     * @param unknown_type $old_keyword 摄影师图片关键字
     * @param unknown_type $old_cate 摄影师图片分类
     * @param unknown_type $old_author 摄影师图片作者
     */
    public function addImagesRemark($imagesid,$remark,$type=1,$old_title='',$old_keyword = '',$old_cate = '',$old_author='')
    {
        $mod = M('ImagesRemark');
        $where = array();
        $data = array();
        $userid = $this->login['id'];
        $where['userid'] = $userid;
        $where['images_id'] = $imagesid;
        if(0 === $type)
        {
            //插入影影师图片的描述
            if('' !== $old_title)
            {
                $data['old_title'] = $old_title;
            }
            if('' !== $old_keyword)
            {
                $data['old_keyword'] = $old_keyword;
            }
            if('' !== $remark)
            {
                $data['old_remark'] = $remark;
                $data['remark'] = $remark;
            }
            if('' !== $old_author)
            {
                $data['old_author'] = $old_author;
            }
            if('' !== $old_cate)
            {
                $data['old_cate'] = $old_cate;
            }
        }
        else
        {
            if('' !== $remark)
            {
                $data['remark'] = $remark;
            }
        }
        $result = $mod->where($where)->find();
        if($result)
        {
            $result = $mod->where($where)->save($data);
        }
        else 
        {
            $data['userid'] = $userid;
            $data['images_id'] = $imagesid;
            $result = $mod->data($data)->add();
        }
        return $result;
    }
    
    public function getUserInfo($userid = 0)
    {
        $mod = M('Photoer');
        $userid = (0 === $userid)?$this->login['id']:$userid;
        return $mod->getById($userid);
    }
    
}
?>