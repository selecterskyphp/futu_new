<?php
class HomeAction extends BaseAction
{
    public function _initialize() {
        parent::_initialize();
        $mod = M('column');
		$column = $mod->where('parentid=0 and state=1')->order('sort asc,groupid desc')->select();
		$this->assign('column',$column);
    }
    /**
    +----------------------------------------------------------
    * 默认操作
    +----------------------------------------------------------
    */
    public function index()
    {
        $this->display();
    }

    /**
    +----------------------------------------------------------
    * 探针模式
    +----------------------------------------------------------
    */
    public function checkEnv()
    {
        //load('pointer',THINK_PATH.'/Tpl/Autoindex');//载入探针函数
        //$env_table = check_env();//根据当前函数获取当前环境
        //echo $env_table;
    }

}
?>