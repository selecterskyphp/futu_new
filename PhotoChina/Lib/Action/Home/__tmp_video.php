<?php return array (
  1343742710 => 
  array (
    'file' => 'I108590910814831343732437000I.xml',
    'title' => 'Tom Daley \'not affected\' by tweets',
    'desc1' => 'A 17-year-old was arrested today after malicious tweets were sent to Olympic diver Tom Daley. Dorset Police said the teenager was held at a guesthouse in Weymouth hours after 18-year-old Daley received the messages on social networking site Twitter.',
    'desc2' => 'Team GB Chef de Mission Andy Hunt says diver Tom Daley hasn\'t been affected by malicious tweets that were sent to him',
    'pic' => 'I108590910814831343732437000I-39680-1.jpg',
    'video' => 'I108590910814831343732437000I.mp4',
  ),
  1343741428 => 
  array (
    'file' => 'I0021343692048594I.xml',
    'title' => 'GB MEN\'S GYMNASTICS TEAM - GVs',
    'desc1' => 'Great Britain\'s bronze-winning gymnastics team were last night left to reflect on securing the country\'s first Olympic men\'s team medal in a century.Louis Smith, Max Whitlock, Daniel Purvis, Sam Oldham and Kristian Thomas earned their place in the history books after a breathtaking performance at the North Greenwich arena yesterday.',
    'desc2' => 'GVs of the Great Britain men\'s gymnastics team after they won bronze at the Olympics - Louis Smith, Sam Oldham, Kristian Thomas, Max Whitlock and Daniel Purvis',
    'pic' => 'I0021343692048594I-0-1.jpg',
    'video' => 'I0021343692048594I.mp4',
  ),
  1343727926 => 
  array (
    'file' => 'I108570110812751343715573000I.xml',
    'title' => 'GB men\'s gymnastics team make history',
    'desc1' => 'Louis Smith, Max Whitlock, Daniel Purvis, Sam Oldham and Kristian Thomas earned their place in the history books after a breathtaking performance at the North Greenwich arena yesterday.',
    'desc2' => 'Great Britain\'s bronze-winning gymnastics team were last night left to reflect on securing the country\'s first Olympic men\'s team medal in a century',
    'pic' => 'I108570110812751343715573000I-29240-1.jpg',
    'video' => 'I108570110812751343715573000I.mp4',
  ),
  1343725744 => 
  array (
    'file' => 'I0011343691424519I.xml',
    'title' => 'GYMNASTS DELIGHTED WITH BRONZE',
    'desc1' => 'Great Britain\'s bronze-winning gymnastics team were last night left to reflect on securing the country\'s first Olympic men\'s team medal in a century.Louis Smith, Max Whitlock, Daniel Purvis, Sam Oldham and Kristian Thomas earned their place in the history books after a breathtaking performance at the North Greenwich arena yesterday.',
    'desc2' => 'Interview with Great Britain\'s bronze medalist-winning men\'s gymnastics team - Louis Smith, Sam Oldham, Kristian Thomas, Max Whitlock, Daniel Purvis',
    'pic' => 'I0011343691424519I-0-1.jpg',
    'video' => 'I0011343691424519I.mp4',
  ),
  1343707220 => 
  array (
    'file' => 'I108554910811231343674029000I.xml',
    'title' => 'Kate glitters at arts reception',
    'desc1' => NULL,
    'desc2' => 'The Duchess of Cambridge dazzled as she was greeted by the Prime Minister at a star-studded reception for the arts tonight. Kate was heralded by a brass trio as she walked down the red carpet at the Royal Academy of Arts wearing a dove grey dress, matching heels and glittering hooped earrings before being met by David Cameron. The Duchess, who had her hair tied back, arrived at the arts institution in central London for a reception celebrating the best of British music, film, art and entertainment after spending part of the day watching Zara Phillips compete in the Olympics.',
    'pic' => NULL,
    'video' => 'I108554910811231343674029000I-0-1.jpg',
  ),
  1343699312 => 
  array (
    'file' => 'I108520910807831343654594000I.xml',
    'title' => 'RICHARDS-ROSS: 400M WILL BE CLOSE',
    'desc1' => NULL,
    'desc2' => 'Sanya Richards-Ross says the women\'s 400m will be a hard-fought contest, saying there is great competition across the field.The American listed a handful of athletes, including Britain\'s Christine Ohuruogu, that could challenge for gold.',
    'pic' => NULL,
    'video' => 'I108520910807831343654594000I-126960-1.jpg',
  ),
  1343698336 => 
  array (
    'file' => 'I108497810805521343638195000I.xml',
    'title' => 'TARGETT: RACE DIDN\'T GO TO PLAN',
    'desc1' => NULL,
    'desc2' => 'Australian swimmer Matt Targett says he failed to get any sleep after he missed out on a medal in the Mens 4x100m relay.Targett admits the race didn\'t go to plan for the team, saying he expected the team to do something special.',
    'pic' => NULL,
    'video' => 'I108497810805521343638195000I-0-1.jpg',
  ),
  1343695327 => 
  array (
    'file' => 'I108507810806521343645827000I.xml',
    'title' => 'OLYMPICS AFFECTS LONDON TRANSPORT',
    'desc1' => NULL,
    'desc2' => 'The first working day of the Olympics has seen communters and Olympic-goers as the transport system faced it\'s biggest challenge yet.London Bridge station is expected to be one of the busiest in the city during the Olympic games and passengers have been advised to avoid using the station.',
    'pic' => NULL,
    'video' => 'I108507810806521343645827000I-0-1.jpg',
  ),
  1343694486 => 
  array (
    'file' => 'I108518010807541343652620000I.xml',
    'title' => 'CROWDS GATHER IN DALEY\'S HOMETOWN',
    'desc1' => NULL,
    'desc2' => 'A passionate home crowd has gathered in Plymouth to cheer on Tom Daley as the 18-year-old dives for Olympic gold.',
    'pic' => NULL,
    'video' => 'I108518010807541343652620000I-0-1.jpg',
  ),
  1343693454 => 
  array (
    'file' => 'I108519610807701343653492000I.xml',
    'title' => 'TENNANT READY FOR AUSTRALIAN BATTLE',
    'desc1' => NULL,
    'desc2' => 'British cyclist Andy Tennant says he is looking forward to the battle against the Australians in the team pursuit event in the velodrome.',
    'pic' => NULL,
    'video' => 'I108519610807701343653492000I-0-1.jpg',
  ),
  1343689816 => 
  array (
    'file' => 'I108530810808821343662515000I.xml',
    'title' => 'BRITS UPBEAT DESPITE DIVING DISAPPOINTMENT',
    'desc1' => NULL,
    'desc2' => 'British fans remained positive despite divers Tom Daley and Pete Waterfield narrowly missing out on a medal in the men\'s synchronised 10m platform diving final.Spectators in the Aquatics Centre commented on the amazing atmosphere in the stadium when the divers took to the board.',
    'pic' => NULL,
    'video' => 'I108530810808821343662515000I-0-1.jpg',
  ),
  1343689703 => 
  array (
    'file' => 'I0011343673206956I.xml',
    'title' => 'Smith pleased with efforts',
    'desc1' => 'Smith should be a medal contender by the time of the 2016 Rio de Janeiro Olympics and it was considered unrealistic to hope for a podium finish this time.She set a new British clean and jerk record with a lift of 121kg at the second attempt, having earlier stalled on 90kg in the snatch.The combined lifts, a total of 211kg, exceeded her previous best by three kilograms.',
    'desc2' => 'Interview with British weightlifter Zoe Smith after setting a new British clean and jerk record at the London 2012 Games',
    'pic' => 'I0011343673206956I-0-1.jpg',
    'video' => 'I0011343673206956I.mp4',
  ),
  1343689171 => 
  array (
    'file' => 'Iage1343668277363I.xml',
    'title' => 'Family pride as dive pair miss out',
    'desc1' => 'Pete Waterfield revealed he apologised to team-mate Tom Daley after they missed out on a medal in the 10 metre platform synchro at the Aquatics Centre this afternoon.The British duo had been on course for the first home gold medal of these Olympics when they led their all-conquering Chinese rivals at the halfway stage.But a fluffed fourth dive, when Waterfield was most culpable after over-rotating his back three-and-a-half somersaults, saw them crash out of the top three.',
    'desc2' => 'Pete Waterfield and Tom Daley finished fourth in the 10 metre platform synchro after leading at the halfway mark',
    'pic' => 'Iage1343668277363I-13040-1.jpg',
    'video' => 'Iage1343668277363I.mp4',
  ),
  1343688425 => 
  array (
    'file' => 'Iage1343674863735I.xml',
    'title' => 'Pride for record-breaker Smith',
    'desc1' => 'Smith should be a medal contender by the time of the 2016 Rio de Janeiro Olympics and it was considered unrealistic to hope for a podium finish this time.She set a new British clean and jerk record with a lift of 121kg at the second attempt, having earlier stalled on 90kg in the snatch.The combined lifts, a total of 211kg, exceeded her previous best by three kilograms.',
    'desc2' => 'Weightlifter Zoe Smith gives her reaction to breaking the British clean and jerk record during competition at London 2012',
    'pic' => 'Iage1343674863735I-43000-1.jpg',
    'video' => 'Iage1343674863735I.mp4',
  ),
  1343684371 => 
  array (
    'file' => 'I108539510809691343666420000I.xml',
    'title' => 'OLYMPIC PARK - GVS',
    'desc1' => NULL,
    'desc2' => 'GVs of the Olympic Park in Stratford',
    'pic' => NULL,
    'video' => 'I108539510809691343666420000I-0-1.jpg',
  ),
  1343678702 => 
  array (
    'file' => 'I108539910809731343666528000I.xml',
    'title' => 'LONDON BRIDGE BUSY ON FIRST MONDAY OF GAMES',
    'desc1' => NULL,
    'desc2' => 'The first working day of the Olympics has seen communters and Olympic-goers as the transport system faced it\'s biggest challenge yet.London Bridge station was expected to be one of the busiest in the city during the Olympic games and passengers have been advised to avoid using the station.',
    'pic' => NULL,
    'video' => 'I108539910809731343666528000I-0-1.jpg',
  ),
  1343677958 => 
  array (
    'file' => 'Iage1343672968146I.xml',
    'title' => 'Richards-Ross eyes competition',
    'desc1' => 'US 200m and 400m runner Sanya Richards-Ross discusses her competition and the US team\'s decision to make a stand on Twitter against Rule 40 (Olympic rule about promoting sponsors).',
    'desc2' => 'USA 400m runner Sanya Richards-Ross on Christine Ohuruogu and the US team\'s Twitter campaign against the ban on non-Olympic sponsors',
    'pic' => 'Iage1343672968146I-58600-1.jpg',
    'video' => 'Iage1343672968146I.mp4',
  ),
  1343675163 => 
  array (
    'file' => 'I0031343651667622I.xml',
    'title' => 'TICKETS SOLD TO FILL EMPTY SEATS',
    'desc1' => 'Jackie Brock-Doyle from LOCOG and Mark Adams from IOC says they are doing everything to sort of empty seats & encouraging people to give up accredited ones so they can resell them. Also Jackie explains how and why the flame was moved from the stadium into Olympic Park last night',
    'desc2' => 'Jackie Brock-Doyle from LOCOG and Mark Adams from IOC says they are doing everything to sort of empty seats & encouraging people to give up accredited ones so they can resell them. Also Jackie explains how and why the flame was moved from the stadium into Olympic Park last night',
    'pic' => 'I0031343651667622I-0-1.jpg',
    'video' => 'I0031343651667622I.mp4',
  ),
  1343673627 => 
  array (
    'file' => 'I108532310808971343663241000I.xml',
    'title' => 'WATERFIELDS LOOK TO INDIVIDUAL DIVE FOR PETE',
    'desc1' => NULL,
    'desc2' => 'Pete Waterfield\'s family believes if he can reproduce his synchronised performance in the individual event then he has every chance of winning a medal.Waterfield and diving partner Tom Daley narrowly missed out on a medal in the men\'s synchronised 10m platform diving final, finishing fourth.',
    'pic' => NULL,
    'video' => 'I108532310808971343663241000I-0-1.jpg',
  ),
  1343670112 => 
  array (
    'file' => 'I0021343657988617I.xml',
    'title' => 'VARNISH: I\'M RELAXED ABOUT TEAM SPRINT',
    'desc1' => 'Jess Varnish says she is relaxed about her team sprint in the velodrome despite there being plenty of tough competition in the event.Varnish listed Australia, Germany, France and China as countries they should look out for as they go for Olympic gold.',
    'desc2' => 'Interview with women\'s team sprint member Jess Varnish on Armitstead\'s medal, Australia, the track, Pendleton and public expectations',
    'pic' => 'I0021343657988617I-0-1.jpg',
    'video' => 'I0021343657988617I.mp4',
  ),
  1343669115 => 
  array (
    'file' => 'I0031343666806804I.xml',
    'title' => 'Farmers form official milk campaign',
    'desc1' => 'Hundreds of UK dairy farmers have formed a group to try to prevent more cuts to the price they are paid for milk.Dairy Farmers Together was launched at a rally in Lanark Agricultural Centre and will be given £100,000 by the Scottish Government.',
    'desc2' => 'Bob Carruth from NFU Scotland says dairy farmers need three agreements to protect their industry',
    'pic' => 'I0031343666806804I-0-1.jpg',
    'video' => 'I0031343666806804I.mp4',
  ),
  1343668739 => 
  array (
    'file' => 'Iage1343659438679I.xml',
    'title' => 'Varnish hoping to shine',
    'desc1' => 'Jess Varnish is to ride alongside the retiring Victoria Pendleton in the two-woman, two-lap event for Britain.The pair broke the world record at February\'s Track World Cup event in London, but finished fourth at the World Championships as Germany won, with Australia second and China third.',
    'desc2' => 'Team GB women\'s sprint team member Jess Varnish on Lizzie Armitstead\'s medal and partnering with Victoria Pendleton',
    'pic' => 'Iage1343659438679I-24040-1.jpg',
    'video' => 'Iage1343659438679I.mp4',
  ),
  1343668100 => 
  array (
    'file' => 'I108523810808121343656121000I.xml',
    'title' => 'HOUVENAGHAL: TEAM IS POSITIVE',
    'desc1' => 'Wendy Houvenaghal says the whole team are in a positive mindset ahead of the Womens Team Pursuit.Houvenaghal is hoping to put behind her the disappointment of the World Championships in Melbourne and claim Olympic gold.',
    'desc2' => 'Press conference with Team GB cyclist Wendy Houvenaghal ahead of the Team Pursuit.',
    'pic' => 'I108523810808121343656121000I-0-1.jpg',
    'video' => 'I108523810808121343656121000I.mp4',
  ),
  1343667354 => 
  array (
    'file' => 'IPkg1343659715624I.xml',
    'title' => 'Tennant: It\'s going to be a battle',
    'desc1' => 'Andy Tennant says it all comes down to conditions such as temperature and pressure outside. He also predicts that world records will be broken.',
    'desc2' => 'Team GB\'s Team Pursuit member Andy Tennant says \'it\'s going to be a battle\' as he and his team enter the Olympics as one of the favourites',
    'pic' => 'IPkg1343659715624I-0-1.jpg',
    'video' => 'IPkg1343659715624I.mp4',
  ),
  1343665807 => 
  array (
    'file' => 'I108518210807571343648121000I.xml',
    'title' => 'TICKETS SOLD TO FILL EMPTY SEATS',
    'desc1' => 'Jackie Brock-Doyle from LOCOG and Mark Adams from IOC says they are doing everything to sort of empty seats & encouraging people to give up accredited ones so they can resell them. Also Jackie explains how and why the flame was moved from the stadium into Olympic Park last night',
    'desc2' => 'Jackie Brock-Doyle from LOCOG and Mark Adams from IOC says they are doing everything to sort of empty seats & encouraging people to give up accredited ones so they can resell them. Also Jackie explains how and why the flame was moved from the stadium into Olympic Park last night',
    'pic' => 'I108518210807571343648121000I-0-1.jpg',
    'video' => 'I108518210807571343648121000I.mp4',
  ),
  1343665131 => 
  array (
    'file' => 'Ihel1343660296331I.xml',
    'title' => 'Houvenaghel \'ready to race\'',
    'desc1' => 'Houvenaghel will be part of the team pursuit squad in London - her own event having been dropped from the Olympics.',
    'desc2' => 'Team GB track cyclist Wendy Houvenaghel says she\'s focussed and \'ready to race\'',
    'pic' => 'Ihel1343660296331I-17240-1.jpg',
    'video' => 'Ihel1343660296331I.mp4',
  ),
  1343663705 => 
  array (
    'file' => 'Ilia1343657023935I.xml',
    'title' => 'Misery for Aus relay swimmers',
    'desc1' => 'Austalian relay swimmer Matt Targett says the team are devastated not to have won the medal they were expecting.',
    'desc2' => 'Matt Targett reacts to the Austraila men\'s 4x100m relay swimming team not getting a medal',
    'pic' => 'Ilia1343657023935I-29960-1.jpg',
    'video' => 'Ilia1343657023935I.mp4',
  ),
  1343661701 => 
  array (
    'file' => 'I108511210806861343647841000I.xml',
    'title' => 'DOPIN PUTS SPORT "AT DANGER"',
    'desc1' => NULL,
    'desc2' => 'Professor Arne Ljungqvist has said it is a shame that we immediately suspect athletes who have a sudden rise in performance because of the problem of drugs in sport.Ljungqvist said sport is "at danger" of losing its competitive charm should this continue.',
    'pic' => NULL,
    'video' => 'I108511210806861343647841000I-0-1.jpg',
  ),
  1343657834 => 
  array (
    'file' => 'I108508510806591343646427000I.xml',
    'title' => 'SAVILE SALE RAISES MONEY FOR CHARITY',
    'desc1' => NULL,
    'desc2' => 'Sir Jimmy Savile\'s gold suits, Cuban cigars and beloved Rolls-Royce are going on sale as the late DJ\'s belongings go up for auction.A tweed jacket and kilt, the first item of 549 lots to go under the hammer, went for £280.',
    'pic' => NULL,
    'video' => 'I108508510806591343646427000I-0-1.jpg',
  ),
  1343657422 => 
  array (
    'file' => 'I108495410805281343635718000I.xml',
    'title' => 'NELSON RECEIVES OLYMPIC MAKEOVER',
    'desc1' => NULL,
    'desc2' => 'Lord Admiral Nelson gets into the Olympic spirit as his statue sports an Olympic-themed hat in Trafalgar Square.It is one of many statues to have undergone a makeover as part of a summer of cultural \'firsts\' to mark the London 2012 Olympics.',
    'pic' => NULL,
    'video' => 'I108495410805281343635718000I-0-1.jpg',
  ),
  1343653443 => 
  array (
    'file' => 'I108496610805401343637011000I.xml',
    'title' => 'SAVILE\'S BELONGINGS GO ON SALE',
    'desc1' => NULL,
    'desc2' => 'Sir Jimmy Savile\'s gold suits, Cuban cigars and beloved Rolls-Royce will go on sale today as the late DJ\'s belongings go up for auction.Thousands of items of memorabilia owned by the broadcaster and Top of the Pops host will be sold to go towards his charitable trust.',
    'pic' => NULL,
    'video' => 'I108496610805401343637011000I-0-1.jpg',
  ),
  1343635176 => 
  array (
    'file' => 'Ipkg1343630545666I.xml',
    'title' => 'Adlington battles way to bronze',
    'desc1' => 'Rebecca Adlington last night claimed Great Britain\'s first Olympic medal in the pool when she produced a customary performance full of skill and guts as she took bronze in the 400 metres freestyle.',
    'desc2' => 'Fans react to Rebecca Adlington\'s bronze medal in the 400m freestyle',
    'pic' => 'Ipkg1343630545666I-3840-1.jpg',
    'video' => 'Ipkg1343630545666I.mp4',
  ),
  1343634115 => 
  array (
    'file' => 'I0031343595981066I.xml',
    'title' => 'ADLINGTON BATTLES WAY TO BRONZE',
    'desc1' => 'Rebecca Adlington last night claimed Great Britain\'s first Olympic medal in the pool when she produced a customary performance full of skill and guts as she took bronze in the 400 metres freestyle.',
    'desc2' => 'Fans react to Rebecca Adlington\'s 400m freestyle bronze medal',
    'pic' => 'I0031343595981066I-0-1.jpg',
    'video' => 'I0031343595981066I.mp4',
  ),
  1343601916 => 
  array (
    'file' => 'I0011343575908505I.xml',
    'title' => 'Olympic legends support the Games',
    'desc1' => 'Gvs of Olympic track and field legends Dick Fosbury, Maurice Green, Daley Thompson, Edwin Moses and Haile Gebrselassie.',
    'desc2' => 'Gvs of Olympic track and field legends Dick Fosbury, Maurice Green, Daley Thompson, Edwin Moses and Haile Gebrselassie in Stratford, London',
    'pic' => 'I0011343575908505I-0-1.jpg',
    'video' => 'I0011343575908505I.mp4',
  ),
  1343601687 => 
  array (
    'file' => 'I0021343561515203I.xml',
    'title' => 'Trott: Australia are our biggest rivals',
    'desc1' => 'Press conference with British cyclist Laura Trott',
    'desc2' => 'Press conference with British cyclist Laura Trott',
    'pic' => 'I0021343561515203I-0-1.jpg',
    'video' => 'I0021343561515203I.mp4',
  ),
  1343601631 => 
  array (
    'file' => 'I0041343575316815I.xml',
    'title' => 'Maurice: Pistorius should be allowed to compete in the Olympics',
    'desc1' => 'Press conference with Olympic gold medallist Maurice Greene in London',
    'desc2' => 'Press conference with Olympic gold medallist Maurice Greene in London',
    'pic' => 'I0041343575316815I-0-1.jpg',
    'video' => 'I0041343575316815I.mp4',
  ),
  1343601579 => 
  array (
    'file' => 'I0061343575426361I.xml',
    'title' => 'Gebrselassie backs Paula',
    'desc1' => 'Press conference with Olympic gold medallist Haile Gebrselassie in London',
    'desc2' => 'Press conference with Olympic gold medallist Haile Gebrselassie in London',
    'pic' => 'I0061343575426361I-0-1.jpg',
    'video' => 'I0061343575426361I.mp4',
  ),
  1343601315 => 
  array (
    'file' => 'I0001343561391309I.xml',
    'title' => 'Team GB Cycling Gvs: Pendleton, Trott and Thomas',
    'desc1' => 'GVs of British cyclists, Victoria Pendleton, Laura Trott and Geraint Thomas at the Adidas Media Centre, London',
    'desc2' => 'GVs of British cyclists, Victoria Pendleton, Laura Trott and Geraint Thomas at the Adidas Media Centre, London',
    'pic' => 'I0001343561391309I-0-1.jpg',
    'video' => 'I0001343561391309I.mp4',
  ),
  1343600041 => 
  array (
    'file' => 'I108478410803581343588344000I.xml',
    'title' => 'TEAM GB CELEBRATES FIRST MEDALS',
    'desc1' => NULL,
    'desc2' => 'Team GB celebrated its first Olympic medal success today with cyclist Lizzie Armitstead battling through torrential rain for a silver and swimmer Rebecca Adlington earning a courageous bronze.Armitstead earned second place in the women\'s road race on The Mall and although Adlington was unable to defend her 400 metres freestyle title from the Beijing Games, she did enough to secure third place.Armitstead, 23, from Otley near Leeds, banished the disappointment of the men\'s road race as crowds billowing "Lizzie, Lizzie" swept her to silver.',
    'pic' => NULL,
    'video' => 'I108478410803581343588344000I-0-1.jpg',
  ),
  1343599498 => 
  array (
    'file' => 'I0051343575399517I.xml',
    'title' => 'Moses: Pistorius is inspirational',
    'desc1' => 'Press conference with Olympic gold medallist Ed Moses in London',
    'desc2' => 'Press conference with Olympic gold medallist Ed Moses in London',
    'pic' => 'I0051343575399517I-0-1.jpg',
    'video' => 'I0051343575399517I.mp4',
  ),
  1343599130 => 
  array (
    'file' => 'I0081343575461610I.xml',
    'title' => 'Fosbury backs Pistorius',
    'desc1' => 'Press conference with Olympic gold medallist Dick Fosbury in London',
    'desc2' => 'Press conference with Olympic gold medallist Dick Fosbury in London',
    'pic' => 'I0081343575461610I-0-1.jpg',
    'video' => 'I0081343575461610I.mp4',
  ),
  1343597873 => 
  array (
    'file' => 'Iead1343575882230I.xml',
    'title' => 'Armitstead silver gets Britain off the mark',
    'desc1' => 'Armitstead was beaten to victory at the end of a pulsating 140-kilometre cycling road race, which featured two climbs of Surrey\'s Box Hill.',
    'desc2' => 'Lizzie Armitstead has won silver to claim Great Britain\'s first medal of London 2012',
    'pic' => 'Iead1343575882230I-0-1.jpg',
    'video' => 'Iead1343575882230I.mp4',
  ),
  1343597131 => 
  array (
    'file' => 'I108429610798701343559658000I.xml',
    'title' => 'Hunt: Radcliffe is on the team',
    'desc1' => 'Andy Hunt comments on possibility that Paula Radcliffe will have to withdraw and both Hunt and Moynihan give their view on yesterday\'s road race and the idea that everyone\'s out to make sure Britain lose.',
    'desc2' => 'Press conference with Andy Hunt and Colin Moynihan, BOA Chairman',
    'pic' => 'I108429610798701343559658000I-0-1.jpg',
    'video' => 'I108429610798701343559658000I.mp4',
  ),
  1343596973 => 
  array (
    'file' => 'Iage1343576824187I.xml',
    'title' => 'Radcliffe out of marathon',
    'desc1' => 'Paula Radcliffe\'s Olympic hopes evaporated today as she withdrew from London 2012 marathon because of injury.Speculation had been rife the 38-year-old world record-holder would pull out after a foot problem flared up again and today the news was confirmed.A statement on the UK Athletics website read: "The British Olympic Association and UK Athletics announced this afternoon that marathon athlete Paula Radcliffe is being withdrawn from Team GB due to medical reasons.',
    'desc2' => 'Paula Radcliffe has been forced to pull out of the Olympic marathon because of injury',
    'pic' => 'Iage1343576824187I-1920-1.jpg',
    'video' => 'Iage1343576824187I.mp4',
  ),
  1343596749 => 
  array (
    'file' => 'I0031343561766792I.xml',
    'title' => 'Thomas: It\'s said to see so many empty seats',
    'desc1' => 'Press conference with Olympic gold medallist and British cyclist Geraint Thomas',
    'desc2' => 'Press conference with Olympic gold medallist and British cyclist Geraint Thomas',
    'pic' => 'I0031343561766792I-0-1.jpg',
    'video' => 'I0031343561766792I.mp4',
  ),
  1343595838 => 
  array (
    'file' => 'I108479410803681343588410000I.xml',
    'title' => 'Lizzie\'s silver success',
    'desc1' => 'Team GB cyclist Lizzie Armitstead discusses the road race in which she won a silver medal.',
    'desc2' => 'Lizzie Armitstead talks about her silver in the road race, Team GB\'s first medal of London 2012',
    'pic' => 'I108479410803681343588410000I-0-1.jpg',
    'video' => 'I108479410803681343588410000I.mp4',
  ),
  1343594224 => 
  array (
    'file' => 'I0021343584927718I.xml',
    'title' => 'Rhode makes history',
    'desc1' => 'OLYMPICS Shooting United States (3.48) - Rhode says?Every emotion has hit me. It seems like a dream and it makes me cry.It\'s a bummer to miss a bird (denied her a perfect 100), but it just happens sometimes. Weather conditions were difficult, although I think it made me focus more.It\'s about the journey, and the one to this Olympics has been tough - my gun was stolen and I had a breast cancer scare - which makes it sweeter to win the medal.There are definitely a few Olympics left in me yet.',
    'desc2' => 'US shooter Kim Rhode makes history by winning gold at London 2012, making her the first American to win individual medals at five consecutive Olympics',
    'pic' => 'I0021343584927718I-0-1.jpg',
    'video' => 'I0021343584927718I.mp4',
  ),
  1343593152 => 
  array (
    'file' => 'I0091343577433417I.xml',
    'title' => 'Moyinhan: Radcliffe will be sorely missed',
    'desc1' => 'Interview with BOA chairman Colin Moyinhan in Stratford. He talks about Lizzie Armitstead, the sprirt withing Team GB and Paula Radcliffe.',
    'desc2' => 'Interview with BOA chairman Colin Moyinhan in Stratford',
    'pic' => 'I0091343577433417I-0-1.jpg',
    'video' => 'I0091343577433417I.mp4',
  ),
  1343590559 => 
  array (
    'file' => 'Iage1343569902638I.xml',
    'title' => 'Coe unveils crowd gap plans',
    'desc1' => 'Troops, students and teachers could all be asked to fill gaps left in empty Olympic stadiums, Lord Coe said today.Members of the military are being brought in at the last minute, students and teachers from the local area are also being invited and other fans could have their tickets upgraded, organisers said.Tickets for double sessions, such as those for hockey, basketball, water polo and handball, are also being recycled and re-sold as people leave.',
    'desc2' => 'Lord Coe is unconcerned by empty seats and says troops, students and teachers will be invited to fill gaps at the Olympics',
    'pic' => 'Iage1343569902638I-13040-1.jpg',
    'video' => 'Iage1343569902638I.mp4',
  ),
  1343590103 => 
  array (
    'file' => 'Ipkg1343583560243I.xml',
    'title' => 'Olympic legends back Pistorius',
    'desc1' => 'Oscar Pistorius, who had both legs amputated below the knee when he was just 11 months old, failed to qualify for the Beijing Olympic Games and is now determined to fight for a place on the podium at London 2012.',
    'desc2' => 'Olympic Gold medallists Ed Moses, Dick Fosbury and Maurice Greene are supporting double amputee sprinter Oscar Pistorius\' quest for an Olympic medal at London 2012',
    'pic' => 'Ipkg1343583560243I-15920-1.jpg',
    'video' => 'Ipkg1343583560243I.mp4',
  ),
  1343588521 => 
  array (
    'file' => 'I108458510801591343573887000I.xml',
    'title' => 'BOA chief hails first medal',
    'desc1' => 'Lord Moynihan discusses Team GB\'s first medal won by Lizzie Armitstead in the women\'s cycling road race.',
    'desc2' => 'Interview with British Olympic Association chairman Lord Moynihan on Lizzie Armitstead\'s silver medal and Paula Radcliffe pulling out of the marathon',
    'pic' => 'I108458510801591343573887000I-0-1.jpg',
    'video' => 'I108458510801591343573887000I.mp4',
  ),
  1343587477 => 
  array (
    'file' => 'I0011343561511089I.xml',
    'title' => 'Pendleton slams Meares rivalry as \'sensationalism\'',
    'desc1' => 'Press conference with Olympic gold medallist Victoria Pendleton, London',
    'desc2' => 'Press conference with Olympic gold medallist Victoria Pendleton, London',
    'pic' => 'I0011343561511089I-0-1.jpg',
    'video' => 'I0011343561511089I.mp4',
  ),
  1343587381 => 
  array (
    'file' => 'I0071343575443065I.xml',
    'title' => 'Daley wants \'strict\' drugs testing',
    'desc1' => 'Press conference with Olympic gold medallist Daley Thompson in London',
    'desc2' => 'Press conference with Olympic gold medallist Daley Thompson in London',
    'pic' => 'I0071343575443065I-0-1.jpg',
    'video' => 'I0071343575443065I.mp4',
  ),
  1343586383 => 
  array (
    'file' => 'I0011343565504302I.xml',
    'title' => 'Coe reveals plan to fill empty seats',
    'desc1' => 'Coe Says they have put a four point plan in place to prevent it happening again which includes selling more tickets, handing out spares to the military and local teachers and Frankie Fredericks who says the atmosphere is amazing and the empty seats are not an issue.',
    'desc2' => 'Clips from Lord Seb Coe and Olympic sprinter Frankie Fredericks about the empty seats during the first day of the Olympic Games',
    'pic' => 'I0011343565504302I-100400-1.jpg',
    'video' => 'I0011343565504302I.mp4',
  ),
  1343583353 => 
  array (
    'file' => 'Iage1343578805534I.xml',
    'title' => 'BOA chief hails first medal',
    'desc1' => 'British Olympic Association chairman Lord Moynihan discusses Lizzie Armitstead getting Team GB\'s first medal, a silver in the cycling road race.',
    'desc2' => 'BOA chairman Lord Moynihan on Lizzie Armitstead\'s silver in the women\'s road race and Paula Radcliffe\'s withdrawal because of injury',
    'pic' => 'Iage1343578805534I-49920-1.jpg',
    'video' => 'Iage1343578805534I.mp4',
  ),
  1343576282 => 
  array (
    'file' => 'Imas1343569710097I.xml',
    'title' => 'Thomas assesses GB\'s position',
    'desc1' => 'Thomas won team pursuit gold in Beijing.',
    'desc2' => 'Team GB\'s Geraint Thomas says things are more exciting than ever for cyclists',
    'pic' => 'Imas1343569710097I-0-1.jpg',
    'video' => 'Imas1343569710097I.mp4',
  ),
  1343572240 => 
  array (
    'file' => 'Iton1343563667068I.xml',
    'title' => 'Pendleton on Meares',
    'desc1' => 'Victoria Pendleton discusses competing at London 2012.',
    'desc2' => 'Great Britain\'s Victoria Pendleton discusses her rivalry with Australian Anna Meares',
    'pic' => 'Iton1343563667068I-17640-1.jpg',
    'video' => 'Iton1343563667068I.mp4',
  ),
  1343571396 => 
  array (
    'file' => 'I0021343559346179I.xml',
    'title' => 'BOA Chairman calls for all seats to be filled',
    'desc1' => 'BOA Chairman Colin Moynihan calls for all seats to be filled, says it made a real difference to the rowers yesterday that Eton Dorney was sold out and says all BOA seats are being used.',
    'desc2' => 'Press conference with BOA Chairman Colin Moynihan',
    'pic' => 'I0021343559346179I-0-1.jpg',
    'video' => 'I0021343559346179I.mp4',
  ),
  1343570391 => 
  array (
    'file' => 'Iage1343564722165I.xml',
    'title' => 'Radcliffe fitness \'not looking good\'',
    'desc1' => 'Paula Radcliffe admits the situation is "not looking good" as her participation in the Olympics hangs in the balance.A foot problem has flared up again casting a major doubt over whether she will compete in the marathon in London and a Mail on Sunday article suggested she had admitted defeat in her battle to compete.Radcliffe, 38, insisted a decision to rule her out had not been made, but conceded it was touch and go.',
    'desc2' => 'Paula Radcliffe admits it is a touch and go that she will be fit for the Olympic marathon',
    'pic' => 'Iage1343564722165I-18960-1.jpg',
    'video' => 'Iage1343564722165I.mp4',
  ),
  1343568628 => 
  array (
    'file' => 'Ipkg1343562584708I.xml',
    'title' => 'Inquiry into empty seats at Games',
    'desc1' => 'Organisers were prompted to act after gaps were visible at a number of venues, including the Aquatics Centre where British medal hope Hannah Miley missed out on a podium place.',
    'desc2' => 'An urgent investigation has been launched after Olympic fans expressed disappointment at the sight of rows of empty seats on the first day of the London 2012 Games.',
    'pic' => 'Ipkg1343562584708I-61040-1.jpg',
    'video' => 'Ipkg1343562584708I.mp4',
  ),
  1343564824 => 
  array (
    'file' => 'I0011343554696727I.xml',
    'title' => 'Australian swimmers celebrate winning Olympic gold',
    'desc1' => 'Olympic gold medal winners Cate Campbell, Melanie Schlanger, Libby Trickett and Yolane Kukla hold a press conference after their success in the 4 by 4 relay swimming event.',
    'desc2' => 'Press conference with Olympic gold medal winners Cate Campbell, Melanie Schlanger, Libby Trickett and Yolane Kukla from the Australian swimming team',
    'pic' => 'I0011343554696727I-48120-1.jpg',
    'video' => 'I0011343554696727I.mp4',
  ),
  1343564705 => 
  array (
    'file' => 'Ilia1343560307383I.xml',
    'title' => 'Australia girls take gold',
    'desc1' => 'Cate Campbell and Melanie Schlanger celebrate their victory after winning gold medals at London 2012.',
    'desc2' => 'The Australia women won the gold medel in the 4x100m relay',
    'pic' => 'Ilia1343560307383I-0-1.jpg',
    'video' => 'Ilia1343560307383I.mp4',
  ),
  1343558100 => 
  array (
    'file' => 'Ipkg1343555724783I.xml',
    'title' => 'Marvin and Rochelle tie the knot',
    'desc1' => 'The singers from JLS and The Saturdays got married in front of plenty of their showbiz friends, including Harry Styles and Alexandra Burke.',
    'desc2' => 'Marvin Humes and Rochelle Wiseman have got married.',
    'pic' => 'Ipkg1343555724783I-27840-1.jpg',
    'video' => 'Ipkg1343555724783I.mp4',
  ),
  1343511675 => 
  array (
    'file' => 'I0041343477439532I.xml',
    'title' => 'Smith: Training is fantastic',
    'desc1' => 'Interview with British weightlifter Zoe Smith at Team GB House',
    'desc2' => 'Interview with British weightlifter Zoe Smith at Team GB House',
    'pic' => 'I0041343477439532I-0-1.jpg',
    'video' => 'I0041343477439532I.mp4',
  ),
  1343511456 => 
  array (
    'file' => 'I0021343477485913I.xml',
    'title' => 'Albanian weightlifter Hysen Pulaku fails drug test',
    'desc1' => 'IOC Mark Adams tells the press during a media conference about the Opening Ceremony that an Albanian weightlifter has failed a drugs test and will no longer compete at the Games. Jackie Brock-Doyle from LOCOG admits there are empty seats at swimming this morning but they are accredited seats and not the public ones.',
    'desc2' => 'Press conference with IOC\'s Mark Adams and LOCOG\'s Jackie Broack-Doyle',
    'pic' => 'I0021343477485913I-0-1.jpg',
    'video' => 'I0021343477485913I.mp4',
  ),
  1343510502 => 
  array (
    'file' => 'I108399310795671343492931000I.xml',
    'title' => 'Disappointment as GB miss out at road race',
    'desc1' => 'Great Britain\'s Mark Cavendish expressed frustration that rival teams were content so long as he did not win the London 2012 Olympic Games road race.The 27-year-old world champion from the Isle of Man finished 29th, 40 seconds behind, as controversial Kazakh Alexandr Vinokourov triumphed on The Mall, with Colombia\'s Rigoberto Uran second and Norway\'s Alexander Kristoff third.',
    'desc2' => 'GVs of fans at the National Cycling Centre in Manchester watching the Olympic road race plus vox pops with Rose Joyce, Marion Rubins and Simon Orme and son Louis',
    'pic' => 'I108399310795671343492931000I-0-1.jpg',
    'video' => 'I108399310795671343492931000I.mp4',
  ),
  1343508597 => 
  array (
    'file' => 'I0021343477292995I.xml',
    'title' => 'Perdue: I want to do my family proud',
    'desc1' => 'Interview with British weightlifter Natasha Perdue at Team GB House',
    'desc2' => 'Interview with British weightlifter Natasha Perdue at Team GB House',
    'pic' => 'I0021343477292995I-0-1.jpg',
    'video' => 'I0021343477292995I.mp4',
  ),
  1343506642 => 
  array (
    'file' => 'I0051343477465944I.xml',
    'title' => 'Kirkbride: It\'s a hard group',
    'desc1' => 'Interview with British weightlifter Peter Kirkbride at Team GB House, London',
    'desc2' => 'Interview with British weightlifter Peter Kirkbride at Team GB House, London',
    'pic' => 'I0051343477465944I-0-1.jpg',
    'video' => 'I0051343477465944I.mp4',
  ),
  1343506040 => 
  array (
    'file' => 'I0011343436698313I.xml',
    'title' => 'Public \'blown away\' by opening ceremony',
    'desc1' => 'GVs of people leaving and voxes after the Olympics Opening Ceremony',
    'desc2' => 'GVs of people leaving and voxes after the Olympics Opening Ceremony',
    'pic' => 'I0011343436698313I-26600-1.jpg',
    'video' => 'I0011343436698313I.mp4',
  ),
  1343505000 => 
  array (
    'file' => 'Iage1343485388940I.xml',
    'title' => 'Smith looks to the future',
    'desc1' => 'Weightlifter Zoe Smith will compete for Great Britain in the 58kg weight category.',
    'desc2' => 'Weightlifter Zoe Smith on the opening ceremony, competing in the 58kg weight category and abuse on Twitter',
    'pic' => 'Iage1343485388940I-21600-1.jpg',
    'video' => 'Iage1343485388940I.mp4',
  ),
  1343503867 => 
  array (
    'file' => 'Iton1343489523689I.xml',
    'title' => 'Leighton aiming for medal',
    'desc1' => 'The British women\'s waterpolo captain Fran Leighton says the team can achieve medal success and are not just at the Olympics to make up the numbers.',
    'desc2' => 'British waterpolo captain Fran Leighton on the opening ceremony, confidence and the Olympic village',
    'pic' => 'Iton1343489523689I-0-1.jpg',
    'video' => 'Iton1343489523689I.mp4',
  ),
  1343502836 => 
  array (
    'file' => 'I0031343477390586I.xml',
    'title' => 'Oliver: It\'s time to get down to business',
    'desc1' => 'Interview with British weightlifter Jack Oliver in Stratford, London',
    'desc2' => 'Interview with British weightlifter Jack Oliver in Stratford, London',
    'pic' => 'I0031343477390586I-0-1.jpg',
    'video' => 'I0031343477390586I.mp4',
  ),
  1343501805 => 
  array (
    'file' => 'I0021343496417124I.xml',
    'title' => 'Stratford transport GVs',
    'desc1' => 'Stock footage of people passing through Stratford station on the way to the Olympic Park.',
    'desc2' => 'GVs of crowds passing through Stratford station to get to the Olympic Park',
    'pic' => 'I0021343496417124I-0-1.jpg',
    'video' => 'I0021343496417124I.mp4',
  ),
  1343501096 => 
  array (
    'file' => 'I0061343495240956I.xml',
    'title' => 'Canadian Olympic team update',
    'desc1' => 'Canada\'s Dimitri Soudas says he loved the Opening Ceremony and lists the teams results on Saturday 28th July. He also mentions that a Canadian athlete gave the Queen a pin after the ceremony.',
    'desc2' => 'Press conference with Dimitri Soudas, Canadian Olympic Committee Executive Director',
    'pic' => 'I0061343495240956I-99200-1.jpg',
    'video' => 'I0061343495240956I.mp4',
  ),
  1343499535 => 
  array (
    'file' => 'I108391310794881343490518000I.xml',
    'title' => 'Boris on opening ceremony',
    'desc1' => 'London Mayor Boris Johnson was delighted with the opening ceremony for the 2012 Games.',
    'desc2' => 'Interview clips with Mayor of London Boris Johnson discusses the Olympic opening ceremony',
    'pic' => 'I108391310794881343490518000I-0-1.jpg',
    'video' => 'I108391310794881343490518000I.mp4',
  ),
  1343499186 => 
  array (
    'file' => 'I0091343485359996I.xml',
    'title' => 'Morris has high hopes for Olympics',
    'desc1' => 'Interview with British water polo player Rosie Morris at Stratford',
    'desc2' => 'Interview with British water polo player Rosie Morris at Stratford',
    'pic' => 'I0091343485359996I-0-1.jpg',
    'video' => 'I0091343485359996I.mp4',
  ),
  1343499177 => 
  array (
    'file' => 'I0051343481543190I.xml',
    'title' => 'Opening ceremony volunteers thrilled with Olympic experience',
    'desc1' => 'Three volunteers from the opening ceremony speak about their expereinces. Betsey Lau-Robinson is an NHS nurse at UCL and was part of the NHS dancers; Tony Sanders is retired and played the part of the cricket umpire; 16 year old Isata Kamara was part of the 70s dancers group.',
    'desc2' => 'Three volunteers from the opening ceremony speak about their experiences.',
    'pic' => 'I0051343481543190I-0-1.jpg',
    'video' => 'I0051343481543190I.mp4',
  ),
  1343497577 => 
  array (
    'file' => 'I0061343477611301I.xml',
    'title' => 'Evans: The ceremony was \'electric\'',
    'desc1' => 'Interview with British weightlifter Gareth Evans at Team GB House, London',
    'desc2' => 'Interview with British weightlifter Gareth Evans at Team GB House, London',
    'pic' => 'I0061343477611301I-0-1.jpg',
    'video' => 'I0061343477611301I.mp4',
  ),
  1343497572 => 
  array (
    'file' => 'I0081343485269638I.xml',
    'title' => 'Scott: Team spirit is high',
    'desc1' => 'Interview with British water polo player Ed Scott Stratford',
    'desc2' => 'Interview with British water polo player Ed Scott at Stratford',
    'pic' => 'I0081343485269638I-0-1.jpg',
    'video' => 'I0081343485269638I.mp4',
  ),
  1343494993 => 
  array (
    'file' => 'I0071343485236066I.xml',
    'title' => 'Figes: We\'re here to challenge the best players',
    'desc1' => 'Interview with British water polo player Craig Figes at Stratford, London',
    'desc2' => 'Interview with British water polo player Craig Figes at Stratford, London',
    'pic' => 'I0071343485236066I-0-1.jpg',
    'video' => 'I0071343485236066I.mp4',
  ),
  1343493205 => 
  array (
    'file' => 'I0101343485933633I.xml',
    'title' => 'Leighton: Anything is possible',
    'desc1' => 'Interview with British water polo player Fran Leighton at Stratford, London',
    'desc2' => 'Interview with British water polo player Fran Leighton at Stratford, London',
    'pic' => 'I0101343485933633I-0-1.jpg',
    'video' => 'I0101343485933633I.mp4',
  ),
  1343492236 => 
  array (
    'file' => 'I0071343424007444I.xml',
    'title' => 'Crowds gather at Hyde Park to watch opening ceremony',
    'desc1' => 'GVs of people watching the opening ceremony on big screens at Hyde Park event and vox pops with Leigh Parker from Watford and Caroline Farrell of Southend.',
    'desc2' => 'GVs of people watching the opening ceremony on big screens at Hyde park',
    'pic' => 'I0071343424007444I-0-1.jpg',
    'video' => 'I0071343424007444I.mp4',
  ),
  1343488913 => 
  array (
    'file' => 'I0031343479521394I.xml',
    'title' => 'Olympic cauldron to stay in stadium',
    'desc1' => 'Thomas Heatherwick who designed the Cauldron says every country will get to take away one of the 204 petals that makes up the cauldron; it will now sit in the South Vom of the stadium just like in 1948; Danny decided who would light the flame and chose the young athletes because the Games is about the future and not just the past.',
    'desc2' => 'Press confernce with Thomas Heatherwick who designed the Cauldron for the Opening Ceremony',
    'pic' => 'I0031343479521394I-0-1.jpg',
    'video' => 'I0031343479521394I.mp4',
  ),
  1343488421 => 
  array (
    'file' => 'I108374210793161343475168000I.xml',
    'title' => 'Albanian weightlifter fails drug test',
    'desc1' => 'Pulaku, 20, tested positive for stanozolol on July 23. His file is due to be sent to the International Weightlifting Federation.',
    'desc2' => 'Albanian weightlifter Hysen Pulaku has been thrown out of the London 2012 Olympics after failing a drugs test',
    'pic' => 'I108374210793161343475168000I-29440-1.jpg',
    'video' => 'I108374210793161343475168000I.mp4',
  ),
  1343487419 => 
  array (
    'file' => 'I0011343434353250I.xml',
    'title' => 'Ceremony fireworks light up Olympic park',
    'desc1' => 'Gvs of the fireworks at the London 2012 opening ceremony',
    'desc2' => 'Gvs of the fireworks at the London 2012 opening ceremony',
    'pic' => 'I0011343434353250I-75720-1.jpg',
    'video' => 'I0011343434353250I.mp4',
  ),
  1343486950 => 
  array (
    'file' => 'I0041343479566527I.xml',
    'title' => 'Khan reveals ideas behind Opening Ceremony choreography',
    'desc1' => 'Akram Khan, who choreographed part of the Opening Ceremony says he drew his inspiration from the concept of Eastern time and the Japanese idea of keeping things calm and simple which was in contrast with Danny Boyle\'s busy pieces',
    'desc2' => 'Press conference with Akram Khan who choreographed part of the Opening Ceremony',
    'pic' => 'I0041343479566527I-0-1.jpg',
    'video' => 'I0041343479566527I.mp4',
  ),
  1343483700 => 
  array (
    'file' => 'Iize1343472775734I.xml',
    'title' => 'Gvs of the red arrows flying over the London 2012 opening ceremony',
    'desc1' => 'Gvs of the red arrows flying over the London 2012 Olympic ceremony.',
    'desc2' => 'Gvs of the red arrows flying over the London 2012 opening ceremony.',
    'pic' => 'Iize1343472775734I-0-1.jpg',
    'video' => 'Iize1343472775734I.mp4',
  ),
  1343483412 => 
  array (
    'file' => 'I0051343409154021I.xml',
    'title' => 'Storry: We really believe we can win',
    'desc1' => 'Interview with GB hockey goalkeeper Beth Storry',
    'desc2' => 'Interview with GB hockey goalkeeper Beth Storry',
    'pic' => 'I0051343409154021I-0-1.jpg',
    'video' => 'I0051343409154021I.mp4',
  ),
  1343478542 => 
  array (
    'file' => 'Irry1343473449214I.xml',
    'title' => 'Beth hoping to produce perfect Storry',
    'desc1' => 'The 34-year-old, who made her Olympic debut in Beijing as the team did not qualify in 2004, has seen the team move up from ninth in the world rankings four years ago to fourth',
    'desc2' => 'Great Britain goalkeeper Beth Storry believes the team have peaked at exactly the right time and are the closest they have ever been to producing their very best form.',
    'pic' => 'Irry1343473449214I-10120-1.jpg',
    'video' => 'Irry1343473449214I.mp4',
  ),
  1343476186 => 
  array (
    'file' => 'I0031343407259163I.xml',
    'title' => 'Gvs: Japanese Tennis Press Conference',
    'desc1' => 'Cutaways from the Japanese tennis media conference at the Olympics Main Press Centre',
    'desc2' => 'Cutaways from the Japanese tennis media conference at the Olympics Main Press Centre',
    'pic' => 'I0031343407259163I-0-1.jpg',
    'video' => 'I0031343407259163I.mp4',
  ),
  1343474873 => 
  array (
    'file' => 'I0011343423277947I.xml',
    'title' => 'Crowds soak up Olympic vibe in Eastbourne',
    'desc1' => 'Crowds watch the Olympics opening ceremony on a big screen in Eastbourne.',
    'desc2' => 'Crowds watch the Olympics opening ceremony on a big screen in Eastbourne.',
    'pic' => 'I0011343423277947I-0-1.jpg',
    'video' => 'I0011343423277947I.mp4',
  ),
  1343474696 => 
  array (
    'file' => 'I0061343423206763I.xml',
    'title' => 'Duran Duran wow crowds at Hyde Park',
    'desc1' => 'Interview with Simon Le Bon and Nick Rhodes from Duran Duran after performing at Hyde Park Olympic opening ceremony celebration concert',
    'desc2' => 'Interview with Simon Le Bon and Nick Rhodes from Duran Duran',
    'pic' => 'I0061343423206763I-0-1.jpg',
    'video' => 'I0061343423206763I.mp4',
  ),
  1343441344 => 
  array (
    'file' => 'I108276210783361343382390000I.xml',
    'title' => 'AIRPORT TWEET MAN IN APPEAL VICTORY',
    'desc1' => 'A man found guilty of sending a menacing tweet concerning Robin Hood Airport in South Yorkshire has won his challenge against conviction.Paul Chambers, 28, was fined £385 and ordered to pay £600 costs at Doncaster Magistrates\' Court in May 2010 after being convicted of sending "a message of a menacing character", contrary to provisions of the 2003 Communications Act.He said he sent the tweet to his 600 followers in a moment of frustration after Robin Hood Airport was closed by snow in January 2010, and never thought anyone would take his "silly joke" seriously.It read: "Crap! Robin Hood Airport is closed. You\'ve got a week and a bit to get your s*** together, otherwise I\'m blowing the airport sky high!"The Lord Chief Justice Lord Judge, sitting with Mr Justice Owen and Mr Justice Griffith Williams, said: "We have concluded that, on an objective assessment, the decision of the Crown Court that this \'tweet\' constituted or included a message of a menacing character was not open to it."On this basis, the appeal against conviction must be allowed."Mr Chambers, who has received backing from celebrities such as broadcaster Stephen Fry and comedian Al Murray, said later: "I am relieved, vindicated - it is ridiculous it ever got this far.',
    'desc2' => 'GVs of Paul Chambers outside the court, and shots of his supporters comedian Al Murray and Tory MP Louise Mensch. Plus Interviews with Al Murray and Paul Chambers',
    'pic' => 'I108276210783361343382390000I-0-1.jpg',
    'video' => 'I108276210783361343382390000I.mp4',
  ),
  1343440511 => 
  array (
    'file' => 'I0031343401953640I.xml',
    'title' => 'Daley: We are the strongest diving team',
    'desc1' => 'Daley also talks about his rivalry with Chinese divers and the comments made by performance director Alexei Evangulov in early 2012 about him doing too much publicity.',
    'desc2' => 'Press conference with Team GB Tom Daley, Alexei Evangulov and Peter Waterfield',
    'pic' => 'I0031343401953640I-0-1.jpg',
    'video' => 'I0031343401953640I.mp4',
  ),
  1343440319 => 
  array (
    'file' => 'I0041343402066389I.xml',
    'title' => 'Daley: It\'s been a tough year',
    'desc1' => 'Tom Daley GVs and speaking about what a difficult year it\'s been for him but concentrating on diving; wants to see BMX and gymnastics and he talks about watching his diving partner Pete Waterfield win Silver in Athens when he was just 10 years old.',
    'desc2' => 'Press conference with Olympic diver Tom Daley',
    'pic' => 'I0041343402066389I-0-1.jpg',
    'video' => 'I0041343402066389I.mp4',
  ),
  1343428989 => 
  array (
    'file' => 'I108302510785991343396834000I.xml',
    'title' => 'BOYLE HAILS 15,000 SHOW VOLUNTEERS',
    'desc1' => NULL,
    'desc2' => 'Danny Boyle has dedicated his £27 million Olympics opening extravaganza to the 15,000 volunteers who will help bring it to life as organisers said they expected a sell-out crowd.Seven years of planning will come to a head when Boyle\'s Isles of Wonder spectacular begins at 9pm and the Games get under way.Some 62,000 people in the audience at the Olympic Stadium in east London and a potential global audience of billions will see the Olympic Torch complete its 70-day, 8,000-mile journey around the UK as the mystery surrounding who will light the cauldron is revealed.Dedicating the show to the volunteers, Boyle said: "What you think about really is you think about the volunteers. This is a live performance and it\'s the actors, and in our case they\'re volunteers, who have to get up there and do it.',
    'pic' => NULL,
    'video' => 'I108302510785991343396834000I-0-1.jpg',
  ),
  1343428056 => 
  array (
    'file' => 'I0011343392122672I.xml',
    'title' => 'FINAL COUNTDOWN TO OLYMPICS BEGINS - gvs',
    'desc1' => 'The Red Arrows fly over Edinburgh Castle on opening ceremony day for the Olympics - GVs',
    'desc2' => 'The Red Arrows fly over Edinburgh Castle on opening ceremony day for the Olympics - GVs',
    'pic' => 'I0011343392122672I-0-1.jpg',
    'video' => 'I0011343392122672I.mp4',
  ),
  1343427848 => 
  array (
    'file' => 'I0031343398691821I.xml',
    'title' => 'GVs: Outside the Olympic Stadium',
    'desc1' => 'Assorted GVs from outside the Olympic Stadium ahead of opening ceremony',
    'desc2' => 'Assorted GVs from outside the Olympic Stadium',
    'pic' => 'I0031343398691821I-0-1.jpg',
    'video' => 'I0031343398691821I.mp4',
  ),
  1343427486 => 
  array (
    'file' => 'I0041343393119544I.xml',
    'title' => 'Mears: I\'m going to enjoy it',
    'desc1' => 'Chris Mears talks about overcoming health issues and aiming for medals in the diving competition London 2012.',
    'desc2' => 'Press conference with diving hopeful Chris Mears',
    'pic' => 'I0041343393119544I-0-1.jpg',
    'video' => 'I0041343393119544I.mp4',
  ),
  1343427441 => 
  array (
    'file' => 'I0021343391731831I.xml',
    'title' => 'Saskia Clark looks ahead to the Games',
    'desc1' => 'Interview with Team GB Sailor Saskia Clark ahead of London 2012',
    'desc2' => 'Interview with Team GB Sailor Saskia Clark ahead of London 2012',
    'pic' => 'I0021343391731831I-0-1.jpg',
    'video' => 'I0021343391731831I.mp4',
  ),
  1343425586 => 
  array (
    'file' => 'I0011343390929074I.xml',
    'title' => 'Armitstead confident GB team will surprise',
    'desc1' => 'Lizzie Armitstead is adamant team unity and the potential to surprise will be strengths of Great Britain\'s women in Sunday\'s Olympic road race. Britain have options in their four-rider team, with Armitstead joined by 2008 Olympic champion Nicole Cooke, Emma Pooley and Lucy Martin. Following last September\'s World Championships, there was a public fall-out when Armitstead, having finished a disappointing seventh, accused fourth-placed finisher Cooke of riding "for herself". Armitstead had been the nominated leader.',
    'desc2' => 'Interview with cyclist Lizzie Armitstead',
    'pic' => 'I0011343390929074I-0-1.jpg',
    'video' => 'I0011343390929074I.mp4',
  ),
  1343424185 => 
  array (
    'file' => 'IZED1343385903559I.xml',
    'title' => 'Jackson named Australian Olympic flag bearer',
    'desc1' => 'Interview with Australian flag bearer Lauren Jackson',
    'desc2' => 'Interview with Australian flag bearer Lauren Jackson',
    'pic' => 'IZED1343385903559I-0-1.jpg',
    'video' => 'IZED1343385903559I.mp4',
  ),
  1343423667 => 
  array (
    'file' => 'I0051343419045380I.xml',
    'title' => 'CONCERT KICKS OFF GAMES CELEBRATION',
    'desc1' => 'Tens of thousands of people began their Olympic celebrations this evening at a huge concert in London\'s Hyde Park to coincide with the opening ceremony.The extravaganza featuring acts representing the nations of the UK kicked off with Scottish singer-songwriter Paolo Nutini, with chart-topping act Snow Patrol set to round off the event',
    'desc2' => 'GVs of crowd watching bands perform at Hyde Park concert to celebrate Olympic opening ceremony',
    'pic' => 'I0051343419045380I-0-1.jpg',
    'video' => 'I0051343419045380I.mp4',
  ),
  1343422503 => 
  array (
    'file' => 'Iyal1343415783604I.xml',
    'title' => 'QUEEN HAILS SPIRIT OF TOGETHERNESS',
    'desc1' => 'The Queen praised the Olympic ideal of "togetherness" tonight as she welcomed world leaders to the UK ahead of the London 2012 opening ceremony.',
    'desc2' => 'The Queen makes a speech about the Olympics, plus GVs of her meeting Michelle Obama and world leaders',
    'pic' => 'Iyal1343415783604I-0-1.jpg',
    'video' => 'Iyal1343415783604I.mp4',
  ),
  1343420732 => 
  array (
    'file' => 'Ills1343397297067I.xml',
    'title' => 'Mills eyes sailing competition',
    'desc1' => 'Mills and partner Saskia Clark are tipped for success on home waters this summer - something Clark admits she could not have envisaged not so long ago.',
    'desc2' => 'Sailor Hannah Mills eyes the opposition in Weymouth for the sailing competition',
    'pic' => 'Ills1343397297067I-0-1.jpg',
    'video' => 'Ills1343397297067I.mp4',
  ),
  1343419947 => 
  array (
    'file' => 'I0011343396328977I.xml',
    'title' => 'USA Basketball team \'honoured\' to be at London 2012',
    'desc1' => 'USA basketball team meet the media ahead of Olympics',
    'desc2' => 'USA basketball team meet the media ahead of the Olympics, quotes from Team USA managing director Jerry Colangelo and Coach Mike Krzyzewski',
    'pic' => 'I0011343396328977I-0-1.jpg',
    'video' => 'I0011343396328977I.mp4',
  ),
  1343419904 => 
  array (
    'file' => 'I108308210786561343402972000I.xml',
    'title' => 'STOP ATTACK ON ALEPPO, SYRIA TOLD',
    'desc1' => NULL,
    'desc2' => 'Syria should stop its assault on Aleppo, Ban Ki-Moon said during a visit to London today.The United Nations Secretary General, flanked by Foreign Secretary William Hague, called on Syrian President Bashar Assad to end the attack as Mr Hague warned of a massacre in the city.',
    'pic' => NULL,
    'video' => 'I108308210786561343402972000I-0-1.jpg',
  ),
  1343419546 => 
  array (
    'file' => 'I0031343392455136I.xml',
    'title' => 'Mills hopes for gold',
    'desc1' => 'Interviews with Olympic hopeful Hannah Mills',
    'desc2' => 'Interviews with Olympic hopeful Hannah Mills',
    'pic' => 'I0031343392455136I-0-1.jpg',
    'video' => 'I0031343392455136I.mp4',
  ),
  1343415974 => 
  array (
    'file' => 'I0041343411122446I.xml',
    'title' => 'SNOW PATROL AT OLYMPIC CONCERT',
    'desc1' => 'Snow Patrol are among the acts to perform at the BT London Live Opening Ceremony Celebration Concert in Hyde Park.',
    'desc2' => 'Snow Patrol frontman Gary Lightbody discusses appearance at Olympic opening ceremony celebration concert',
    'pic' => 'I0041343411122446I-0-1.jpg',
    'video' => 'I0041343411122446I.mp4',
  ),
  1343415402 => 
  array (
    'file' => 'Ipkg1343406098564I.xml',
    'title' => 'Daley relishes home support',
    'desc1' => 'Tom Daley is set to be one of Great Britain\'s most high-profile stars over the next fortnight, with the eyes of the nation set to fall on the 18-year-old as he bids to add an Olympic medal to an already decorated young career.',
    'desc2' => 'Team GB Olympic diver Tom Daley says having his family there to watch him will make all the difference',
    'pic' => 'Ipkg1343406098564I-27400-1.jpg',
    'video' => 'Ipkg1343406098564I.mp4',
  ),
  1343414982 => 
  array (
    'file' => 'Ipkg1343401692944I.xml',
    'title' => 'Kobe Bryant hopes to watch gymnastics',
    'desc1' => 'Team USA are expected to be the frontrunners in the basketball competition.',
    'desc2' => 'LA Lakers star Kobe Bryant says he hopes to watch as many sports as possible at the Olympics',
    'pic' => 'Ipkg1343401692944I-0-1.jpg',
    'video' => 'Ipkg1343401692944I.mp4',
  ),
  1343413942 => 
  array (
    'file' => 'I108285810784331343389234000I.xml',
    'title' => 'FIRST LADY DELIVERS MESSAGE TO CHILDREN',
    'desc1' => NULL,
    'desc2' => 'Being an Olympian is not just about winning gold medals and setting records, Michelle Obama said today, as she urged youngsters to stay healthy and "believe in" themselves.Olympic athletes are not simply born, but work hard for their success and refuse to give up, the US First Lady said at a Let\'s Move! event at the US Embassy in London.',
    'pic' => NULL,
    'video' => 'I108285810784331343389234000I-0-1.jpg',
  ),
  1343413601 => 
  array (
    'file' => 'Ipkg1343411329033I.xml',
    'title' => 'Terry charged by FA',
    'desc1' => 'A fortnight after being cleared in court of racially abusing QPR defender Ferdinand, Chelsea captain Terry faced being sanctioned by the FA over the same incident.Terry immediately denied the charge in a statement released to Press Association Sport, saying: "I deny the charge and I will be requesting the opportunity to attend the commission for a personal hearing."',
    'desc2' => 'John Terry was today charged with using alleged racist language towards Anton Ferdinand by the Football Association',
    'pic' => 'Ipkg1343411329033I-5440-1.jpg',
    'video' => 'Ipkg1343411329033I.mp4',
  ),
  1343413471 => 
  array (
    'file' => 'I0011343388592344I.xml',
    'title' => 'First Lady Michelle Obama addresses team USA',
    'desc1' => 'Michelle Obama addresses team USA followed by gvs of a meet and greet.US First Lady Michelle Obama addresses American athletes at their Olympic base in London today.Mrs Obama urged America\'s athletes to "have fun, breathe a bit, but also win", as she met them at their training base in east London. The First Lady is leading the US presidential delegation which includes a ringside seat at tonight\'s ceremony.',
    'desc2' => 'Michelle Obama addresses team USA followed by gvs of a meet and greet',
    'pic' => 'I0011343388592344I-0-1.jpg',
    'video' => 'I0011343388592344I.mp4',
  ),
  1343413102 => 
  array (
    'file' => 'I108310510786791343404983000I.xml',
    'title' => 'TERRY CHARGED BY FA',
    'desc1' => NULL,
    'desc2' => 'John Terry was today charged with using alleged racist language towards Anton Ferdinand by the Football Association.A fortnight after being cleared in court of racially abusing QPR defender Ferdinand, Chelsea captain Terry faced being sanctioned by the FA over the same incident.Terry immediately denied the charge in a statement released to Press Association Sport, saying: "I deny the charge and I will be requesting the opportunity to attend the commission for a personal hearing."',
    'pic' => NULL,
    'video' => 'I108310510786791343404983000I-0-1.jpg',
  ),
  1343411173 => 
  array (
    'file' => 'I0021343397405307I.xml',
    'title' => 'Chandler: It\'s been a long hard ride',
    'desc1' => 'American basketball stars speak to media at Olympic media: Deron Williams, Tyson Chandler and Kobe Bryant',
    'desc2' => 'American basketball stars speak to media at Olympic media: Deron Williams, Tyson Chandler and Kobe Bryant',
    'pic' => 'I0021343397405307I-0-1.jpg',
    'video' => 'I0021343397405307I.mp4',
  ),
  1343410659 => 
  array (
    'file' => 'I108288810784611343390132000I.xml',
    'title' => 'Torch arrives by boat at City Hall',
    'desc1' => NULL,
    'desc2' => 'Gvs of the Olympic Torch arriving by boat at City Hall and gvs of torchbearer Amber Charles on the Olympic rings floatilla',
    'pic' => NULL,
    'video' => 'I108288810784611343390132000I-300960-1.jpg',
  ),
  1343408857 => 
  array (
    'file' => 'I108295510785291343390132000I.xml',
    'title' => 'Torch arrives by boat at City Hall',
    'desc1' => NULL,
    'desc2' => 'Gvs of the Olympic Torch arriving by boat at City Hall and gvs of torchbearer Amber Charles on the Olympic rings floatilla',
    'pic' => NULL,
    'video' => 'I108295510785291343390132000I-300960-1.jpg',
  ),
  1343406315 => 
  array (
    'file' => 'Ison1343386238120I.xml',
    'title' => 'Jackson "humbled" to carry Australia\'s flag',
    'desc1' => 'The 31-year-old, who has been part of the Australian Opals team that has won silver medals at the last three Olympics, was handed the role by Australia Olympic Committee chef de mission Nick Green',
    'desc2' => 'Australian basketball player Lauren Jackson cannot wait to carry her country\'s flag at the opening ceremony',
    'pic' => 'Ison1343386238120I-0-1.jpg',
    'video' => 'Ison1343386238120I.mp4',
  ),
  1343404897 => 
  array (
    'file' => 'I108283410784081343389234000I.xml',
    'title' => 'FIRST LADY DELIVERS MESSAGE TO CHILDREN',
    'desc1' => NULL,
    'desc2' => 'Being an Olympian is not just about winning gold medals and setting records, Michelle Obama said today, as she urged youngsters to stay healthy and "believe in" themselves.Olympic athletes are not simply born, but work hard for their success and refuse to give up, the US First Lady said at a Let\'s Move! event at the US Embassy in London.',
    'pic' => NULL,
    'video' => 'I108283410784081343389234000I-0-1.jpg',
  ),
  1343402666 => 
  array (
    'file' => 'Ipkg1343399358756I.xml',
    'title' => 'Boyle previews opening ceremony',
    'desc1' => 'Seven years of planning will come to a head when Boyle\'s Isles of Wonder spectacular begins at 9pm and the Games get under way.',
    'desc2' => 'Danny Boyle dedicated his £27 million Olympics opening extravaganza to the 15,000 volunteers who will help bring it to life tonight as organisers said they expected a sell-out crowd',
    'pic' => 'Ipkg1343399358756I-28600-1.jpg',
    'video' => 'Ipkg1343399358756I.mp4',
  ),
  1343398696 => 
  array (
    'file' => 'I108276610783401343382390000I.xml',
    'title' => 'AIRPORT TWEET MAN IN APPEAL VICTORY',
    'desc1' => 'A man found guilty of sending a menacing tweet concerning Robin Hood Airport in South Yorkshire has won his challenge against conviction.Paul Chambers, 28, was fined £385 and ordered to pay £600 costs at Doncaster Magistrates\' Court in May 2010 after being convicted of sending "a message of a menacing character", contrary to provisions of the 2003 Communications Act.He said he sent the tweet to his 600 followers in a moment of frustration after Robin Hood Airport was closed by snow in January 2010, and never thought anyone would take his "silly joke" seriously.It read: "Crap! Robin Hood Airport is closed. You\'ve got a week and a bit to get your s*** together, otherwise I\'m blowing the airport sky high!"The Lord Chief Justice Lord Judge, sitting with Mr Justice Owen and Mr Justice Griffith Williams, said: "We have concluded that, on an objective assessment, the decision of the Crown Court that this \'tweet\' constituted or included a message of a menacing character was not open to it."On this basis, the appeal against conviction must be allowed."Mr Chambers, who has received backing from celebrities such as broadcaster Stephen Fry and comedian Al Murray, said later: "I am relieved, vindicated - it is ridiculous it ever got this far.',
    'desc2' => 'GVs of Paul Chambers outside the court, and shots of his supporters comedian Al Murray and Tory MP Louise Mensch. Plus Interviews with Al Murray and Paul Chambers',
    'pic' => 'I108276610783401343382390000I-0-1.jpg',
    'video' => 'I108276610783401343382390000I.mp4',
  ),
  1343394324 => 
  array (
    'file' => 'I108274610783201343373316000I.xml',
    'title' => 'Bells ring out for the Olympic Games',
    'desc1' => NULL,
    'desc2' => 'The London landmark was joined by hundreds of churches and other organisations across the nation as bell ringers greeted the official start of London 2012. The hour bell of the landmark Palace of Westminster clock began chiming at 8.12am. It pealed 40 times over the following three minutes. Special permission had to be gained for the hour bell at the Palace of Westminster to be allowed to toll out of its regular sequence.',
    'pic' => NULL,
    'video' => 'I108274610783201343373316000I-0-1.jpg',
  ),
  1343390520 => 
  array (
    'file' => 'I108204410776181343306001000I.xml',
    'title' => 'ABSOLUTELY FABULOUS STARS CARRY OLYMPIC TORCH',
    'desc1' => NULL,
    'desc2' => 'Joanna Lumley and Jennifer Saunders carry the Olympic torch through Sloane Square.',
    'pic' => NULL,
    'video' => 'I108204410776181343306001000I-0-1.jpg',
  ),
  1343387789 => 
  array (
    'file' => 'I108270110782751343373316000I.xml',
    'title' => 'Bells ring out for the Olympic Games',
    'desc1' => NULL,
    'desc2' => 'The London landmark was joined by hundreds of churches and other organisations across the nation as bell ringers greeted the official start of London 2012. The hour bell of the landmark Palace of Westminster clock began chiming at 8.12am. It pealed 40 times over the following three minutes. Special permission had to be gained for the hour bell at the Palace of Westminster to be allowed to toll out of its regular sequence.',
    'pic' => NULL,
    'video' => 'I108270110782751343373316000I-0-1.jpg',
  ),
  1343377890 => 
  array (
    'file' => 'Ivic1343372607656I.xml',
    'title' => 'Djokovic eyes Olympic glory',
    'desc1' => 'The 25-year-old wept after winning a bronze medal for Serbia with victory over James Blake in Beijing four years ago, and he is hoping to climb to the top step of the podium when the medals are decided at Wimbledon next weekend.',
    'desc2' => 'Novak Djokovic is targeting Olympic gold in London and his place among the sporting immortals.',
    'pic' => 'Ivic1343372607656I-0-1.jpg',
    'video' => 'Ivic1343372607656I.mp4',
  ),
  1343354120 => 
  array (
    'file' => 'I108245310780271343322399000I.xml',
    'title' => 'Michael Phelps ignoring medal tally',
    'desc1' => NULL,
    'desc2' => 'Press conference with USA swimmer Michael Phelps.',
    'pic' => NULL,
    'video' => 'I108245310780271343322399000I-0-1.jpg',
  ),
  1343352181 => 
  array (
    'file' => 'I108222910778031343312749000I.xml',
    'title' => 'David Beckham visits Downing Street - GVs',
    'desc1' => NULL,
    'desc2' => 'David Beckham posed outside Downing Street today before meeting the Prime Minister to discuss how to combat child malnutrition.',
    'pic' => NULL,
    'video' => 'I108222910778031343312749000I-0-1.jpg',
  ),
  1343351909 => 
  array (
    'file' => 'Iage1343313805088I.xml',
    'title' => '\'Psycho\' guilty of murder',
    'desc1' => 'A killer who called himself \'Psycho\' has been found guilty of murdering an Indian student at random.Kiaran Stapleton walked up to stranger Anuj Bidve, 23, in the street in Salford, Greater Manchester, and shot him in the head at point blank range.Stapleton, 21 had admitted manslaughter on the grounds of diminished responsibility but a jury at Manchester Crown Court rejected that argument and convicted him of murder.',
    'desc2' => 'Kiaran Stapleton has been found guilty of murdering Anuj Bidve in a random attack last Boxing Day',
    'pic' => 'Iage1343313805088I-14240-1.jpg',
    'video' => 'Iage1343313805088I.mp4',
  ),
  1343351097 => 
  array (
    'file' => 'I0011343319391622I.xml',
    'title' => '\'PSYCHO\' GUILTY OF STUDENT\'S MURDER - Family statement',
    'desc1' => 'A killer who called himself \'Psycho\' has been found guilty of murdering an Indian student at random in Salford.Kiaran Stapleton walked up to stranger Anuj Bidve, 23, in the street and shot him in the head at point blank range.Stapleton, 21 had admitted manslaughter on the grounds of diminished responsibility but a jury at Manchester Crown Court rejected that argument and convicted him of murder.',
    'desc2' => 'Statement from Subhash Bidve, father of Anuj Bidve, following the conviction of Kiaran Stapleton for murdering Anuj',
    'pic' => 'I0011343319391622I-0-1.jpg',
    'video' => 'I0011343319391622I.mp4',
  ),
  1343350907 => 
  array (
    'file' => 'I108240310779771343319321000I.xml',
    'title' => 'Serbian press conference',
    'desc1' => 'A press conference with the Serbian Olympic squad.',
    'desc2' => 'Press conference with the Serbian Olympic team including tennis star Novak Djokovic and swimmer Milorad Cavic who had a close race with Michael Phelps in Beijing',
    'pic' => 'I108240310779771343319321000I-0-1.jpg',
    'video' => 'I108240310779771343319321000I.mp4',
  ),
  1343349668 => 
  array (
    'file' => 'I108223510778111343310537000I.xml',
    'title' => 'Mitt Romney meets Cameron',
    'desc1' => 'Gvs of David Cameron meeting Republican US Presidential candidate Mitt Romney at Downing Street',
    'desc2' => 'Gvs of David Cameron meeting Republican US Presidential candidate Mitt Romney at Downing Street',
    'pic' => 'I108223510778111343310537000I-0-1.jpg',
    'video' => 'I108223510778111343310537000I.mp4',
  ),
  1343348126 => 
  array (
    'file' => 'I108226010778341343304558000I.xml',
    'title' => 'IOC briefing on the flag debacle',
    'desc1' => 'Briefing with IOC communications director Mark Adams - comment on the flag issue at Hampden last night',
    'desc2' => 'Briefing with IOC communications director Mark Adams - comment on the flag issue at Hampden last night',
    'pic' => 'I108226010778341343304558000I-0-1.jpg',
    'video' => 'I108226010778341343304558000I.mp4',
  ),
  1343343707 => 
  array (
    'file' => 'I108254810781271343327542000I.xml',
    'title' => 'Usain Bolt press conference',
    'desc1' => '1- He will also run any leg in the relay2- He might run the 4x400m relay if he feels up for it3- He is always fit and ready, it\'s all about the championships for him4- He is just focussed on winning5- It won\'t be the end of the world if he loses, he never thinks of losing6- He is not worried about his starts7- He is all about defending his titles8- He will be disappointed if he loses any race9- This could be the fastest 100m final ever10- He is still training just as closely with Yohan Blake11- Nothing has changed for him this time compare with 2008',
    'desc2' => 'Press conference with Jamaican triple Olympic gold medallist Usain Bolt',
    'pic' => 'I108254810781271343327542000I-0-1.jpg',
    'video' => 'I108254810781271343327542000I.mp4',
  ),
  1343343137 => 
  array (
    'file' => 'I0021343319863499I.xml',
    'title' => '\'PSYCHO\' GUILTY OF STUDENT\'S MURDER - CPS and Police',
    'desc1' => 'A killer who called himself \'Psycho\' has been found guilty of murdering an Indian student at random in Salford.Kiaran Stapleton walked up to stranger Anuj Bidve, 23, in the street and shot him in the head at point blank range.Stapleton, 21 had admitted manslaughter on the grounds of diminished responsibility but a jury at Manchester Crown Court rejected that argument and convicted him of murder.',
    'desc2' => 'Statements from DCS Mary Doyle from Greater Manchester Police and Nazir Afzal from the CPS plus GVs of a police van carrying Kiaran Stapleton',
    'pic' => 'I0021343319863499I-0-1.jpg',
    'video' => 'I0021343319863499I.mp4',
  ),
  1343341600 => 
  array (
    'file' => 'Iage1343333233512I.xml',
    'title' => 'Bolt: I\'m ready',
    'desc1' => 'Press conference with Jamaican sprinter Usain Bolt.',
    'desc2' => 'Usain Bolt says he is ready to defend his Olympic crown and dismisses talk of a rift with friend and rival Yohan Blake',
    'pic' => 'Iage1343333233512I-93680-1.jpg',
    'video' => 'Iage1343333233512I.mp4',
  ),
  1343340597 => 
  array (
    'file' => 'I108259810781721343331840000I.xml',
    'title' => 'USA Benefit Gala',
    'desc1' => NULL,
    'desc2' => 'US Olympic Committee to host Benefit Gala at USA House for US Olympic and Paralympic Teams on eve of London 2012 Opening Ceremony.',
    'pic' => NULL,
    'video' => 'I108259810781721343331840000I-0-1.jpg',
  ),
  1343337618 => 
  array (
    'file' => 'Iage1343331926548I.xml',
    'title' => 'Federer: Nadal is a miss',
    'desc1' => 'Press conference with Switzerland\'s Roger Federer ahead of the Olympics.',
    'desc2' => 'Roger Federer on Rafa Nadal pulling out of the Olympics and where his doubles gold medal ranks in his achievements',
    'pic' => 'Iage1343331926548I-77040-1.jpg',
    'video' => 'Iage1343331926548I.mp4',
  ),
  1343337232 => 
  array (
    'file' => 'I108251010780841343325573000I.xml',
    'title' => 'ROYAL WELCOME FOR TORCH AT PALACE',
    'desc1' => NULL,
    'desc2' => 'The Duke and Duchess of Cambridge and Prince Harry welcomed the Olympic Torch to Buckingham Palace tonight as it neared the end of its journey.William, Kate and Harry watched as the flame was carried into the grounds of the Queen\'s London home.It has travelled hundreds of miles across the UK in the run-up to the Olympics which will be officially opened by the Queen tomorrow during a spectacular ceremony.',
    'pic' => NULL,
    'video' => 'I108251010780841343325573000I-0-1.jpg',
  ),
  1343337104 => 
  array (
    'file' => 'I108248610780601343323233000I.xml',
    'title' => 'The torch arrives at Downing Street',
    'desc1' => NULL,
    'desc2' => 'Mr Cameron and wife Samantha watched a relay exchange take place on his doorstep in Downing Street tonight as war hero Kate Nesbitt handed over the flame to pensioner Florence Rowe. Miss Nesbitt, 24, from Plymouth, Devon, was the first woman in the Royal Navy to be awarded the Military Cross. She was given the award in honour of her bravery in administering emergency medical treatment to injured servicemen while under enemy fire during an ambush in Afghanistan in March 2009.',
    'pic' => NULL,
    'video' => 'I108248610780601343323233000I-0-1.jpg',
  ),
  1343335838 => 
  array (
    'file' => 'I0061343327559177I.xml',
    'title' => 'Roger Federer press conference',
    'desc1' => 'Tennis legend Roger Federer discusses playing in the London 2012 Olympics.',
    'desc2' => 'Press conference with Wimbledon champion and world number one Roger Federer',
    'pic' => 'I0061343327559177I-0-1.jpg',
    'video' => 'I0061343327559177I.mp4',
  ),
  1343335240 => 
  array (
    'file' => 'I108255510781301343327423000I.xml',
    'title' => 'Bolt and Powell GVs',
    'desc1' => 'GVs from a press conference with Jamaican sprinters Usain Bolt and Asafa Powell.',
    'desc2' => 'GVs of Jamaican sprinters Usain Bolt and Asafa Powell',
    'pic' => 'I108255510781301343327423000I-0-1.jpg',
    'video' => 'I108255510781301343327423000I.mp4',
  ),
  1343332925 => 
  array (
    'file' => 'I108213210777061343308905000I.xml',
    'title' => 'LAWYERS TO REVIEW RIOTS TRIAL',
    'desc1' => NULL,
    'desc2' => 'The father of one of three men killed during last summer\'s riots in Birmingham has said lawyers have been instructed to review a criminal trial which saw eight men cleared of murder - leaving the victims\' families in ``utter shock and despair\'\'. Haroon Jahan and brothers Shazad Ali and Abdul Musavir died after being hit by a Mazda car as they tried to protect shops in Winson Green from looters last August. Eight men were accused of murdering the three friends in a ``modern day chariot charge\'\' but were acquitted by a jury last Thursday, following a lengthy trial at Birmingham Crown Court.',
    'pic' => NULL,
    'video' => 'I108213210777061343308905000I-0-1.jpg',
  ),
  1343329647 => 
  array (
    'file' => 'I0021343302002310I.xml',
    'title' => 'Ian Thorpe press conference',
    'desc1' => 'INT with Australian swimming gold medallist Ian Thorpe',
    'desc2' => 'INT with Australian swimming gold medallist Ian Thorpe',
    'pic' => 'I0021343302002310I-0-1.jpg',
    'video' => 'I0021343302002310I.mp4',
  ),
  1343328297 => 
  array (
    'file' => 'Iage1343323501281I.xml',
    'title' => 'Phelps playing it cool',
    'desc1' => 'Press conference with USA swimmer Michael Phelps ahead of his final Olympics.',
    'desc2' => '14-time Olympic champion Michael Phelps on his medal haul, his final games and dealing with the pressure',
    'pic' => 'Iage1343323501281I-16120-1.jpg',
    'video' => 'Iage1343323501281I.mp4',
  ),
  1343326267 => 
  array (
    'file' => 'Iech1343318016026I.xml',
    'title' => 'CAMERON MEETS U.S. CANDIDATE ROMNEY',
    'desc1' => 'Prime Minister David Cameron today met the Republican candidate for US president Mitt Romney during his campaigning and fundraising visit to London.The meeting in 10 Downing Street came after Mr Romney appeared to question Britain\'s readiness to host the Olympic Games, telling a US television station there were "disconcerting" signs and it was "hard to know just how well it will turn out".He today told the Prime Minister he was inspired by the enthusiasm of the British people who have lined the route of the torch relay ahead of tomorrow\'s opening ceremony.',
    'desc2' => 'GVs of US Presidential candidate Mitt Romney and a speech plus clips addressing the Olympics and foreign policy',
    'pic' => 'Iech1343318016026I-0-1.jpg',
    'video' => 'Iech1343318016026I.mp4',
  ),
  1343320224 => 
  array (
    'file' => 'Iage1343309916671I.xml',
    'title' => 'BOA on Bale ban talk',
    'desc1' => 'British Olympic Association chairman Lord Moynihan today criticised Sepp Blatter for suggesting Gareth Bale could be banned from playing for Tottenham during London 2012.Moynihan claimed he could "understand" the anger of Great Britain fans who saw Bale pull out of the Games with a back injury only for the winger to take part in a pre-season friendly for Spurs yesterday.That was almost two full days before Team GB\'s opening Olympic match against Senegal tonight and sparked widespread criticism of the 23-year-old.',
    'desc2' => 'The British Olympic Association react to Sepp Blatter\'s suggestion Gareth Bale should be banned for playing for Spurs after pulling out of Team GB with an injury',
    'pic' => 'Iage1343309916671I-18640-1.jpg',
    'video' => 'Iage1343309916671I.mp4',
  ),
  1343319283 => 
  array (
    'file' => 'I108198810775611343304068000I.xml',
    'title' => 'PM: Flags mix-up \'honest mistake\'',
    'desc1' => NULL,
    'desc2' => 'David Cameron has said every effort will be made to ensure mistakes such as Wednesday night\'s Olympic flag mix-up never happen again.The Prime Minister said the error in which the South Korean flag was wrongly shown instead of sworn enemy North Korea\'s at a football game was "an honest mistake" for which an apology has been issued, adding: "Every effort will be taken to make sure this won\'t happen again."The North Korean women\'s team staged a protest ahead of their match with Colombia after the South Korean flag was wrongly shown on a big screen at Glasgow\'s Hampden Park stadium. As a consequence, the players walked off the pitch and delayed the match by an hour.',
    'pic' => NULL,
    'video' => 'I108198810775611343304068000I-0-1.jpg',
  ),
  1343317752 => 
  array (
    'file' => 'I108211710776911343307981000I.xml',
    'title' => '\'PSYCHO\' GUILTY OF STUDENT\'S MURDER',
    'desc1' => NULL,
    'desc2' => 'A killer who called himself \'Psycho\' has been found guilty of murdering an Indian student at random in Salford. Kiaran Stapleton walked up to stranger Anuj Bidve, 23, in the street and shot him in the head at point blank range. Stapleton, 21 had admitted manslaughter on the grounds of diminished responsibility but a jury at Manchester Crown Court rejected that argument and convicted him of murder.',
    'pic' => NULL,
    'video' => 'I108211710776911343307981000I-0-1.jpg',
  ),
  1343316804 => 
  array (
    'file' => 'Iage1343308929318I.xml',
    'title' => 'BOA: No Idowu rift',
    'desc1' => 'The British Olympic Association today denied they were the latest governing body to have fallen out with Phillips Idowu after Charles van Commenee insisted he would never apologise to the triple-jumper.BOA chairman Lord Moynihan dismissed talk of a rift with Idowu after the organisation revealed they had asked to see his medical records in order to assess his fitness for London 2012.Idowu was described by his agent as "incredibly disappointed and surprised" by the BOA\'s decision and had still to comply with their request this morning.',
    'desc2' => 'Team GB and the BOA discuss the ongoing mystery surrounding the fitness of triple jumper Phillips Idowu',
    'pic' => 'Iage1343308929318I-26320-1.jpg',
    'video' => 'Iage1343308929318I.mp4',
  ),
  1343314458 => 
  array (
    'file' => 'Irpe1343306403746I.xml',
    'title' => 'Thorpe on GB chances',
    'desc1' => 'Australian swimming legend Ian Thorpe discusses missing out on the team for London 2012 and who he thinks will do well at the Games.',
    'desc2' => 'Olympic swimming legend Ian Thorpe discusses missing out on London 2012 and Team GB\'s chances in the pool',
    'pic' => 'Irpe1343306403746I-87800-1.jpg',
    'video' => 'Irpe1343306403746I.mp4',
  ),
  1343313888 => 
  array (
    'file' => 'I0031343232589023I.xml',
    'title' => 'Arena: The match was a \'positive\' experience',
    'desc1' => 'Arena talks about the draw against Spurs and whether or not David Beckham will be back from London in time for the next MSL match.',
    'desc2' => 'Press conference with Bruce Arena, LA Galaxy head coach after their match against Tottenham 1-1',
    'pic' => 'I0031343232589023I-0-1.jpg',
    'video' => 'I0031343232589023I.mp4',
  ),
  1343311940 => 
  array (
    'file' => 'I108189210774661343298250000I.xml',
    'title' => 'Hunt on Phillips Idowu',
    'desc1' => NULL,
    'desc2' => 'Press conference with Team GB chef de mission Andy Hunt and BOA chairman Colin Moynihan (Hunt first in edit) plus cutaways. They talk about Phillips Idowu',
    'pic' => NULL,
    'video' => 'I108189210774661343298250000I-0-1.jpg',
  ),
  1343310553 => 
  array (
    'file' => 'Ipkg1343301597872I.xml',
    'title' => 'Badminton squad on the move',
    'desc1' => 'Just two days after arriving in Stratford ahead of the Games, the likes of mixed doubles hopefuls Chris Adcock and Imogen Bankier are leaving for a hotel closer to their Wembley Arena venue.',
    'desc2' => 'Great Britain\'s badminton players are checking out of the Olympic village as they step up their London 2012 preparations.',
    'pic' => 'Ipkg1343301597872I-41720-1.jpg',
    'video' => 'Ipkg1343301597872I.mp4',
  ),
  1343310445 => 
  array (
    'file' => 'Iage1343305808447I.xml',
    'title' => 'PM: Flag mix-up \'honest mistake\'',
    'desc1' => 'David Cameron has said every effort will be made to ensure mistakes such as last night\'s Olympic flag mix-up never happen again.The Prime Minister said the error in which the South Korean flag was wrongly shown instead of sworn enemy North Korea\'s at a football game was "an honest mistake" for which an apology has been issued."Every effort will be taken to make sure this won\'t happen again," Mr Cameron said.',
    'desc2' => 'David Cameron says the Korean flag mix-up was an accident',
    'pic' => 'Iage1343305808447I-19200-1.jpg',
    'video' => 'Iage1343305808447I.mp4',
  ),
  1343307726 => 
  array (
    'file' => 'I108190110774741343299259000I.xml',
    'title' => 'Moynihan: Flag was a mistake',
    'desc1' => NULL,
    'desc2' => 'Press conference with Team GB chef de mission Andy Hunt and BOA chairman Colin Moynihan. Moynihan speaks first in footage. On Bale, North Korea flag and Steve Redgrave as the man to light the flame at the ceremony',
    'pic' => NULL,
    'video' => 'I108190110774741343299259000I-0-1.jpg',
  ),
  1343307680 => 
  array (
    'file' => 'I108189310774651343294536000I.xml',
    'title' => 'David Walliams runs with Olympic torch',
    'desc1' => 'Walliams said he was "humbled" by the honour and was thrilled he was allowed to keep the tracksuit',
    'desc2' => 'Little Britain comedian David Walliams carried the Olympic torch in Islington',
    'pic' => 'I108189310774651343294536000I-30960-1.jpg',
    'video' => 'I108189310774651343294536000I.mp4',
  ),
  1343305886 => 
  array (
    'file' => 'I108183010774031343292889000I.xml',
    'title' => 'Walliams carries the torch',
    'desc1' => NULL,
    'desc2' => 'Gvs of David Walliams carrying the Olympic torch in Islington plus an interview with him',
    'pic' => NULL,
    'video' => 'I108183010774031343292889000I-0-1.jpg',
  ),
  1343304403 => 
  array (
    'file' => 'I0041343234472361I.xml',
    'title' => 'Jonassen: We\'re ready',
    'desc1' => 'GB Badminton pre-tournament press conference with Coach Kenneth Jonassen, Chris Adcock and Imogen Bankier',
    'desc2' => 'GB Badminton pre-tournament press conference with Coach Kenneth Jonassen, Chris Adcock and Imogen Bankier',
    'pic' => 'I0041343234472361I-0-1.jpg',
    'video' => 'I0041343234472361I.mp4',
  ),
  1343298929 => 
  array (
    'file' => 'Iaxy1343292902983I.xml',
    'title' => 'Arena: We hope Beckham will be back for next MLS match',
    'desc1' => 'Arena says the match was good for both clubs and they are pleased with the final result. He says he cannot confirm if David Beckham will be back from London in time for the next MLS match.',
    'desc2' => 'LA Galaxy head coach Bruce Arena says their 1-1 draw against Spurs was a great opportunity for his team to play a world class team',
    'pic' => 'Iaxy1343292902983I-10120-1.jpg',
    'video' => 'Iaxy1343292902983I.mp4',
  ),
  1343282290 => 
  array (
    'file' => 'I108095610765291343206649000I.xml',
    'title' => 'BLAIR IN STAFF PRAYER ADMISSION',
    'desc1' => NULL,
    'desc2' => 'He once denied that he had prayed with former US president George Bush - but today Tony Blair admitted he once ordered members of staff to do just that. The former prime minister, taking part in a debate on the role of religion in public life alongside the Archbishop of Canterbury, said he had done so despite the now-famous instruction from press secretary Alastair Campbell that: ``You don\'t do God\'\'. He told the audience of 450 people in Westminster, central London: ``I remember the Salvation Army coming to see me when I was leader of the opposition.',
    'pic' => NULL,
    'video' => 'I108095610765291343206649000I-0-1.jpg',
  ),
  1343278669 => 
  array (
    'file' => 'Iing1343209596817I.xml',
    'title' => 'COOKE TIPPED FOR TIMELY RETURN TO FORM',
    'desc1' => 'The 29-year-old from Swansea won gold by the Great Wall of China in Beijing four years\' ago and the World Championships later that year, but has been in indifferent form of late.Lizzie Armitstead was the nominated leader at last September\'s World Championships in Copenhagen and after a disappointing finish there was a public spat with Cooke.',
    'desc2' => 'Defending champion Nicole Cooke has been tipped to rise to the occasion of the London 2012 Olympic Games road race on Sunday.',
    'pic' => 'Iing1343209596817I-59880-1.jpg',
    'video' => 'Iing1343209596817I.mp4',
  ),
  1343278412 => 
  array (
    'file' => 'I108102610765991343210154000I.xml',
    'title' => 'FINCH WAITS ON INJURED PAIR',
    'desc1' => NULL,
    'desc2' => 'Great Britain coach Chris Finch revealed injured duo Mike Lenzly and Dan Clark are ``50/50 at best\'\' to be fit for the Olympic Games. Lenzly has a torn calf muscle while Finch said Clark\'s ankle sprain is a bad one, with Friday the deadline for a decision to be made on their availability. ``Mike and Dan are both going through pretty extensive fitness testing today,\'\' Finch said.',
    'pic' => NULL,
    'video' => 'I108102610765991343210154000I-0-1.jpg',
  ),
  1343277863 => 
  array (
    'file' => 'I108100410765771343209235000I.xml',
    'title' => 'London enjoys long-awaited sunshine',
    'desc1' => NULL,
    'desc2' => 'Britain is basking in more sunshine and high temperatures as the weather continues to take a turn for the better.Forecasters did warn however, that showers are expected in the capital on Friday but they could well have cleared by the time the Olympic ceremony starts.',
    'pic' => NULL,
    'video' => 'I108100410765771343209235000I-0-1.jpg',
  ),
  1343277259 => 
  array (
    'file' => 'I108110110766741343213996000I.xml',
    'title' => 'GB swimmers will not attend opening ceremony',
    'desc1' => NULL,
    'desc2' => 'Team GB\'s swimming team will not attend the opening ceremony of the 2012 Games on Friday night due to the fear of it having a negative impact on their performance.',
    'pic' => NULL,
    'video' => 'I108110110766741343213996000I-0-1.jpg',
  ),
  1343276858 => 
  array (
    'file' => 'I108108810766611343213514000I.xml',
    'title' => 'SCOTLAND TO ALLOW GAY MARRIAGE',
    'desc1' => NULL,
    'desc2' => 'Scotland could be the first part of the UK to allow same-sex couples to marry. Deputy First Minister Nicola Sturgeon announced that the Scottish Government plans to bring forward legislation which would permit it. The controversial issue has been backed by politicians from all the main parties north of the border, as well as equality campaigners, but has attracted fierce criticism from some religious organisations.',
    'pic' => NULL,
    'video' => 'I108108810766611343213514000I-0-1.jpg',
  ),
  1343276345 => 
  array (
    'file' => 'I108111510766881343214406000I.xml',
    'title' => 'BORDER STAFF STRIKE CANCELLED',
    'desc1' => NULL,
    'desc2' => 'A planned strike by Home Office staff including immigration officers in a row over jobs and pay has been called off after progress in peace talks. Members of the Public and Commercial Services union were due to stage a 24-hour walkout tomorrow, the eve of the opening ceremony of the Olympics.The Government had announced legal action to try to prevent the strike going ahead.',
    'pic' => NULL,
    'video' => 'I108111510766881343214406000I-0-1.jpg',
  ),
  1343275049 => 
  array (
    'file' => 'I108109810766711343211783000I.xml',
    'title' => 'Finch waits on injured pair',
    'desc1' => 'Lenzly has a torn calf muscle while Finch said Clark\'s ankle sprain is a bad one, with Friday the deadline for a decision to be made on their availability.',
    'desc2' => 'Great Britain coach Chris Finch revealed injured duo Mike Lenzly and Dan Clark are "50/50 at best" to be fit for the Olympic Games.',
    'pic' => 'I108109810766711343211783000I-42240-1.jpg',
    'video' => 'I108109810766711343211783000I.mp4',
  ),
  1343274832 => 
  array (
    'file' => 'I108109510766681343206021000I.xml',
    'title' => 'COOKE TIPPED FOR TIMELY RETURN TO FORM',
    'desc1' => 'The 29-year-old from Swansea won gold by the Great Wall of China in Beijing four years\' ago and the World Championships later that year, but has been in indifferent form of late.Lizzie Armitstead was the nominated leader at last September\'s World Championships in Copenhagen and after a disappointing finish there was a public spat with Cooke.',
    'desc2' => 'Defending champion Nicole Cooke has been tipped to rise to the occasion of the London 2012 Olympic Games road race on Sunday.',
    'pic' => 'I108109510766681343206021000I-59880-1.jpg',
    'video' => 'I108109510766681343206021000I.mp4',
  ),
  1343273506 => 
  array (
    'file' => 'I108115810767311343216105000I.xml',
    'title' => 'Tancock looks ahead to Games',
    'desc1' => NULL,
    'desc2' => 'Team GB swimmer Liam Tancock looks ahead to the Games and paints a picture of what it is like to live in the Olympic Village.',
    'pic' => NULL,
    'video' => 'I108115810767311343216105000I-0-1.jpg',
  ),
  1343273318 => 
  array (
    'file' => 'Iage1343217035196I.xml',
    'title' => 'AVB on Modric future',
    'desc1' => 'Tottenham manager Andre Villas-Boas discusses Luka Modric\'s apology for missing training sessions and his side\'s 1-1 draw with LA Galaxy.',
    'desc2' => 'Andre Villas-Boas discusses Luka Modric\'s apology for missing training and Spurs\' 1-1 draw with LA Galaxy',
    'pic' => 'Iage1343217035196I-32760-1.jpg',
    'video' => 'Iage1343217035196I.mp4',
  ),
  1343272905 => 
  array (
    'file' => 'I0021343217825238I.xml',
    'title' => 'Scotland to allow gay marriage',
    'desc1' => 'Scotland could be the first part of the UK to allow same-sex couples to marry.Deputy First Minister Nicola Sturgeon announced that the Scottish Government plans to bring forward legislation which would permit it.The controversial issue has been backed by politicians from all the main parties north of the border, as well as equality campaigners, but has attracted fierce criticism from some religious organisations.',
    'desc2' => 'Nicola Sturgeon announces the Scottish Government intends to legislate to allow same-sex couples to marry.',
    'pic' => 'I0021343217825238I-0-1.jpg',
    'video' => 'I0021343217825238I.mp4',
  ),
  1343272821 => 
  array (
    'file' => 'I108120410767771343217505000I.xml',
    'title' => 'Team USA beach volleyball team press conference',
    'desc1' => NULL,
    'desc2' => 'Team USA beach volleyball players look ahead to Games.',
    'pic' => NULL,
    'video' => 'I108120410767771343217505000I-0-1.jpg',
  ),
  1343272493 => 
  array (
    'file' => 'I108133410769071343220648000I.xml',
    'title' => 'PM \'CONFIDENT\' SECURITY \'ON TRACK\'',
    'desc1' => NULL,
    'desc2' => 'David Cameron has said he is confident that security preparations for the London Olympics are on schedule. On a visit to HMS Ocean, which is berthed at Greenwich, east London, for the Games, the Prime Minister also paid tribute to the military personnel on duty. When questioned about security preparations, Mr Cameron replied: ``I\'m confident they\'re on track.',
    'pic' => NULL,
    'video' => 'I108133410769071343220648000I-0-1.jpg',
  ),
  1343271410 => 
  array (
    'file' => 'I108123710768101343218479000I.xml',
    'title' => 'TRIBUTES PAID TO RIVER DEATH TEEN',
    'desc1' => NULL,
    'desc2' => 'Tributes have been paid to a teenager who died after getting into difficulty while swimming in a river. Aaron Burgess, who was named locally, was recovered by police diving teams late last night.The 14-year-old, from Longwell Green, was cooling off in a stretch of the River Avon between Bristol and Bath yesterday evening when he disappeared under the water and did not resurface.',
    'pic' => NULL,
    'video' => 'I108123710768101343218479000I-0-1.jpg',
  ),
  1343269468 => 
  array (
    'file' => 'I0021343231725159I.xml',
    'title' => 'USA Fencing team',
    'desc1' => 'The USA fencing teams hold a press conference at the Olympic Park ahead of the opening of the Games.',
    'desc2' => 'Press conference and cutaways with the USA men\'s and women\'s fencing teams (in order) Miles Chamley-Watson, Race Imboden and Nicole Ross',
    'pic' => 'I0021343231725159I-0-1.jpg',
    'video' => 'I0021343231725159I.mp4',
  ),
  1343269008 => 
  array (
    'file' => 'I108124710768201343218826000I.xml',
    'title' => 'SAME-SEX MARRIAGE LAW TO PROGRESS',
    'desc1' => NULL,
    'desc2' => 'The Scottish Government said it plans to legalise same-sex marriage, with Deputy First Minister Nicola Sturgeon insisting the controversial move is the ``right thing to do\'\'.Nearly two-thirds of all people in Scotland who responded to a consultation on the issue said they are against the change and many religious groups, including the Catholic Church and the Church of Scotland, are bitterly opposed to it. Ms Sturgeon insisted churches, and individuals within them, would not have to conduct same-sex marriages if they do not agree with them.',
    'pic' => NULL,
    'video' => 'I108124710768201343218826000I-0-1.jpg',
  ),
  1343267654 => 
  array (
    'file' => 'I108134410769171343221118000I.xml',
    'title' => 'TRIBUTES PAID TO RIVER DEATH TEEN',
    'desc1' => NULL,
    'desc2' => 'Tributes have been paid to a teenager who died after getting into difficulty while swimming in the River Avon. Aaron Burgess, who was named locally, was recovered by police diving teams.The 14-year-old, from Longwell Green, was cooling off in a stretch of the River Avon between Bristol and Bath  when he disappeared under the water and did not resurface.',
    'pic' => NULL,
    'video' => 'I108134410769171343221118000I-0-1.jpg',
  ),
  1343266822 => 
  array (
    'file' => 'I108140910769821343223061000I.xml',
    'title' => 'Ticket holders can watch diving on TV screens',
    'desc1' => NULL,
    'desc2' => 'People who have tickets for diving at the Olympics may have a restricted view and will have to settle for watching the action on TV screens, according to Debbie Jevans, Director of Sport for LOCOG.',
    'pic' => NULL,
    'video' => 'I108140910769821343223061000I-0-1.jpg',
  ),
  1343265737 => 
  array (
    'file' => 'I108146710770401343226495000I.xml',
    'title' => 'FAMILY TRIBUTE TO LANDSLIDE VICTIM',
    'desc1' => NULL,
    'desc2' => 'Family and friends of the victim of a Dorset landslide have paid tribute to a ``genuine, warm and funny young woman\'\'. Charlotte Blackman died after being crushed as 400 tonnes of rock fell on her during a trip to the beach. The 22-year-old, from Heanor, Derbyshire, was on holiday with her family and boyfriend when part of a 160ft-high cliff-face on the Jurassic Coast collapsed and sent a mountain of rocks plummeting onto the sand below.',
    'pic' => NULL,
    'video' => 'I108146710770401343226495000I-0-1.jpg',
  ),
  1343265059 => 
  array (
    'file' => 'I108141610769891343224107000I.xml',
    'title' => 'Women\'s hockey players ready for Games',
    'desc1' => NULL,
    'desc2' => 'Team GB\'s women\'s hockey team ready for Games.',
    'pic' => NULL,
    'video' => 'I108141610769891343224107000I-0-1.jpg',
  ),
  1343257151 => 
  array (
    'file' => 'I108170010772731343246228000I.xml',
    'title' => 'MUHAMMAD ALI HONOURED AT SPORT GALA',
    'desc1' => 'Muhammad Ali was being honoured tonight by celebrities from the world of sport and showbusiness.The 70-year-old former heavyweight boxing world champion, who also won an Olympic gold medal, was being celebrated at the Sports For Peace gala in central London.Ali\'s younger brother, Rahaman Ali, 69, said: "He\'s very happy and proud to be here.',
    'desc2' => 'GVs of Victoria and Albert Museum, GV of Rahman Ali arriving, GV and INT with Boris Becker, GV of Bob Geldof, GV and INT Wladimir Klitschko and GV and INT with Lewis Hamilton.',
    'pic' => 'I108170010772731343246228000I-0-1.jpg',
    'video' => 'I108170010772731343246228000I.mp4',
  ),
  1343254896 => 
  array (
    'file' => 'I108160910771821343237416000I.xml',
    'title' => 'CHARLES AND CAMILLA SURPRISE TORCH BEARERS',
    'desc1' => NULL,
    'desc2' => 'The Prince of Wales and The Duchess of Cornwall were on hand to suprise a pair of Olympic Torch bearers as the flame made its way through Haringay.Prince Charles had nominated Jay Kamiraz to run a leg of the relay, having turned his life around using Charles\' charity, The Prince\'s Trust.',
    'pic' => NULL,
    'video' => 'I108160910771821343237416000I-0-1.jpg',
  ),
  1343252732 => 
  array (
    'file' => 'Iint1343233126844I.xml',
    'title' => 'HARRY POTTER STAR CARRIES TORCH',
    'desc1' => 'Rupert Grint cast a spell on crowds of well-wishers today as the Harry Potter star carried the Olympic Torch.Grint, who played Ron Weasley in the film franchise, was cheered on by hundreds of locals as he left Middlesex University in north-west London for his leg of the torch relay.The actor was mobbed as he ended his run, with spectators desperate for a touch of the torch.Day 68 of the relay was undertaken in glorious sunshine, and Grint was happy that his workload was made easier by the route.',
    'desc2' => 'GVs of actor Rupert Grint carrying the Olympic torch and interview with him',
    'pic' => 'Iint1343233126844I-0-1.jpg',
    'video' => 'Iint1343233126844I.mp4',
  ),
  1343249912 => 
  array (
    'file' => 'Iage1343247002738I.xml',
    'title' => 'N Korea protest at flag mix-up',
    'desc1' => 'North Korea staged a protest at tonight\'s Olympic women\'s football match against Colombia at Hampden after a national flag blunder.The South Korean flag was shown by mistake on the big screen inside the stadium as the players warmed up on the pitch prior to the match, and as a consequence North Korea refused to play the match at the scheduled 7.45pm start time.Their players walked off the pitch but were persuaded to return when the teams were announced again with each player\'s face displayed next to the North Korean flag.',
    'desc2' => 'An Olympic women\'s football match was delayed after a protest by North Korea when the South Korean flag was accidentally displayed',
    'pic' => 'Iage1343247002738I-23680-1.jpg',
    'video' => 'Iage1343247002738I.mp4',
  ),
  1343249495 => 
  array (
    'file' => 'I108167810772511343235906000I.xml',
    'title' => 'Olympic kick-off in Glasgow',
    'desc1' => 'The Olympic Games began in Glasgow with the USA v France women\'s football match.',
    'desc2' => 'GVs from Hampden Park in Glasgow where the USA played France in an Olympic women\'s football match today.',
    'pic' => 'I108167810772511343235906000I-0-1.jpg',
    'video' => 'I108167810772511343235906000I.mp4',
  ),
  1343249131 => 
  array (
    'file' => 'I0021343243491615I.xml',
    'title' => 'Olympics underway in Scotland',
    'desc1' => 'The Olympics are underway in Scotland where Glasgow has hosted two women\'s football matches.',
    'desc2' => 'The first Olympic football match in Scotland took place at Hampden, Glasgow, when the USA women\'s team played France',
    'pic' => 'I0021343243491615I-5960-1.jpg',
    'video' => 'I0021343243491615I.mp4',
  ),
  1343247943 => 
  array (
    'file' => 'I108137210769451343221728000I.xml',
    'title' => 'BRYANT DETERMINED TO BREAK OLYMPIC DUCK',
    'desc1' => NULL,
    'desc2' => 'Great Britain judo heavyweight Karina Bryant is determined to finally do herself justice at London 2012, which could be a last shot for Olympic glory. The 33-year-old Camberley fighter has delivered five silver medals from World Championships since 2003, but to date a place on the Olympic podium has proved illusive from her previous three Games. Bryant, though, is feeling confident this time will prove different as she looks to build on a bronze medal from the 2011 European Championships in Russia after fighting her way back to fitness from a next injury.',
    'pic' => NULL,
    'video' => 'I108137210769451343221728000I-0-1.jpg',
  ),
  1343247287 => 
  array (
    'file' => 'I108144410770171343225936000I.xml',
    'title' => 'World Anti-doping Agency Olympic press conference',
    'desc1' => NULL,
    'desc2' => 'A report in French sports daily L\'Equipe has named Morocco\'s Mariem Alaoui Selsouli, the favourite for the Olympics 1,500m gold medal, as having tested positive for a banned diuretic.',
    'pic' => NULL,
    'video' => 'I108144410770171343225936000I-0-1.jpg',
  ),
  1343246300 => 
  array (
    'file' => 'I108137810769511343222071000I.xml',
    'title' => 'Team GB women\'s hockey team press conference',
    'desc1' => NULL,
    'desc2' => 'Team GB women\'s hockey team look ahead to Games.',
    'pic' => NULL,
    'video' => 'I108137810769511343222071000I-0-1.jpg',
  ),
  1343244703 => 
  array (
    'file' => 'I0011343239481141I.xml',
    'title' => 'Olympic kick-off in Glasgow',
    'desc1' => 'The Olympic Games began in Glasgow with the USA v France women\'s football match.',
    'desc2' => 'GVs from Hampden Park in Glasgow where the USA played France in an Olympic women\'s football match today.',
    'pic' => 'I0011343239481141I-0-1.jpg',
    'video' => 'I0011343239481141I.mp4',
  ),
  1343242468 => 
  array (
    'file' => 'I108135110769231343221395000I.xml',
    'title' => 'Cameron on latest GDP figures',
    'desc1' => NULL,
    'desc2' => 'The latest figures on the UK\'s double-dip recession defied even the gloomiest of projections, heaping more pressure on Chancellor George Osborne. While experts cautioned that the figures may not paint a true picture of the UK economy, there were calls for Mr Osborne to come up with new initiatives to counter today\'s 0.7% decline.',
    'pic' => NULL,
    'video' => 'I108135110769231343221395000I-0-1.jpg',
  ),
  1343241723 => 
  array (
    'file' => 'Iage1343238161946I.xml',
    'title' => 'Life\'s a beach for Tancock',
    'desc1' => 'Swimmer Liam Tancock talks about growing up in an active family at the seaside and using social media during the Olympic Games.',
    'desc2' => 'Team GB swimmer Liam Tancock talks about growing up on the seaside and using Twitter during the Olympics',
    'pic' => 'Iage1343238161946I-23280-1.jpg',
    'video' => 'Iage1343238161946I.mp4',
  ),
  1343241714 => 
  array (
    'file' => 'I108138710769601343221445000I.xml',
    'title' => 'Usain Bolt tribute',
    'desc1' => 'Midlands farmer Tom Robinson has created an amazing tribute to Jamaican sprinter Usain Bolt by carving a giant image of the world?s fastest man in his 15 acre field of maize at the National Forest Adventure Farm near Burton on Trent. INTERVIEW WITH Tom Robinson and Donald Quarrie.',
    'desc2' => 'Midlands farmer Tom Robinson has created an amazing tribute to Jamaican sprinter Usain Bolt  - gvs of it from the sky plus interviews with Donald Quarrie and Tom Robinson',
    'pic' => 'I108138710769601343221445000I-0-1.jpg',
    'video' => 'I108138710769601343221445000I.mp4',
  ),
  1343241567 => 
  array (
    'file' => 'I0041343235627672I.xml',
    'title' => 'Dad visits river death scene - GVs',
    'desc1' => 'The devastated friends of a teenager who drowned when he got into difficulty after using a weir as a slide comforted each other today at the scene of his death.More than 50 friends from Aaron Burgess\'s school and youth club have laid flowers and cards and lit candles next to the River Avon.The 14-year-old, from Longwell Green, near Bristol, was cooling off in a stretch of the River Avon between Bristol and Bath yesterday evening when he disappeared under the water and did not resurface.Emergency services were called to the river where the teenager had been swimming on one the hottest days of the year so far at about 5.40pm.',
    'desc2' => 'GVs of Aaron Burgess\' father, Jay Burgess, visiting the site next to the River Avon where his son drowned on Tuesday evening',
    'pic' => 'I0041343235627672I-0-1.jpg',
    'video' => 'I0041343235627672I.mp4',
  ),
  1343240473 => 
  array (
    'file' => 'I0031343235167721I.xml',
    'title' => 'FRIENDS WEEP FOR RIVER DEATH BOY - GVs',
    'desc1' => 'The devastated friends of a teenager who drowned when he got into difficulty after using a weir as a slide comforted each other today at the scene of his death.More than 50 friends from Aaron Burgess\'s school and youth club have laid flowers and cards and lit candles next to the River Avon.The 14-year-old, from Longwell Green, near Bristol, was cooling off in a stretch of the River Avon between Bristol and Bath yesterday evening when he disappeared under the water and did not resurface.',
    'desc2' => 'GVs of flowers laid next to the River Avon at the spot where Aaron Burgess died on Monday evening. Also shots of his friends gathering around the makeshift shrine',
    'pic' => 'I0031343235167721I-0-1.jpg',
    'video' => 'I0031343235167721I.mp4',
  ),
  1343239032 => 
  array (
    'file' => 'I108147210770451343227568000I.xml',
    'title' => 'INQUIRY AFTER JOGGER\'S CABLE DEATH',
    'desc1' => NULL,
    'desc2' => 'A health and safety investigation has been launched after a man died when he apparently ran into a low-hanging high-voltage cable in Essex. James Kew, 42, from the Saffron Walden area, was out jogging with three other people when he collided with the cable in a field near Newport,at about 8.40pm on Tuesday. It is thought the cable, which carried about 11,000 volts, was hanging across the path.',
    'pic' => NULL,
    'video' => 'I108147210770451343227568000I-0-1.jpg',
  ),
  1343238817 => 
  array (
    'file' => 'I108143110770041343225286000I.xml',
    'title' => 'Boxing press conference - Adams',
    'desc1' => NULL,
    'desc2' => 'Interview with British boxer Nicola Adams at a boxing press conference in the Olympic Park',
    'pic' => NULL,
    'video' => 'I108143110770041343225286000I-0-1.jpg',
  ),
  1343233281 => 
  array (
    'file' => 'I108118710767601343216758000I.xml',
    'title' => 'Tributes paid to river death teen - GVs',
    'desc1' => NULL,
    'desc2' => 'Tributes have been paid to a teenager who died after getting into difficulty while swimming in a river. Aaron Burgess, who was named locally, was recovered by police diving teams latelast night. The 14-year-old, from Longwell Green, was cooling off in a stretch of the River Avon between Bristol and Bath yesterday evening when he disappeared under the water and did not resurface.',
    'pic' => NULL,
    'video' => 'I108118710767601343216758000I-0-1.jpg',
  ),
  1343229960 => 
  array (
    'file' => 'I108101710765901343209904000I.xml',
    'title' => 'GAMES LANES BARE ON FIRST DAY',
    'desc1' => NULL,
    'desc2' => 'A total of 30 miles of Olympic Games Lanes have been introduced as part of the 109-mile Olympic Route Network (ORN) around London.The lanes have caused jams in some parts of London, while in others they are barely being used.',
    'pic' => NULL,
    'video' => 'I108101710765901343209904000I-83960-1.jpg',
  ),
  1343229948 => 
  array (
    'file' => 'IUSA1343224570793I.xml',
    'title' => 'Beach volleyballers hail venue',
    'desc1' => 'The beach volleyball girls said their outfits draw the crowds but their athleticism creates fans.',
    'desc2' => 'The USA beach volleyball team can\'t wait to introduce more people to the sport at Horseguards Parade',
    'pic' => 'IUSA1343224570793I-0-1.jpg',
    'video' => 'IUSA1343224570793I.mp4',
  ),
  1343228680 => 
  array (
    'file' => 'I108128010768531343219507000I.xml',
    'title' => 'Glasgow ready for Games',
    'desc1' => NULL,
    'desc2' => 'The people of Glasgow get in the Olympic spirit.',
    'pic' => NULL,
    'video' => 'I108128010768531343219507000I-0-1.jpg',
  ),
  1343223673 => 
  array (
    'file' => 'I108112010766931343214243000I.xml',
    'title' => 'Scotland to allow gay marriage',
    'desc1' => 'Scotland could be the first part of the UK to allow same-sex couples to marry.Deputy First Minister Nicola Sturgeon announced that the Scottish Government plans to bring forward legislation which would permit it.The controversial issue has been backed by politicians from all the main parties north of the border, as well as equality campaigners, but has attracted fierce criticism from some religious organisations.',
    'desc2' => 'Nicola Sturgeon announces the Scottish Government intends to legislate to allow same-sex couples to marry.',
    'pic' => 'I108112010766931343214243000I-0-1.jpg',
    'video' => 'I108112010766931343214243000I.mp4',
  ),
  1343222970 => 
  array (
    'file' => 'I108089210764651343200910000I.xml',
    'title' => 'Spurs held by Galaxy',
    'desc1' => NULL,
    'desc2' => 'Tottenham were held to a 1-1 draw in a pre-season friendly by a Los Angeles Galaxy side featuring former striker Robbie Keane. Gareth Bale gave Spurs a first half lead in California before the home side levelled before the break. Bale converted a 17th minute cross from summer signing Gylfi Sigurdsson but David Lopes replied before the break to make sure the match ended 1-1.',
    'pic' => NULL,
    'video' => 'I108089210764651343200910000I-0-1.jpg',
  ),
  1343222502 => 
  array (
    'file' => 'Iall1343215348395I.xml',
    'title' => 'Finch waits on injured pair',
    'desc1' => 'Lenzly has a torn calf muscle while Finch said Clark\'s ankle sprain is a bad one, with Friday the deadline for a decision to be made on their availability.',
    'desc2' => 'Great Britain coach Chris Finch revealed injured duo Mike Lenzly and Dan Clark are "50/50 at best" to be fit for the Olympic Games.',
    'pic' => 'Iall1343215348395I-42240-1.jpg',
    'video' => 'Iall1343215348395I.mp4',
  ),
  1343217321 => 
  array (
    'file' => 'I108091610764891343201859000I.xml',
    'title' => 'Jamaica hold pre-Games press conference',
    'desc1' => NULL,
    'desc2' => 'Michael Frater, Don Quarrie and Novlene Williams-Mills look ahead to the Games.',
    'pic' => NULL,
    'video' => 'I108091610764891343201859000I-0-1.jpg',
  ),
  1343209408 => 
  array (
    'file' => 'I108091010764831343201231000I.xml',
    'title' => 'Basking in the sun on Brighton beach - GVs',
    'desc1' => NULL,
    'desc2' => 'People congregated on Brighton beach as the summer finally arrived on British soil.',
    'pic' => NULL,
    'video' => 'I108091010764831343201231000I-0-1.jpg',
  ),
  1343207822 => 
  array (
    'file' => 'Ison1343202971400I.xml',
    'title' => 'Ronson and Katy B carry torch',
    'desc1' => 'Both music stars were nominated to carry the flame after they produced an anthem for the Games, sampling the sounds made by international athletes training for London 2012.  The pair will perform the track at a concert to mark the arrival of the Olympic torch in London\'s Hyde Park on Thursday, with a line-up including Dizzee Rascal and Rizzle Kicks',
    'desc2' => 'Grammy award-winning producer Mark Ronson joined singer Katy B at Brunel University in west London as they carried the torch on day 67 of the relay.',
    'pic' => 'Ison1343202971400I-14080-1.jpg',
    'video' => 'Ison1343202971400I.mp4',
  ),
  1343181972 => 
  array (
    'file' => 'I108054310761161343139292000I.xml',
    'title' => 'TANNER: ROWING SQUAD "OUSTANDINGLY FIT"',
    'desc1' => NULL,
    'desc2' => 'Team Leader of the Great Britain rowing squad David Tanner has said his team is "oustandingly fit" going into the London 2012 games.Rowing, which is being held at Eton Dorney, is traditionally one of Great Britain\'s stronger sports, and the athletes will be hoping to win more golds at their home Olympics.',
    'pic' => NULL,
    'video' => 'I108054310761161343139292000I-0-1.jpg',
  ),
  1343180693 => 
  array (
    'file' => 'I108021610757891343110798000I.xml',
    'title' => 'AYOADE MAKES HOLLYWOOD DEBUT',
    'desc1' => NULL,
    'desc2' => 'Richard Ayoade, star of British sitcom The IT Crowd, is making his Hollywood debut in a star-studded comedy line-up.Ayoade lines up alongside Ben Stiller, Vince Vaughn and Jonah Hill in new movie The Watch, and says he must have been recruited because the budget had been used on signing up his co-stars.',
    'pic' => NULL,
    'video' => 'I108021610757891343110798000I-0-1.jpg',
  ),
  1343177344 => 
  array (
    'file' => 'I108050710760801343137231000I.xml',
    'title' => 'SMITH: I\'LL STAY POSITIVE',
    'desc1' => NULL,
    'desc2' => 'Artistic gymnast Louis Smith says he will focus on positive thoughts as he looks to improve on his previous Olympic showing in London.Smith won a gold medal in Beijing four years ago, and is hoping to bring home gold for the home crowd.',
    'pic' => NULL,
    'video' => 'I108050710760801343137231000I-0-1.jpg',
  ),
  1343177180 => 
  array (
    'file' => 'I108041910759931343131288000I.xml',
    'title' => 'Exercising bus a symbol of "wasting energy"',
    'desc1' => NULL,
    'desc2' => 'A bus has been transformed into an exercising artwork that can perform push-ups.Artist David Cerny created The London Booster by attaching huge arms, suspension mechanics and adding groaning sound effects to the 1957 double decker bus.The Czech says the piece is a symbol of "wasted energy".',
    'pic' => NULL,
    'video' => 'I108041910759931343131288000I-0-1.jpg',
  ),
  1343176492 => 
  array (
    'file' => 'I108044210760151343133964000I.xml',
    'title' => 'EDINBURGH LIONS BATTLE FOR DOMINANCE',
    'desc1' => NULL,
    'desc2' => 'Edinburgh Zoo\'s resident female Asiatic lion Kamlesh is getting to know new big cat male and hopeful suitor Jayendra. Known as Jay to his keepers, he has been settling in well to his new home.The one-year-old lion arrived from Bristol Zoo towards the end of June and 14-year-old Kamlesh is showing him who is the boss.',
    'pic' => NULL,
    'video' => 'I108044210760151343133964000I-0-1.jpg',
  ),
  1343171375 => 
  array (
    'file' => 'I108057310761461343141982000I.xml',
    'title' => 'TWEDDLE: GYMNASTICS HAS IMPROVED',
    'desc1' => NULL,
    'desc2' => 'Great Britain gymnast Beth Tweddle says the standard in her sport has improved, with athletes performing bigger and better skills.Tweddle was speaking alongside Rebecca Tunney, who is Team GB\'s youngest member at 15.Tweddle says the two are like sisters, handing on advice to the teenager whenever she needs it.',
    'pic' => NULL,
    'video' => 'I108057310761461343141982000I-0-1.jpg',
  ),
  1343170907 => 
  array (
    'file' => 'I108064910762221343144607000I.xml',
    'title' => 'FOSTER: ADLINGTON CAN INSPIRE OTHERS',
    'desc1' => NULL,
    'desc2' => 'Swimming legend Mark Foster believes Rebecca Adlington going from "zero to hero" in Beijing should inspire the other swimmers onto greater things at their home Olympics.The Great Britain swimming squad arrived at London City Airport and looked relaxed as they posed for photographs just days before the games begin.',
    'pic' => NULL,
    'video' => 'I108064910762221343144607000I-114040-1.jpg',
  ),
  1343170022 => 
  array (
    'file' => 'Irle1343145122249I.xml',
    'title' => 'Rower Searle can be a golden oldie',
    'desc1' => 'The lure of a home Olympics tempted the 40-year-old out of retirement in 2009, but he was told then his place in the squad would only be available on merit, which is precisely why he includes himself among Britain\'s medal contenders at Eton Dorney.',
    'desc2' => 'Greg Searle has scoffed at thoughts he is past it, believing he has as much chance of bagging an Olympic gold as he did at his first Games in Barcelona in 1992.',
    'pic' => 'Irle1343145122249I-0-1.jpg',
    'video' => 'Irle1343145122249I.mp4',
  ),
  1343169277 => 
  array (
    'file' => 'I108064310762161343144401000I.xml',
    'title' => 'CANADIAN DIVERS WARM UP FOR GAMES',
    'desc1' => NULL,
    'desc2' => 'The Canadian Olympic diving team have been practicing at the Plymouth Life Centre in Devon today in preparation for the start of the Olympic games. Three-time Olympic medalist Emilie Heymans, 30, and Eric Sehn, 27, who will be making his debut at the games, are among those tipped for a place on the podium.',
    'pic' => NULL,
    'video' => 'I108064310762161343144401000I-0-1.jpg',
  ),
  1343169273 => 
  array (
    'file' => 'Iner1343145705957I.xml',
    'title' => 'Rowing leader backs Team GB',
    'desc1' => 'The rowing leader stopped short of a target or indication they will top their haul of six from 2008 Olympics in Beijing.',
    'desc2' => 'David Tanner, British Rowing\'s performance director, was Greg Searle\'s team leader in 1992, and has backed the rower and his team-mates to deliver medals as expected at these Games',
    'pic' => 'Iner1343145705957I-0-1.jpg',
    'video' => 'Iner1343145705957I.mp4',
  ),
  1343167971 => 
  array (
    'file' => 'Iage1343144744063I.xml',
    'title' => 'Smith takes Twitter break',
    'desc1' => 'Gymnast Louis Smith admits the weight of expectation to improve on the Olympic bronze medal he won four years ago has forced him into a social media blackout.The pommel horse specialist, who won Britain\'s first Olympic medal in 80 years in Beijing, is going to ground in a bid to focus on producing a world-beating performance at the North Greenwich arena during London 2012.Smith faces stiff competition to improve on his record at the last Games in the form of double world champion Krisztian Berki of Hungary, Japan\'s Kohei Uchimura and France\'s Cyril Tommasone.',
    'desc2' => 'British gymnast Louis Smith on expectation, training and his social media blackout',
    'pic' => 'Iage1343144744063I-52440-1.jpg',
    'video' => 'Iage1343144744063I.mp4',
  ),
  1343167917 => 
  array (
    'file' => 'I108061010761831343143486000I.xml',
    'title' => 'BLAKE CONFIDENT AHEAD OF GAMES',
    'desc1' => NULL,
    'desc2' => 'Jamaican sprinter Dominique Blake says she is as prepared as she will ever be going into the London Olympics.The 400m runner, who has been based in Birmingham with the rest of her team, says training has been going well and is looking to stay positive throughout the games.',
    'pic' => NULL,
    'video' => 'I108061010761831343143486000I-0-1.jpg',
  ),
  1343166968 => 
  array (
    'file' => 'Iage1343147982230I.xml',
    'title' => 'Tweddle fit for Games',
    'desc1' => 'Beth Tweddle, Team GB\'s best women\'s gymnastics medal hope on the asymmetrical bars, confirmed she is fully fit for the Games despite still sleeping with a £3,500 ice machine strapped to her left knee.The 27-year-old is taking one final shot at Olympic glory before retiring from the sport after a glittering career which has seen her claim three world titles and six European titles.',
    'desc2' => 'Beth Tweddle discusses recovering from injury and mentoring Team GB\'s youngest member, 15-year-old Rebecca Tunney',
    'pic' => 'Iage1343147982230I-34640-1.jpg',
    'video' => 'Iage1343147982230I.mp4',
  ),
  1343166820 => 
  array (
    'file' => 'I108080810763811343157952000I.xml',
    'title' => 'Ronson carries torch',
    'desc1' => 'Musicians Mark Ronson and Katy B carried the Olympic flame as the torch relay made its way around London.',
    'desc2' => 'GVs of singer Katy B handing over the Olympic flame to Mark Ronson and interview with the pair discussing the song they created for Coca Cola for the event',
    'pic' => 'I108080810763811343157952000I-0-1.jpg',
    'video' => 'I108080810763811343157952000I.mp4',
  ),
  1343166656 => 
  array (
    'file' => 'IINT1343155511488I.xml',
    'title' => 'SEARCH FOR WOMAN AFTER ROCK FALL',
    'desc1' => 'A major search operation was under way tonight for a young woman holidaying with her family after 400 tonnes of rock fell on top of her during a landslide on a beach.Emergency services believe the heavy rain over the last few weeks combined with the recent heatwave caused the landslip near a holiday resort in Dorset.Despite the size of the landslide, emergency services believe the woman - who is in her 20s - could still be alive.',
    'desc2' => 'GVs of landslide and interview clips from Group manager Mick Stead, of Dorset Fire and Rescue',
    'pic' => 'IINT1343155511488I-0-1.jpg',
    'video' => 'IINT1343155511488I.mp4',
  ),
  1343166036 => 
  array (
    'file' => 'Iage1343159923632I.xml',
    'title' => 'GB swimmers ready to go',
    'desc1' => 'Great Britain\'s Rebecca Adlington arrives with the Team GB swimming team at London City Airport ahead of the London 2012 Olympic Games.',
    'desc2' => 'Mark Foster on Britain\'s Olympic swimming hopes and Rebecca Adlington defending her titles',
    'pic' => 'Iage1343159923632I-34240-1.jpg',
    'video' => 'Iage1343159923632I.mp4',
  ),
  1343164502 => 
  array (
    'file' => 'I108032910758991343126829000I.xml',
    'title' => 'Physios able to prescribe medicines alone',
    'desc1' => 'Physiotherapists and podiatrists are to be able to prescribe their patients with medicines, the Department of Health (DH) announced.Following suitable training, the medics will be able to give their patients medication such as painkillers and anti-inflammatories.It will mean that podiatrists and physiotherapists will no longer have to refer their patients back to another healthcare worker - such as a GP - if medication is needed.',
    'desc2' => 'Physiotherapists and podiatrists are now able to prescribe their patients medication with out a doctor\'s approval.',
    'pic' => 'I108032910758991343126829000I-0-1.jpg',
    'video' => 'I108032910758991343126829000I.mp4',
  ),
  1343163704 => 
  array (
    'file' => 'I108067910762521343145532000I.xml',
    'title' => 'AMERICANS TARGET WIMBLEDON GLORY AT OLYMPICS',
    'desc1' => NULL,
    'desc2' => 'Serena and Venus Williams will be targeting more Wimbledon success as they hope to bring a gold medal back from the London Olympics.The tennis is being hosted in SW19, where the Williams sisters have had remarkable success over the years, and the US will be hoping their male pairing of John Isner and Andy Roddick can also return bathed in glory',
    'pic' => NULL,
    'video' => 'I108067910762521343145532000I-0-1.jpg',
  ),
  1343161822 => 
  array (
    'file' => 'Iage1343153313837I.xml',
    'title' => 'US tennis players back at Wimbledon',
    'desc1' => 'The USA tennis team arrive for the London 2012 Games.',
    'desc2' => 'The USA tennis team discuss returning to play at Wimbledon for the Olympics',
    'pic' => 'Iage1343153313837I-43480-1.jpg',
    'video' => 'Iage1343153313837I.mp4',
  ),
  1343159835 => 
  array (
    'file' => 'I108052810761011343138311000I.xml',
    'title' => 'COULSON: I DIDN\'T DAMAGE DOWLER INVESTIGATION',
    'desc1' => NULL,
    'desc2' => 'David Cameron\'s former spin doctor Andy Coulson and former News International chief executive Rebekah Brooks are among eight people who will be charged with phone hacking.Coulson has denied the charge saying that he would not damage the Milly Dowler investigation, one of four charges that he faces.',
    'pic' => NULL,
    'video' => 'I108052810761011343138311000I-0-1.jpg',
  ),
  1343158744 => 
  array (
    'file' => 'I108037510759481343129411000I.xml',
    'title' => 'CAMERON WELCOMES QUEEN TO NO. 10',
    'desc1' => NULL,
    'desc2' => 'Prime Minister David Cameron welcomes Queen Elizabeth II to Downing Street as he hosts a Diamond Jubilee lunch in her honour.The Queen was accompanied by her husband the Duke of Edinburgh, while ex-Prime Ministers Tony Blair, Gordon Brown and Sir John Major were also in attendance.',
    'pic' => NULL,
    'video' => 'I108037510759481343129411000I-0-1.jpg',
  ),
  1343157000 => 
  array (
    'file' => 'I108071610762891343147929000I.xml',
    'title' => 'COOKE TIPPED FOR TIMELY RETURN TO FORM',
    'desc1' => 'Defending champion Nicole Cooke has been tipped to rise to the occasion of the London 2012 Olympic Games road race on Sunday.The 29-year-old from Swansea won gold by the Great Wall of China in Beijing four years\' ago and the World Championships later that year, but has been in indifferent form of late.Lizzie Armitstead was the nominated leader at last September\'s World Championships in Copenhagen and after a disappointing finish there was a public spat with Cooke.',
    'desc2' => 'Interviews with Great Britain\'s women\'s road coach Chris Newton and 2008 Olympic champion Nicole Cooke',
    'pic' => 'I108071610762891343147929000I-0-1.jpg',
    'video' => 'I108071610762891343147929000I.mp4',
  ),
  1343154686 => 
  array (
    'file' => 'I108055510761281343140355000I.xml',
    'title' => '18,000 TROOPS FOR OLYMPICS SECURITY',
    'desc1' => NULL,
    'desc2' => 'More than 18,000 troops will provide security for the Olympics as 1,200 servicemen and women put on 48 hours notice last week will be deployed in the wake of the Olympics security shambles.Locog chief executive Paul Deighton added: "The reason that this decision has been taken is just to absolutely de-risk any aspect of the operation."',
    'pic' => NULL,
    'video' => 'I108055510761281343140355000I-0-1.jpg',
  ),
  1343152329 => 
  array (
    'file' => 'I108030610758801343124184000I.xml',
    'title' => 'HACKING: COULSON AND BROOKS CHARGED',
    'desc1' => NULL,
    'desc2' => 'David Cameron\'s former spin doctor Andy Coulson and former News International chief executive Rebekah Brooks are to face charges over phone hacking, it was announced today.Brooks will face two charges - one relating to the alleged accessing of murdered schoolgirl Milly Dowler\'s phone messages.Coulson is also accused in relation to allegedly hacking into Milly\'s phone, Crown Prosecution Service legal adviser Alison Levitt QC said.',
    'pic' => NULL,
    'video' => 'I108030610758801343124184000I-0-1.jpg',
  ),
  1343145938 => 
  array (
    'file' => 'I108047810760511343136036000I.xml',
    'title' => 'EX-BANK CHIEF CHARGED WITH FRAUD',
    'desc1' => NULL,
    'desc2' => 'The former chairman of Anglo Irish Bank has been charged in Ireland over a failed insider trading scam.Sean Fitzpatrick was brought before the Criminal Courts in Dublin for alleged financial irregularities.He was earlier arrested at Dublin Airport as he arrived from the USA after arranging to meet officers.',
    'pic' => NULL,
    'video' => 'I108047810760511343136036000I-0-1.jpg',
  ),
  1343140765 => 
  array (
    'file' => 'I0021343136611859I.xml',
    'title' => 'New Archbishop defends gay marriage stance',
    'desc1' => 'A Catholic bishop who has voiced his opposition to proposals to legalise same-sex marriage has been named as the new Archbishop of Glasgow.Philip Tartaglia, 61, who is the existing Bishop of Paisley, was appointed by Pope Benedict XVI today.He will take over from Archbishop Mario Conti, who is retiring after 10 years in the post.',
    'desc2' => 'New Archbishop of Glasgow Phillip Tartaglia will continue to defend his stance on same sex marriage legislation',
    'pic' => 'I0021343136611859I-0-1.jpg',
    'video' => 'I0021343136611859I.mp4',
  ),
  1343123236 => 
  array (
    'file' => 'Ihel1343119006432I.xml',
    'title' => 'Volleyball\'s Michel spots stars in Olympics village',
    'desc1' => 'Michel said the team have been watching hours of video to prepare themselves for their first match of the tournament.',
    'desc2' => 'Volleyball player Ciara Michel says she has been spotting her sporting heroes while living in the Olympic village',
    'pic' => 'Ihel1343119006432I-0-1.jpg',
    'video' => 'Ihel1343119006432I.mp4',
  ),
  1343121909 => 
  array (
    'file' => 'Iery1343119197913I.xml',
    'title' => 'Archers relishing Lord\'s appearance',
    'desc1' => 'Lord\'s cricket ground is the home of archery for London 2012, and the British team are excited to perform in front of a home crowd at such a historic location.',
    'desc2' => 'Great Britain\'s archers are looking forward to going for gold at one of the most iconic venues in the world of sport.',
    'pic' => 'Iery1343119197913I-0-1.jpg',
    'video' => 'Iery1343119197913I.mp4',
  ),
  1343121881 => 
  array (
    'file' => 'Iper1343117670879I.xml',
    'title' => 'Volleyball coach is sure Team GB can win',
    'desc1' => 'The squad\'s build-up to the games was riddled with financial worries - they paid for their training camps themselves after funding was cut - and it was often wondered whether or not they would make the grade for London.',
    'desc2' => 'A clash with world champions Russia presents the toughest of starts for coach Audrey Cooper and her squad, in a match which pits the third favourites against the 500/1 outsiders.',
    'pic' => 'Iper1343117670879I-74760-1.jpg',
    'video' => 'Iper1343117670879I.mp4',
  ),
  1343050924 => 
  array (
    'file' => 'I7121343025228181I.xml',
    'title' => 'TEST',
    'desc1' => 'TEEST',
    'desc2' => 'TEST',
    'pic' => 'I7121343025228181I-0-1.jpg',
    'video' => 'I7121343025228181I.mp4',
  ),
);?>