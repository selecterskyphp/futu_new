<?php
class GroupAction extends HomeAction
{
    private $dao = null;
    function _initialize()
    {
        parent::_initialize();
        if(!$this->islogin)
        {
            $this->error('请先登录');
        }
        $this->dao = M('GrouponDetail');
    }
    public function index()
    {
        import ('@.ORG.Page');
        $id = isset($_GET['id'])?intval($_GET['id']):0;
        $hot = isset($_GET['hot'])?intval($_GET['hot']):0;
        $sess_id = session_id();
        
        if(0 == $id)
        {
            $cateArr = $this->getCateArr(10000);
        }
        else
        {
            $cateArr = $this->getCateArr($id);
        }
        if(false === $cateArr)
        {
            $this->error('参数错误');
        }
        $cateCount = count($cateArr);
        $column = $this->getColumn($cateArr);
        $this->assign('menu',$column[0]);
        $this->assign('second_menu',$column[1]);
        $this->assign('three_menu',$column[2]);
        $where = 'state=\'3\'';
        if($hot == 1)
        {
            $where .= ' and is_24hot=\'1\'';
        }
        $cateWhere = '';
        if($cateCount === 3)
        {
            //以三级分类优先  每级分类可以放1-99个子分类
            
            $second_id = $cateArr[1]['id'];
            $three_id = $cateArr[2]['id'];
            $allowlist = $cateArr[2]['allowlist'];
            $cateWhere .= ' and (type_one='.$id.' or type_two='.$id.' or type_three = '.$id.')';
        }
        elseif($cateCount === 2)
        {
            //二级分类
            $second_id = $cateArr[1]['id'];
            $three_id = $column[2][0]['groupid'];
            $max_id  = $id + 100;
            $allowlist = $cateArr[1]['allowlist'];
            $cateWhere .= ' and ((type_one>='.$id.' and type_one<'.$max_id.') or (type_two>='.$id.' and type_two<'.$max_id.') or (type_three>='.$id.' and type_three<'.$max_id.'))';
        }
        else 
        {
            //一级分类
            $second_id = $column[1][0]['groupid'];
            $three_id = $column[2][0]['groupid'];
            $max_id = $id + 10000;
            $allowlist = $cateArr[0]['allowlist'];
            $cateWhere .= ' and ((type_one>='.$id.' and type_one<'.$max_id.') or (type_two>='.$id.' and type_two<'.$max_id.') or (type_three>='.$id.' and type_three<'.$max_id.'))';
        }
        if(100100 == $id)
        {
            //伦敦奥运
            header("Location:  http://www.osports.cn/photochina.jsp?sid={$sess_id}&user=xieyouding&type=1");
            exit;
        }
        if(100400 == $id)
        {
            //伦敦奥运
            header("Location:  http://www.osports.cn/photochina.jsp?sid={$sess_id}&user=xieyouding&type=2");
            exit;
        }
        if(50000 == $id)
        {
            //体育
            header("Location:  http://www.osports.cn/photochina.jsp?sid={$sess_id}&user=xieyouding&type=0");
            exit;
        }
        if($id>0)
        {
            $where .= $cateWhere;
        }
        $this->assign('second_id',$second_id);
        $this->assign('three_id',$three_id);
        $this->assign('id',$id);
        //var_dump($second_id,$three_id);
        $this->assign('nav',$this->getNav($cateArr));
        $this->assign('is_24hot',$hot);
        //$where = '';
        $count=$this->dao->where($where)->count();
		$page=new Page($count,36);
		$show=$page->show();
		
		$this->assign("page",$show);
		$list=$this->dao->order('update_time desc')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
		//$list = $mod->where($where)->order('buytime desc')->select();
		$this->assign('list',$list);
        $this->display();
    }
    public function osports()
    {
        if(!$this->islogin)
        {
            $this->error('您还没有登录');
        }
        $sess_id = session_id();
        header("Location:  http://www.osports.cn/photochina.jsp?sid={$sess_id}&user=xieyouding&type=0");
    }
}
?>