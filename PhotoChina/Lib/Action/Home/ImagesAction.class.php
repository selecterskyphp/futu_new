<?php
class ImagesAction extends HomeAction
{
    private $dao = null,$type=0,$gid = 0;
    function _initialize()
    {
        parent::_initialize();
        if(!$this->islogin)
        {
            $this->error('请先登录');
        }
        $this->dao = M('ImagesDetail');
        $this->gid = isset($_GET['gid'])?intval($_GET['gid']):0;
        $this->type = isset($_GET['type'])?intval($_GET['type']):0;
        $this->assign('gid',$this->gid);
    }
    public function index()
    {
        import ('@.ORG.Page');
        $gremarkMod = M('GrouponRemark');
        $gmod = M('GrouponDetail');
        $gid = $this->gid;
        //var_dump(checkstr($_GET['keyword']));
        $keyword = isset($_GET['keyword'])?checkstr($_GET['keyword']):'';
        $type_one = isset($_GET['type_one'])?intval($_GET['type_one']):-1;
        $address = isset($_GET['address'])?checkstr($_GET['address']):'';
        $photo_time = isset($_GET['photo_time'])?strtotime($_GET['photo_time']):'';
        $width = isset($_GET['width'])?intval($_GET['width']):-1;
        $filesize = isset($_GET['filesize'])?intval($_GET['filesize']):-1;
        $renwu_name = isset($_GET['renwu_name'])?checkstr($_GET['renwu_name']):'';
        $ren = isset($_GET['ren'])?intval($_GET['ren']):-1;
        $texie = isset($_GET['"texie"'])?checkstr($_GET['"texie"']):-1;
        $fileext = isset($_GET['"fileext"'])?checkstr($_GET['"fileext"']):-1;
        
        
        //$where = 'b.state=\'3\'';
        $where = 'b.id>0';
        $isfind = false;
        if($gid>0)
        {
            $where .= ' and a.group_id='.$gid;
            //$sql = "select a.title,b.remark from __TABLE__ a left jion ".C('DB_PREFIX').'groupon_remark b on a.id=b.group_id where a.id='.$gid
            $gdata = $gmod->where('id='.$gid)->field('title,check_time,check_oper')->find();
            $gremark = $gremarkMod->where('group_id='.$gid)->getField('remark');
            $this->assign('gdata',$gdata);
            $this->assign('gremark',$gremark);
        }
        
        $where1 = '';
        $where2 = '';
        if(strlen($keyword)>0)
        {
            $where1 = ' and b.id=\''.$keyword.'\'';
            $where2 = ' and b.images_key like \'%'.$keyword.'%\'';
            //$where2 = ' and b.images_key=\''.$keyword.'\'';
            $isfind = true;
        }
        if($type_one>0)
        {
            $max_id = $type_one + 10000;
            $where .= ' and (b.type_one>='.$type_one.' and b.type_one<'.$max_id.')';
            $isfind = true;
            $this->type = $type_one;
        }
        if(strlen($address)>0)
        {
            $where .= ' and b.city like \'%'.$address.'%\'';
            $isfind = true;
        }
        if($photo_time&&$photo_time>0)
        {
            $max = $photo_time+(60*60*24);
            $where .= ' and b.photo_time>='.$photo_time.' and b.photo_time<='.$max;
            $isfind = true;
        }
        if($width>0)
        {
            $min = ($width-1)*1024+1;
            $max = $width*1000;
            $where .= ' and b.width>='.$min;
            if($width<9)
            {
                $where .= ' and b.width<='.$max;
            }
            $isfind = true;
        }
        if($filesize>0)
        {
            $min = ($filesize-1)*1024+1;
            $max = $filesize*1024;
            $where .= ' and b.filesize>='.$min;
            if($filesize<19)
            {
                $where .= ' and b.filesize<='.$max;
            }
            $isfind = true;
        }
        if(strlen($renwu_name)>0)
        {
            $where .= ' and b.renwu_name like \'%'.$renwu_name.'%\'';
            $isfind = true;
        }
        if($ren>=0)
        {
            $where .= ' and b.ren='.$ren;
            $isfind = true;
        }
        if($texie>=0)
        {
            $where .= ' and b.texie='.$texie;
            $isfind = true;
        }
        if($fileext>0)
        {
            $where .= ' and b.fileext='.$fileext;
            $isfind = true;
        }
        //分类
        $type = $this->type;
        if(0 == $type)
        {
            $cateArr = $this->getCateArr(10000);
        }
        else 
        {
            $cateArr = $this->getCateArr($type);
        }
        if(false === $cateArr)
        {
            $this->error('参数错误');
        }
        $cateCount = count($cateArr);
        $column = $this->getColumn($cateArr);
        $this->assign('menu',$column[0]);
        $this->assign('second_menu',$column[1]);
        $this->assign('three_menu',$column[2]);
        $this->assign('type',$type);
        if($cateCount === 3)
        {
            //以三级分类优先  每级分类可以放1-99个子分类
            $second_id = $cateArr[1]['id'];
            $three_id = $cateArr[2]['id'];
        }
        elseif($cateCount === 2)
        {
            //二级分类
            $second_id = $cateArr[1]['id'];
            $three_id = $column[2][0]['groupid'];
            $max_id  = $type + 100;
        }
        else 
        {
            //一级分类
            $second_id = $column[1][0]['groupid'];
            $three_id = $column[2][0]['groupid'];
        }
        $this->assign('second_id',$second_id);
        $this->assign('three_id',$three_id);
        $this->assign('nav',$this->getNav($cateArr));
        
        $mod = M('GrouponImages');
        $pre = C('DB_PREFIX');
        $sql = "select count(*) as tt from __TABLE__ a inner join ".$pre."images_detail b on a.images_id=b.id  where ".$where;
        $whereKeyword = $where1;
        //$count=$this->dao->where($where)->count();
        $result = $mod->query($sql.$whereKeyword);
        //var_dump($mod->getlastsql());
        $count = $result[0]['tt'];
        if(strlen($keyword)>0)
        {
            if($count==0)
            {
                $whereKeyword = $where2;
                $result = $mod->query($sql.$whereKeyword);
                $count = $result[0]['tt'];
            }
        }
		$page=new Page($count,36);
		$show=$page->show();
		$where .= $whereKeyword;
		$sql ="select b.*,a.group_id as gid,c.remark from __TABLE__ a inner join ".$pre."images_detail b on a.images_id=b.id inner join ".$pre."images_remark c on a.images_id=c.images_id where ".$where.' order by id desc limit '.$page->firstRow.','.$page->listRows;
		$this->assign("page",$show);
		//$list=$this->dao->order('check_time desc')->where($where)
		//->limit($page->firstRow.','.$page->listRows)->select();
		$list = $mod->query($sql);
		//var_dump($list,$mod->getlastsql());
		//exit;
		if($isfind)
		{
		    $this->assign('findmsg',' >> <span class="red">搜索  <b>'.$keyword.' 的结果</span>');
		}
		//var_dump($this->dao->getlastsql());
		
		//$list = $mod->where($where)->order('buytime desc')->select();
		$this->assign('list',$list);
        $this->display();
    }
    public function search()
    {
         import ('@.ORG.Page');
         import ('@.ORG.sphinxapi');
        //var_dump(checkstr($_GET['keyword']));
        $pagesize = 36;
        $keyword = isset($_GET['keyword'])?checkstr($_GET['keyword']):'';
        $type_one = isset($_GET['type_one'])?intval($_GET['type_one']):-1;
        $address = isset($_GET['address'])?checkstr($_GET['address']):'';
        $photo_time = isset($_GET['photo_time'])?strtotime($_GET['photo_time']):'';
        $width = isset($_GET['width'])?intval($_GET['width']):-1;
        $filesize = isset($_GET['filesize'])?intval($_GET['filesize']):-1;
        $renwu_name = isset($_GET['renwu_name'])?checkstr($_GET['renwu_name']):'';
        $ren = isset($_GET['ren'])?intval($_GET['ren']):-1;
        $texie = isset($_GET['"texie"'])?checkstr($_GET['"texie"']):-1;
        $fileext = isset($_GET['"fileext"'])?checkstr($_GET['"fileext"']):-1;
        $page = isset($_GET['p'])?intval($_GET['p']):1;
        if($page<1)
        {
            $page = 1;
        }
        $fields = 'a.id,a.url,a.bigurl,a.up_time,a.userid,a.filesize,a.value,a.width,a.height';
        $mod = M('ImagesDetail');
        $pre = C('DB_PREFIX');
        $sql ="select {$fields},b.group_id as gid,c.remark from __TABLE__ a 
        	left join ".$pre."groupon_images b on b.images_id=a.id 
        	left join ".$pre."images_remark c on a.id=c.images_id where ";
        if(strlen($keyword)<1 || strlen($keyword)>32)
        {
            $this->error('请输入正确的关键字');
        }
        $this->assign('findmsg',' >> <span class="red">搜索  <b>'.$keyword.'</b> 的结果</span>');
        if(is_numeric($keyword))
        {
            $list = $mod->query($sql.' a.id=\''.$keyword.'\'');
            if($list)
            {
                $this->assign('list',$list);
                $this->display();
                exit();
            }
        }
        $cl = new SphinxClient ();
        $cl->SetServer ( '192.168.11.2', 9312);
        $cl->SetConnectTimeout ( 3 );
        $cl->SetArrayResult ( true );
        $cl->SetMatchMode ( SPH_MATCH_ALL);
        $cl->SetSortMode(SPH_SORT_EXTENDED,'@id desc' );
        $cl->SetLimits (($page-1)*$pagesize,$pagesize);
        $res = $cl->Query ($keyword, "images" );
        $ids = getSphinxDataList($res);
        //var_dump($res);
        //exit;
        $list = array();
        if($ids)
        {
            $list = $mod->query($sql.' a.id in ('.$ids.') order by a.id desc');
            
        }
        //var_dump($mod->getlastsql());
        $this->assign('list',$list);
        $page=new Page($res['total'],$pagesize);
        $show=$page->show();
        $this->assign('page',$show);
        $this->display();
    }
    public function show()
    {
        $id = isset($_GET['id'])?intval($_GET['id']):0;
        $self = isset($_GET['self'])?intval($_GET['self']):0;//是否是显示摄影师自己的图片 1为是
        $gid = $this->gid;
        $where = 'id='.$id;
        $errscript = '<script language="javascript">setTimeout(function(){top.$.jBox.close(true);},3000);</script>';
        if(!$gid)
        {
            $this->error('图组不存在'.$errscript);
        }
        if(!$id)
        {
            $this->error('图片不存在'.$errscript);
        }
        $vo = $this->dao->where($where)->find();
        if(!$vo)
        {
            $this->error('图片不存在'.$errscript);
        }
        $iRemarkM = M('ImagesRemark');
        if(0 === $self)
        {
            $gMod=M('GrouponDetail');
            $gRemarkM = M('GrouponRemark');
            $gimagesM = M('GrouponImages');
            $pre = $gimagesM->where('group_id='.$gid.' and images_id>'.$id)->getField('images_id');
            $next = $gimagesM->where('group_id='.$gid.' and images_id<'.$id)->getField('images_id');
            $vo['remark'] = $iRemarkM->where('images_id='.$id)->getField('remark');
        }
        else 
        {
            $gMod=M('GroupDetail');
            $gRemarkM = M('GroupRemark');
            $pre = $gMod->where('old_group_id='.$gid.' and id>'.$id)->getField('id');
            $next = $gMod->where('old_group_id='.$gid.' and id<'.$id)->getField('id');
            $vo['remark'] = $iRemarkM->where('images_id='.$id)->getField('old_remark');
        }
        
        $vo['gremark'] = $gRemarkM->where('group_id='.$gid)->getField('remark');
        $vo['title']=$gMod->where('id='.$gid)->getField('title');
       // var_dump($vo);
        $this->assign('next',$next);
        $this->assign('pre',$pre);
        $this->assign('images',$vo);
        $this->assign('self',$self);
        $this->display();
    }
}
?>