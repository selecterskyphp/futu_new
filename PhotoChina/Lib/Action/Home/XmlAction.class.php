<?php
class XmlAction extends HomeAction
{
    private $list,$dir;
    private function toContent($content)
    {
        foreach($content as $v)
        {
            //var_dump($v);
            $str .= '<p>'.$v[0].'</p>';
        }
        return $str;
    }
    function _initialize()
    {
        parent::_initialize();
        if(!$this->islogin)
        {
            $this->error('请先登录');
        }
        $this->dir = 'E:/txt';
        $cateArr = $this->getCateArr(100600);
        if(false === $cateArr)
        {
            $this->error('参数错误');
        }
        $cateCount = count($cateArr);
        $column = $this->getColumn($cateArr);
        $this->assign('menu',$column[0]);
        $this->assign('second_menu',$column[1]);
        $this->assign('three_menu',$column[2]);
        if($cateCount === 3)
        {
            //以三级分类优先  每级分类可以放1-99个子分类
            $second_id = $cateArr[1]['id'];
            $three_id = $cateArr[2]['id'];
        }
        elseif($cateCount === 2)
        {
            //二级分类
            $second_id = $cateArr[1]['id'];
            $three_id = $column[2][0]['groupid'];
            $max_id  = $type + 100;
        }
        else 
        {
            //一级分类
            $second_id = $column[1][0]['groupid'];
            $three_id = $column[2][0]['groupid'];
        }
        $this->assign('second_id',$second_id);
        $this->assign('three_id',$three_id);
        $this->assign('nav',$this->getNav($cateArr));
        
        $g = isset($_GET['x'])?urldecode(trim($_GET['x'])):'';
        if($g)
        {
            $file = $this->dir.'/'.$g;
            //var_dump($file);
            @$xml = simplexml_load_file($file);
            if(!is_object($xml))
            {
                die('读取文件失败');
            }
            $list = $xml->NewsItem->NewsComponent->NewsComponent;
            $arr = array();
            foreach ($list as $v)
            {
                //if(count($v)<>4) continue;
                $data = $v->NewsComponent->ContentItem->DataContent->nitf;
                $img = '';
                if($v->NewsComponent[1] && $v->NewsComponent[1]->NewsComponent)
                {
                    $img = $v->NewsComponent[1]->NewsComponent->NewsComponent->ContentItem;
                    $img = get_object_vars($img);
                    $img = '/txt/'.$img["@attributes"]['Href'];
                }
                $body = $data->body;
                $body = get_object_vars($body);
                $title = $body['body.head']->hedline->hl1;
                $content = $this->toContent($body['body.content']->p);
                $arr[] = array('pic'=>$img,'title'=>$title,'desc'=>$content);
            }
            $this->list = $arr;
            $this->assign('xml',$g);
        }
    }
    public function detail()
    {
		$this->assign('list',$this->list);
		$this->display();
    }
    public function index()
    {
        $fp=opendir($this->dir);
        $arr = array();
        while(false!=$file=readdir($fp))
        {
            //列出所有文件并去掉'.'和'..'
            if($file!='.' &&$file!='..'&&strpos(strtolower($file), '.xml')>0)
            {
                //赋值给数组
                $ctime = filemtime($this->dir . '/' .$file);
                if(strpos(strtolower($file), '.html')>0)
                {
                    $title = str_replace('.html', '', str_replace('.xml', '', $file));
                    $arr[$ctime]=array('file'=>$file,'title'=>$title);
                }
                else 
                {
                    @$xml = simplexml_load_file($this->dir . '/' .$file);
                    if(is_object($xml))
                    {
                        $title = $xml->NewsItem->NewsComponent->NewsComponent->NewsComponent->NewsLines->HeadLine;
                        $title = trim($title);
                        if(strlen($title)>1)
                        {
                            $arr[$ctime]=array('file'=>$file,'title'=>$title);
                        }
                    }
                }
                
            }
        }
        krsort($arr);
		$this->assign('list',$arr);
        $this->display();
    } 
    function down()
    {
        $id = isset($_GET['id'])?intval($_GET['id']):0;
        $count = count($this->list);
        for($i=0;$i<$count;$i++)
        {
            if($id === $i+1)
            {
                $data = $this->list[$i];
                $content = $data['title'] ."\r\n";
                $content .= str_replace('</p>',"\r\n",str_replace('<p>', '', $data['desc'])) ."\r\n";
                $file = $data['title'].'.txt';
                file_put_contents($file, iconv('utf-8', 'gb2312', $content));
                import('@.ORG.Http');
                HTTP::download($file);
                break;
            }
        }
    }
    function downhtml()
    {
        $f = isset($_GET['f'])?urldecode(trim($_GET['f'])):'';
        $file = $this->dir . '/' . $f;
        if(is_readable($file))
        {
            import('@.ORG.Http');
            HTTP::download($file);
        }
        else {
            $this->error('文件不存在');
        }
    }
    function downpic()
    {
        $id = isset($_GET['id'])?intval($_GET['id']):0;
        $count = count($this->list);
        
        for($i=0;$i<$count;$i++)
        {
            if($id === $i+1)
            {
                $data = $this->list[$i];
                $pic = 'E:'.$data['pic'];
                if(is_readable($pic))
                {
                    import('@.ORG.Http');
                    HTTP::download($pic);
                }
                else 
                {
                    $this->error('图片不存在');
                }
                break;
            }
        }
    }
}
?>