<?php
class ApiAction extends UserAction
{
    private $sid,$domain,$dao,$type;
    private $debug;
    private $photo;
    public function _initialize() 
    {
        //$sess_id= session_id();
        $this->sid = isset($_GET['sid'])?trim($_GET['sid']):'';
        $this->domain = isset($_GET['domain'])?trim($_GET['domain']):'';
        $this->type = isset($_GET['type'])?intval($_GET['type']):0;
        $this->debug = 'sid='.$this->sid.',domain='.$this->domain.',type='.$this->type ;
        if('' == $this->sid || '' == $this->domain)
        {
            $this->out('参数错误！');
        }
        $this->dao = M('Photoer');
        $result = $this->dao->where('sess_id=\''.$this->sid.'\'')->find();
        if(!$result)
        {
            $this->out('用户验证失败！');
        }
        $this->login = $result;
        $_SESSION['login'] = $this->login;
        $this->islogin = true;
        $this->debug .= ',account='.iconv('utf-8','gb2312',$result['account']);
        $result = $this->dao->where('account=\''.$this->domain.'\'')->find();
        if(!$result)
        {
            $this->out('合作机构验证失败！');
        }
        $this->photo = $result;
    }
    public function auth()
    {
        $this->out('用户验证成功！');
    }
    
    public function buyimg()
    {
        $imgid = isset($_GET['imgid'])?trim($_GET['imgid']):'';
        $price = isset($_GET['price'])?trim($_GET['price']):'';
        $this->debug = $this->debug . ',imgid='.$imgid.',price='.$price;
        if('' == $imgid || '' == $price)
        {
            $this->out('参数错误！');
        }
        $imgid = explode(',',$imgid);
        $price = explode(',', $price);
        if(empty($imgid) || count($imgid) !== count($price))
        {
            $this->out('参数错误！');
        }
        $mod = M('column');
        $allowlist = $mod->where('groupid=100000')->getField('allowlist');
        //没有下载权限
        if($allowlist)
        {
            $arr = explode(',', $allowlist);
            if($this->type>0)
            {
                if(!in_array($this->login['account'], $arr))
                {
                    $this->out('用户权限不够！');
                }
            }
            else
            {
                if(in_array($this->login['account'], $arr))
                {
                    $this->out('用户权限不够！');
                }
            }
        }
        $isSucc = true;
        for($i=0;$i<count($imgid);$i++)
        {
            $pdata = array();
            if($this->type>0)
            {
                $pdata['value'] = 200;
            }
            else
            {
                $pdata['value'] = $price[$i];
            }
            $pdata['id'] = $imgid[$i];
            $pdata['userid'] = $this->photo['id'];
            $pdata['up_time'] = time();
            $pdata['url'] = "http://www.osports.cn/thumb.do?photoId=$imgid[$i]";
            $result = $this->buy($pdata,0);
            if(!$result)
            {
                $isSucc = false;
                break;
            }
        }
        if(!$isSucc)
        {
            $this->out('用户余额不足！'.$this->getLastError());
        }
        else 
        {
            $this->out('用户扣款成功！');
        }
    }
    private function getPicUrl($pid)
    {
        $url = file_get_contents("http://www.osports.cn/thumb.do?photoId={$pid}");
        return trim($url);
    }
    private function out($msg)
    {
        $this->debug .= ',msg='.iconv('utf-8','gb2312',$msg);
        $this->printdebug();
        die($msg);
    }
    private function printdebug()
    {
        $this->debug = $this->debug.',time='.date('Y-m-d H:i:s')."\r\n";
        $file = './debug.txt';
        $fp = fopen($file,'a+');
        fwrite($fp, $this->debug );
        fclose($fp);
        //file_put_contents('./'.'debug.txt', $this->debug);
    }
}
?>