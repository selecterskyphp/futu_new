<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" type="image/x-icon" href="/favicon.ico" />
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
<link href="../Public/Css/common.css" type="text/css" rel="stylesheet" />
<script language="javascript" src="__PUBLIC__/Js/jquery-1.4.2.min.js"></script>

<script language="javascript" src="__PUBLIC__/Js/validator.js"></script>
<script language="javascript" src="../Public/Js/myfun.js"></script>
<script language="javascript" src="../Public/Js/function.js"></script>
<link href="../Public/Js/jbox-v2.3/Skins/GrayCool/jbox.css" type="text/css" rel="stylesheet"/>
<script language="javascript" src="../Public/Js/jbox-v2.3/jquery.jBox-2.3.min.js"></script>
<script language="javascript" src="../Public/Js/jbox-v2.3/i18n/jquery.jBox-zh-CN.js"></script>


<script lanuage="javascript">
<!--
//脚本系统常量
var PUB = '../Public';
var PUBLIC = '__PUBLIC__';
var TMPL = '__TMPL__';
var ROOT = '__ROOT__';
var APP  =   '__APP__';
var URL = '__URL__';
var ACTION = '__ACTION__';
var SELF = '__SELF__';
var LOGIN_URL='<?php echo U('Index/doLogin');?>';
var REG_PHOTO_URL = '<?php echo U('Index/reg',array('type'=>2));?>';
var REG_DOWN_URL = '<?php echo U('Index/reg',array('type'=>1));?>';
-->
</script>
<link href="../Public/Js/nivo-slider/nivo-slider.css" type="text/css" rel="stylesheet" />
<link href="../Public/Js/jcarousel-lite/style.css" type="text/css" rel="stylesheet" />
<link href="../Public/Js/nivo-slider/themes/default/default.css" type="text/css" rel="stylesheet"/>

<title>富图中国</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="../Public/Css/products.css" type="text/css" rel="stylesheet" />

</head>
<body>
<div id="mainBox">
<!--top begin-->
<div id="top">
<div class="logo">
<a href="<?php echo U('Index/index');?>"><img src="../Public/images/logo.jpg" width="202" height="74" /></a>
</div>
<div class="topRight">
 <div class="topSearch">
 <form action="<?php echo U('Images/search');?>" method="get" onsubmit="return Vali.vali(this,2)">
     <input name="m" type="hidden" value="Images"/>
     <input name="a" type="hidden" value="search"/>
     <input name="keyword" type="text" style="margin-left:10px; padding-left:5px; width:150px; height:18px; line-height:18px" value="图片ID或关键词" onfocus="cjx.chkfocusValue(this)" onblur="cjx.chkblurValue(this)" dataType="LimitB" min="1" max="50" allowDefaultValue="false" msg="请输入查询关键字"/>
     <select name="type_one">
       <option value="">全部分类</option>
       <?php if(is_array($column)): foreach($column as $key=>$v): ?><option value="<?php echo ($v["groupid"]); ?>"><?php echo ($v["name"]); ?></option><?php endforeach; endif; ?>
     </select>
     <input type="submit" style="background:url(../Public/images/ss.jpg) no-repeat; border:none; width:49px; height:22px" value="搜索" />
     <!-- <input type="button" name="button2" id="button2" style="background:url(../Public/images/gjss.jpg) no-repeat; border:none; width:63px; height:22px" value="高级搜索" onclick="showSearch()" /> -->
 </form>
 </div>
 <div class="topLink">
 <?php if(($islogin)  ==  "true"): ?><a href="<?php echo ($login["member_url"]); ?>"><?php echo ($login["role"]); ?></a> <a href="<?php echo ($login["member_url"]); ?>" class="red"><?php echo ($login["account"]); ?></a>,您好! <?php if(($login['type'])  ==  "1"): ?><a href="<?php echo U('Down-Cart/index');?>">购物车</a><?php else: ?><a href="<?php echo U('Photo-Up/index');?>">上传照片</a><?php endif; ?> | <a href="<?php echo U('Index/logout');?>">注销</a><?php else: ?><span class="red">游客</span>,您好! <a href="#" onclick="showLogin()">登录</a> | <a href="#" onclick="showregist()">注册</a><?php endif; ?>
 </div>
</div>
</div>
<!--top end-->

<!--图片/图组头部Menu begin-->
<div id="Menu">
<div class="menuLeft"></div>
<div class="menu">
<ul>
<li><a href="<?php echo U('Group/index',array('hot'=>1));?>" class="none"><img src="../Public/images/24.gif" />小时热图</a></li>
<?php if(is_array($column)): $k = 0; $__LIST__ = $column;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): ++$k;$mod = ($k % 2 )?><?php if(($vo["name"])  ==  "体育"): ?><li><a href="<?php echo U('Group/osports');?>"><?php echo ($vo["name"]); ?></a></li>
<?php else: ?>
<li><a href="<?php echo U('Group/index',array('id'=>$vo['groupid']));?>"><?php echo ($vo["name"]); ?></a></li><?php endif; ?><?php endforeach; endif; else: echo "" ;endif; ?>
<!-- <li><a href="<?php echo U('Group/index',array('id'=>20000));?>">创意图片</a></li>
<li><a href="<?php echo U('Group/index',array('id'=>30000));?>">摄影专题</a></li>
<li><a href="<?php echo U('Group/index',array('id'=>60000));?>">历史图片</a></li>
<li><a href="<?php echo U('Group/index',array('id'=>40000));?>">素材图片</a></li>-->
</ul>
</div>
<div class="menuRight"></div>
<?php if(!empty($second_menu)): ?><div class="subMenu">
<?php if(is_array($second_menu)): $i = 0; $__LIST__ = $second_menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): ++$i;$mod = ($i % 2 )?><li <?php if(($vo["groupid"])  ==  $second_id): ?>class="on"<?php endif; ?>><a href="<?php echo U('Group/index',array('id'=>$vo['groupid']));?>"><?php echo ($vo["name"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
<!-- <li class="on"><a href="<?php echo U('Group/index',array('id'=>1,'sid'=>1));?>">时事</a></li>
<li><a href="<?php echo U('Group/index',array('id'=>1,'sid'=>1));?>">体育</a></li>
<li><a href="<?php echo U('Group/index',array('id'=>1,'sid'=>1));?>">文娱</a></li>
<li><a href="<?php echo U('Group/index',array('id'=>1,'sid'=>1));?>">财经</a></li>
<li><a href="<?php echo U('Group/index',array('id'=>1,'sid'=>1));?>">时尚</a></li> -->
</div><?php endif; ?>
</div>
<!--图片/图组头部Menu end-->
<div class="clear"></div>
<!--高级搜索头部Menu begin-->

<div id="gjSearch_div" style="display:none">
<script type="text/javascript" src="__PUBLIC__/Js/MyDate/WdatePicker.js"></script>
<form action="<?php echo U('Images/search');?>" method="get">
<input type="hidden" name="m" value="Images" />
<input type="hidden" name="a" value="search" />
<input type="hidden" name="issearch" value="1" />
<div class="gjSearch" >
拍摄地点 &nbsp;
  <input type="text" name="address" maxlength="20" length="10" style="width:170px"/>
  &nbsp;&nbsp;拍摄时间 &nbsp;
  <input  class="Wdate input-text  "  name="photo_time" type="text" size="25" onFocus="WdatePicker({dateFmt:'yyyy-MM-dd'})"  style="width:140px;"/>&nbsp;
  &nbsp;&nbsp;
  <select name="type_one" id="select9" style="width:85px" >
    <option value="">所有分类</option>
    <?php if(is_array($column)): foreach($column as $key=>$v): ?><option value="<?php echo ($v["groupid"]); ?>"><?php echo ($v["name"]); ?></option><?php endforeach; endif; ?>
  </select>&nbsp;
  <select name="width"  style="width:85px">
    <option value="">不限长边像素</option>
    <option value="1">1-1000像素</option>
    <option value="1000">1000-2000PX</option>
    <option value="2000">2000-3000PX</option>
    <option value="3000">3000-4000PX</option>
    <option value="4000">4000-5000PX</option>
    <option value="5000">5000-6000PX</option>
    <option value="6000">6000-7000PX</option>
    <option value="7000">7000-8000PX</option>
    <option value="8000">8000-9000PX</option>
    <option value="9000">9000PX以上</option>
  </select>&nbsp;
  <select name="filesize" style="width:85px">
    <option value="">不限文件大小</option>
    <option value="1">1M及以下</option>
    <option value="2">1M-2M</option>
    <option value="3">2M-3M</option>
    <option value="4">3M-4M</option>
    <option value="5">4M-5M</option>
    <option value="6">5M-6M</option>
    <option value="7">7M-8M</option>
    <option value="8">8M-9M</option>
    <option value="10">10M-11M</option>
    <option value="11">11M-12M</option>
    <option value="12">12M-13M</option>
    <option value="13">13M-14M</option>
    <option value="14">15M-16M</option>
    <option value="15">16M-17M</option>
    <option value="16">17M-18M</option>
    <option value="17">18M-19M</option>
    <option value="18">19M-20M</option>
    <option value="19">20M以上</option>
  </select>&nbsp;
</div>
<div class="gjSearch">
人物名称 &nbsp;
  <input name="renwu_name" type="text"  style="width:170px"/>&nbsp;
  <select name="position" style="width:64px">
    <option value="">不限</option>
    <option value="1">横图</option>
    <option value="2">竖图</option>
    <option value="3">方图</option>
  </select>&nbsp;
  <select name="ren" style="width:64px">
    <option value="">不限</option>
    <option value="0">无人物图</option>
    <option value="1">1人</option>
    <option value="2">2人</option>
    <option value="3">3人</option>
    <option value="4">多人合影</option>
  </select>&nbsp;
  <select name="texie" id="select6" style="width:64px">
    <option value="">不限</option>
    <option value="1">特写</option>
    <option value="2">半身</option>
    <option value="3">全身</option>
  </select>&nbsp;
  <select name="fileext" id="select7" style="width:64px" >
    <option value="">格式不限</option>
    <option value="1">JPG/JPEG</option>
    <option value="2">PSD</option>
  </select>&nbsp;
  <!-- <input name="textfield2" type="text" id="textfield2" value="主色调" style="width:103px"/>&nbsp; -->
&nbsp;关键字<input name="keyword" type="text" value="" style="width:170px"/>&nbsp;
<input type="submit" value="提交" style="width:43px; height:21px; background:url(../Public/images/ss2.jpg) no-repeat; border:none; color:#ffffff" />
</div>
</form>
</div>
<!--高级搜索头部Menu end-->

<!--nav begin-->
<div id="nav">
<div class="navLeft"><?php if($findmsg): ?><?php echo ($findmsg); ?><?php else: ?><?php echo ($nav); ?><?php endif; ?><?php if($gdata): ?>>> <a href="<?php echo U('Images/index',array('gid'=>$gid,'type'=>$type));?>"><?php echo ($gdata["title"]); ?></a><?php endif; ?></div>
<div class="navCenter">&nbsp;<!-- 每页
<select name="select" id="select">
    <option>20</option>
  </select>
组 上一页 跳至第 
<select name="select2" id="select2">
  <option>1</option>
</select>
页 下一页 1/110 --></div>
<div class="navRight"></div>
</div>
<!--nav end-->

<!--content begin-->
<div id="content">
<!--left begin-->

<!--left end-->

<!--right begin-->
<div id="right">
<?php if(is_array($list)): $k = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): ++$k;$mod = ($k % 2 )?><div style="border:1px solid #333;margin:5px 0px; padding:5px">
<?php if(strlen($vo['pic']) == 0): ?><p><img src="<?php echo ($vo["pic"]); ?>" style="max-height:200px;max-width:200px"></p><?php endif; ?>
<h1><?php echo ($vo["title"]); ?></h1>
<div><?php echo ($vo["desc"]); ?></div>
<p style="text-align:right;marign-right:10px"><a href="<?php echo U('Xml/down',array('id'=>$k));?>">下载</a></p>
</div><?php endforeach; endif; else: echo "" ;endif; ?>
<div class="clear0"></div>
</div>
<!--right end-->
</div>
<!--content end-->

<!--nav begin-->
<div id="nav">
<div class="navLeft_2">&nbsp;</div>
<div class="navCenter">&nbsp; <!-- 每页
<select name="select" id="select">
    <option>20</option>
  </select>
组 上一页 跳至第 
<select name="select2" id="select2">
  <option>1</option>
</select>
页 下一页 1/110 --><?php echo ($page); ?></div>
<div class="navRight"></div>
</div>
<div style="border:1px solid #ccc;height:200px;padding:10px">
<textarea rows="5" cols="20" readonly style=" border:0;line-height:16px;background:#c0c0c0;width:100%;height:200px"><?php echo ($gremark); ?>&nbsp;&nbsp;&nbsp;&nbsp;


（责任编辑:<?php echo getAdminRealName($gdata['check_oper']);?>）

编辑日期：<?php echo (date("Y-m-d H:i:s",$gdata["check_time"])); ?></textarea>
</div>
<!--nav end-->
<!--about begin-->
<!--about begin-->
<div id="about">
<a href="<?php echo U('About/index');?>">关于我们</a> | <a href="<?php echo U('About/contact');?>">联系我们</a> | <a href="<?php echo U('About/copyright');?>">版权声明</a>
</div>
<!--about end-->

<!--copyRight begin-->
<div id="copyRight">
浙ICP备10203621号 2011 PHOTOCHINA.COM.CN All Rights Reserved  推荐使用IE6.0及以上 FIREFOX3.0及以上浏览器访问本站以获得最佳效果 如有问题请联系我们
</div>
<!--copyRight end-->
<!--copyRight end-->

</div>
</body>
</html>