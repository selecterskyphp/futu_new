<?php
class VideoAction extends HomeAction
{
    private $list,$dir,$cache_file;
    private function toContent($content)
    {
        foreach($content as $v)
        {
            //var_dump($v);
            $str .= '<p>'.$v[0].'</p>';
        }
        return $str;
    }
    function _initialize()
    {
        parent::_initialize();
        if(!$this->islogin)
        {
            $this->error('请先登录');
        }
        $this->dir = 'E:/txt/video';
        $this->cache_file = CACHE_PATH.'__tmp_video.php';
        $cateArr = $this->getCateArr(100700);
        if(false === $cateArr)
        {
            $this->error('参数错误');
        }
        $cateCount = count($cateArr);
        $column = $this->getColumn($cateArr);
        $this->assign('menu',$column[0]);
        $this->assign('second_menu',$column[1]);
       // var_dump($column[1]);
        
        $this->assign('three_menu',$column[2]);
        if($cateCount === 3)
        {
            //以三级分类优先  每级分类可以放1-99个子分类
            $second_id = $cateArr[1]['id'];
            $three_id = $cateArr[2]['id'];
        }
        elseif($cateCount === 2)
        {
            //二级分类
            $second_id = $cateArr[1]['id'];
            $three_id = $column[2][0]['groupid'];
            $max_id  = $type + 100;
        }
        else 
        {
            //一级分类
            $second_id = $column[1][0]['groupid'];
            $three_id = $column[2][0]['groupid'];
        }
        $this->assign('second_id',$second_id);
        $this->assign('three_id',$three_id);
        $this->assign('nav',$this->getNav($cateArr));
    }
    public function detail()
    {
		$this->assign('list',$this->list);
		$this->display();
    }
    public function index()
    {
        $fp=opendir($this->dir);
        $arr = array();
        while(false!=$file=readdir($fp))
        {
            //列出所有文件并去掉'.'和'..'
            if($file!='.' &&$file!='..'&&strpos(strtolower($file), '.xml')>0)
            {
                //赋值给数组
                $ctime = filemtime($this->dir . '/' .$file);
                if(strpos(strtolower($file), '.html')>0)
                {
                    $title = str_replace('.html', '', str_replace('.xml', '', $file));
                    $arr[$ctime]=array('file'=>$file,'title'=>$title);
                }
                else 
                {
                    @$xml = simplexml_load_file($this->dir . '/' .$file);
                    if(is_object($xml))
                    {
                        $title = $xml->NewsItem->NewsComponent->NewsLines->HeadLine;
                        $tmp = $xml->NewsItem->NewsComponent->NewsComponent[0]->ContentItem->DataContent;
                        $tmp = get_object_vars($tmp);
                        $desc1 = $tmp['p'];
                        $tmp = $xml->NewsItem->NewsComponent->NewsComponent[1]->ContentItem->DataContent;
                        $tmp = get_object_vars($tmp);
                        $desc2 = $tmp['p'];
                        $tmp = $xml->NewsItem->NewsComponent->NewsComponent[2]->NewsComponent->ContentItem;
                        $tmp = get_object_vars($tmp);
                        $pic = $tmp["@attributes"]['Href'];
                        
                        $tmp = $xml->NewsItem->NewsComponent->NewsComponent[3]->NewsComponent->ContentItem;
                        $tmp = get_object_vars($tmp);
                        $video = $tmp["@attributes"]['Href'];
                        $title = trim($title);
                        $tmparr=array(
                            	'file'=>$file,
                            	'title'=>$title,
                                'desc1'=>$desc1,
                                'desc2'=>$desc2,
                                'pic'=>$pic,
                                'video'=>$video);
                        //var_dump($tmparr);
                        //exit;
                        if(strlen($title)>1)
                        {
                            $arr[$ctime]=$tmparr;
                        }
                    }
                }
                
            }
        }
        krsort($arr);
        $content = var_export($arr,true);
        $content = '<?php return '.$content.';?>';
        file_put_contents($this->cache_file, $content);
		$this->assign('list',$arr);
        $this->display();
    } 
    function down()
    {
        $id = isset($_GET['id'])?intval($_GET['id']):0;
        $this->list = include($this->cache_file);
        //var_dump($this->list,$this->cache_file);
        foreach ($this->list as $k=>$v)
        {
            if($k == $id)
            {
                $data = $v;
                $file = $this->dir.'/'.$data['video'];
                import('@.ORG.Http');
                HTTP::download($file);
                break;
            }
        }
    }
    function downhtml()
    {
        $f = isset($_GET['f'])?urldecode(trim($_GET['f'])):'';
        $file = $this->dir . '/' . $f;
        if(is_readable($file))
        {
            import('@.ORG.Http');
            HTTP::download($file);
        }
        else {
            $this->error('文件不存在');
        }
    }
    function downpic()
    {
        $id = isset($_GET['id'])?intval($_GET['id']):0;
        $count = count($this->list);
        
        for($i=0;$i<$count;$i++)
        {
            if($id === $i+1)
            {
                $data = $this->list[$i];
                $pic = 'E:'.$data['pic'];
                if(is_readable($pic))
                {
                    import('@.ORG.Http');
                    HTTP::download($pic);
                }
                else 
                {
                    $this->error('图片不存在');
                }
                break;
            }
        }
    }
}
?>