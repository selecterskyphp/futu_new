/**
 * 
 */
var Mpp = {};
Mpp.Event = function()
{
	//定义系统事件，这些事件不进行绑定
	this.parentMenthod = ['proto','override'];
	var inArray = function(array_,name)
	{
		//是否是系统事件
		var i=0;
		for(i=0;i<array_.length;i++)
		{
			if(array_[i] === name)
			{
				return true;
			}
		}
		return false;
	};
	this.eventListArr = [];//已经绑定的事件名称列表
	this.eventListFnArr = [];//已经绑定的事件函数列表
	this.id = this.id || '';
	this.dom = document.getElementById(this.id) || document.body || document;
	this._on = function(eventname,fn,useCapture)
	{
		this.eventListArr.push(eventname);
		this.eventListFnArr.push(fn);
		this.dom.attachEvent(eventname,fn);
	},
	this._un = function(eventname,fn)
	{
		return this.dom.detachEvent(eventname,fn);
	},
	this.on = function(eventname,fn,useCapture,option)
	{
		var config = {
			singe:false//同事件是否只注册一次
		};
		Ext.apply(config,option);
		if(config.singe === true)
		{
			if(!inArray(this.eventListArr,eventname))
			{
				this._on(eventname,fn,useCapture);
			}
		}
		else
		{
			this._on(eventname,fn,useCapture);
		}
	};
	this.un = function(eventname,fn)
	{
		var i;
		var isFind = false;
		do{
			isFind = true;
			for(i=0;i<this.eventListArr;i++)
			{
				if(eventname === this.eventListArr[i])
				{
					if(typeof fn === 'function')
					{
						if(fn === this.eventListFnArr[i])
						{
							this._un(eventname, fn);
							this.eventListArr.splice(i,1);
							this.eventListFnArr.splice(i,1);
							break;
						}
					}
					else
					{
						this._un(eventname, this.eventListFnArr[i]);
						this.eventListArr.splice(i,1);
						this.eventListFnArr.splice(i,1);
						break;
					}
				}
				
			}
			if(i === this.eventListArr.length && this.eventListArr.length > 0 )
			{
				isFind = false;
			}
		}while(isFind)
	},
	this.register = function()
	{
		//注册
		var i,j;
		for(i in this)
		{
			if(!this.hasOwnProperty(i) && typeof this[i] === 'function' && false === inArray(this.parentMenthod,i))
			{
				this.bind(i,this[i]);
			}
		}
	};
	this.unregister = function()
	{
		
	};
	this.init = this.init || function(){};
	this.init();
};
var Alarm = function()
{
	this.type = 'a';
	this.init = function()
	{
		alert('sub');
	}
	Alarm.superclass.constructor.call(this);
};
Ext.extend(Alarm,Activex,{
	onclick:function(a,b,c){
		alert('aaaa'+a);
	}
});


Alarm = new Alarm();
