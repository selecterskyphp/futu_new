-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2012 年 04 月 07 日 17:16
-- 服务器版本: 5.1.28
-- PHP 版本: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `pcdb`
--

-- --------------------------------------------------------

--
-- 表的结构 `pc_access`
--

DROP TABLE IF EXISTS `pc_access`;
CREATE TABLE IF NOT EXISTS `pc_access` (
  `role_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `node_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `model` varchar(50) DEFAULT '',
  KEY `groupId` (`role_id`),
  KEY `nodeId` (`node_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `pc_application`
--

DROP TABLE IF EXISTS `pc_application`;
CREATE TABLE IF NOT EXISTS `pc_application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(24) CHARACTER SET utf8 NOT NULL,
  `createtime` int(11) NOT NULL,
  `query_value` int(11) NOT NULL,
  `state` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `pc_application`
--

INSERT INTO `pc_application` (`id`, `account`, `createtime`, `query_value`, `state`) VALUES
(1, 'sunnyboy', 13445555, 100, '1'),
(2, 'admin', 2434, 1234, '0'),
(3, 'sunnyboy', 13445555, 100, '1'),
(4, 'admin', 2434, 1234, '0');

-- --------------------------------------------------------

--
-- 表的结构 `pc_article`
--

DROP TABLE IF EXISTS `pc_article`;
CREATE TABLE IF NOT EXISTS `pc_article` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userid` int(11) unsigned NOT NULL DEFAULT '0',
  `username` varchar(40) NOT NULL DEFAULT '',
  `title` varchar(120) NOT NULL DEFAULT '',
  `title_style` varchar(40) NOT NULL DEFAULT '',
  `keywords` varchar(120) NOT NULL DEFAULT '',
  `copyfrom` varchar(255) NOT NULL DEFAULT '',
  `fromlink` varchar(80) NOT NULL DEFAULT '0',
  `description` mediumtext NOT NULL,
  `content` text NOT NULL,
  `template` varchar(30) NOT NULL DEFAULT '',
  `thumb` varchar(100) NOT NULL DEFAULT '',
  `posid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `recommend` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `readgroup` varchar(255) NOT NULL DEFAULT '',
  `readpoint` int(10) unsigned NOT NULL DEFAULT '0',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `url` varchar(50) NOT NULL DEFAULT '',
  `hits` int(11) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `status` (`id`,`status`,`listorder`),
  KEY `catid` (`id`,`catid`,`status`),
  KEY `listorder` (`id`,`catid`,`status`,`listorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `pc_attachment`
--

DROP TABLE IF EXISTS `pc_attachment`;
CREATE TABLE IF NOT EXISTS `pc_attachment` (
  `aid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `moduleid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `id` int(8) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(50) NOT NULL DEFAULT '',
  `filepath` varchar(80) NOT NULL DEFAULT '',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `fileext` char(10) NOT NULL DEFAULT '',
  `isimage` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `isthumb` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0',
  `uploadip` char(15) NOT NULL DEFAULT '',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`aid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `pc_attachment`
--

INSERT INTO `pc_attachment` (`aid`, `moduleid`, `catid`, `id`, `filename`, `filepath`, `filesize`, `fileext`, `isimage`, `isthumb`, `userid`, `createtime`, `uploadip`, `status`) VALUES
(1, 4, 0, 0, '未命名.jpg', './Uploads/201112/4ed725b1558a3.jpg', 85222, 'jpg', 1, 0, 1, 1322722737, '127.0.0.1', 0),
(2, 4, 0, 0, 'soft.jpg', './Uploads/201112/4ed725b1947ac.jpg', 20293, 'jpg', 1, 0, 1, 1322722737, '127.0.0.1', 0),
(3, 4, 0, 0, '未命名.jpg', './Uploads/201112/4ed725b1b731c.jpg', 85222, 'jpg', 1, 0, 1, 1322722737, '127.0.0.1', 0);

-- --------------------------------------------------------

--
-- 表的结构 `pc_buy_images`
--

DROP TABLE IF EXISTS `pc_buy_images`;
CREATE TABLE IF NOT EXISTS `pc_buy_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL,
  `account` varchar(20) NOT NULL COMMENT '用户帐号',
  `type` enum('1','2','3') NOT NULL COMMENT '支付类型 1为点数支付 2为张数支付 3免费购买',
  `url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `up_time` int(11) NOT NULL,
  `buytime` int(11) NOT NULL,
  `buy_values` int(11) NOT NULL,
  `sell_values` int(10) unsigned NOT NULL COMMENT '摄影师得到的点数',
  `images_id` int(11) NOT NULL,
  `up_userid` int(11) unsigned NOT NULL,
  `up_account` varchar(20) NOT NULL,
  `ip` varchar(30) NOT NULL COMMENT '下载来源IP',
  `ptitle` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '图片标题',
  `remark` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- 转存表中的数据 `pc_buy_images`
--

INSERT INTO `pc_buy_images` (`id`, `userid`, `account`, `type`, `url`, `up_time`, `buytime`, `buy_values`, `sell_values`, `images_id`, `up_userid`, `up_account`, `ip`, `ptitle`, `remark`) VALUES
(6, 3, 'cjx', '3', '3f9f61f890c6b60f1ed6eac84c31c83b.jpg', 1333438475, 1333817819, 0, 0, 6, 4, '', '127.0.0.1', '', '合作机构下载'),
(7, 3, 'cjx', '2', '3f9f61f890c6b60f1ed6eac84c31c83b.jpg', 1333438475, 1333817969, 0, 50, 6, 4, '', '127.0.0.1', '', ''),
(8, 3, 'cjx', '2', '3f9f61f890c6b60f1ed6eac84c31c83b.jpg', 1333438475, 1333818009, 0, 50, 6, 4, '', '127.0.0.1', '', ''),
(9, 3, 'cjx', '3', '3f9f61f890c6b60f1ed6eac84c31c83b.jpg', 1333438475, 1333818154, 0, 0, 6, 4, '', '127.0.0.1', '', '合作机构下载'),
(10, 3, 'cjx', '1', '3f9f61f890c6b60f1ed6eac84c31c83b.jpg', 1333438475, 1333818291, 95, 50, 6, 4, '', '127.0.0.1', '', '');

-- --------------------------------------------------------

--
-- 表的结构 `pc_cart`
--

DROP TABLE IF EXISTS `pc_cart`;
CREATE TABLE IF NOT EXISTS `pc_cart` (
  `pid` int(10) unsigned NOT NULL COMMENT '图片ID',
  `userid` int(10) unsigned NOT NULL COMMENT '用户ID',
  `url` varchar(128) NOT NULL COMMENT '图片地址',
  `up_time` int(11) NOT NULL COMMENT '图片上传时间',
  `up_userid` int(11) NOT NULL COMMENT '上传图片的用户ID',
  `createtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `title` varchar(128) NOT NULL COMMENT '图组标题',
  `type_one_name` varchar(50) NOT NULL COMMENT '图组分类',
  `type_one` int(11) NOT NULL,
  `add_type` tinyint(1) unsigned NOT NULL COMMENT '放入购物车方式 1手动放入 2自动放入'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `pc_cart`
--

INSERT INTO `pc_cart` (`pid`, `userid`, `url`, `up_time`, `up_userid`, `createtime`, `title`, `type_one_name`, `type_one`, `add_type`) VALUES
(6, 3, '3f9f61f890c6b60f1ed6eac84c31c83b.jpg', 1333438475, 4, 1333538906, '图组标题2cjx', '时事', 10100, 0);

-- --------------------------------------------------------

--
-- 表的结构 `pc_category`
--

DROP TABLE IF EXISTS `pc_category`;
CREATE TABLE IF NOT EXISTS `pc_category` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `catname` varchar(30) NOT NULL DEFAULT '',
  `catdir` varchar(30) NOT NULL DEFAULT '',
  `parentdir` varchar(50) NOT NULL DEFAULT '',
  `parentid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `moduleid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `module` char(24) NOT NULL DEFAULT '',
  `arrparentid` varchar(100) NOT NULL DEFAULT '',
  `arrchildid` varchar(100) NOT NULL DEFAULT '',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(150) NOT NULL DEFAULT '',
  `keywords` varchar(200) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `listorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ishtml` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ismenu` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `image` varchar(100) NOT NULL DEFAULT '',
  `child` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `url` varchar(100) NOT NULL DEFAULT '',
  `template_list` varchar(20) NOT NULL DEFAULT '',
  `template_show` varchar(20) NOT NULL DEFAULT '',
  `pagesize` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `readgroup` varchar(100) NOT NULL DEFAULT '',
  `listtype` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lang` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `urlruleid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `presentpoint` tinyint(3) NOT NULL DEFAULT '0',
  `chargepoint` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `paytype` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `repeatchargedays` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `postgroup` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `parentid` (`parentid`),
  KEY `listorder` (`listorder`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `pc_category`
--

INSERT INTO `pc_category` (`id`, `catname`, `catdir`, `parentdir`, `parentid`, `moduleid`, `module`, `arrparentid`, `arrchildid`, `type`, `title`, `keywords`, `description`, `listorder`, `ishtml`, `ismenu`, `hits`, `image`, `child`, `url`, `template_list`, `template_show`, `pagesize`, `readgroup`, `listtype`, `lang`, `urlruleid`, `presentpoint`, `chargepoint`, `paytype`, `repeatchargedays`, `postgroup`) VALUES
(1, '222', '', '', 0, 0, '', '0', '1', 0, '', '', '', 0, 0, 0, 0, '', 0, '/index.php?m=&a=index&id=1', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, '');

-- --------------------------------------------------------

--
-- 表的结构 `pc_column`
--

DROP TABLE IF EXISTS `pc_column`;
CREATE TABLE IF NOT EXISTS `pc_column` (
  `parentid` int(11) NOT NULL,
  `secondid` int(11) NOT NULL,
  `name` varchar(32) CHARACTER SET utf8 NOT NULL,
  `groupid` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL COMMENT '排序ID',
  `state` tinyint(1) unsigned NOT NULL COMMENT '0为隐藏 1为显示',
  PRIMARY KEY (`groupid`),
  UNIQUE KEY `groupid` (`groupid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `pc_column`
--

INSERT INTO `pc_column` (`parentid`, `secondid`, `name`, `groupid`, `sort`, `state`) VALUES
(0, 0, '时事', 10000, 0, 1),
(10000, 0, '时事', 10100, 0, 1),
(10000, 0, '体育', 10200, 1, 1),
(10000, 0, '文娱', 10300, 2, 1),
(0, 0, '文娱', 20000, 1, 1),
(20000, 0, '事件', 20100, 0, 1),
(20000, 0, '财经', 20200, 1, 1),
(0, 0, '时尚', 30000, 2, 1),
(30000, 0, '视觉', 30100, 0, 1),
(0, 0, '财经', 40000, 3, 1),
(40000, 0, '自然', 40100, 0, 1),
(40000, 0, '美食', 40200, 1, 1),
(40000, 0, '家居', 40300, 2, 1),
(0, 0, '体育', 50000, 4, 1),
(50000, 0, '三个月内', 50100, 0, 1),
(50000, 0, '半年内', 50200, 1, 1),
(0, 0, '24小时热图', 60000, 0, 1),
(0, 0, '图表', 70000, 5, 1),
(0, 0, '漫画', 80000, 6, 1);

-- --------------------------------------------------------

--
-- 表的结构 `pc_config`
--

DROP TABLE IF EXISTS `pc_config`;
CREATE TABLE IF NOT EXISTS `pc_config` (
  `id` smallint(8) unsigned NOT NULL AUTO_INCREMENT,
  `varname` varchar(20) NOT NULL DEFAULT '',
  `info` varchar(100) NOT NULL DEFAULT '',
  `groupid` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `value` text NOT NULL,
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `varname` (`varname`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=52 ;

--
-- 转存表中的数据 `pc_config`
--

INSERT INTO `pc_config` (`id`, `varname`, `info`, `groupid`, `value`, `type`) VALUES
(1, 'site_name', '网站名称', 2, '富图中国', 2),
(2, 'site_url', '网站网址', 2, 'http://localhost', 2),
(3, 'logo', '网站LOGO', 2, './Public/Images/logo.gif', 2),
(4, 'site_company_name', '企业名称', 2, 'yourphp企业建站系统', 2),
(5, 'site_email', '站点邮箱', 2, 'admin@yourphp.cn', 2),
(6, 'site_contact_name', '联系人', 2, 'liuxun', 2),
(7, 'site_tel', '联系电话', 2, '0317-5022625', 2),
(8, 'site_mobile', '手机号码', 2, '13292793176', 2),
(9, 'site_fax', '传真号码', 2, '0317-5022625', 2),
(10, 'site_address', '公司地址', 2, '河北省沧州市肃宁县宅南村', 2),
(11, 'qq', '客服QQ', 2, '147613338', 2),
(12, 'seo_title', '网站标题', 3, 'yourphp企业网站管理系统-企业建站-企业网站-行业网站建设-门户网站建设', 2),
(13, 'seo_keywords', '关键词', 3, '富图中国', 2),
(14, 'seo_description', '网站简介', 3, '', 2),
(15, 'mail_type', '邮件发送模式', 4, '1', 2),
(16, 'mail_server', '邮件服务器', 4, 'smtp.yourphp.cn', 2),
(17, 'mail_port', '邮件发送端口', 4, '25', 2),
(18, 'mail_from', '发件人地址', 4, 'admin@yourphp.cn', 2),
(19, 'mail_auth', 'AUTH LOGIN验证', 4, '1', 2),
(20, 'mail_user', '验证用户名', 4, 'admin@yourphp.cn', 2),
(21, 'mail_password', '验证密码', 4, '', 2),
(22, 'attach_maxsize', '允许上传附件大小', 5, '5200000', 1),
(23, 'attach_allowext', '允许上传附件类型', 5, 'jpg,jpeg,gif,png,doc,docx,rar,zip,swf', 2),
(24, 'watermark_enable', '是否开启图片水印', 5, '1', 1),
(25, 'watemard_text', '水印文字内容', 5, 'YourPHP', 2),
(26, 'watemard_text_size', '文字大小', 5, '18', 1),
(27, 'watemard_text_color', 'watemard_text_color', 5, '#FFFFFF', 2),
(28, 'watemard_text_face', '字体', 5, 'elephant.ttf', 2),
(29, 'watermark_minwidth', '图片最小宽度', 5, '300', 1),
(30, 'watermark_minheight', '水印最小高度', 5, '300', 1),
(31, 'watermark_img', '水印图片名称', 5, 'mark.png', 2),
(32, 'watermark_pct', '水印透明度', 5, '80', 1),
(33, 'watermark_quality', 'JPEG 水印质量', 5, '100', 1),
(34, 'watermark_pospadding', '水印边距', 5, '10', 1),
(35, 'watermark_pos', '水印位置', 5, '9', 1),
(36, 'PAGE_LISTROWS', '列表分页数', 6, '15', 1),
(37, 'URL_MODEL', 'URL访问模式', 6, '0', 1),
(38, 'URL_PATHINFO_DEPR', '参数分割符', 6, '/', 2),
(39, 'URL_HTML_SUFFIX', 'URL伪静态后缀', 6, '.html', 2),
(40, 'TOKEN_ON', '令牌验证', 6, '1', 1),
(41, 'TOKEN_NAME', '令牌表单字段', 6, '__hash__', 2),
(42, 'TMPL_CACHE_ON', '模板编译缓存', 6, '0', 1),
(43, 'TMPL_CACHE_TIME', '模板缓存有效期', 6, '-1', 1),
(44, 'HTML_CACHE_ON', '静态缓存', 6, '0', 1),
(45, 'HTML_CACHE_TIME', '缓存有效期', 6, '60', 1),
(46, 'HTML_READ_TYPE', '缓存读取方式', 6, '0', 1),
(47, 'HTML_FILE_SUFFIX', '静态文件后缀', 6, '.html', 2),
(48, 'ADMIN_ACCESS', 'ADMIN_ACCESS', 6, '114400dbcf82d642937e34559a21e475', 2),
(49, 'DEFAULT_THEME', '默认模板', 6, 'Default', 2),
(50, 'HOME_ISHTML', '首页生成html', 6, '0', 1),
(51, 'URL_URLRULE', 'URL', 6, '', 2);

-- --------------------------------------------------------

--
-- 表的结构 `pc_credit_detail`
--

DROP TABLE IF EXISTS `pc_credit_detail`;
CREATE TABLE IF NOT EXISTS `pc_credit_detail` (
  `id` int(11) NOT NULL,
  `account` varchar(50) NOT NULL,
  `send_time` int(11) NOT NULL,
  `credit_value` int(11) NOT NULL,
  `pay_time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `pc_credit_detail`
--

INSERT INTO `pc_credit_detail` (`id`, `account`, `send_time`, `credit_value`, `pay_time`) VALUES
(0, 'wangxb', 1322756890, 500, 1322756890),
(0, 'wangxb', 1322756890, 500, 1322756890);

-- --------------------------------------------------------

--
-- 表的结构 `pc_dbsource`
--

DROP TABLE IF EXISTS `pc_dbsource`;
CREATE TABLE IF NOT EXISTS `pc_dbsource` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `host` varchar(20) NOT NULL,
  `port` int(5) NOT NULL DEFAULT '3306',
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `dbname` varchar(50) NOT NULL,
  `dbtablepre` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `pc_download`
--

DROP TABLE IF EXISTS `pc_download`;
CREATE TABLE IF NOT EXISTS `pc_download` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userid` int(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(40) NOT NULL DEFAULT '',
  `title` varchar(120) NOT NULL DEFAULT '',
  `title_style` varchar(40) NOT NULL DEFAULT '',
  `thumb` varchar(100) NOT NULL DEFAULT '',
  `keywords` varchar(120) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `content` mediumtext NOT NULL,
  `template` varchar(40) NOT NULL DEFAULT '',
  `posid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `recommend` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `readgroup` varchar(100) NOT NULL DEFAULT '',
  `readpoint` smallint(5) unsigned NOT NULL,
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `url` varchar(60) NOT NULL DEFAULT '',
  `file` varchar(80) NOT NULL DEFAULT '',
  `ext` varchar(10) NOT NULL DEFAULT '',
  `size` varchar(10) NOT NULL DEFAULT '',
  `downs` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `status` (`id`,`status`,`listorder`),
  KEY `catid` (`id`,`catid`,`status`),
  KEY `listorder` (`id`,`catid`,`status`,`listorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `pc_downloader`
--

DROP TABLE IF EXISTS `pc_downloader`;
CREATE TABLE IF NOT EXISTS `pc_downloader` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(50) CHARACTER SET utf8 NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `sex` varchar(8) CHARACTER SET utf8 NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 NOT NULL,
  `phone` varchar(32) CHARACTER SET utf8 NOT NULL,
  `post_code` varchar(16) CHARACTER SET utf8 NOT NULL,
  `province` varchar(32) CHARACTER SET utf8 NOT NULL,
  `city` varchar(32) CHARACTER SET utf8 NOT NULL,
  `address` varchar(255) CHARACTER SET utf8 NOT NULL,
  `work_company` varchar(128) CHARACTER SET utf8 NOT NULL,
  `new_tip` tinyint(4) NOT NULL,
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `head_image_path` varchar(255) CHARACTER SET utf8 NOT NULL,
  `state` tinyint(11) NOT NULL DEFAULT '0',
  `role` varchar(16) CHARACTER SET utf8 NOT NULL,
  `user_values` int(11) NOT NULL,
  `discount` int(11) NOT NULL DEFAULT '100',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=45 ;

--
-- 转存表中的数据 `pc_downloader`
--

INSERT INTO `pc_downloader` (`id`, `account`, `name`, `sex`, `password`, `email`, `phone`, `post_code`, `province`, `city`, `address`, `work_company`, `new_tip`, `createtime`, `head_image_path`, `state`, `role`, `user_values`, `discount`) VALUES
(33, 'user', 'user', '1', 'c137340e996daae5fec608dcffd7683442cf8024', '123@126.com', '13857190634', '333333', '广西壮族自治区', '南宁市', '33333', '33343434', 0, 1324886017, '', 0, '下载用户', 0, 95),
(34, 'xieyd2', '谢有定', '1', 'c137340e996daae5fec608dcffd7683442cf8024', 'jingya8825@163.com', '13588038843', '310000', '浙江省', '杭州市', '杭州', '富图', 0, 1325426964, '', 1, '下载用户', 0, 100),
(35, 'cjx', '陈甲新', '1', 'c137340e996daae5fec608dcffd7683442cf8024', '234@qq.com', '13712341231', '320000', '湖南省', '衡阳市', '滨江', '淘宝', 0, 1325494824, '', 0, '下载用户', 97050, 50),
(36, 'cjx2324', '111', '1', 'c137340e996daae5fec608dcffd7683442cf8024', '1@q.com', '11111111111', '111111', '西藏自治区', '昌都地区', '1111', '111', 0, 1325494923, '', 1, '下载用户', 0, 100),
(37, 'xieyouding', '谢有定', '男', 'c137340e996daae5fec608dcffd7683442cf8024', 'jingya8825@sina.com', '13588038843', '310004', '浙江省', '杭州市', '晶晖商务大厦', '富图中国', 0, 1325873867, 'xieyouding.jpg', 0, '下载用户', 8380, 90),
(38, 'eeee', 'werwerwe', '1', 'c137340e996daae5fec608dcffd7683442cf8024', '222@125.com', '13757190063', '333333', '海南省', '海口市', '23232323', '3323423', 0, 1326193346, '', 1, '下载用户', 0, 100),
(39, 'down', 'down', '1', 'cb7ad6eecf07e0de50f64ce98069193e0a0ddd5e', '222@126.com', '13757190063', '333333', '辽宁省', '大连市', '2222222', '222222', 0, 1328949306, '', 0, '下载用户', 910, 90),
(40, 'yangqin', '杨琴', '1', 'c137340e996daae5fec608dcffd7683442cf8024', 'yangqin@photochina.com.cn', '13112341234', '123456', '浙江省', '杭州市', '123456', '123456', 0, 1329288903, '', 1, '下载用户', 0, 100),
(41, 'yangqin1', 'yangqin', '1', 'c137340e996daae5fec608dcffd7683442cf8024', 'yangqi@photochina.com.cn', '13112341234', '123456', '浙江省', '杭州市', '123456', '123456', 0, 1329289054, '', 1, '下载用户', 0, 100),
(42, 'chencongli ', '陈聪丽', '1', 'c137340e996daae5fec608dcffd7683442cf8024', 'wusaihua1@photochina.com.cn', '15158028218', '310000', '浙江省', '杭州市', '杭州', '富图', 0, 1330592560, '', 3, '下载用户', 2000, 100),
(43, 'chenlily8', '陈聪丽', '1', 'c137340e996daae5fec608dcffd7683442cf8024', '32674110@qq.com', '0579-28801080', '310003', '浙江省', '杭州市', '111', '222', 0, 1330592779, '', 0, '下载用户', 1400, 100),
(44, 'zhangzaiyou', '张亚', '1', 'c137340e996daae5fec608dcffd7683442cf8024', '2390560189@qq.com', '18868736822', '310006', '浙江省', '杭州市', '杭州', '富图', 0, 1330593775, '', 0, '下载用户', 2000, 80);

-- --------------------------------------------------------

--
-- 表的结构 `pc_editgroup`
--

DROP TABLE IF EXISTS `pc_editgroup`;
CREATE TABLE IF NOT EXISTS `pc_editgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 NOT NULL,
  `up_account` varchar(50) CHARACTER SET utf8 NOT NULL,
  `up_time` int(11) NOT NULL,
  `type_one` int(11) NOT NULL,
  `type_two` int(11) NOT NULL,
  `type_three` int(11) NOT NULL,
  `type_one_name` varchar(128) CHARACTER SET utf8 NOT NULL,
  `type_two_name` varchar(128) CHARACTER SET utf8 NOT NULL,
  `type_three_name` varchar(128) CHARACTER SET utf8 NOT NULL,
  `main_url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `check_time` int(11) NOT NULL,
  `location` varchar(128) CHARACTER SET utf8 NOT NULL,
  `key` varchar(128) CHARACTER SET utf8 NOT NULL,
  `key_type` varchar(50) CHARACTER SET utf8 NOT NULL,
  `title` varchar(128) CHARACTER SET utf8 NOT NULL,
  `remark` varchar(1024) CHARACTER SET utf8 NOT NULL,
  `province` varchar(128) CHARACTER SET utf8 NOT NULL,
  `photo_date` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- 转存表中的数据 `pc_editgroup`
--

INSERT INTO `pc_editgroup` (`id`, `username`, `up_account`, `up_time`, `type_one`, `type_two`, `type_three`, `type_one_name`, `type_two_name`, `type_three_name`, `main_url`, `check_time`, `location`, `key`, `key_type`, `title`, `remark`, `province`, `photo_date`) VALUES
(6, 'admin', 'xieyd', 1329549125, 0, 0, 0, '', '', '', './Uploads/thumb/32/2012218/s_A80EC0128A9DDAF6BE7D06AD0762242B.jpg', 1329811254, '', '', '', '309172_001', 'Christina Ricci and Robert Pattinson attend the Bel Ami photocall for the 62nd Berlin International Film Festival, in Berlin, Germany on February 17, 2012. The 62nd Berlinale takes place from 09 to 19 February. Photo by Aurore Marechal/ABACAPRESS.COM  # 309172_001', '', 1329549001),
(12, 'honggang', 'yangqin', 1329286870, 0, 0, 0, '', '', '', './Uploads/thumb/46/20120215/s_d9174931cb641448ede4ea62f8d4bdcf43647398.jpg', 1330495638, '', '', '', '26th Annual ASC Awards 2012', 'Charles Haid\n02/12/2012 26th Annual ASC Awards held at Grand Ballroom at Hollywood & Highland in Hollywood, CA Photo by Yoko Maegawa /', '', 0),
(14, 'yuanxiang', 'xieyd', 1330235379, 0, 0, 0, '', '', '', './Uploads/thumb/32/2012226/s_11B7AEC729293ED6E68A957A24DFC3F7.jpg', 1330496288, 'Los Angeles', '', '', 'Johnny Hallyday and Tony Parker Chat After the Clippers Game with their Women in Tow', '漏NATIONAL PHOTO GROUP \nJohnny Hallyday and wife Laeticia Boudou are seen chatting with Tony Parker and his girlfriend Axelle.\nJob: 021912J2\nNon-Exclusive Feb. 18th, 2012 Los Angeles, CA\nNPG.com\n', '', 1330235379),
(15, 'yeyuling', 'xieyd', 1328933045, 0, 0, 0, '', '', '', './Uploads/thumb/32/20120211/s_374026772e65d9193b9fab0e7a51bf7c82f45f92.JPG', 1330497429, 'Perth', '', '', 'Fernando Vedasco - Hopman Cup', 'Burswood Dome, Perth  Hopman Cup 2012\n 01/01/2012\nJarmila Gajdosova (AUS) Group  match\nPhoto: Frey Fotosports International / AMN\n Photo via Newscom', '', 0),
(17, 'zhangjing', 'xieyd', 1328500612, 0, 0, 0, '', '', '', './Uploads/thumb/32/20120206/s_79d198723129eeed0c9699b2e061815fae8247bb.jpg', 1330589296, '', '', '', '中国金陵舞', '2012年1月5日，南京，中国金陵舞。', '', 0);

-- --------------------------------------------------------

--
-- 表的结构 `pc_editgroup_detail`
--

DROP TABLE IF EXISTS `pc_editgroup_detail`;
CREATE TABLE IF NOT EXISTS `pc_editgroup_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `up_acct` varchar(64) CHARACTER SET utf8 NOT NULL,
  `image_id` int(11) NOT NULL,
  `up_time` int(11) NOT NULL,
  `check_time` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `small_url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `big_url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `is_main_face` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  `remark` varchar(512) CHARACTER SET utf8 NOT NULL,
  `key` varchar(128) CHARACTER SET utf8 NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `yiming` varchar(50) CHARACTER SET utf8 NOT NULL,
  `texie` int(11) NOT NULL,
  `ren` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `filesize` int(11) NOT NULL,
  `country` varchar(50) CHARACTER SET utf8 NOT NULL,
  `province` varchar(50) CHARACTER SET utf8 NOT NULL,
  `city` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=248 ;

--
-- 转存表中的数据 `pc_editgroup_detail`
--

INSERT INTO `pc_editgroup_detail` (`id`, `group_id`, `up_acct`, `image_id`, `up_time`, `check_time`, `state`, `url`, `small_url`, `big_url`, `is_main_face`, `points`, `remark`, `key`, `name`, `yiming`, `texie`, `ren`, `position`, `width`, `height`, `filesize`, `country`, `province`, `city`) VALUES
(51, 6, 'xieyd', 461, 1329549150, 1329811258, 0, './Uploads/images/32/2012218/A80EC0128A9DDAF6BE7D06AD0762242B.jpg', './Uploads/thumb/32/2012218/s_A80EC0128A9DDAF6BE7D06AD0762242B.jpg', './Uploads/thumb/32/2012218/m_A80EC0128A9DDAF6BE7D06AD0762242B.jpg', 0, 100, '309172_001', '', '', '', 0, 0, 0, 2401, 3608, 0, '', '', 'Berlin'),
(164, 12, 'yangqin', 305, 1329286893, 1330495648, 0, './Uploads/images/46/20120215/d9174931cb641448ede4ea62f8d4bdcf43647398.jpg', './Uploads/thumb/46/20120215/s_d9174931cb641448ede4ea62f8d4bdcf43647398.jpg', './Uploads/thumb/46/20120215/m_d9174931cb641448ede4ea62f8d4bdcf43647398.jpg', 0, 100, '2012-02-15,美国,B4413_193569_0027', 'Hollywood News Wire', '', '', 0, 0, 0, 3168, 4752, 0, '', '', ''),
(165, 12, 'yangqin', 306, 1329286905, 1330495648, 0, './Uploads/images/46/20120215/9415730a4c9aa0187a8a4c9e5bd1f3c3e8d34376.jpg', './Uploads/thumb/46/20120215/s_9415730a4c9aa0187a8a4c9e5bd1f3c3e8d34376.jpg', './Uploads/thumb/46/20120215/m_9415730a4c9aa0187a8a4c9e5bd1f3c3e8d34376.jpg', 0, 100, '2012-02-15,美国,B4413_193569_0028', 'Hollywood News Wire', '', '', 0, 0, 0, 3168, 4752, 0, '', '', ''),
(166, 12, 'yangqin', 307, 1329286917, 1330495648, 0, './Uploads/images/46/20120215/399cce8c58bb0c81af05464e315be3f73d7773f7.jpg', './Uploads/thumb/46/20120215/s_399cce8c58bb0c81af05464e315be3f73d7773f7.jpg', './Uploads/thumb/46/20120215/m_399cce8c58bb0c81af05464e315be3f73d7773f7.jpg', 0, 100, '2012-02-15,美国,B4413_193569_0029', 'Hollywood News Wire', '', '', 0, 0, 0, 3168, 4752, 0, '', '', ''),
(167, 12, 'yangqin', 308, 1329286929, 1330495648, 0, './Uploads/images/46/20120215/bfbdbc0abb26ea313083a19e159afa9b93722c52.jpg', './Uploads/thumb/46/20120215/s_bfbdbc0abb26ea313083a19e159afa9b93722c52.jpg', './Uploads/thumb/46/20120215/m_bfbdbc0abb26ea313083a19e159afa9b93722c52.jpg', 0, 100, '2012-02-15,美国,B4413_193569_0030', 'Hollywood News Wire', '', '', 0, 0, 0, 3168, 4752, 0, '', '', ''),
(168, 12, 'yangqin', 309, 1329286942, 1330495648, 0, './Uploads/images/46/20120215/fb402e6eb73b33a5b308ea74b4cf6f47ce01a43d.jpg', './Uploads/thumb/46/20120215/s_fb402e6eb73b33a5b308ea74b4cf6f47ce01a43d.jpg', './Uploads/thumb/46/20120215/m_fb402e6eb73b33a5b308ea74b4cf6f47ce01a43d.jpg', 0, 100, '2012-02-15,美国,B4413_193569_0031', 'Hollywood News Wire', '', '', 0, 0, 0, 3168, 4752, 0, '', '', ''),
(169, 12, 'yangqin', 310, 1329286953, 1330495648, 0, './Uploads/images/46/20120215/6bcaadbfdf183d2599d33c1cfc1e76f8f9a87224.jpg', './Uploads/thumb/46/20120215/s_6bcaadbfdf183d2599d33c1cfc1e76f8f9a87224.jpg', './Uploads/thumb/46/20120215/m_6bcaadbfdf183d2599d33c1cfc1e76f8f9a87224.jpg', 0, 100, '2012-02-15,美国,B4413_193569_0032', 'Hollywood News Wire', '', '', 0, 0, 0, 4752, 3168, 0, '', '', ''),
(170, 12, 'yangqin', 311, 1329286966, 1330495648, 0, './Uploads/images/46/20120215/e6c625cc29cab83110f16eaaa531f8fcdf0875cd.jpg', './Uploads/thumb/46/20120215/s_e6c625cc29cab83110f16eaaa531f8fcdf0875cd.jpg', './Uploads/thumb/46/20120215/m_e6c625cc29cab83110f16eaaa531f8fcdf0875cd.jpg', 0, 100, '2012-02-15,美国,B4413_193569_0033', 'Hollywood News Wire', '', '', 0, 0, 0, 3168, 4752, 0, '', '', ''),
(171, 12, 'yangqin', 312, 1329286981, 1330495648, 0, './Uploads/images/46/20120215/3881fab3ef239e6e32e773c53d0acb3a4f0ebc91.jpg', './Uploads/thumb/46/20120215/s_3881fab3ef239e6e32e773c53d0acb3a4f0ebc91.jpg', './Uploads/thumb/46/20120215/m_3881fab3ef239e6e32e773c53d0acb3a4f0ebc91.jpg', 0, 100, '2012-02-15,美国,B4413_193569_0034', 'Hollywood News Wire', '', '', 0, 0, 0, 3168, 4752, 0, '', '', ''),
(172, 12, 'yangqin', 313, 1329286993, 1330495648, 0, './Uploads/images/46/20120215/7e6757ac9c4bd8959e9c287ce83f26322fc4eb85.jpg', './Uploads/thumb/46/20120215/s_7e6757ac9c4bd8959e9c287ce83f26322fc4eb85.jpg', './Uploads/thumb/46/20120215/m_7e6757ac9c4bd8959e9c287ce83f26322fc4eb85.jpg', 0, 100, '2012-02-15,美国,B4413_193569_0035', 'Hollywood News Wire', '', '', 0, 0, 0, 3168, 4752, 0, '', '', ''),
(173, 12, 'yangqin', 314, 1329287005, 1330495649, 0, './Uploads/images/46/20120215/9e1b21fae6dfc95f3d6817ca4ba696712586c630.jpg', './Uploads/thumb/46/20120215/s_9e1b21fae6dfc95f3d6817ca4ba696712586c630.jpg', './Uploads/thumb/46/20120215/m_9e1b21fae6dfc95f3d6817ca4ba696712586c630.jpg', 0, 100, '2012-02-15,美国,B4413_193569_0036', 'Hollywood News Wire', '', '', 0, 0, 0, 3168, 4752, 0, '', '', ''),
(174, 12, 'yangqin', 315, 1329287018, 1330495649, 0, './Uploads/images/46/20120215/4f071de23f9254fdc213bf6259088b036fe17964.jpg', './Uploads/thumb/46/20120215/s_4f071de23f9254fdc213bf6259088b036fe17964.jpg', './Uploads/thumb/46/20120215/m_4f071de23f9254fdc213bf6259088b036fe17964.jpg', 0, 100, '2012-02-15,美国,B4413_193569_0037', 'Hollywood News Wire', '', '', 0, 0, 0, 3168, 4752, 0, '', '', ''),
(175, 14, 'xieyd', 946, 1330235379, 1330496301, 0, './Uploads/images/32/2012226/11B7AEC729293ED6E68A957A24DFC3F7.jpg', './Uploads/thumb/32/2012226/s_11B7AEC729293ED6E68A957A24DFC3F7.jpg', './Uploads/thumb/32/2012226/m_11B7AEC729293ED6E68A957A24DFC3F7.jpg', 0, 100, 'Johnny Hallyday, Laeticia Boudou, Tony Parker', 'Clippers,couple,couples,Johnny Hallyday,LA clippers,Laeticia Boudou,Tony Parker', '', '', 0, 0, 0, 560, 448, 0, 'USA', 'CA', 'Los Angeles'),
(176, 14, 'xieyd', 947, 1330235380, 1330496301, 0, './Uploads/images/32/2012226/0F13AA2A0C92C8B75CC79D397DC55DFF.jpg', './Uploads/thumb/32/2012226/s_0F13AA2A0C92C8B75CC79D397DC55DFF.jpg', './Uploads/thumb/32/2012226/m_0F13AA2A0C92C8B75CC79D397DC55DFF.jpg', 0, 100, 'Johnny Hallyday, Laeticia Boudou, Tony Parker', 'Clippers,couple,couples,Johnny Hallyday,LA clippers,Laeticia Boudou,Tony Parker', '', '', 0, 0, 0, 513, 560, 0, 'USA', 'CA', 'Los Angeles'),
(177, 14, 'xieyd', 948, 1330235381, 1330496301, 0, './Uploads/images/32/2012226/1551D94CF9055468F2190D04972ACA7E.jpg', './Uploads/thumb/32/2012226/s_1551D94CF9055468F2190D04972ACA7E.jpg', './Uploads/thumb/32/2012226/m_1551D94CF9055468F2190D04972ACA7E.jpg', 0, 100, 'Johnny Hallyday, Laeticia Boudou, Tony Parker', 'Clippers,couple,couples,Johnny Hallyday,LA clippers,Laeticia Boudou,Tony Parker', '', '', 0, 0, 0, 358, 560, 0, 'USA', 'CA', 'Los Angeles'),
(178, 14, 'xieyd', 949, 1330235385, 1330496301, 0, './Uploads/images/32/2012226/2055BDF148940027E58B41E636F9EC4E.jpg', './Uploads/thumb/32/2012226/s_2055BDF148940027E58B41E636F9EC4E.jpg', './Uploads/thumb/32/2012226/m_2055BDF148940027E58B41E636F9EC4E.jpg', 0, 100, 'Johnny Hallyday, Laeticia Boudou, Tony Parker', 'Clippers,couple,couples,Johnny Hallyday,LA clippers,Laeticia Boudou,Tony Parker', '', '', 0, 0, 0, 401, 560, 0, 'USA', 'CA', 'Los Angeles'),
(179, 14, 'xieyd', 950, 1330235386, 1330496301, 0, './Uploads/images/32/2012226/668DF7ED174EEC565CBE95566DDCF806.jpg', './Uploads/thumb/32/2012226/s_668DF7ED174EEC565CBE95566DDCF806.jpg', './Uploads/thumb/32/2012226/m_668DF7ED174EEC565CBE95566DDCF806.jpg', 0, 100, 'Johnny Hallyday, Laeticia Boudou, Tony Parker', 'Clippers,couple,couples,Johnny Hallyday,LA clippers,Laeticia Boudou,Tony Parker', '', '', 0, 0, 0, 406, 560, 0, 'USA', 'CA', 'Los Angeles'),
(180, 14, 'xieyd', 951, 1330235388, 1330496301, 0, './Uploads/images/32/2012226/35F9360BDD04A7632CD0DF1E0FE26E49.jpg', './Uploads/thumb/32/2012226/s_35F9360BDD04A7632CD0DF1E0FE26E49.jpg', './Uploads/thumb/32/2012226/m_35F9360BDD04A7632CD0DF1E0FE26E49.jpg', 0, 100, 'Johnny Hallyday, Laeticia Boudou, Tony Parker', 'Clippers,couple,couples,Johnny Hallyday,LA clippers,Laeticia Boudou,Tony Parker', '', '', 0, 0, 0, 511, 560, 0, 'USA', 'CA', 'Los Angeles'),
(181, 14, 'xieyd', 952, 1330235390, 1330496301, 0, './Uploads/images/32/2012226/8F1476DDF0A5BF56CD193F2374163B86.jpg', './Uploads/thumb/32/2012226/s_8F1476DDF0A5BF56CD193F2374163B86.jpg', './Uploads/thumb/32/2012226/m_8F1476DDF0A5BF56CD193F2374163B86.jpg', 0, 100, 'Johnny Hallyday, Laeticia Boudou, Tony Parker', 'Clippers,couple,couples,Johnny Hallyday,LA clippers,Laeticia Boudou,Tony Parker', '', '', 0, 0, 0, 405, 560, 0, 'USA', 'CA', 'Los Angeles'),
(182, 14, 'xieyd', 953, 1330235391, 1330496301, 0, './Uploads/images/32/2012226/90885EA01C0FE61DDE983EB9366BBA2A.jpg', './Uploads/thumb/32/2012226/s_90885EA01C0FE61DDE983EB9366BBA2A.jpg', './Uploads/thumb/32/2012226/m_90885EA01C0FE61DDE983EB9366BBA2A.jpg', 0, 100, 'Johnny Hallyday, Laeticia Boudou, Tony Parker', 'Clippers,couple,couples,Johnny Hallyday,LA clippers,Laeticia Boudou,Tony Parker', '', '', 0, 0, 0, 455, 560, 0, 'USA', 'CA', 'Los Angeles'),
(183, 14, 'xieyd', 954, 1330235394, 1330496301, 0, './Uploads/images/32/2012226/0BE2559CD3E91D90A42158FCB20D99B8.jpg', './Uploads/thumb/32/2012226/s_0BE2559CD3E91D90A42158FCB20D99B8.jpg', './Uploads/thumb/32/2012226/m_0BE2559CD3E91D90A42158FCB20D99B8.jpg', 0, 100, 'Johnny Hallyday, Laeticia Boudou, Tony Parker', 'Clippers,couple,couples,Johnny Hallyday,LA clippers,Laeticia Boudou,Tony Parker', '', '', 0, 0, 0, 373, 560, 0, 'USA', 'CA', 'Los Angeles'),
(184, 14, 'xieyd', 955, 1330235395, 1330496301, 0, './Uploads/images/32/2012226/0C702F7EBF42B4E94B60419C7DFE44B4.jpg', './Uploads/thumb/32/2012226/s_0C702F7EBF42B4E94B60419C7DFE44B4.jpg', './Uploads/thumb/32/2012226/m_0C702F7EBF42B4E94B60419C7DFE44B4.jpg', 0, 100, 'Johnny Hallyday, Laeticia Boudou, Tony Parker', 'Clippers,couple,couples,Johnny Hallyday,LA clippers,Laeticia Boudou,Tony Parker', '', '', 0, 0, 0, 385, 560, 0, 'USA', 'CA', 'Los Angeles'),
(185, 14, 'xieyd', 956, 1330235397, 1330496301, 0, './Uploads/images/32/2012226/E59003EA6F2E0E5946826681A65DCE3D.jpg', './Uploads/thumb/32/2012226/s_E59003EA6F2E0E5946826681A65DCE3D.jpg', './Uploads/thumb/32/2012226/m_E59003EA6F2E0E5946826681A65DCE3D.jpg', 0, 100, 'Johnny Hallyday, Laeticia Boudou, Tony Parker', 'Clippers,couple,couples,Johnny Hallyday,LA clippers,Laeticia Boudou,Tony Parker', '', '', 0, 0, 0, 411, 560, 0, 'USA', 'CA', 'Los Angeles'),
(186, 14, 'xieyd', 957, 1330235398, 1330496301, 0, './Uploads/images/32/2012226/B5F5FDA7816246EA07D4995D3EEE33C4.jpg', './Uploads/thumb/32/2012226/s_B5F5FDA7816246EA07D4995D3EEE33C4.jpg', './Uploads/thumb/32/2012226/m_B5F5FDA7816246EA07D4995D3EEE33C4.jpg', 0, 100, 'Johnny Hallyday, Laeticia Boudou, Tony Parker', 'Clippers,couple,couples,Johnny Hallyday,LA clippers,Laeticia Boudou,Tony Parker', '', '', 0, 0, 0, 517, 560, 0, 'USA', 'CA', 'Los Angeles'),
(187, 14, 'xieyd', 958, 1330235398, 1330496301, 0, './Uploads/images/32/2012226/740C785397246196A42F67EB26A3FEBA.jpg', './Uploads/thumb/32/2012226/s_740C785397246196A42F67EB26A3FEBA.jpg', './Uploads/thumb/32/2012226/m_740C785397246196A42F67EB26A3FEBA.jpg', 0, 100, 'Johnny Hallyday, Laeticia Boudou, Tony Parker', 'Clippers,couple,couples,Johnny Hallyday,LA clippers,Laeticia Boudou,Tony Parker', '', '', 0, 0, 0, 560, 473, 0, 'USA', 'CA', 'Los Angeles'),
(188, 14, 'xieyd', 959, 1330235400, 1330496301, 0, './Uploads/images/32/2012226/6CDED10D4BD6765DB2470ECCC7813F05.jpg', './Uploads/thumb/32/2012226/s_6CDED10D4BD6765DB2470ECCC7813F05.jpg', './Uploads/thumb/32/2012226/m_6CDED10D4BD6765DB2470ECCC7813F05.jpg', 0, 100, 'Johnny Hallyday, Laeticia Boudou, Tony Parker', 'Clippers,couple,couples,Johnny Hallyday,LA clippers,Laeticia Boudou,Tony Parker', '', '', 0, 0, 0, 399, 560, 0, 'USA', 'CA', 'Los Angeles'),
(189, 14, 'xieyd', 960, 1330235401, 1330496302, 0, './Uploads/images/32/2012226/5E318B8AF9B90F224D3B7C6A80E284D6.jpg', './Uploads/thumb/32/2012226/s_5E318B8AF9B90F224D3B7C6A80E284D6.jpg', './Uploads/thumb/32/2012226/m_5E318B8AF9B90F224D3B7C6A80E284D6.jpg', 0, 100, 'Johnny Hallyday, Laeticia Boudou, Tony Parker', 'Clippers,couple,couples,Johnny Hallyday,LA clippers,Laeticia Boudou,Tony Parker', '', '', 0, 0, 0, 560, 385, 0, 'USA', 'CA', 'Los Angeles'),
(190, 14, 'xieyd', 961, 1330235403, 1330496302, 0, './Uploads/images/32/2012226/1CD112061DE8BDDB1C88EDF87F67DB91.jpg', './Uploads/thumb/32/2012226/s_1CD112061DE8BDDB1C88EDF87F67DB91.jpg', './Uploads/thumb/32/2012226/m_1CD112061DE8BDDB1C88EDF87F67DB91.jpg', 0, 100, 'Johnny Hallyday, Laeticia Boudou, Tony Parker', 'Clippers,couple,couples,Johnny Hallyday,LA clippers,Laeticia Boudou,Tony Parker', '', '', 0, 0, 0, 560, 386, 0, 'USA', 'CA', 'Los Angeles'),
(191, 14, 'xieyd', 962, 1330235405, 1330496302, 0, './Uploads/images/32/2012226/D274310766A0BC8BB626429B929D7A27.jpg', './Uploads/thumb/32/2012226/s_D274310766A0BC8BB626429B929D7A27.jpg', './Uploads/thumb/32/2012226/m_D274310766A0BC8BB626429B929D7A27.jpg', 0, 100, 'Johnny Hallyday, Laeticia Boudou, Tony Parker', 'Clippers,couple,couples,Johnny Hallyday,LA clippers,Laeticia Boudou,Tony Parker', '', '', 0, 0, 0, 433, 560, 0, 'USA', 'CA', 'Los Angeles'),
(192, 14, 'xieyd', 963, 1330235407, 1330496302, 0, './Uploads/images/32/2012226/337BBDAF4BB4A908EA2B224EF6C3F976.jpg', './Uploads/thumb/32/2012226/s_337BBDAF4BB4A908EA2B224EF6C3F976.jpg', './Uploads/thumb/32/2012226/m_337BBDAF4BB4A908EA2B224EF6C3F976.jpg', 0, 100, 'Johnny Hallyday, Laeticia Boudou, Tony Parker', 'Clippers,couple,couples,Johnny Hallyday,LA clippers,Laeticia Boudou,Tony Parker', '', '', 0, 0, 0, 449, 560, 0, 'USA', 'CA', 'Los Angeles'),
(193, 15, 'xieyd', 192, 1328933075, 1330497434, 0, './Uploads/images/32/20120211/374026772e65d9193b9fab0e7a51bf7c82f45f92.JPG', './Uploads/thumb/32/20120211/s_374026772e65d9193b9fab0e7a51bf7c82f45f92.JPG', './Uploads/thumb/32/20120211/m_374026772e65d9193b9fab0e7a51bf7c82f45f92.JPG', 0, 100, 'JARMILA_GAJDOSOVA_HOPMAN_CUP_010112_037', 'TENNIS', '', '', 0, 0, 0, 2859, 4535, 0, 'Australia', 'Western Australia', 'Perth'),
(194, 15, 'xieyd', 193, 1328933081, 1330497435, 0, './Uploads/images/32/20120211/d2ca689fa0a8b553ba0c4b1cb2e39399241cc13e.JPG', './Uploads/thumb/32/20120211/s_d2ca689fa0a8b553ba0c4b1cb2e39399241cc13e.JPG', './Uploads/thumb/32/20120211/m_d2ca689fa0a8b553ba0c4b1cb2e39399241cc13e.JPG', 0, 100, 'JARMILA_GAJDOSOVA_HOPMAN_CUP_010112_019', 'TENNIS', '', '', 0, 0, 0, 2173, 1642, 0, 'Australia', 'Western Australia', 'Perth'),
(195, 15, 'xieyd', 194, 1328933085, 1330497435, 0, './Uploads/images/32/20120211/cc9d92d65565d85385bfa77256418b5fdaa6c98a.JPG', './Uploads/thumb/32/20120211/s_cc9d92d65565d85385bfa77256418b5fdaa6c98a.JPG', './Uploads/thumb/32/20120211/m_cc9d92d65565d85385bfa77256418b5fdaa6c98a.JPG', 0, 100, 'LLEYTON_HEWITT_HOPMAN_CUP_010112_051', 'TENNIS', '', '', 0, 0, 0, 1814, 1692, 0, 'Australia', 'Western Australia', 'Perth'),
(196, 15, 'xieyd', 195, 1328933092, 1330497435, 0, './Uploads/images/32/20120211/b951dfa06d846a55c1f0e604adc8c696f5d8d867.JPG', './Uploads/thumb/32/20120211/s_b951dfa06d846a55c1f0e604adc8c696f5d8d867.JPG', './Uploads/thumb/32/20120211/m_b951dfa06d846a55c1f0e604adc8c696f5d8d867.JPG', 0, 100, 'ANABEL_MEDINA_GARRIGUES_HOPMAN_CUP_010112_038', 'TENNIS', '', '', 0, 0, 0, 2173, 2800, 0, 'Australia', 'Western Australia', 'Perth'),
(197, 15, 'xieyd', 196, 1328933098, 1330497435, 0, './Uploads/images/32/20120211/aae3789615b5944a6a07494b9134c2a82503e146.JPG', './Uploads/thumb/32/20120211/s_aae3789615b5944a6a07494b9134c2a82503e146.JPG', './Uploads/thumb/32/20120211/m_aae3789615b5944a6a07494b9134c2a82503e146.JPG', 0, 100, 'JARMILA_GAJDOSOVA_HOPMAN_CUP_010112_013', 'TENNIS', '', '', 0, 0, 0, 1878, 2551, 0, 'Australia', 'Western Australia', 'Perth'),
(198, 15, 'xieyd', 197, 1328933104, 1330497435, 0, './Uploads/images/32/20120211/7f750b8f75e5237a118274ddadeb0ea5dd63b742.JPG', './Uploads/thumb/32/20120211/s_7f750b8f75e5237a118274ddadeb0ea5dd63b742.JPG', './Uploads/thumb/32/20120211/m_7f750b8f75e5237a118274ddadeb0ea5dd63b742.JPG', 0, 100, 'JARMILA_GAJDOSOVA_HOPMAN_CUP_010112_017', 'TENNIS', '', '', 0, 0, 0, 2025, 2645, 0, 'Australia', 'Western Australia', 'Perth'),
(199, 15, 'xieyd', 198, 1328933112, 1330497435, 0, './Uploads/images/32/20120211/e510ddbb3f21f753b2d84d4eabe00856fec8e410.JPG', './Uploads/thumb/32/20120211/s_e510ddbb3f21f753b2d84d4eabe00856fec8e410.JPG', './Uploads/thumb/32/20120211/m_e510ddbb3f21f753b2d84d4eabe00856fec8e410.JPG', 0, 100, 'LLEYTON_HEWITT_HOPMAN_CUP_010112_043', 'TENNIS', '', '', 0, 0, 0, 2752, 3195, 0, 'Australia', 'Western Australia', 'Perth'),
(200, 15, 'xieyd', 199, 1328933121, 1330497435, 0, './Uploads/images/32/20120211/aa1c2a38803e0f93d3008d538f132e86a866b7c6.JPG', './Uploads/thumb/32/20120211/s_aa1c2a38803e0f93d3008d538f132e86a866b7c6.JPG', './Uploads/thumb/32/20120211/m_aa1c2a38803e0f93d3008d538f132e86a866b7c6.JPG', 0, 100, 'JARMILA_GAJDOSOVA_HOPMAN_CUP_010112_030', 'TENNIS', '', '', 0, 0, 0, 3418, 3122, 0, 'Australia', 'Western Australia', 'Perth'),
(201, 15, 'xieyd', 200, 1328933126, 1330497435, 0, './Uploads/images/32/20120211/c6f9cf584617cfddb306ec07161292a272489965.JPG', './Uploads/thumb/32/20120211/s_c6f9cf584617cfddb306ec07161292a272489965.JPG', './Uploads/thumb/32/20120211/m_c6f9cf584617cfddb306ec07161292a272489965.JPG', 0, 100, 'JARMILA_GAJDOSOVA_HOPMAN_CUP_010112_026', 'TENNIS', '', '', 0, 0, 0, 1952, 2762, 0, 'Australia', 'Western Australia', 'Perth'),
(202, 15, 'xieyd', 201, 1328933134, 1330497435, 0, './Uploads/images/32/20120211/f04fbc854280c7d71836ad2f2480bc7611bacf88.JPG', './Uploads/thumb/32/20120211/s_f04fbc854280c7d71836ad2f2480bc7611bacf88.JPG', './Uploads/thumb/32/20120211/m_f04fbc854280c7d71836ad2f2480bc7611bacf88.JPG', 0, 100, 'JARMILA_GAJDOSOVA_HOPMAN_CUP_010112_023', 'TENNIS', '', '', 0, 0, 0, 2668, 2768, 0, 'Australia', 'Western Australia', 'Perth'),
(203, 15, 'xieyd', 202, 1328933138, 1330497435, 0, './Uploads/images/32/20120211/d7a89eeacf4b3923b22296eca0b5641ed1762fc7.JPG', './Uploads/thumb/32/20120211/s_d7a89eeacf4b3923b22296eca0b5641ed1762fc7.JPG', './Uploads/thumb/32/20120211/m_d7a89eeacf4b3923b22296eca0b5641ed1762fc7.JPG', 0, 100, 'JARMILA_GAJDOSOVA_HOPMAN_CUP_010112_016', 'TENNIS', '', '', 0, 0, 0, 2350, 1465, 0, 'Australia', 'Western Australia', 'Perth'),
(204, 15, 'xieyd', 203, 1328933148, 1330497435, 0, './Uploads/images/32/20120211/a9f1626f7a4692098834c62e7cdcb734dc4c48cf.JPG', './Uploads/thumb/32/20120211/s_a9f1626f7a4692098834c62e7cdcb734dc4c48cf.JPG', './Uploads/thumb/32/20120211/m_a9f1626f7a4692098834c62e7cdcb734dc4c48cf.JPG', 0, 100, 'JARMILA_GAJDOSOVA_HOPMAN_CUP_010112_028', 'TENNIS', '', '', 0, 0, 0, 2354, 4303, 0, 'Australia', 'Western Australia', 'Perth'),
(205, 15, 'xieyd', 204, 1328933157, 1330497435, 0, './Uploads/images/32/20120211/da030915dd22ce628c81a1ff5817d77285207981.JPG', './Uploads/thumb/32/20120211/s_da030915dd22ce628c81a1ff5817d77285207981.JPG', './Uploads/thumb/32/20120211/m_da030915dd22ce628c81a1ff5817d77285207981.JPG', 0, 100, 'ANABEL_MEDINA_GARRIGUES_HOPMAN_CUP_010112_034', 'TENNIS', '', '', 0, 0, 0, 2498, 3665, 0, 'Australia', 'Western Australia', 'Perth'),
(206, 15, 'xieyd', 205, 1328933166, 1330497435, 0, './Uploads/images/32/20120211/1028577cb9f6352ba99c135e639c09c0b4fbad15.JPG', './Uploads/thumb/32/20120211/s_1028577cb9f6352ba99c135e639c09c0b4fbad15.JPG', './Uploads/thumb/32/20120211/m_1028577cb9f6352ba99c135e639c09c0b4fbad15.JPG', 0, 100, 'LLEYTON_HEWITT_HOPMAN_CUP_010112_044', 'TENNIS', '', '', 0, 0, 0, 2601, 3405, 0, 'Australia', 'Western Australia', 'Perth'),
(207, 15, 'xieyd', 206, 1328933171, 1330497435, 0, './Uploads/images/32/20120211/777dd7ba1a13126a97252f0f1fb12006f520408b.JPG', './Uploads/thumb/32/20120211/s_777dd7ba1a13126a97252f0f1fb12006f520408b.JPG', './Uploads/thumb/32/20120211/m_777dd7ba1a13126a97252f0f1fb12006f520408b.JPG', 0, 100, 'JARMILA_GAJDOSOVA_HOPMAN_CUP_010112_021', 'TENNIS', '', '', 0, 0, 0, 1558, 2438, 0, 'Australia', 'Western Australia', 'Perth'),
(212, 17, 'xieyd', 34, 1328500708, 1330589305, 0, './Uploads/images/32/20120206/79d198723129eeed0c9699b2e061815fae8247bb.jpg', './Uploads/thumb/32/20120206/s_79d198723129eeed0c9699b2e061815fae8247bb.jpg', './Uploads/thumb/32/20120206/m_79d198723129eeed0c9699b2e061815fae8247bb.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 1812, 2865, 0, 'United States of America', 'New York', 'New York'),
(213, 17, 'xieyd', 35, 1328500713, 1330589305, 0, './Uploads/images/32/20120206/5eab406cf38fd276eb9bb893570e7df6826a6a63.jpg', './Uploads/thumb/32/20120206/s_5eab406cf38fd276eb9bb893570e7df6826a6a63.jpg', './Uploads/thumb/32/20120206/m_5eab406cf38fd276eb9bb893570e7df6826a6a63.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 3000, 1831, 0, 'United States of America', 'New York', 'New York'),
(214, 17, 'xieyd', 36, 1328500719, 1330589305, 0, './Uploads/images/32/20120206/178213744650861632505744b36be65623e42072.jpg', './Uploads/thumb/32/20120206/s_178213744650861632505744b36be65623e42072.jpg', './Uploads/thumb/32/20120206/m_178213744650861632505744b36be65623e42072.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 1996, 3001, 0, 'United States of America', 'New York', 'New York'),
(215, 17, 'xieyd', 37, 1328500725, 1330589305, 0, './Uploads/images/32/20120206/38566c8b1fabe52f78bc729d37c8bf7b18b61934.jpg', './Uploads/thumb/32/20120206/s_38566c8b1fabe52f78bc729d37c8bf7b18b61934.jpg', './Uploads/thumb/32/20120206/m_38566c8b1fabe52f78bc729d37c8bf7b18b61934.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 1940, 2893, 0, 'United States of America', 'New York', 'New York'),
(216, 17, 'xieyd', 38, 1328500733, 1330589305, 0, './Uploads/images/32/20120206/f7141d61430147baa1e1872fcc685591c562b762.jpg', './Uploads/thumb/32/20120206/s_f7141d61430147baa1e1872fcc685591c562b762.jpg', './Uploads/thumb/32/20120206/m_f7141d61430147baa1e1872fcc685591c562b762.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 1936, 2937, 0, 'United States of America', 'New York', 'New York'),
(217, 17, 'xieyd', 39, 1328500738, 1330589305, 0, './Uploads/images/32/20120206/f9013a9a43e93c7c4fc87072977432baf146da1a.jpg', './Uploads/thumb/32/20120206/s_f9013a9a43e93c7c4fc87072977432baf146da1a.jpg', './Uploads/thumb/32/20120206/m_f9013a9a43e93c7c4fc87072977432baf146da1a.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 3000, 1831, 0, 'United States of America', 'New York', 'New York'),
(218, 17, 'xieyd', 40, 1328500745, 1330589306, 0, './Uploads/images/32/20120206/90ef3cd6d63f91db709886c7168adc1c530c04c8.jpg', './Uploads/thumb/32/20120206/s_90ef3cd6d63f91db709886c7168adc1c530c04c8.jpg', './Uploads/thumb/32/20120206/m_90ef3cd6d63f91db709886c7168adc1c530c04c8.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 2784, 1997, 0, 'United States of America', 'New York', 'New York'),
(219, 17, 'xieyd', 41, 1328500750, 1330589306, 0, './Uploads/images/32/20120206/74e025ca563e5f1f4ca40af373e7afda4d55f193.jpg', './Uploads/thumb/32/20120206/s_74e025ca563e5f1f4ca40af373e7afda4d55f193.jpg', './Uploads/thumb/32/20120206/m_74e025ca563e5f1f4ca40af373e7afda4d55f193.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 2720, 1997, 0, 'United States of America', 'New York', 'New York'),
(220, 17, 'xieyd', 42, 1328500756, 1330589306, 0, './Uploads/images/32/20120206/49a10b8ba61c58e5e8d6f8420f48e78a517218c8.jpg', './Uploads/thumb/32/20120206/s_49a10b8ba61c58e5e8d6f8420f48e78a517218c8.jpg', './Uploads/thumb/32/20120206/m_49a10b8ba61c58e5e8d6f8420f48e78a517218c8.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 2824, 1997, 0, 'United States of America', 'New York', 'New York'),
(221, 17, 'xieyd', 43, 1328500761, 1330589306, 0, './Uploads/images/32/20120206/0f82d9f21f41e85d4e8326600be8a717d530ff11.jpg', './Uploads/thumb/32/20120206/s_0f82d9f21f41e85d4e8326600be8a717d530ff11.jpg', './Uploads/thumb/32/20120206/m_0f82d9f21f41e85d4e8326600be8a717d530ff11.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 2940, 1997, 0, 'United States of America', 'New York', 'New York'),
(222, 17, 'xieyd', 44, 1328500765, 1330589306, 0, './Uploads/images/32/20120206/a7372cfdcfbfdc23cac1aca608a2dfe089cdf1f7.jpg', './Uploads/thumb/32/20120206/s_a7372cfdcfbfdc23cac1aca608a2dfe089cdf1f7.jpg', './Uploads/thumb/32/20120206/m_a7372cfdcfbfdc23cac1aca608a2dfe089cdf1f7.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 2580, 1903, 0, 'United States of America', 'New York', 'New York'),
(223, 17, 'xieyd', 45, 1328500772, 1330589306, 0, './Uploads/images/32/20120206/c812bcbef32bf72f4832c1ecf3361658a4a59113.jpg', './Uploads/thumb/32/20120206/s_c812bcbef32bf72f4832c1ecf3361658a4a59113.jpg', './Uploads/thumb/32/20120206/m_c812bcbef32bf72f4832c1ecf3361658a4a59113.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 3000, 1997, 0, 'United States of America', 'New York', 'New York'),
(224, 17, 'xieyd', 46, 1328500779, 1330589306, 0, './Uploads/images/32/20120206/0c8ffded8a6757360e074fab59b481eedeb13154.jpg', './Uploads/thumb/32/20120206/s_0c8ffded8a6757360e074fab59b481eedeb13154.jpg', './Uploads/thumb/32/20120206/m_0c8ffded8a6757360e074fab59b481eedeb13154.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 3000, 1859, 0, 'United States of America', 'New York', 'New York'),
(225, 17, 'xieyd', 47, 1328500787, 1330589306, 0, './Uploads/images/32/20120206/14d6cb166efc6534600055bcc1825c09fd03107b.jpg', './Uploads/thumb/32/20120206/s_14d6cb166efc6534600055bcc1825c09fd03107b.jpg', './Uploads/thumb/32/20120206/m_14d6cb166efc6534600055bcc1825c09fd03107b.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 3000, 1997, 0, 'United States of America', 'New York', 'New York'),
(226, 17, 'xieyd', 48, 1328500794, 1330589306, 0, './Uploads/images/32/20120206/7404fdd55d367f53486da7da87d9ca66d858c0a5.jpg', './Uploads/thumb/32/20120206/s_7404fdd55d367f53486da7da87d9ca66d858c0a5.jpg', './Uploads/thumb/32/20120206/m_7404fdd55d367f53486da7da87d9ca66d858c0a5.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 3000, 1997, 0, 'United States of America', 'New York', 'New York'),
(227, 17, 'xieyd', 49, 1328500801, 1330589306, 0, './Uploads/images/32/20120206/e1fa5b9dfea8d3d9d506c5add338b2d2abbaf207.jpg', './Uploads/thumb/32/20120206/s_e1fa5b9dfea8d3d9d506c5add338b2d2abbaf207.jpg', './Uploads/thumb/32/20120206/m_e1fa5b9dfea8d3d9d506c5add338b2d2abbaf207.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 1752, 2841, 0, 'United States of America', 'New York', 'New York'),
(228, 17, 'xieyd', 50, 1328500808, 1330589306, 0, './Uploads/images/32/20120206/dc1a1d0b2e212d10ac680575032cd233fa1f8620.jpg', './Uploads/thumb/32/20120206/s_dc1a1d0b2e212d10ac680575032cd233fa1f8620.jpg', './Uploads/thumb/32/20120206/m_dc1a1d0b2e212d10ac680575032cd233fa1f8620.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 1997, 2725, 0, 'United States of America', 'New York', 'New York'),
(229, 17, 'xieyd', 51, 1328500818, 1330589306, 0, './Uploads/images/32/20120206/ac37d8981231486a3cf90ce3418069d5cccebddc.jpg', './Uploads/thumb/32/20120206/s_ac37d8981231486a3cf90ce3418069d5cccebddc.jpg', './Uploads/thumb/32/20120206/m_ac37d8981231486a3cf90ce3418069d5cccebddc.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 1996, 3001, 0, 'United States of America', 'New York', 'New York'),
(230, 17, 'xieyd', 52, 1328500825, 1330589306, 0, './Uploads/images/32/20120206/df61deecebbd46b9416374bc36321e984752aeea.jpg', './Uploads/thumb/32/20120206/s_df61deecebbd46b9416374bc36321e984752aeea.jpg', './Uploads/thumb/32/20120206/m_df61deecebbd46b9416374bc36321e984752aeea.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 1996, 3001, 0, 'United States of America', 'New York', 'New York'),
(231, 17, 'xieyd', 53, 1328500836, 1330589306, 0, './Uploads/images/32/20120206/f5eb3337667bb092add9e4b2d7be1fdc3e7dafb5.jpg', './Uploads/thumb/32/20120206/s_f5eb3337667bb092add9e4b2d7be1fdc3e7dafb5.jpg', './Uploads/thumb/32/20120206/m_f5eb3337667bb092add9e4b2d7be1fdc3e7dafb5.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 3000, 1997, 0, 'United States of America', 'New York', 'New York'),
(232, 17, 'xieyd', 54, 1328500841, 1330589306, 0, './Uploads/images/32/20120206/fb2c5dda60d1e593bae2aa9e02fd1781b1f35684.jpg', './Uploads/thumb/32/20120206/s_fb2c5dda60d1e593bae2aa9e02fd1781b1f35684.jpg', './Uploads/thumb/32/20120206/m_fb2c5dda60d1e593bae2aa9e02fd1781b1f35684.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 3000, 1997, 0, 'United States of America', 'New York', 'New York'),
(233, 17, 'xieyd', 55, 1328500847, 1330589306, 0, './Uploads/images/32/20120206/0db98dfea3fe18ac3d61a3c3abb5962b38dde7b3.jpg', './Uploads/thumb/32/20120206/s_0db98dfea3fe18ac3d61a3c3abb5962b38dde7b3.jpg', './Uploads/thumb/32/20120206/m_0db98dfea3fe18ac3d61a3c3abb5962b38dde7b3.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 3000, 1731, 0, 'United States of America', 'New York', 'New York'),
(234, 17, 'xieyd', 56, 1328500854, 1330589306, 0, './Uploads/images/32/20120206/49130841c260b8a1ad66d679b5ab7cfc88e7018a.jpg', './Uploads/thumb/32/20120206/s_49130841c260b8a1ad66d679b5ab7cfc88e7018a.jpg', './Uploads/thumb/32/20120206/m_49130841c260b8a1ad66d679b5ab7cfc88e7018a.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 2564, 1997, 0, 'United States of America', 'New York', 'New York'),
(235, 17, 'xieyd', 57, 1328500861, 1330589306, 0, './Uploads/images/32/20120206/99c396e2eacb11d34e3db6e99cb7f91f8cab747b.jpg', './Uploads/thumb/32/20120206/s_99c396e2eacb11d34e3db6e99cb7f91f8cab747b.jpg', './Uploads/thumb/32/20120206/m_99c396e2eacb11d34e3db6e99cb7f91f8cab747b.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 2788, 1667, 0, 'United States of America', 'New York', 'New York'),
(236, 17, 'xieyd', 58, 1328500868, 1330589306, 0, './Uploads/images/32/20120206/6d644b09a1def64af0e38820ff5acece52b7fd86.jpg', './Uploads/thumb/32/20120206/s_6d644b09a1def64af0e38820ff5acece52b7fd86.jpg', './Uploads/thumb/32/20120206/m_6d644b09a1def64af0e38820ff5acece52b7fd86.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 1824, 2861, 0, 'United States of America', 'New York', 'New York'),
(237, 17, 'xieyd', 59, 1328500874, 1330589307, 0, './Uploads/images/32/20120206/db4c112e9d9461f2cb0a22fcd2b0db30e03b3b7e.jpg', './Uploads/thumb/32/20120206/s_db4c112e9d9461f2cb0a22fcd2b0db30e03b3b7e.jpg', './Uploads/thumb/32/20120206/m_db4c112e9d9461f2cb0a22fcd2b0db30e03b3b7e.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 1588, 2789, 0, 'United States of America', 'New York', 'New York'),
(238, 17, 'xieyd', 60, 1328500880, 1330589307, 0, './Uploads/images/32/20120206/6b14d0e18192edcba1da59df1ac606ee271a551a.jpg', './Uploads/thumb/32/20120206/s_6b14d0e18192edcba1da59df1ac606ee271a551a.jpg', './Uploads/thumb/32/20120206/m_6b14d0e18192edcba1da59df1ac606ee271a551a.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 1620, 2821, 0, 'United States of America', 'New York', 'New York'),
(239, 17, 'xieyd', 61, 1328500887, 1330589307, 0, './Uploads/images/32/20120206/e4c30e979d2420a69163d71660680a779102ec3a.jpg', './Uploads/thumb/32/20120206/s_e4c30e979d2420a69163d71660680a779102ec3a.jpg', './Uploads/thumb/32/20120206/m_e4c30e979d2420a69163d71660680a779102ec3a.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 1572, 2501, 0, 'United States of America', 'New York', 'New York'),
(240, 17, 'xieyd', 62, 1328500891, 1330589307, 0, './Uploads/images/32/20120206/09839be428670b95258c892ef68c647aea8abbbe.jpg', './Uploads/thumb/32/20120206/s_09839be428670b95258c892ef68c647aea8abbbe.jpg', './Uploads/thumb/32/20120206/m_09839be428670b95258c892ef68c647aea8abbbe.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 1812, 2497, 0, 'United States of America', 'New York', 'New York'),
(241, 17, 'xieyd', 63, 1328500896, 1330589307, 0, './Uploads/images/32/20120206/af56f2dcff6309c69b06d8b95c0c0795b4ac5fed.jpg', './Uploads/thumb/32/20120206/s_af56f2dcff6309c69b06d8b95c0c0795b4ac5fed.jpg', './Uploads/thumb/32/20120206/m_af56f2dcff6309c69b06d8b95c0c0795b4ac5fed.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 1692, 2609, 0, 'United States of America', 'New York', 'New York'),
(242, 17, 'xieyd', 64, 1328500902, 1330589307, 0, './Uploads/images/32/20120206/5f6918b9daf5a7f0d508e318375082948ca5c010.jpg', './Uploads/thumb/32/20120206/s_5f6918b9daf5a7f0d508e318375082948ca5c010.jpg', './Uploads/thumb/32/20120206/m_5f6918b9daf5a7f0d508e318375082948ca5c010.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 2712, 1997, 0, 'United States of America', 'New York', 'New York'),
(243, 17, 'xieyd', 65, 1328500908, 1330589307, 0, './Uploads/images/32/20120206/c595c9bdeacd40a196942939bb36c65e5c78b20e.jpg', './Uploads/thumb/32/20120206/s_c595c9bdeacd40a196942939bb36c65e5c78b20e.jpg', './Uploads/thumb/32/20120206/m_c595c9bdeacd40a196942939bb36c65e5c78b20e.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 3000, 1997, 0, 'United States of America', 'New York', 'New York'),
(244, 17, 'xieyd', 66, 1328500913, 1330589307, 0, './Uploads/images/32/20120206/04dc0031327b5b7fb0f9ee35b7cb741b168eeb26.jpg', './Uploads/thumb/32/20120206/s_04dc0031327b5b7fb0f9ee35b7cb741b168eeb26.jpg', './Uploads/thumb/32/20120206/m_04dc0031327b5b7fb0f9ee35b7cb741b168eeb26.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 3000, 1997, 0, 'United States of America', 'New York', 'New York'),
(245, 17, 'xieyd', 67, 1328500919, 1330589307, 0, './Uploads/images/32/20120206/955bd3a27fd2bda9ce1f664c592721bbd8830720.jpg', './Uploads/thumb/32/20120206/s_955bd3a27fd2bda9ce1f664c592721bbd8830720.jpg', './Uploads/thumb/32/20120206/m_955bd3a27fd2bda9ce1f664c592721bbd8830720.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 3000, 1615, 0, 'United States of America', 'New York', 'New York'),
(246, 17, 'xieyd', 68, 1328500924, 1330589307, 0, './Uploads/images/32/20120206/9e33e6131dc8e68cc951315166c978743c3d1ce6.jpg', './Uploads/thumb/32/20120206/s_9e33e6131dc8e68cc951315166c978743c3d1ce6.jpg', './Uploads/thumb/32/20120206/m_9e33e6131dc8e68cc951315166c978743c3d1ce6.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 3000, 1683, 0, 'United States of America', 'New York', 'New York'),
(247, 17, 'xieyd', 69, 1328500929, 1330589307, 0, './Uploads/images/32/20120206/380f52dfd18bbb70edf205f9bad9adcfe8d746dd.jpg', './Uploads/thumb/32/20120206/s_380f52dfd18bbb70edf205f9bad9adcfe8d746dd.jpg', './Uploads/thumb/32/20120206/m_380f52dfd18bbb70edf205f9bad9adcfe8d746dd.jpg', 0, 100, '2012年1月5日，南京，中国金陵舞。', '南京，金陵舞，舞蹈，艺术，', '', '', 0, 0, 0, 3000, 1839, 0, 'United States of America', 'New York', 'New York');

-- --------------------------------------------------------

--
-- 表的结构 `pc_feedback`
--

DROP TABLE IF EXISTS `pc_feedback`;
CREATE TABLE IF NOT EXISTS `pc_feedback` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `telephone` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `content` mediumtext NOT NULL,
  `ip` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(50) NOT NULL DEFAULT '',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `typeid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userid` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `pc_field`
--

DROP TABLE IF EXISTS `pc_field`;
CREATE TABLE IF NOT EXISTS `pc_field` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `moduleid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `field` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(30) NOT NULL DEFAULT '',
  `tips` varchar(150) NOT NULL DEFAULT '',
  `required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `minlength` int(10) unsigned NOT NULL DEFAULT '0',
  `maxlength` int(10) unsigned NOT NULL DEFAULT '0',
  `pattern` varchar(255) NOT NULL DEFAULT '',
  `errormsg` varchar(255) NOT NULL DEFAULT '',
  `class` varchar(20) NOT NULL DEFAULT '',
  `type` varchar(20) NOT NULL DEFAULT '',
  `setup` mediumtext NOT NULL,
  `ispost` tinyint(1) NOT NULL DEFAULT '0',
  `unpostgroup` varchar(60) NOT NULL DEFAULT '',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `issystem` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=103 ;

--
-- 转存表中的数据 `pc_field`
--

INSERT INTO `pc_field` (`id`, `moduleid`, `field`, `name`, `tips`, `required`, `minlength`, `maxlength`, `pattern`, `errormsg`, `class`, `type`, `setup`, `ispost`, `unpostgroup`, `listorder`, `status`, `issystem`) VALUES
(1, 1, 'title', '标题', '', 1, 3, 80, '', '标题必填3-80个字', '', 'title', 'array (\n  ''thumb'' => ''1'',\n  ''style'' => ''0'',\n  ''size'' => '''',\n)', 1, '', 0, 1, 1),
(2, 1, 'keywords', '关键词', '', 0, 0, 0, '', '', '', 'text', 'array (\n  ''size'' => ''55'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n)', 1, '', 0, 1, 1),
(3, 1, 'description', 'SEO简介', '', 0, 0, 0, '', '', '', 'textarea', 'array (\n  ''rows'' => ''4'',\n  ''cols'' => ''55'',\n  ''default'' => '''',\n)', 1, '', 0, 1, 1),
(4, 1, 'content', '内容', '', 0, 0, 0, '', '', '', 'editor', 'array (\n  ''toolbar'' => ''full'',\n  ''default'' => '''',\n  ''height'' => '''',\n  ''showpage'' => ''1'',\n  ''enablekeylink'' => ''0'',\n  ''replacenum'' => '''',\n  ''enablesaveimage'' => ''0'',\n  ''flashupload'' => ''1'',\n  ''alowuploadexts'' => ''*.jpg;*.jpeg;*.gif;*.doc;*.rar;*.zip;*.xls'',\n)', 1, '', 0, 1, 1),
(5, 2, 'catid', '栏目', '', 1, 1, 6, 'digits', '必须选择一个栏目', '', 'catid', '', 1, '', 0, 1, 1),
(6, 2, 'title', '标题', '', 1, 0, 0, '', '标题必须为1-80个字符', '', 'title', 'array (\n  ''thumb'' => ''1'',\n  ''style'' => ''1'',\n  ''size'' => ''55'',\n)', 1, '', 0, 1, 1),
(7, 2, 'keywords', '关键词', '', 0, 0, 0, '', '', '', 'text', 'array (\n  ''size'' => ''55'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n)', 1, '', 0, 1, 1),
(8, 2, 'description', 'SEO简介', '', 0, 0, 0, '', '', '', 'textarea', 'array (\n  ''rows'' => ''4'',\n  ''cols'' => ''55'',\n  ''default'' => '''',\n)', 1, '', 0, 1, 1),
(9, 2, 'content', '内容', '', 0, 0, 0, '', '', '', 'editor', 'array (\n  ''toolbar'' => ''full'',\n  ''default'' => '''',\n  ''height'' => '''',\n  ''show_add_description'' => ''1'',\n  ''show_auto_thumb'' => ''1'',\n  ''showpage'' => ''1'',\n  ''enablekeylink'' => ''0'',\n  ''replacenum'' => '''',\n  ''enablesaveimage'' => ''0'',\n  ''flashupload'' => ''1'',\n  ''alowuploadexts'' => '''',\n)', 1, '', 0, 1, 1),
(10, 2, 'createtime', '发布时间', '', 1, 0, 0, '', '', '', 'datetime', '', 1, '3,4', 0, 1, 1),
(11, 2, 'copyfrom', '来源', '', 0, 0, 0, '0', '', '', 'text', 'array (\n  ''size'' => ''20'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 0, 1, 1),
(12, 2, 'fromlink', '来源网址', '', 0, 0, 0, '', '', '', 'text', 'array (\n  ''size'' => ''20'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n)', 1, '', 0, 1, 1),
(13, 2, 'readgroup', '访问权限', '', 0, 0, 0, '', '', '', 'groupid', 'array (\n  ''inputtype'' => ''checkbox'',\n  ''fieldtype'' => ''varchar'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => ''85'',\n  ''default'' => '''',\n)', 1, '3,4', 0, 1, 1),
(14, 2, 'posid', '推荐位', '', 0, 0, 0, '', '', '', 'posid', '', 1, '3,4', 0, 1, 1),
(15, 2, 'template', '模板', '', 0, 0, 0, '', '', '', 'template', '', 1, '3,4', 0, 1, 1),
(16, 2, 'status', '状态', '', 0, 0, 0, '', '', '', 'radio', 'array (\n  ''options'' => ''发布|1\r\n定时发布|0'',\n  ''fieldtype'' => ''tinyint'',\n  ''labelwidth'' => ''75'',\n  ''default'' => ''1'',\n)', 1, '3,4', 0, 1, 1),
(17, 3, 'catid', '栏目', '', 1, 1, 6, '', '必须选择一个栏目', '', 'catid', '', 1, '', 0, 1, 1),
(18, 3, 'title', '标题', '', 1, 1, 80, '', '标题必须为1-80个字符', '', 'title', 'array (\n  ''thumb'' => ''1'',\n  ''style'' => ''1'',\n  ''size'' => ''55'',\n)', 1, '', 0, 1, 1),
(19, 3, 'keywords', '关键词', '', 0, 0, 80, '', '', '', 'text', 'array (\n  ''size'' => ''55'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 0, 1, 1),
(20, 3, 'description', 'SEO简介', '', 0, 0, 0, '', '', '', 'textarea', 'array (\n  ''fieldtype'' => ''mediumtext'',\n  ''rows'' => ''4'',\n  ''cols'' => ''55'',\n  ''default'' => '''',\n)', 1, '', 0, 1, 1),
(21, 3, 'content', '内容', '', 0, 0, 0, '', '', '', 'editor', 'array (\n  ''toolbar'' => ''full'',\n  ''default'' => '''',\n  ''height'' => '''',\n  ''showpage'' => ''1'',\n  ''enablekeylink'' => ''0'',\n  ''replacenum'' => '''',\n  ''enablesaveimage'' => ''0'',\n  ''flashupload'' => ''1'',\n  ''alowuploadexts'' => '''',\n)', 1, '', 10, 1, 1),
(22, 3, 'createtime', '发布时间', '', 1, 0, 0, '', '', '', 'datetime', '', 1, '3,4', 93, 1, 1),
(31, 2, 'recommend', '允许评论', '', 0, 0, 1, '', '', '', 'radio', 'array (\n  ''options'' => ''允许评论|1\r\n不允许评论|0'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => '''',\n  ''default'' => ''1'',\n)', 1, '3,4', 0, 0, 0),
(30, 3, 'xinghao', '型号', '', 0, 0, 30, '', '', '', 'text', 'array (\n  ''size'' => ''20'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 0, 1, 1),
(25, 3, 'readgroup', '访问权限', '', 0, 0, 0, '', '', '', 'groupid', 'array (\n  ''inputtype'' => ''checkbox'',\n  ''fieldtype'' => ''tinyint'',\n  ''labelwidth'' => ''85'',\n  ''default'' => '''',\n)', 1, '3,4', 96, 0, 1),
(26, 3, 'posid', '推荐位', '', 0, 0, 0, '', '', '', 'posid', '', 1, '3,4', 97, 1, 1),
(27, 3, 'template', '模板', '', 0, 0, 0, '', '', '', 'template', '', 1, '3,4', 98, 1, 1),
(28, 3, 'status', '状态', '', 0, 0, 0, '', '', '', 'radio', 'array (\n  ''options'' => ''发布|1\r\n定时发布|0'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => ''75'',\n  ''default'' => ''1'',\n)', 1, '3,4', 99, 1, 1),
(29, 3, 'price', '价格', '', 0, 0, 0, '', '', '', 'number', 'array (\n  ''numbertype'' => ''1'',\n  ''decimaldigits'' => ''2'',\n  ''default'' => ''0'',\n)', 1, '', 0, 1, 1),
(34, 3, 'recommend', '允许评论', '', 0, 0, 1, '', '', '', 'radio', 'array (\n  ''options'' => ''允许评论|1\r\n不允许评论|0'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => '''',\n  ''default'' => '''',\n)', 1, '3,4', 0, 0, 0),
(32, 2, 'readpoint', '阅读收费', '', 0, 0, 3, '', '', '', 'number', 'array (\n  ''size'' => ''5'',\n  ''numbertype'' => ''1'',\n  ''decimaldigits'' => ''0'',\n  ''default'' => '''',\n)', 1, '3,4', 0, 0, 0),
(33, 2, 'hits', '点击次数', '', 0, 0, 8, '', '', '', 'number', 'array (\n  ''size'' => ''5'',\n  ''numbertype'' => ''1'',\n  ''decimaldigits'' => ''0'',\n  ''default'' => '''',\n)', 1, '', 0, 0, 0),
(35, 3, 'readpoint', '阅读收费', '', 0, 0, 5, '', '', '', 'number', 'array (\n  ''size'' => ''5'',\n  ''numbertype'' => ''1'',\n  ''decimaldigits'' => ''0'',\n  ''default'' => ''0'',\n)', 1, '3,4', 0, 0, 0),
(36, 3, 'hits', '点击次数', '', 0, 0, 8, '', '', '', 'number', 'array (\n  ''size'' => ''10'',\n  ''numbertype'' => ''1'',\n  ''decimaldigits'' => ''0'',\n  ''default'' => ''0'',\n)', 1, '', 0, 0, 0),
(37, 4, 'catid', '栏目', '', 1, 1, 6, '', '必须选择一个栏目', '', 'catid', '', 1, '', 0, 1, 1),
(38, 4, 'title', '标题', '', 1, 1, 80, '', '标题必须为1-80个字符', '', 'title', 'array (\n  ''thumb'' => ''1'',\n  ''style'' => ''1'',\n  ''size'' => ''55'',\n)', 1, '', 0, 1, 1),
(39, 4, 'keywords', '关键词', '', 0, 0, 80, '', '', '', 'text', 'array (\n  ''size'' => ''55'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 0, 1, 1),
(40, 4, 'description', 'SEO简介', '', 0, 0, 0, '', '', '', 'textarea', 'array (\n  ''fieldtype'' => ''mediumtext'',\n  ''rows'' => ''4'',\n  ''cols'' => ''55'',\n  ''default'' => '''',\n)', 1, '', 0, 1, 1),
(41, 4, 'content', '内容', '', 0, 0, 0, '', '', '', 'editor', 'array (\n  ''toolbar'' => ''full'',\n  ''default'' => '''',\n  ''height'' => '''',\n  ''showpage'' => ''1'',\n  ''enablekeylink'' => ''0'',\n  ''replacenum'' => '''',\n  ''enablesaveimage'' => ''0'',\n  ''flashupload'' => ''1'',\n  ''alowuploadexts'' => '''',\n)', 1, '', 10, 1, 1),
(42, 4, 'createtime', '发布时间', '', 1, 0, 0, '', '', '', 'datetime', '', 1, '3,4', 93, 1, 1),
(43, 4, 'recommend', '允许评论', '', 0, 0, 1, '', '', '', 'radio', 'array (\n  ''options'' => ''允许评论|1\r\n不允许评论|0'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => '''',\n  ''default'' => '''',\n)', 1, '3,4', 0, 0, 0),
(44, 4, 'readpoint', '阅读收费', '', 0, 0, 5, '', '', '', 'number', 'array (\n  ''size'' => ''5'',\n  ''numbertype'' => ''1'',\n  ''decimaldigits'' => ''0'',\n  ''default'' => ''0'',\n)', 1, '3,4', 0, 0, 0),
(45, 4, 'hits', '点击次数', '', 0, 0, 8, '', '', '', 'number', 'array (\n  ''size'' => ''10'',\n  ''numbertype'' => ''1'',\n  ''decimaldigits'' => ''0'',\n  ''default'' => ''0'',\n)', 1, '', 0, 0, 0),
(46, 4, 'readgroup', '访问权限', '', 0, 0, 0, '', '', '', 'groupid', 'array (\n  ''inputtype'' => ''checkbox'',\n  ''fieldtype'' => ''tinyint'',\n  ''labelwidth'' => ''85'',\n  ''default'' => '''',\n)', 1, '3,4', 96, 0, 1),
(47, 4, 'posid', '推荐位', '', 0, 0, 0, '', '', '', 'posid', '', 1, '3,4', 97, 1, 1),
(48, 4, 'template', '模板', '', 0, 0, 0, '', '', '', 'template', '', 1, '3,4', 98, 1, 1),
(49, 4, 'status', '状态', '', 0, 0, 0, '', '', '', 'radio', 'array (\n  ''options'' => ''发布|1\r\n定时发布|0'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => ''75'',\n  ''default'' => ''1'',\n)', 1, '3,4', 99, 1, 1),
(50, 5, 'catid', '栏目', '', 1, 1, 6, '', '必须选择一个栏目', '', 'catid', '', 1, '', 0, 1, 1),
(51, 5, 'title', '标题', '', 1, 1, 80, '', '标题必须为1-80个字符', '', 'title', 'array (\n  ''thumb'' => ''1'',\n  ''style'' => ''1'',\n  ''size'' => ''55'',\n)', 1, '', 0, 1, 1),
(52, 5, 'keywords', '关键词', '', 0, 0, 80, '', '', '', 'text', 'array (\n  ''size'' => ''55'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 0, 1, 1),
(53, 5, 'description', 'SEO简介', '', 0, 0, 0, '', '', '', 'textarea', 'array (\n  ''fieldtype'' => ''mediumtext'',\n  ''rows'' => ''4'',\n  ''cols'' => ''55'',\n  ''default'' => '''',\n)', 1, '', 0, 1, 1),
(54, 5, 'content', '内容', '', 0, 0, 0, '', '', '', 'editor', 'array (\n  ''toolbar'' => ''full'',\n  ''default'' => '''',\n  ''height'' => '''',\n  ''showpage'' => ''1'',\n  ''enablekeylink'' => ''0'',\n  ''replacenum'' => '''',\n  ''enablesaveimage'' => ''0'',\n  ''flashupload'' => ''1'',\n  ''alowuploadexts'' => '''',\n)', 1, '', 10, 1, 1),
(55, 5, 'createtime', '发布时间', '', 1, 0, 0, '', '', '', 'datetime', '', 1, '3,4', 93, 1, 1),
(56, 5, 'recommend', '允许评论', '', 0, 0, 1, '', '', '', 'radio', 'array (\n  ''options'' => ''允许评论|1\r\n不允许评论|0'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => '''',\n  ''default'' => '''',\n)', 1, '3,4', 0, 0, 0),
(57, 5, 'readpoint', '阅读收费', '', 0, 0, 5, '', '', '', 'number', 'array (\n  ''size'' => ''5'',\n  ''numbertype'' => ''1'',\n  ''decimaldigits'' => ''0'',\n  ''default'' => ''0'',\n)', 1, '3,4', 0, 0, 0),
(58, 5, 'hits', '点击次数', '', 0, 0, 8, '', '', '', 'number', 'array (\n  ''size'' => ''10'',\n  ''numbertype'' => ''1'',\n  ''decimaldigits'' => ''0'',\n  ''default'' => ''0'',\n)', 1, '', 0, 0, 0),
(59, 5, 'readgroup', '访问权限', '', 0, 0, 0, '', '', '', 'groupid', 'array (\n  ''inputtype'' => ''checkbox'',\n  ''fieldtype'' => ''tinyint'',\n  ''labelwidth'' => ''85'',\n  ''default'' => '''',\n)', 1, '3,4', 96, 0, 1),
(60, 5, 'posid', '推荐位', '', 0, 0, 0, '', '', '', 'posid', '', 1, '3,4', 97, 1, 1),
(61, 5, 'template', '模板', '', 0, 0, 0, '', '', '', 'template', '', 1, '3,4', 98, 1, 1),
(62, 5, 'status', '状态', '', 0, 0, 0, '', '', '', 'radio', 'array (\n  ''options'' => ''发布|1\r\n定时发布|0'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => ''75'',\n  ''default'' => ''1'',\n)', 1, '3,4', 99, 1, 1),
(63, 3, 'pics', '图片', '', 0, 0, 0, '', '', '', 'images', 'array (\n  ''size'' => ''55'',\n  ''default'' => '''',\n  ''upload_maxnum'' => ''10'',\n  ''upload_maxsize'' => ''2'',\n  ''upload_allowext'' => ''*.jpeg;*.jpg;*.gif'',\n  ''watermark'' => ''0'',\n  ''more'' => ''1'',\n)', 1, '', 0, 1, 0),
(64, 4, 'pics', '图组', '', 0, 0, 0, '', '', '', 'images', 'array (\n  ''size'' => ''55'',\n  ''default'' => '''',\n  ''upload_maxnum'' => ''20'',\n  ''upload_maxsize'' => ''2'',\n  ''upload_allowext'' => ''*.jpeg;*.jpg;*.png;*.gif'',\n  ''watermark'' => ''0'',\n  ''more'' => ''1'',\n)', 1, '', 0, 1, 0),
(65, 5, 'file', '上传文件', '', 0, 0, 0, '', '', '', 'file', 'array (\n  ''size'' => ''55'',\n  ''default'' => '''',\n  ''upload_maxsize'' => ''2'',\n  ''upload_allowext'' => ''*.zip;*.rar;*.doc;*.ppt'',\n  ''more'' => ''1'',\n)', 1, '', 0, 1, 0),
(66, 5, 'ext', '文档类型', '', 0, 0, 10, '', '', '', 'text', 'array (\n  ''size'' => ''10'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 0, 1, 0),
(67, 5, 'size', '文档大小', '', 0, 0, 10, '', '', '', 'text', 'array (\n  ''size'' => ''10'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 0, 1, 0),
(68, 5, 'downs', '下载次数', '', 0, 0, 0, '', '', '', 'number', 'array (\n  ''size'' => ''10'',\n  ''numbertype'' => ''1'',\n  ''decimaldigits'' => ''0'',\n  ''default'' => '''',\n)', 1, '', 0, 0, 0),
(69, 6, 'username', '姓名', '', 1, 2, 20, 'cn_username', '', '', 'text', 'array (\n  ''size'' => ''10'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 2, 1, 0),
(70, 6, 'telephone', '电话', '', 0, 0, 0, 'tel', '', '', 'text', 'array (\n  ''size'' => ''20'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 4, 1, 0),
(71, 6, 'email', '邮箱', '', 1, 0, 50, 'email', '', '', 'text', 'array (\n  ''size'' => ''20'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 2, 1, 0),
(72, 6, 'content', '内容', '', 1, 4, 200, '', '', '', 'textarea', 'array (\n  ''fieldtype'' => ''mediumtext'',\n  ''rows'' => ''5'',\n  ''cols'' => ''60'',\n  ''default'' => '''',\n)', 1, '', 5, 1, 0),
(81, 7, 'status', '状态', '', 0, 0, 1, '', '', '', 'radio', 'array (\n  ''options'' => ''已审核|1\r\n未审核|0'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => ''75'',\n  ''default'' => ''1'',\n)', 1, '3,4', 99, 1, 1),
(73, 6, 'ip', '提交IP', '', 0, 0, 0, '', '', '', 'text', 'array (\n  ''size'' => ''20'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 0, '3,4', 6, 1, 0),
(74, 6, 'title', '标题', '', 1, 4, 50, '', '', '', 'text', 'array (\n  ''size'' => ''40'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '3,4', 1, 1, 0),
(80, 35, 'createtime', '发布时间', '', 1, 0, 0, '', '', '', 'datetime', '', 1, '3,4', 93, 1, 1),
(76, 6, 'createtime', '提交时间', '', 0, 0, 0, '', '', '', 'datetime', '', 0, '3,4', 98, 1, 0),
(79, 6, 'typeid', '反馈类别', '', 0, 0, 0, '', '', '', 'typeid', 'array (\n  ''inputtype'' => ''select'',\n  ''fieldtype'' => ''smallint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => '''',\n  ''default'' => ''4'',\n)', 1, '', 0, 1, 0),
(78, 6, 'status', '审核状态', '', 0, 0, 1, '', '', '', 'radio', 'array (\n  ''options'' => ''己审核|1\r\n未审核|0'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => '''',\n  ''default'' => ''0'',\n)', 0, '3,4', 99, 1, 0),
(82, 7, 'name', '网站名称', '', 1, 2, 50, '', '', '', 'text', 'array (\n  ''size'' => ''40'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 1, 1, 0),
(83, 7, 'logo', '网站LOGO', '', 0, 0, 0, '', '', '', 'image', 'array (\n  ''size'' => ''50'',\n  ''default'' => '''',\n  ''upload_maxsize'' => '''',\n  ''upload_allowext'' => ''jpg,jpeg,gif,png'',\n  ''watermark'' => ''0'',\n  ''more'' => ''0'',\n)', 1, '', 2, 1, 0),
(84, 7, 'siteurl', '网站地址', '', 1, 10, 150, 'url', '', '', 'text', 'array (\n  ''size'' => ''50'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 3, 1, 0),
(85, 7, 'typeid', '友情链接分类', '', 0, 0, 0, '', '', '', 'typeid', 'array (\n  ''inputtype'' => ''select'',\n  ''fieldtype'' => ''smallint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => '''',\n  ''default'' => ''1'',\n)', 1, '', 0, 1, 0),
(86, 7, 'linktype', '链接类型', '', 0, 0, 1, '', '', '', 'radio', 'array (\n  ''options'' => ''文字链接|1\r\nLOGO链接|2'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => '''',\n  ''default'' => ''1'',\n)', 1, '', 0, 1, 0),
(87, 7, 'siteinfo', '站点简介', '', 0, 0, 0, '', '', '', 'textarea', 'array (\n  ''fieldtype'' => ''mediumtext'',\n  ''rows'' => ''3'',\n  ''cols'' => ''60'',\n  ''default'' => '''',\n)', 1, '', 4, 1, 0),
(88, 8, 'createtime', '提交时间', '', 1, 0, 0, '', '', '', 'datetime', '', 0, '3,4', 93, 1, 1),
(89, 8, 'status', '状态', '', 0, 0, 0, '', '', '', 'radio', 'array (\n  ''options'' => ''已审核|1\r\n未审核|0'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => ''75'',\n  ''default'' => ''0'',\n)', 0, '3,4', 99, 1, 1),
(90, 8, 'title', '标题', '', 1, 2, 50, '', '', '', 'text', 'array (\n  ''size'' => ''40'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 0, 1, 0),
(91, 8, 'username', '姓名', '', 1, 2, 20, '', '', '', 'text', 'array (\n  ''size'' => ''10'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 0, 1, 0),
(92, 8, 'telephone', '电话', '', 0, 0, 0, 'tel', '', '', 'text', 'array (\n  ''size'' => ''20'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 1, 1, 0),
(93, 8, 'email', '邮箱', '', 1, 0, 40, 'email', '', '', 'text', 'array (\n  ''size'' => ''20'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 1, '', 0, 1, 0),
(94, 8, 'content', '内容', '', 1, 2, 200, '', '', '', 'textarea', 'array (\n  ''fieldtype'' => ''mediumtext'',\n  ''rows'' => ''4'',\n  ''cols'' => ''50'',\n  ''default'' => '''',\n)', 1, '', 10, 1, 0),
(95, 8, 'reply_content', '回复', '', 0, 0, 0, '', '', '', 'textarea', 'array (\n  ''fieldtype'' => ''mediumtext'',\n  ''rows'' => ''4'',\n  ''cols'' => ''50'',\n  ''default'' => '''',\n)', 0, '3,4', 10, 1, 0),
(96, 8, 'ip', 'IP', '', 0, 0, 15, '', '', '', 'text', 'array (\n  ''size'' => ''15'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 0, '3,4', 90, 1, 0),
(97, 9, 'createtime', '发布时间', '', 1, 0, 0, '', '', '', 'datetime', '', 0, '3,4', 93, 1, 1),
(98, 9, 'status', '状态', '', 0, 0, 0, '', '', '', 'radio', 'array (\n  ''options'' => ''已审核|1\r\n未审核|0'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''labelwidth'' => ''75'',\n  ''default'' => ''1'',\n)', 0, '3,4', 99, 1, 1),
(99, 9, 'name', '客服名称', '', 0, 2, 20, '', '', '', 'text', 'array (\n  ''size'' => ''20'',\n  ''default'' => '''',\n  ''ispassword'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n)', 0, '', 0, 1, 0),
(100, 9, 'type', '客服类型', '', 1, 1, 2, '0', '', '', 'select', 'array (\n  ''options'' => ''QQ|1\r\nMSN|2\r\n旺旺|3\r\n贸易通|6\r\n电话|4\r\n代码|5'',\n  ''multiple'' => ''0'',\n  ''fieldtype'' => ''tinyint'',\n  ''numbertype'' => ''1'',\n  ''size'' => '''',\n  ''default'' => '''',\n)', 0, '', 0, 1, 0),
(101, 9, 'code', 'ID或代码', '', 0, 2, 0, '0', '', '', 'textarea', 'array (\n  ''fieldtype'' => ''mediumtext'',\n  ''rows'' => ''4'',\n  ''cols'' => ''50'',\n  ''default'' => '''',\n)', 0, '', 10, 1, 0),
(102, 9, 'skin', '风格样式', '', 0, 0, 3, '0', '', '', 'select', 'array (\n  ''options'' => ''无风格图标|0\r\nQQ风格1|q1\r\nQQ风格2|q2\r\nQQ风格3|q3\r\nQQ风格4|q4\r\nQQ风格5|q5\r\nQQ风格6|q6\r\nQQ风格7|q7\r\nMSN小图|m1\r\nMSN大图1|m2\r\nMSN大图2|m3\r\nMSN大图3|m4\r\n旺旺小图|w2\r\n旺旺大图|w1\r\n贸易通|al1'',\n  ''multiple'' => ''0'',\n  ''fieldtype'' => ''varchar'',\n  ''numbertype'' => ''1'',\n  ''size'' => '''',\n  ''default'' => '''',\n)', 0, '', 0, 1, 0);

-- --------------------------------------------------------

--
-- 表的结构 `pc_gonggao`
--

DROP TABLE IF EXISTS `pc_gonggao`;
CREATE TABLE IF NOT EXISTS `pc_gonggao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `content` text NOT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `pc_gonggao`
--

INSERT INTO `pc_gonggao` (`id`, `title`, `content`, `time`) VALUES
(1, '1111111111111', 'ddddddddddddddddddddddd222222222222222222222', 1328937463),
(3, '公告测试', '这是测试公告', 1328951541);

-- --------------------------------------------------------

--
-- 表的结构 `pc_groupon_detail`
--

DROP TABLE IF EXISTS `pc_groupon_detail`;
CREATE TABLE IF NOT EXISTS `pc_groupon_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `up_time` int(11) NOT NULL COMMENT '审核客户创建时间',
  `image_count` int(11) NOT NULL,
  `check_time` int(11) NOT NULL COMMENT '审核时间',
  `check_oper` varchar(50) NOT NULL COMMENT '后台审核客服帐号',
  `c_type` tinyint(2) NOT NULL COMMENT '1资料图片 2时事图片 ',
  `main_url` varchar(128) NOT NULL,
  `main_url_date` int(11) NOT NULL COMMENT '主图上传时间',
  `main_url_userid` int(11) unsigned NOT NULL COMMENT '主图的用户ID',
  `type_one` int(11) NOT NULL,
  `type_two` int(11) NOT NULL,
  `type_three` int(11) NOT NULL,
  `type_one_name` varchar(50) NOT NULL,
  `type_two_name` varchar(50) NOT NULL,
  `type_three_name` varchar(50) NOT NULL,
  `title` varchar(32) NOT NULL,
  `group_key` varchar(64) NOT NULL,
  `book_name` varchar(128) NOT NULL,
  `state` tinyint(2) unsigned NOT NULL COMMENT '图组状态 1待审核 2部分通过 3通过 4没通过 100编辑状态 200已删除',
  `photo_size` int(11) NOT NULL,
  `is_top` tinyint(4) NOT NULL,
  `is_24hot` tinyint(4) NOT NULL,
  `is_editor_tj` tinyint(4) NOT NULL,
  `city` varchar(64) NOT NULL,
  `topic_id` int(11) NOT NULL DEFAULT '0',
  `is_topic` int(11) NOT NULL DEFAULT '0',
  `province` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `photo_date` int(11) NOT NULL,
  `author` varchar(50) NOT NULL COMMENT '作者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='已发布组' AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `pc_groupon_detail`
--

INSERT INTO `pc_groupon_detail` (`id`, `up_time`, `image_count`, `check_time`, `check_oper`, `c_type`, `main_url`, `main_url_date`, `main_url_userid`, `type_one`, `type_two`, `type_three`, `type_one_name`, `type_two_name`, `type_three_name`, `title`, `group_key`, `book_name`, `state`, `photo_size`, `is_top`, `is_24hot`, `is_editor_tj`, `city`, `topic_id`, `is_topic`, `province`, `country`, `photo_date`, `author`) VALUES
(2, 1333446277, 2, 1333527402, 'admin', 2, '275df07eb84f04e628abd1016a4d9c89.jpg', 1333438467, 4, 10100, 20400, 30100, '时事', '事件', '视觉', '图组标题2cjx', '', '', 3, 0, 0, 0, 0, '淮南市', 0, 0, '安徽省', '中国', 1325808000, ''),
(3, 1333779345, 0, 0, 'admin', 2, '', 0, 0, 0, 0, 0, '', '', '', 'aaa', '', '', 100, 0, 0, 0, 0, '村', 0, 0, '', '', 0, '');

-- --------------------------------------------------------

--
-- 表的结构 `pc_groupon_remark`
--

DROP TABLE IF EXISTS `pc_groupon_remark`;
CREATE TABLE IF NOT EXISTS `pc_groupon_remark` (
  `group_id` int(11) unsigned NOT NULL COMMENT '图组ID',
  `userid` int(11) NOT NULL COMMENT '上传用户帐号',
  `remark` varchar(4000) NOT NULL COMMENT '图片描述'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `pc_groupon_remark`
--

INSERT INTO `pc_groupon_remark` (`group_id`, `userid`, `remark`) VALUES
(1, 0, '图组说明'),
(2, 0, 'asdad2222222 chx'),
(3, 0, 'aaaa');

-- --------------------------------------------------------

--
-- 表的结构 `pc_group_detail`
--

DROP TABLE IF EXISTS `pc_group_detail`;
CREATE TABLE IF NOT EXISTS `pc_group_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL COMMENT '上传用户ID',
  `account` varchar(50) NOT NULL,
  `up_time` int(11) NOT NULL,
  `image_count` int(11) NOT NULL,
  `check_time` int(11) NOT NULL COMMENT '审核时间',
  `check_oper` varchar(50) NOT NULL,
  `c_type` tinyint(2) NOT NULL COMMENT '1资料图片 2时事图片 ',
  `main_url` varchar(128) NOT NULL,
  `main_url_date` int(11) NOT NULL COMMENT '主图上传时间',
  `title` varchar(32) NOT NULL,
  `group_key` varchar(64) NOT NULL,
  `book_name` varchar(128) NOT NULL,
  `state` tinyint(3) unsigned NOT NULL COMMENT '图组状态 1待审核 2部分通过 3全部通过 4未通过 100未上传图片 200已删除',
  `city` varchar(64) NOT NULL,
  `province` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `photo_date` int(11) NOT NULL,
  `author` varchar(55) NOT NULL COMMENT '作者',
  `edit_state` tinyint(1) unsigned NOT NULL COMMENT '图组是否在编辑状态 0否 1是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='摄像员组' AUTO_INCREMENT=12 ;

--
-- 转存表中的数据 `pc_group_detail`
--

INSERT INTO `pc_group_detail` (`id`, `userid`, `account`, `up_time`, `image_count`, `check_time`, `check_oper`, `c_type`, `main_url`, `main_url_date`, `title`, `group_key`, `book_name`, `state`, `city`, `province`, `country`, `photo_date`, `author`, `edit_state`) VALUES
(4, 4, 'selectersky', 1333438443, 2, 1333527402, 'admin', 1, '3f9f61f890c6b60f1ed6eac84c31c83b.jpg', 1333438475, '', '', '', 3, '杭州', '浙江', '', 0, '陈甲新1', 1),
(5, 4, 'selectersky', 1333638575, 2, 0, '', 1, '8452e17e5c3eccc1038e00dfea03c702.jpg', 1333638589, '杭州西博烟花大会', '烟花,西博会,运河,节日,庆典,', '', 1, '杭州', '', '', 0, '陈甲新1', 0),
(6, 4, 'selectersky', 1333638776, 1, 0, '', 1, '914f2835a69abd31f8f1f40b10cebb51.jpg', 1333638781, 'qqw', '', '', 1, '', '', '', 0, '陈甲新1', 0),
(7, 4, 'selectersky', 1333638796, 1, 0, '', 1, '914f2835a69abd31f8f1f40b10cebb51.jpg', 1333638799, 'qqq', '', '', 1, '', '', '', 0, '陈甲新1', 0),
(8, 4, 'selectersky', 1333638850, 1, 0, 'admin', 1, '914f2835a69abd31f8f1f40b10cebb51.jpg', 1333638853, 'aaa', '', '', 1, '', '', '', 0, '陈甲新1', 1),
(11, 4, 'selectersky', 1333643246, 0, 0, '', 1, '', 0, '', '', '', 100, '', '', '', 0, '陈甲新1', 0);

-- --------------------------------------------------------

--
-- 表的结构 `pc_group_remark`
--

DROP TABLE IF EXISTS `pc_group_remark`;
CREATE TABLE IF NOT EXISTS `pc_group_remark` (
  `group_id` int(11) unsigned NOT NULL COMMENT '图组ID',
  `userid` int(11) NOT NULL COMMENT '上传用户帐号',
  `remark` varchar(4000) NOT NULL COMMENT '图片描述'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `pc_group_remark`
--

INSERT INTO `pc_group_remark` (`group_id`, `userid`, `remark`) VALUES
(4, 4, ''),
(5, 4, '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.'),
(6, 4, ''),
(7, 4, ''),
(8, 4, 'aaaa');

-- --------------------------------------------------------

--
-- 表的结构 `pc_guestbook`
--

DROP TABLE IF EXISTS `pc_guestbook`;
CREATE TABLE IF NOT EXISTS `pc_guestbook` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL DEFAULT '',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL DEFAULT '',
  `telephone` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(40) NOT NULL DEFAULT '',
  `content` mediumtext NOT NULL,
  `reply_content` mediumtext NOT NULL,
  `ip` varchar(15) NOT NULL DEFAULT '',
  `userid` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `pc_hot_picture`
--

DROP TABLE IF EXISTS `pc_hot_picture`;
CREATE TABLE IF NOT EXISTS `pc_hot_picture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `picturename` varchar(64) CHARACTER SET utf8 NOT NULL,
  `url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `begintime` int(11) NOT NULL,
  `endtime` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `place` varchar(32) CHARACTER SET utf8 NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `imgurl` varchar(128) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- 转存表中的数据 `pc_hot_picture`
--

INSERT INTO `pc_hot_picture` (`id`, `group_id`, `picturename`, `url`, `begintime`, `endtime`, `number`, `place`, `width`, `height`, `imgurl`) VALUES
(22, 0, '', '', 1327939200, 1327979193, 6, '热门图组推荐', 0, 0, './Uploads/image/2a17136e1f4d68befbe68e0f6c08741614552b9c.jpg'),
(23, 0, '', '', 1326643200, 1328016721, 0, '热门图组推荐', 0, 0, './Uploads/image/611e36ca6e5eed2faa8e495f67d650ac668630f2.jpg'),
(24, 0, '', '', 1326643200, 1328016738, 0, '热门图组推荐', 0, 0, './Uploads/image/0cf027ff40af4638d0ead1bccea0de5b8d7df9e0.jpg'),
(25, 0, '', '', 1326643200, 1328016752, 0, '热门图组推荐', 0, 0, './Uploads/image/a940528a680d7f76691cde2fe9f79306c9494977.jpg'),
(26, 0, '', '', 1326643200, 1328016766, 0, '热门图组推荐', 0, 0, './Uploads/image/51d9a9269cb365613e6bdf3289494c69dc4d7f0f.jpg'),
(27, 0, '', '', 1326643200, 1328016779, 0, '热门图组推荐', 0, 0, './Uploads/image/af5503a4b6222de6dc1c52239647719bcb2e18bd.jpg'),
(28, 0, '', '', 1326643200, 1328016793, 0, '热门图组推荐', 0, 0, './Uploads/image/16fafd4ff1ced1f40a21334a3017ca90f003e5fd.jpg'),
(29, 0, '', '', 1328025600, 1330070255, 0, '热门图组推荐', 0, 0, './Uploads/image/be56db5fb058bacdc20e8ed52f0a9e3e7cfe9c68.jpg'),
(30, 0, '', '', 1328025600, 1330156749, 0, '热门图组推荐', 0, 0, './Uploads/image/e11faefde910b8a29b4c08afd965e0af33a1b990.jpg'),
(31, 0, '', '', 1328457600, 1335761259, 0, '热门图组推荐', 0, 0, './Uploads/image/3c6e09e37f80eff84f94db13b46702f61ac3d709.jpg'),
(32, 0, '', '', 1328457600, 1335761282, 0, '热门图组推荐', 0, 0, './Uploads/image/bf8f515c9ed189edf838d1e3aaaa5542768d5237.jpg');

-- --------------------------------------------------------

--
-- 表的结构 `pc_images_detail`
--

DROP TABLE IF EXISTS `pc_images_detail`;
CREATE TABLE IF NOT EXISTS `pc_images_detail` (
  `state` enum('1','3','4','200') NOT NULL COMMENT '图片状态 1待审核 3通过 4未通过 200已删除',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL COMMENT '上传用户ID',
  `c_type` tinyint(2) unsigned NOT NULL COMMENT '1时效图片 2资料图片',
  `group_id` int(11) NOT NULL,
  `old_group_id` int(11) unsigned NOT NULL COMMENT '摄影师上传的图组ID',
  `up_account` varchar(50) CHARACTER SET utf8 NOT NULL,
  `up_time` int(11) NOT NULL,
  `check_time` int(11) NOT NULL,
  `release_time` int(11) NOT NULL,
  `check_oper` varchar(50) CHARACTER SET utf8 NOT NULL,
  `url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `type_one` varchar(50) CHARACTER SET utf8 NOT NULL,
  `type_two` varchar(50) CHARACTER SET utf8 NOT NULL,
  `type_three` varchar(32) CHARACTER SET utf8 NOT NULL,
  `title` varchar(32) CHARACTER SET utf8 NOT NULL,
  `images_key` varchar(64) CHARACTER SET utf8 NOT NULL,
  `book_name` varchar(128) CHARACTER SET utf8 NOT NULL,
  `country` varchar(50) CHARACTER SET utf8 NOT NULL,
  `province` varchar(50) CHARACTER SET utf8 NOT NULL,
  `city` varchar(32) CHARACTER SET utf8 NOT NULL,
  `value` int(24) NOT NULL,
  `down_number` int(11) NOT NULL DEFAULT '0',
  `is_top` tinyint(4) NOT NULL,
  `texie` tinyint(4) NOT NULL COMMENT '特写',
  `ren` tinyint(4) NOT NULL COMMENT '多少人照',
  `position` tinyint(4) NOT NULL COMMENT '1横图 2竖图 3方图 0其它',
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `filesize` int(11) NOT NULL,
  `fileext` tinyint(1) unsigned NOT NULL COMMENT '图生扩展名 1jpg/jpeg 2psd',
  `photo_time` int(11) NOT NULL,
  `renwu_name` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '人物姓名',
  `yiming` varchar(50) NOT NULL COMMENT '艺名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- 转存表中的数据 `pc_images_detail`
--

INSERT INTO `pc_images_detail` (`state`, `id`, `userid`, `c_type`, `group_id`, `old_group_id`, `up_account`, `up_time`, `check_time`, `release_time`, `check_oper`, `url`, `type_one`, `type_two`, `type_three`, `title`, `images_key`, `book_name`, `country`, `province`, `city`, `value`, `down_number`, `is_top`, `texie`, `ren`, `position`, `width`, `height`, `filesize`, `fileext`, `photo_time`, `renwu_name`, `yiming`) VALUES
('3', 5, 4, 1, 2, 4, '', 1333438467, 1333527270, 0, 'admin', '275df07eb84f04e628abd1016a4d9c89.jpg', '10100', '20400', '30100', '', '烟花,西博会,运河,节日,庆典,2323232wesdsd', '', '中国1', '浙江1', '杭州1', 400, 0, 0, 1, 1, 1, 4256, 2832, 1711, 0, 1319373001, '121212xxx', ''),
('3', 6, 4, 1, 2, 4, '', 1333438475, 1333527270, 0, 'admin', '3f9f61f890c6b60f1ed6eac84c31c83b.jpg', '10100', '20400', '30100', '', '1', '', '1', '1', '1', 100, 0, 0, 2, 2, 2, 4256, 2832, 1843, 0, 1319373003, 'qqq', ''),
('1', 7, 4, 1, 0, 5, '', 1333638581, 0, 0, '', '8b6afc2fe04892ef82bbd6a3a4c2a8b9.jpg', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 4256, 2832, 2159, 0, 1319373005, '', ''),
('1', 8, 4, 1, 0, 5, '', 1333638589, 0, 0, '', '8452e17e5c3eccc1038e00dfea03c702.jpg', '', '', '', '', '', '', '中国', '浙江', '杭州', 0, 0, 0, 0, 0, 0, 4256, 2832, 1798, 0, 1319373009, '', ''),
('1', 9, 4, 1, 0, 6, '', 1333638781, 0, 0, '', '914f2835a69abd31f8f1f40b10cebb51.jpg', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 320, 240, 11, 0, 1333638781, '', ''),
('1', 10, 4, 1, 0, 7, '', 1333638799, 0, 0, '', '914f2835a69abd31f8f1f40b10cebb51.jpg', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 320, 240, 11, 0, 1333638799, '', ''),
('3', 11, 4, 1, 3, 8, '', 1333638853, 1333779349, 0, 'admin', '914f2835a69abd31f8f1f40b10cebb51.jpg', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 320, 240, 11, 0, 1333638853, '', ''),
('1', 12, 4, 1, 0, 11, '', 1333643250, 0, 0, '', '914f2835a69abd31f8f1f40b10cebb51.jpg', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 320, 240, 11, 0, 1333643250, '', '');

-- --------------------------------------------------------

--
-- 表的结构 `pc_images_remark`
--

DROP TABLE IF EXISTS `pc_images_remark`;
CREATE TABLE IF NOT EXISTS `pc_images_remark` (
  `images_id` int(11) unsigned NOT NULL COMMENT '图片ID',
  `userid` int(11) NOT NULL COMMENT '上传用户帐号',
  `old_title` varchar(255) NOT NULL COMMENT '图片原来标题',
  `old_cate` varchar(255) NOT NULL COMMENT '摄影师上传的图片分类',
  `old_author` varchar(255) NOT NULL COMMENT '摄影师上传的作者',
  `old_keyword` varchar(255) NOT NULL COMMENT '图片原来的关键字',
  `old_remark` varchar(4000) NOT NULL COMMENT '图片原来描述',
  `remark` varchar(4000) NOT NULL COMMENT '图片发布后的描述'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `pc_images_remark`
--

INSERT INTO `pc_images_remark` (`images_id`, `userid`, `old_title`, `old_cate`, `old_author`, `old_keyword`, `old_remark`, `remark`) VALUES
(7, 4, '', '', 'Xie Youding', '烟花,西博会,运河,节日,庆典,', '杭州西博烟花大会', '杭州西博烟花大会'),
(6, 4, '', '', 'Xie Youding', '烟花,西博会,运河,节日,庆典,', '杭州西博烟花大会2', '1'),
(5, 4, '', '', 'Xie Youding', '烟花,西博会,运河,节日,庆典,', '杭州西博烟花大会1', 'asdcjx'),
(8, 4, '', '体育', 'Xie Youding', '烟花,西博会,运河,节日,庆典,', '杭州西博烟花大会', '杭州西博烟花大会'),
(9, 4, '', '', '', '', '', ''),
(10, 4, '', '', '', '', '', ''),
(11, 4, '', '', '', '', 'aa', 'aa'),
(12, 4, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- 表的结构 `pc_kefu`
--

DROP TABLE IF EXISTS `pc_kefu`;
CREATE TABLE IF NOT EXISTS `pc_kefu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(20) NOT NULL DEFAULT '',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `code` mediumtext NOT NULL,
  `skin` varchar(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `pc_kefu`
--

INSERT INTO `pc_kefu` (`id`, `status`, `listorder`, `createtime`, `name`, `type`, `code`, `skin`) VALUES
(1, 1, 4, 1306807701, '咨询电话', 4, '0317-5022625', '0'),
(2, 1, 3, 1306808546, '技术咨询', 1, '147613338', 'q3'),
(3, 1, 3, 1306808886, 'QQ客服', 1, '147613338', 'q3'),
(4, 1, 2, 1306811439, 'MSN客服', 2, 'snliuxun@msn.cn', 'm2'),
(5, 1, 0, 1306830001, '旺旺客服', 3, 'snliuxun', 'w1');

-- --------------------------------------------------------

--
-- 表的结构 `pc_keyword`
--

DROP TABLE IF EXISTS `pc_keyword`;
CREATE TABLE IF NOT EXISTS `pc_keyword` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `type_name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- 转存表中的数据 `pc_keyword`
--

INSERT INTO `pc_keyword` (`id`, `type_id`, `name`, `type_name`) VALUES
(2, 1, '23染色法', '11111'),
(3, 1, '', '11111'),
(4, 1, '', '11111'),
(5, 1, '阿凡达是否', '11111'),
(6, 2, '天天天天天', '222'),
(7, 6, '富图', '社会新闻'),
(8, 5, '111111111', '时事');

-- --------------------------------------------------------

--
-- 表的结构 `pc_keyword_type`
--

DROP TABLE IF EXISTS `pc_keyword_type`;
CREATE TABLE IF NOT EXISTS `pc_keyword_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `parent_name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- 转存表中的数据 `pc_keyword_type`
--

INSERT INTO `pc_keyword_type` (`id`, `name`, `parent_id`, `parent_name`) VALUES
(5, '时事', 0, ''),
(6, '社会新闻', 5, '时事'),
(7, '生态环境', 5, '时事'),
(8, '财经', 0, '');

-- --------------------------------------------------------

--
-- 表的结构 `pc_link`
--

DROP TABLE IF EXISTS `pc_link`;
CREATE TABLE IF NOT EXISTS `pc_link` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `logo` varchar(80) NOT NULL DEFAULT '',
  `siteurl` varchar(150) NOT NULL DEFAULT '',
  `typeid` smallint(5) unsigned NOT NULL,
  `linktype` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `siteinfo` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `pc_link`
--

INSERT INTO `pc_link` (`id`, `status`, `listorder`, `createtime`, `name`, `logo`, `siteurl`, `typeid`, `linktype`, `siteinfo`) VALUES
(1, 1, 0, 1306547518, 'Yourphp企业网站管理系统', 'http://demo.yourphp.cn/Public/Images/logo.gif', 'http://www.yourphp.cn', 2, 2, 'php企业网站管理系统'),
(2, 1, 0, 1306554684, '企业网站管理系统', '', 'http://www.yourphp.cn', 2, 1, '');

-- --------------------------------------------------------

--
-- 表的结构 `pc_mainpicture`
--

DROP TABLE IF EXISTS `pc_mainpicture`;
CREATE TABLE IF NOT EXISTS `pc_mainpicture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `remark` varchar(128) CHARACTER SET utf8 NOT NULL,
  `begintime` int(11) NOT NULL,
  `endtime` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `imgurl` varchar(128) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- 转存表中的数据 `pc_mainpicture`
--

INSERT INTO `pc_mainpicture` (`id`, `url`, `remark`, `begintime`, `endtime`, `number`, `imgurl`) VALUES
(33, '', 'MOTORSPORT - DAKAR ARGENTINA CHILE PERU 2012 - STAGE', 1326720647, 1328016650, 1, './Uploads/image/c47fe509686c13b77ec8f2ebf14cd1945f6bf99b.jpg'),
(34, '', 'NFL: JAN 01 Titans at Texans', 1326720826, 1328016828, 2, './Uploads/image/2832530963cf41fe81aeaea41c20b2ee924fc5a0.jpg'),
(35, '', 'NBA: JAN 02 Thunder at Mavericks', 1326720850, 1328016852, 3, './Uploads/image/81123f8642e2f869c8f4cb944c37a82ba53e628b.jpg'),
(36, '', 'December 30 2011: Iowa Hawkeye marching band ', 1326720871, 1328016872, 4, './Uploads/image/59ef6d64a58079b0c04d1bfd66a63370e510650e.jpg'),
(37, '5', 'December 30 2011: Oklahoma Sooners Brennan Clay ', 1326720892, 1328016894, 0, './Uploads/image/0792e0040868cbac5c109cf1d2fdae69af2cf314.jpg'),
(38, 'http://a.photochina.com.cn/index.php?m=Main&a=decide&t=2', '首页图片', 1325752825, 1330936845, 0, './Uploads/image/caa768e1dcbfd05c2d711cc4f23947b7f13bf582.jpg'),
(39, '', 'NHL: DEC 30 Sabres at Capitals ', 1328503513, 1335761115, 1, './Uploads/image/56fbe5ac3107d59390f24be86a957736a7e9f54d.jpg'),
(40, '', 'December 30 2011: Iowa Hawkeye marching band ', 1328503571, 1335761173, 2, './Uploads/image/3446f02f6802785c689b08687afa89f5964eca0f.jpg'),
(41, '', 'MOTORSPORT - DAKAR ARGENTINA CHILE PERU 2012 - STAGE 4 - SAN JUAN', 1328503621, 1335761222, 3, './Uploads/image/6488b5c13c97b0616123b5fb1440e2331cd0b440.jpg');

-- --------------------------------------------------------

--
-- 表的结构 `pc_menu`
--

DROP TABLE IF EXISTS `pc_menu`;
CREATE TABLE IF NOT EXISTS `pc_menu` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `parentid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `model` char(20) NOT NULL DEFAULT '',
  `action` char(20) NOT NULL DEFAULT '',
  `data` char(50) NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `group_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `remark` varchar(255) NOT NULL DEFAULT '',
  `listorder` smallint(6) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `parentid` (`parentid`),
  KEY `model` (`model`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=111 ;

--
-- 转存表中的数据 `pc_menu`
--

INSERT INTO `pc_menu` (`id`, `parentid`, `model`, `action`, `data`, `type`, `status`, `group_id`, `name`, `remark`, `listorder`) VALUES
(1, 0, 'Main', 'main', 'menuid=42', 1, 1, 0, '后台首页', '', 0),
(2, 0, 'User', '', '', 1, 1, 0, '系统管理', '系统设置', 6),
(73, 2, 'User', '', '', 1, 1, 0, '用户管理', '用户管理', 0),
(5, 0, 'Photoer', '', 'menuid=9', 1, 1, 0, '会员管理', '', 1),
(71, 2, 'Role', 'index', '', 1, 1, 0, '角色管理', '操作员角色管理', 1),
(39, 2, 'Menu', '', '', 1, 1, 0, '后台管理菜单', '后台管理菜单', 2),
(15, 39, 'Menu', 'add', '', 1, 1, 0, '添加菜单', '', 0),
(86, 85, 'PhotoerImageGroup', 'query', '', 1, 1, 0, '摄影师图组', '', 0),
(76, 5, 'Photoer', 'index', 'type=1', 1, 1, 0, '下载用户管理', '', 1),
(74, 73, 'User', 'add', '', 1, 1, 0, '添加用户', '', 0),
(9, 5, 'Photoer', 'index', '', 1, 1, 0, '摄影师管理', '会员资料管理', 0),
(79, 5, 'AddvalueQuery', '', '', 1, 1, 0, '充值消费记录查询', '充值记录查询', 2),
(11, 5, 'Role', '', '', 1, 1, 0, '会员组管理', '', 4),
(12, 11, 'Role', 'add', '', 1, 1, 0, '添加会员组', '', 0),
(13, 5, 'Node', '', '', 1, 1, 0, '权限节点管理', '', 6),
(14, 13, 'Node', 'add', '', 1, 1, 0, '添加权限节点', '', 0),
(85, 0, 'PhotoerImageGroup', 'query', '', 1, 1, 0, '相片管理中心', '', 2),
(17, 3, 'Category', '', '', 1, 1, 0, '栏目管理', '栏目管理', 1),
(83, 81, 'Picture', 'add', '', 1, 1, 0, '图片上传', '', 0),
(82, 0, 'User', '', '', 1, 1, 0, '报表统计', '', 5),
(80, 0, 'gonggao', '', '', 1, 1, 0, '新闻管理', '新闻管理', 3),
(84, 80, 'request', '', '', 1, 1, 0, '需求留言管理', '', 1),
(75, 71, 'Role', 'add', '', 1, 1, 0, '添加角色', '角色添加', 0),
(31, 17, 'Category', 'repair_cache', '', 1, 1, 0, '恢复栏目数据', '', 0),
(78, 5, 'tuijian', '', '', 1, 0, 0, '推荐管理', '', 3),
(34, 6, 'Createhtml', 'Createlist', '', 1, 1, 0, '更新列表页', '', 0),
(35, 6, 'Createhtml', 'Createshow', '', 1, 1, 0, '更新内容页', '', 0),
(37, 26, 'Picture', 'add', '', 1, 1, 0, '添加图片', '', 0),
(38, 27, 'Download', 'add', '', 1, 1, 0, '添加文件', '', 0),
(40, 1, 'Main', 'password', '', 1, 1, 0, '修改密码', '', 2),
(41, 1, 'Main', 'profile', '', 1, 1, 0, '个人资料', '', 3),
(42, 1, 'Main', 'main', '', 1, 1, 0, '后台首页', '', 1),
(46, 44, 'Database', 'recover', '', 1, 1, 0, '恢复数据库', '', 0),
(52, 28, 'Type', 'add', '', 1, 1, 0, '添加类别', '', 0),
(53, 4, 'Link', 'index', '', 1, 1, 0, '友情链接', '', 0),
(54, 53, 'Link', 'add', '', 1, 1, 0, '添加链接', '', 0),
(56, 4, 'Order', 'index', '', 1, 1, 0, '订单管理', '', 0),
(99, 82, 'Baobiao', 'photojs', '', 1, 1, 0, '合作机构结算管理', '', 0),
(88, 85, 'waitrealsephotoer', '', '', 1, 1, 0, '待发布图组管理', '', 1),
(70, 69, 'Dbsource', 'add', '', 1, 1, 0, '添加数据源', '', 0),
(89, 85, 'realsephotoer', '', '', 1, 1, 0, '发布图组管理', '', 2),
(90, 85, 'hotphotogroup', '', '', 1, 1, 0, '热门图组管理', '', 4),
(91, 85, 'Phototype', '', '', 1, 1, 0, '图片类型管理', '', 7),
(92, 85, 'Topicimagegroup', '', '', 1, 1, 0, '专题图组管理', '', 3),
(93, 2, 'Role', 'auth', '', 1, 1, 0, '角色权限管理', '', 0),
(94, 5, 'Downloader', 'add', '', 1, 0, 0, '会员充值管理', '', 5),
(95, 85, 'Keyword', '', '', 1, 1, 0, '关键字管理', '', 6),
(96, 95, 'Keyword', 'type', '', 1, 1, 0, '关键字类型管理', '', 0),
(97, 85, 'Mainpicture', '', '', 1, 1, 0, '广告图片管理', '', 5),
(100, 82, 'Baobiao', 'phototj', '', 1, 1, 0, '合作机构上传图片统计', '', 0),
(101, 82, 'Baobiao', 'supplement', '', 1, 1, 0, '代理图片销售统计', '', 0),
(102, 82, 'Baobiao', 'downer_info', '', 1, 1, 0, '下载用户充值统计', '', 0),
(103, 82, 'Baobiao', '', '', 1, 1, 0, '富图图片销售统计', '', 0),
(104, 86, 'PhotoerImageGroup', 'editgroup', '', 1, 1, 0, '编辑图组', '', 0),
(105, 80, 'gonggao', '', '', 1, 1, 0, '公告管理', '', 0),
(106, 5, 'Org', '', '', 1, 1, 0, '机构管理', '', 7),
(107, 106, 'Org', 'add', '', 1, 1, 0, '添加机构', '', 0),
(108, 85, 'Photoer', 'index', '', 1, 1, 0, '编辑图组管理', '', 3),
(109, 82, 'baobiao', 'down_info', '', 1, 1, 0, '用户购买统计', '', 0),
(110, 82, 'baobiao', 'down_info', '', 1, 1, 0, '结算管理', '', 0);

-- --------------------------------------------------------

--
-- 表的结构 `pc_module`
--

DROP TABLE IF EXISTS `pc_module`;
CREATE TABLE IF NOT EXISTS `pc_module` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(200) NOT NULL DEFAULT '',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `issystem` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `issearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `listfields` varchar(255) NOT NULL DEFAULT '',
  `setup` mediumtext NOT NULL,
  `listorder` smallint(3) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `postgroup` varchar(100) NOT NULL DEFAULT '',
  `ispost` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- 转存表中的数据 `pc_module`
--

INSERT INTO `pc_module` (`id`, `title`, `name`, `description`, `type`, `issystem`, `issearch`, `listfields`, `setup`, `listorder`, `status`, `postgroup`, `ispost`) VALUES
(1, '单页模型', 'Page', '单页模型', 1, 1, 0, '*', '', 0, 1, '', 0),
(2, '文章模型', 'Article', '新闻文章', 1, 1, 1, 'id,catid,url,title,title_style,userid,username,hits,keywords,description,thumb,createtime,status', '', 0, 1, '', 0),
(3, '产品模型', 'Product', '产品展示', 1, 1, 1, 'id,catid,url,title,title_style,userid,username,hits,keywords,description,thumb,createtime,status,xinghao,price', '', 0, 1, '', 0),
(4, '图片模型', 'Picture2', '图片展示', 1, 1, 1, 'id,catid,url,title,title_style,userid,username,hits,keywords,description,thumb,createtime,status', '', 0, 1, '', 0),
(5, '下载模型', 'Download', '文件下载', 1, 1, 1, 'id,catid,url,title,title_style,userid,username,hits,keywords,description,thumb,createtime,status,ext,size', '', 0, 1, '', 0),
(6, '信息反馈', 'Feedback', '信息反馈', 1, 0, 0, '*', '', 0, 1, '1,2,3,4', 0),
(7, '友情链接', 'Link', '友情链接', 2, 0, 0, '*', '', 0, 1, '', 0),
(8, '在线留言', 'Guestbook', '在线留言', 1, 0, 0, '*', '', 0, 1, '1,2,3,4', 0),
(9, '在线客服', 'Kefu', '在线客服', 2, 0, 0, '*', '', 0, 1, '', 0);

-- --------------------------------------------------------

--
-- 表的结构 `pc_node`
--

DROP TABLE IF EXISTS `pc_node`;
CREATE TABLE IF NOT EXISTS `pc_node` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  `title` varchar(50) NOT NULL DEFAULT '',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `remark` varchar(255) NOT NULL DEFAULT '',
  `listorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `pid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `name` (`name`,`status`,`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- 转存表中的数据 `pc_node`
--

INSERT INTO `pc_node` (`id`, `name`, `title`, `status`, `remark`, `listorder`, `pid`, `level`) VALUES
(1, 'Admin', '后台管理', 1, '后台项目', 0, 0, 1),
(2, 'Index', '后台默认', 1, '控制器', 0, 1, 2),
(3, 'Config', '系统设置', 1, '控制器', 0, 1, 2),
(4, 'index', '站点配置', 1, '动作', 0, 3, 3),
(5, 'sys', '系统参数', 1, '动作', 0, 3, 3),
(6, 'seo', 'SEO设置', 1, '动作', 0, 3, 3),
(7, 'add', '添加变量', 1, '动作', 0, 3, 3),
(8, 'Menu', '菜单管理', 1, '菜单控制器', 0, 1, 2),
(9, 'index', '菜单列表', 1, '菜单列表-动作', 0, 8, 3),
(10, 'add', '添加菜单', 1, '添加菜单-动作', 0, 8, 3),
(11, 'index', '后台默认动作', 1, '后台默认动作', 0, 2, 3),
(12, 'Main', '后台首页', 1, '控制器', 0, 1, 2),
(13, 'profile', '个人资料', 1, '动作', 0, 12, 3),
(14, 'password', '修改密码', 1, '动作', 0, 12, 3),
(15, 'index', '后台首页', 1, '动作', 0, 12, 3),
(16, 'main', '欢迎首页', 1, '动作', 0, 12, 3),
(17, 'attach', '附件设置', 1, '动作', 0, 3, 3),
(18, 'mail', '系统邮箱', 1, '动作', 0, 3, 3),
(19, 'Posid', '推荐位', 1, '控制器', 0, 1, 2),
(20, 'index', '列表', 1, '动作', 0, 19, 3),
(21, 'Category', '模型管理', 1, '', 0, 1, 2),
(22, 'User', '会员管理', 1, '', 0, 1, 2),
(23, 'Createhtml', '网站更新', 1, '', 0, 1, 2),
(24, 'index', '栏目管理', 1, '', 0, 21, 3),
(25, 'index', '会员资料管理', 1, '', 0, 22, 3),
(26, 'index', '更新首页', 1, '', 0, 23, 3),
(27, 'Createlist', '更新列表页', 1, '', 0, 23, 3),
(28, 'Createshow', '更新内容页', 1, '', 0, 23, 3),
(29, 'Updateurl', '更新URL', 1, '', 0, 23, 3),
(30, 'Org', '机构管理', 1, '', 0, 22, 3),
(31, 'Org_add', '机构添加', 1, '', 0, 30, 4);

-- --------------------------------------------------------

--
-- 表的结构 `pc_order`
--

DROP TABLE IF EXISTS `pc_order`;
CREATE TABLE IF NOT EXISTS `pc_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sn` char(22) NOT NULL DEFAULT '',
  `password` varchar(40) NOT NULL DEFAULT '',
  `module` varchar(20) NOT NULL DEFAULT '',
  `userid` int(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(40) NOT NULL DEFAULT '',
  `price` decimal(10,0) unsigned NOT NULL DEFAULT '0',
  `productlist` mediumtext NOT NULL,
  `note` mediumtext NOT NULL,
  `realname` varchar(40) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `tel` varchar(50) NOT NULL DEFAULT '',
  `mobile` varchar(18) NOT NULL DEFAULT '',
  `fax` varchar(50) NOT NULL DEFAULT '',
  `address` varchar(80) NOT NULL DEFAULT '',
  `zipcode` varchar(10) NOT NULL DEFAULT '',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ip` char(15) NOT NULL DEFAULT '',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sn` (`sn`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `pc_org`
--

DROP TABLE IF EXISTS `pc_org`;
CREATE TABLE IF NOT EXISTS `pc_org` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL,
  `from_path` varchar(128) NOT NULL,
  `to_path` varchar(128) NOT NULL,
  `thumb_path` varchar(128) NOT NULL,
  `name` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `web_thumb_path` varchar(128) NOT NULL,
  `web_images_path` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `pc_org`
--

INSERT INTO `pc_org` (`id`, `userid`, `from_path`, `to_path`, `thumb_path`, `name`, `username`, `web_thumb_path`, `web_images_path`) VALUES
(1, 32, 'E:\\upload2', 'E:\\php\\ftnew\\Uploads\\images', 'E:\\php\\ftnew\\Uploads\\thumb', '谢有定2', 'xieyd', './Uploads/images', './Uploads/thumb'),
(2, 31, 'e:\\upload', 'E:\\php\\ftnew\\Uploads\\images', 'E:\\php\\ftnew\\Uploads\\thumb', '陈甲新', 'selectersky', './Uploads/images', './Uploads/thumb');

-- --------------------------------------------------------

--
-- 表的结构 `pc_page`
--

DROP TABLE IF EXISTS `pc_page`;
CREATE TABLE IF NOT EXISTS `pc_page` (
  `id` smallint(5) NOT NULL,
  `title` varchar(100) NOT NULL DEFAULT '',
  `thumb` varchar(100) NOT NULL DEFAULT '',
  `keywords` varchar(250) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `content` mediumtext NOT NULL,
  `template` varchar(30) NOT NULL DEFAULT '',
  `listorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `pc_page`
--

INSERT INTO `pc_page` (`id`, `title`, `thumb`, `keywords`, `description`, `content`, `template`, `listorder`) VALUES
(8, '关于我们', './Uploads/201104/4d9764a7b953d.jpg', 'yourphp,企业建站系统,发布', 'yourphp,企业建站系统,发布', '<p>\r\n	关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们lllll</p>\r\n<p>\r\n	[page]</p>\r\n<p>\r\n	sdfgasdfaf中华人民共和国关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我们关于我</p>\r\n<p>\r\n	111</p>\r\n', '', 0),
(11, '公司简介', '', '', '公司简介', '<p>\r\n	公司简介</p>\r\n', '', 0),
(12, '联系我们', './Uploads/201104/4d96e3f1522b3.jpg', '联系我们', '联系我们', '<p>\r\n	联系我们</p>\r\n', '', 0);

-- --------------------------------------------------------

--
-- 表的结构 `pc_pay`
--

DROP TABLE IF EXISTS `pc_pay`;
CREATE TABLE IF NOT EXISTS `pc_pay` (
  `id` int(11) NOT NULL,
  `type` tinyint(1) unsigned NOT NULL COMMENT '操作类型 1为充值 2为消费 3结算 4销售',
  `account` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '操作的帐号',
  `userid` int(11) unsigned NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '操作的帐号的真实姓名',
  `oper` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '充值者用户名 只对充值有效',
  `points` int(11) NOT NULL,
  `use_points` int(10) unsigned NOT NULL COMMENT '这次操作后的可用点数 如果是后结算用户 则为累计点数',
  `createtime` int(11) unsigned NOT NULL,
  `remark` varchar(255) CHARACTER SET utf8 NOT NULL,
  `pid` int(10) unsigned NOT NULL COMMENT '图片ID 只针对消费',
  `url` varchar(128) NOT NULL COMMENT '图片地址',
  `up_time` int(11) NOT NULL COMMENT '图片上传时间'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `pc_pay`
--

INSERT INTO `pc_pay` (`id`, `type`, `account`, `userid`, `name`, `oper`, `points`, `use_points`, `createtime`, `remark`, `pid`, `url`, `up_time`) VALUES
(0, 1, 'cjx', 3, '', 'admin', 100, 1112, 0, 'sfsfsf', 0, '', 0),
(0, 2, 'cjx', 3, '', 'admin', 2, 1110, 1333556594, 'dddd', 0, '', 0),
(0, 1, 'cjx', 3, '陈甲新', 'admin', 1, 1111, 1333634079, '', 0, '', 0),
(0, 2, 'cjx', 3, '陈甲新', 'admin', 1, 1110, 1333634087, '', 0, '', 0),
(0, 2, 'cjx', 3, '陈甲新', '', 95, 730, 1333815762, '', 6, '3f9f61f890c6b60f1ed6eac84c31c83b.jpg', 1333438475),
(0, 2, 'cjx', 3, '陈甲新', '', 95, 635, 1333815880, '', 6, '3f9f61f890c6b60f1ed6eac84c31c83b.jpg', 1333438475),
(0, 2, 'cjx', 3, '陈甲新', '', 95, 540, 1333816188, '', 6, '3f9f61f890c6b60f1ed6eac84c31c83b.jpg', 1333438475),
(0, 4, 'selectersky', 4, '陈甲新1', '', 50, 50, 1333816188, '', 6, '3f9f61f890c6b60f1ed6eac84c31c83b.jpg', 1333438475),
(0, 2, 'cjx', 3, '陈甲新', '', 95, 445, 1333816457, '', 6, '3f9f61f890c6b60f1ed6eac84c31c83b.jpg', 1333438475),
(0, 4, 'selectersky', 4, '陈甲新1', '', 50, 100, 1333816457, '', 6, '3f9f61f890c6b60f1ed6eac84c31c83b.jpg', 1333438475),
(0, 2, 'cjx', 3, '陈甲新', '', 95, 445, 1333817576, '', 6, '3f9f61f890c6b60f1ed6eac84c31c83b.jpg', 1333438475),
(0, 4, 'selectersky', 4, '陈甲新1', '', 50, 150, 1333817576, '', 6, '3f9f61f890c6b60f1ed6eac84c31c83b.jpg', 1333438475),
(0, 4, 'selectersky', 4, '陈甲新1', '', 50, 200, 1333817691, '', 6, '3f9f61f890c6b60f1ed6eac84c31c83b.jpg', 1333438475),
(0, 4, 'selectersky', 4, '陈甲新1', '', 50, 250, 1333817820, '', 6, '3f9f61f890c6b60f1ed6eac84c31c83b.jpg', 1333438475),
(0, 4, 'selectersky', 4, '陈甲新1', '', 50, 300, 1333817969, '', 6, '3f9f61f890c6b60f1ed6eac84c31c83b.jpg', 1333438475),
(0, 4, 'selectersky', 4, '陈甲新1', '', 50, 350, 1333818009, '', 6, '3f9f61f890c6b60f1ed6eac84c31c83b.jpg', 1333438475),
(0, 4, 'selectersky', 4, '陈甲新1', '', 50, 400, 1333818154, '', 6, '3f9f61f890c6b60f1ed6eac84c31c83b.jpg', 1333438475),
(0, 2, 'cjx', 3, '陈甲新', '', 95, 350, 1333818291, '', 6, '3f9f61f890c6b60f1ed6eac84c31c83b.jpg', 1333438475),
(0, 4, 'selectersky', 4, '陈甲新1', '', 50, 450, 1333818291, '', 6, '3f9f61f890c6b60f1ed6eac84c31c83b.jpg', 1333438475);

-- --------------------------------------------------------

--
-- 表的结构 `pc_photoer`
--

DROP TABLE IF EXISTS `pc_photoer`;
CREATE TABLE IF NOT EXISTS `pc_photoer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(2) unsigned NOT NULL COMMENT '用户类型 1为下载用户 2为摄影师',
  `subtype` tinyint(2) unsigned NOT NULL COMMENT '用户子类型',
  `account` varchar(32) CHARACTER SET utf8 NOT NULL,
  `password` varchar(64) CHARACTER SET utf8 NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `sex` varchar(8) CHARACTER SET utf8 NOT NULL,
  `card_no` varchar(32) CHARACTER SET utf8 NOT NULL COMMENT '身份证号码',
  `email` varchar(50) CHARACTER SET utf8 NOT NULL,
  `qq` varchar(16) CHARACTER SET utf8 DEFAULT NULL COMMENT 'QQ号或msn号',
  `phone` varchar(32) CHARACTER SET utf8 NOT NULL,
  `province` varchar(32) CHARACTER SET utf8 NOT NULL,
  `city` varchar(32) CHARACTER SET utf8 NOT NULL,
  `post_code` varchar(8) CHARACTER SET utf8 DEFAULT NULL,
  `link_addr` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `company_name` varchar(128) CHARACTER SET utf8 DEFAULT NULL COMMENT '单位名称',
  `pen_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `work_years` tinyint(4) DEFAULT NULL,
  `main_photo` varchar(128) CHARACTER SET utf8 DEFAULT NULL COMMENT '主要从事摄影',
  `main_post_local` varchar(128) CHARACTER SET utf8 DEFAULT NULL COMMENT '主要作品曾出版在哪些地方',
  `all_equipment` varchar(128) CHARACTER SET utf8 DEFAULT NULL COMMENT '您所用的摄影器材',
  `cameras` varchar(128) CHARACTER SET utf8 DEFAULT NULL COMMENT '数码相机',
  `bank` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '所在银行',
  `bank_area` varchar(128) CHARACTER SET utf8 DEFAULT NULL COMMENT '银行开户行',
  `bank_truename` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '开户行真实姓名',
  `bank_num` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '银行帐号',
  `join_team` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `user_say` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `head_image_path` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `createtime` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `pvalue` int(11) unsigned NOT NULL COMMENT '可用点数',
  `need_pay` int(10) unsigned NOT NULL COMMENT '累计需要支付的点数，针对后结算用户',
  `type_style` int(11) NOT NULL DEFAULT '0',
  `allow_down_num` int(10) unsigned NOT NULL COMMENT '充许下载图片数量',
  `downed_num` int(10) unsigned NOT NULL COMMENT '已经下载的图片数量',
  `zheke` decimal(10,3) NOT NULL COMMENT '下载图片的折扣',
  `start_time` int(11) unsigned NOT NULL COMMENT '开通时间',
  `end_time` int(11) unsigned NOT NULL COMMENT '过期时间',
  `every_pay_day` int(11) unsigned NOT NULL COMMENT '结算周期 单位为天',
  `pay_time` int(10) unsigned NOT NULL COMMENT '上一次结算时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `pc_photoer`
--

INSERT INTO `pc_photoer` (`id`, `type`, `subtype`, `account`, `password`, `name`, `sex`, `card_no`, `email`, `qq`, `phone`, `province`, `city`, `post_code`, `link_addr`, `company_name`, `pen_name`, `work_years`, `main_photo`, `main_post_local`, `all_equipment`, `cameras`, `bank`, `bank_area`, `bank_truename`, `bank_num`, `join_team`, `user_say`, `head_image_path`, `createtime`, `state`, `pvalue`, `need_pay`, `type_style`, `allow_down_num`, `downed_num`, `zheke`, `start_time`, `end_time`, `every_pay_day`, `pay_time`) VALUES
(3, 1, 1, 'cjx', 'c137340e996daae5fec608dcffd7683442cf8024', '陈甲新', '男', '', 'aaa@qq.com', 'QQ号1', '联系电话', '浙江省', '湖州市', '1212', '121212', '工作单位', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, './Uploads/headimage/logo_0_00aae79.gif', 1332068344, 1, 350, 95, 0, 1, 10, '0.950', 1332069159, 0, 120, 0),
(4, 2, 1, 'selectersky', '136f758ed10d105b5e4c7a3a891ff90571e686c7', '陈甲新1', '男', '身份证号码', 'ssss@qq.com1', 'QQ号1', '联系电话1', '浙江省', '绍兴市', '邮编1', '地址1', '工作单位', '投稿署名1', 3, '人物摄影,风光摄影', '样本,杂志', 'Linhoff,Nikon', 'Canon数码,Nikon数码,', '招商银行', '(详细到支行一级', '开户名', '银行帐', '                        加的摄影团体<BR>曾举办过的摄', '您的宝贵', './Uploads/headimage/logo_0_00aae79.gif', 1332254341, 1, 450, 0, 0, 0, 0, '0.500', 1332254456, 0, 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `pc_photo_type`
--

DROP TABLE IF EXISTS `pc_photo_type`;
CREATE TABLE IF NOT EXISTS `pc_photo_type` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- 转存表中的数据 `pc_photo_type`
--

INSERT INTO `pc_photo_type` (`id`, `name`) VALUES
(1, '新闻摄影'),
(2, '纪实摄影'),
(3, '人物摄影'),
(4, '风光摄影'),
(5, '体育摄影'),
(6, '广告摄影'),
(7, '其它');

-- --------------------------------------------------------

--
-- 表的结构 `pc_picture`
--

DROP TABLE IF EXISTS `pc_picture`;
CREATE TABLE IF NOT EXISTS `pc_picture` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userid` int(11) unsigned NOT NULL DEFAULT '0',
  `username` varchar(40) NOT NULL DEFAULT '',
  `title` varchar(120) NOT NULL DEFAULT '',
  `title_style` varchar(40) NOT NULL DEFAULT '',
  `thumb` varchar(100) NOT NULL DEFAULT '',
  `keywords` varchar(120) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `content` mediumtext NOT NULL,
  `url` varchar(60) NOT NULL DEFAULT '',
  `template` varchar(40) NOT NULL DEFAULT '',
  `posid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `recommend` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `readgroup` varchar(100) NOT NULL DEFAULT '',
  `readpoint` smallint(5) NOT NULL DEFAULT '0',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `pics` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`id`,`status`,`listorder`),
  KEY `catid` (`id`,`catid`,`status`),
  KEY `listorder` (`id`,`catid`,`status`,`listorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `pc_posid`
--

DROP TABLE IF EXISTS `pc_posid`;
CREATE TABLE IF NOT EXISTS `pc_posid` (
  `id` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL DEFAULT '',
  `listorder` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `pc_posid`
--

INSERT INTO `pc_posid` (`id`, `name`, `listorder`) VALUES
(1, '首页推荐', 0),
(2, '首页幻灯片', 0),
(3, '推荐产品', 0),
(4, '促销产品', 0);

-- --------------------------------------------------------

--
-- 表的结构 `pc_product`
--

DROP TABLE IF EXISTS `pc_product`;
CREATE TABLE IF NOT EXISTS `pc_product` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userid` int(11) unsigned NOT NULL DEFAULT '0',
  `username` varchar(40) NOT NULL DEFAULT '',
  `title` varchar(120) NOT NULL DEFAULT '',
  `title_style` varchar(40) NOT NULL DEFAULT '',
  `keywords` varchar(80) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `content` mediumtext NOT NULL,
  `template` varchar(40) NOT NULL DEFAULT '',
  `thumb` varchar(100) NOT NULL DEFAULT '',
  `posid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `recommend` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `readgroup` varchar(100) NOT NULL DEFAULT '',
  `readpoint` smallint(5) NOT NULL DEFAULT '0',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `url` varchar(60) NOT NULL DEFAULT '',
  `xinghao` varchar(30) NOT NULL DEFAULT '',
  `pics` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`id`,`status`,`listorder`),
  KEY `catid` (`id`,`catid`,`status`),
  KEY `listorder` (`id`,`catid`,`status`,`listorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `pc_role`
--

DROP TABLE IF EXISTS `pc_role`;
CREATE TABLE IF NOT EXISTS `pc_role` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `remark` varchar(255) NOT NULL DEFAULT '',
  `pid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `listorder` smallint(6) unsigned NOT NULL DEFAULT '0',
  `allowpost` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `allowpostverify` tinyint(1) unsigned NOT NULL,
  `allowsearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `allowupgrade` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `allowsendmessage` tinyint(1) unsigned NOT NULL,
  `allowattachment` tinyint(1) NOT NULL,
  `maxpostnum` smallint(5) unsigned NOT NULL DEFAULT '0',
  `maxmessagenum` smallint(5) unsigned NOT NULL DEFAULT '0',
  `price_y` decimal(8,2) unsigned NOT NULL DEFAULT '0.00',
  `price_m` decimal(8,2) unsigned NOT NULL DEFAULT '0.00',
  `price_d` decimal(8,2) unsigned NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- 转存表中的数据 `pc_role`
--

INSERT INTO `pc_role` (`id`, `name`, `status`, `remark`, `pid`, `listorder`, `allowpost`, `allowpostverify`, `allowsearch`, `allowupgrade`, `allowsendmessage`, `allowattachment`, `maxpostnum`, `maxmessagenum`, `price_y`, `price_m`, `price_d`) VALUES
(2, '普通管理员', 1, '普通管理员', 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, '0.00', '0.00', '0.00'),
(3, '图片总监', 1, '编辑部', 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, '0.00', '0.00', '0.00'),
(4, '图片编辑', 1, '编辑部', 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, '0.00', '0.00', '0.00'),
(6, '外联总监', 1, '外联部', 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, '0.00', '0.00', '0.00'),
(13, '营销总监', 1, '营销部', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0.00', '0.00', '0.00'),
(14, '1011', 0, '11', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0.00', '0.00', '0.00');

-- --------------------------------------------------------

--
-- 表的结构 `pc_role_auth`
--

DROP TABLE IF EXISTS `pc_role_auth`;
CREATE TABLE IF NOT EXISTS `pc_role_auth` (
  `id` int(11) NOT NULL,
  `roleid` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `pc_role_auth`
--

INSERT INTO `pc_role_auth` (`id`, `roleid`, `menu_id`) VALUES
(0, 7, 1),
(0, 7, 40),
(0, 7, 41),
(0, 7, 42),
(0, 7, 2),
(0, 7, 73),
(0, 7, 74),
(0, 7, 71),
(0, 7, 75),
(0, 7, 39),
(0, 7, 15),
(0, 7, 93),
(0, 7, 5),
(0, 7, 76),
(0, 7, 9),
(0, 7, 85),
(0, 7, 86),
(0, 7, 104),
(0, 7, 89),
(0, 7, 90),
(0, 7, 91),
(0, 7, 97),
(0, 7, 82),
(0, 7, 107),
(0, 7, 100),
(0, 7, 106),
(0, 7, 102),
(0, 7, 105),
(0, 7, 80),
(0, 7, 84),
(0, 1, 1),
(0, 1, 40),
(0, 1, 41),
(0, 1, 42),
(0, 1, 2),
(0, 1, 73),
(0, 1, 74),
(0, 1, 71),
(0, 1, 75),
(0, 1, 39),
(0, 1, 15),
(0, 1, 93),
(0, 1, 5),
(0, 1, 76),
(0, 1, 9),
(0, 1, 79),
(0, 1, 11),
(0, 1, 12),
(0, 1, 13),
(0, 1, 14),
(0, 1, 78),
(0, 1, 94),
(0, 1, 85),
(0, 1, 86),
(0, 1, 104),
(0, 1, 88),
(0, 1, 89),
(0, 1, 90),
(0, 1, 91),
(0, 1, 92),
(0, 1, 95),
(0, 1, 96),
(0, 1, 97),
(0, 1, 82),
(0, 1, 99),
(0, 1, 100),
(0, 1, 101),
(0, 1, 102),
(0, 1, 103),
(0, 1, 80),
(0, 1, 84),
(0, 3, 1),
(0, 3, 40),
(0, 3, 41),
(0, 3, 42),
(0, 3, 2),
(0, 3, 73),
(0, 3, 74),
(0, 3, 71),
(0, 3, 75),
(0, 3, 39),
(0, 3, 15),
(0, 3, 93),
(0, 3, 5),
(0, 3, 76),
(0, 3, 9),
(0, 3, 79),
(0, 3, 11),
(0, 3, 12),
(0, 3, 13),
(0, 3, 14),
(0, 3, 78),
(0, 3, 94),
(0, 3, 85),
(0, 3, 86),
(0, 3, 104),
(0, 3, 88),
(0, 3, 89),
(0, 3, 90),
(0, 3, 91),
(0, 3, 92),
(0, 3, 95),
(0, 3, 96),
(0, 3, 97),
(0, 3, 82),
(0, 3, 99),
(0, 3, 100),
(0, 3, 101),
(0, 3, 102),
(0, 3, 103),
(0, 3, 80),
(0, 3, 84),
(0, 3, 105),
(0, 2, 41);

-- --------------------------------------------------------

--
-- 表的结构 `pc_role_user`
--

DROP TABLE IF EXISTS `pc_role_user`;
CREATE TABLE IF NOT EXISTS `pc_role_user` (
  `role_id` mediumint(9) unsigned DEFAULT '0',
  `user_id` char(32) DEFAULT '0',
  KEY `group_id` (`role_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `pc_role_user`
--

INSERT INTO `pc_role_user` (`role_id`, `user_id`) VALUES
(0, '28'),
(NULL, '2'),
(NULL, '3'),
(NULL, '33'),
(NULL, '34'),
(NULL, '35'),
(NULL, '36'),
(NULL, '37'),
(NULL, '38'),
(NULL, '39'),
(1, '3'),
(3, '4'),
(NULL, '42'),
(3, '6'),
(3, '7'),
(NULL, '44'),
(4, '9'),
(NULL, '43'),
(4, '11'),
(4, '12'),
(3, '13'),
(NULL, '40'),
(NULL, '41'),
(3, '14'),
(3, '16'),
(2, '17');

-- --------------------------------------------------------

--
-- 表的结构 `pc_shopping`
--

DROP TABLE IF EXISTS `pc_shopping`;
CREATE TABLE IF NOT EXISTS `pc_shopping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` char(50) CHARACTER SET utf8 NOT NULL,
  `photoid` int(20) NOT NULL,
  `phototime` int(25) NOT NULL,
  `phototitle` char(50) CHARACTER SET utf8 NOT NULL,
  `photoplace` char(50) CHARACTER SET utf8 NOT NULL,
  `photovalue` int(20) NOT NULL,
  `url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `up_account` varchar(50) NOT NULL,
  `is_top` int(11) NOT NULL,
  `type` varchar(64) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- 转存表中的数据 `pc_shopping`
--

INSERT INTO `pc_shopping` (`id`, `username`, `photoid`, `phototime`, `phototitle`, `photoplace`, `photovalue`, `url`, `up_account`, `is_top`, `type`) VALUES
(6, 'chenlily8', 324, 1330775070, '', 'Los Angeles', 100, './Uploads/images/32/2012226/1C6DE31310327F53F10D18A38E61556C.jpg', '', 0, '3'),
(8, 'chenlily8', 19, 1330775357, '', 'Los Angeles', 100, './Uploads/images/32/20120205/b597c517a33f1cd498a01e0f89ffffca5753d3dc.jpg', '', 0, '文娱'),
(15, 'chenlily8', 322, 1330775640, '', 'Los Angeles', 100, './Uploads/images/32/2012226/C5B465300C082E80A4FB6A8C9D1272B9.jpg', '', 0, '3'),
(17, 'chenlily8', 261, 1330775901, '', 'London', 100, './Uploads/images/32/20120206/8b7c3bed8233464c276dfee585b2a41722139942.jpg', '', 0, '文娱'),
(20, 'chenlily8', 350, 1330848018, '', '', 0, './Uploads/images/32/2012226/3F372C524E5E9B25F2F481CF067A6813.jpg', '', 0, '4'),
(21, 'chenlily8', 327, 1330950472, '', '', 100, './Uploads/images/32/2012226/BA44FB5F448E0D8F7ADFB8D0FFCAF164.jpg', '', 0, '3');

-- --------------------------------------------------------

--
-- 表的结构 `pc_topic_detail`
--

DROP TABLE IF EXISTS `pc_topic_detail`;
CREATE TABLE IF NOT EXISTS `pc_topic_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `pc_topic_detail`
--

INSERT INTO `pc_topic_detail` (`id`, `topic_id`, `group_id`) VALUES
(2, 1, 25),
(3, 1, 32),
(4, 1, 32);

-- --------------------------------------------------------

--
-- 表的结构 `pc_topic_group`
--

DROP TABLE IF EXISTS `pc_topic_group`;
CREATE TABLE IF NOT EXISTS `pc_topic_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `remark` varchar(1024) NOT NULL,
  `key` varchar(512) NOT NULL,
  `group_count` int(11) NOT NULL,
  `type_one_id` int(11) NOT NULL,
  `type_two_id` int(11) NOT NULL,
  `type_three_id` int(11) NOT NULL,
  `type_one_name` varchar(50) NOT NULL,
  `type_two_name` varchar(50) NOT NULL,
  `type_three_name` varchar(50) NOT NULL,
  `createtime` int(11) NOT NULL,
  `lastedittime` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `pc_topic_group`
--

INSERT INTO `pc_topic_group` (`id`, `title`, `remark`, `key`, `group_count`, `type_one_id`, `type_two_id`, `type_three_id`, `type_one_name`, `type_two_name`, `type_three_name`, `createtime`, `lastedittime`, `state`) VALUES
(1, '专题图组11111', '说明2222222222', '', 0, 4101, 4101, 4104, '野生动物', '野生动物', '景观植物', 1325423315, 1325423315, 0);

-- --------------------------------------------------------

--
-- 表的结构 `pc_trade_detail`
--

DROP TABLE IF EXISTS `pc_trade_detail`;
CREATE TABLE IF NOT EXISTS `pc_trade_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buyer` varchar(50) CHARACTER SET utf8 NOT NULL,
  `seller` varchar(50) CHARACTER SET utf8 NOT NULL,
  `image_id` int(11) NOT NULL,
  `trade_time` int(11) NOT NULL,
  `image_up_time` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  `jifen` int(11) NOT NULL,
  `client_ip` varchar(32) CHARACTER SET utf8 NOT NULL,
  `url` varchar(128) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `pc_trade_detail`
--

INSERT INTO `pc_trade_detail` (`id`, `buyer`, `seller`, `image_id`, `trade_time`, `image_up_time`, `points`, `jifen`, `client_ip`, `url`) VALUES
(1, 'test', 'wangxb', 3, 1322756890, 1322756890, 100, 0, '127.0.0.1', 'Uploads\\images\\60ab016a82a3b4bd308d0055435e91d2b5db2975.jpg'),
(2, 'wangxb', 'wangxb', 4, 1322756890, 1322756890, 100, 0, '127.0.0.1', 'Uploads\\images\\60ab016a82a3b4bd308d0055435e91d2b5db2975.jpg'),
(3, 'test', 'test', 4, 1322756890, 1322756890, 100, 0, '127.0.0.1', 'Uploads\\images\\60ab016a82a3b4bd308d0055435e91d2b5db2975.jpg');

-- --------------------------------------------------------

--
-- 表的结构 `pc_tuijian`
--

DROP TABLE IF EXISTS `pc_tuijian`;
CREATE TABLE IF NOT EXISTS `pc_tuijian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(50) CHARACTER SET utf8 NOT NULL,
  `startdate` date NOT NULL,
  `enddate` date NOT NULL,
  `position` varchar(50) CHARACTER SET utf8 NOT NULL,
  `modi_info` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `pc_tuijian`
--

INSERT INTO `pc_tuijian` (`id`, `account`, `startdate`, `enddate`, `position`, `modi_info`) VALUES
(1, 'wangxb', '2011-01-02', '2012-01-01', '优秀摄影师推荐', '');

-- --------------------------------------------------------

--
-- 表的结构 `pc_type`
--

DROP TABLE IF EXISTS `pc_type`;
CREATE TABLE IF NOT EXISTS `pc_type` (
  `typeid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '',
  `parentid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `description` varchar(200) NOT NULL DEFAULT '',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `listorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keyid` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`typeid`),
  KEY `parentid` (`parentid`,`listorder`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- 转存表中的数据 `pc_type`
--

INSERT INTO `pc_type` (`typeid`, `name`, `parentid`, `description`, `status`, `listorder`, `keyid`) VALUES
(1, '友情链接', 0, '友情链接分类', 1, 0, 1),
(3, '合作伙伴', 1, '合作伙伴', 1, 1, 1),
(2, '默认分类', 1, '默认分类', 1, 0, 1),
(4, '反馈类别', 0, '信息反馈类别', 1, 0, 4),
(5, '产品购买', 4, '产品购买', 1, 0, 5),
(6, '商务合作', 4, '商务合作', 1, 0, 6),
(7, '其他反馈', 4, '其他反馈', 1, 0, 7);

-- --------------------------------------------------------

--
-- 表的结构 `pc_upload_group_detail`
--

DROP TABLE IF EXISTS `pc_upload_group_detail`;
CREATE TABLE IF NOT EXISTS `pc_upload_group_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `up_account` varchar(50) CHARACTER SET utf8 NOT NULL,
  `up_time` int(11) NOT NULL,
  `photo_date` int(11) NOT NULL,
  `check_time` int(11) NOT NULL,
  `check_oper` varchar(50) CHARACTER SET utf8 NOT NULL,
  `main_filename` varchar(128) CHARACTER SET utf8 NOT NULL,
  `main_url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `image_num` int(11) NOT NULL,
  `type` varchar(50) CHARACTER SET utf8 NOT NULL,
  `title` varchar(128) CHARACTER SET utf8 NOT NULL,
  `remark` varchar(3000) CHARACTER SET utf8 NOT NULL,
  `city` varchar(50) CHARACTER SET utf8 NOT NULL,
  `check_state` tinyint(4) NOT NULL,
  `typename` varchar(50) CHARACTER SET utf8 NOT NULL,
  `typeid` int(11) NOT NULL,
  `edit_state` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=103 ;

--
-- 转存表中的数据 `pc_upload_group_detail`
--

INSERT INTO `pc_upload_group_detail` (`id`, `up_account`, `up_time`, `photo_date`, `check_time`, `check_oper`, `main_filename`, `main_url`, `image_num`, `type`, `title`, `remark`, `city`, `check_state`, `typename`, `typeid`, `edit_state`) VALUES
(1, 'xieyd', 1326951115, 0, 1326951458, 'admin', '', './Uploads/thumb/32/20120119/s_a76b6e695e20ed3d208137d1a83e116a54eeae02.jpg', 13, '时效图片', 'Eiffel Tower Skating Rink - Pari', 'Skaters gliding along the ice on the first floor of the Eiffel Tower, 57 meters above ground level, on the first floor of the Eiffel Tower in Paris, France, December 20, 2011. The rink will only be in operation until February 1st. It is the second consecutive year that the ice skating rink opens in the Eiffel Tower. At 200 square meters (2,150 sq feet), it is only about a third of the size of New Yorks famed Rockefeller Center ice rink. Last year, more than 1,000 skaters visited the Towers frozen surface, d', 'Paris', 2, '', 0, 0),
(3, 'xieyd', 1328415182, 0, 0, '', '', './Uploads/thumb/32/20120205/s_d9808e3cafc415109539b54442e10454383c12b1.jpg', 6, '时效图片', 'Jack Osbourne Strolls in Beverly', '漏NATIONAL PHOTO GROUP \nJack Osbourne is seen taking a walk in beverly Hills.\nJob: 010512J1\nNon-Exclusive Jan. 5th, 2012 Beverly Hills, CA\nNPG.com', 'Los Angeles', 2, '', 0, 0),
(4, 'xieyd', 1328424300, 0, 0, '', '', './Uploads/thumb//20120205/s_11b81784d919f24ee732c044b965cb3116927bfd.JPG', 10, '时效图片', 'NHL: DEC 30 Sabres at Capitals', '30 December 2011:   Buffalo Sabres goalie Ryan Miller (30) gives up a goal in action against Washington Capitals center Brooks Laich (21), at the Verizon Center in Washington, D.C. where the Washington Capitals defeated the Buffalo Sabres 3-1. Photo via Newscom', 'WASHINGTON', 2, '', 0, 0),
(5, 'photoer', 1328488751, 0, 0, '', '', './Uploads/thumb/30/20120206/s_0c56354b685007d8ee0c3195dcbd73a46ccaf217.jpg', 4, '时效图片', '', '', '', 2, '', 0, 0),
(6, 'xieyd', 1328500612, 0, 0, '', '', './Uploads/thumb/32/20120206/s_79d198723129eeed0c9699b2e061815fae8247bb.jpg', 36, '时效图片', 'China Jinling Dance Company New ', 'Xu Xinyu dancing with The China Jinling Dance Company of Nanjing in " The Peony Pavilion" on January 5, 2012 at the David Koch Theatre in Lincoln Center. \r\nphoto by Robin Platzer/', 'New York', 2, '', 0, 1),
(7, 'xieyd', 1328501015, 0, 0, '', '', './Uploads/thumb/32/20120206/s_c40ba0f278c2d535f01b300e12c2f459685a4863.jpg', 17, '时效图片', 'Totem Cirque Du Soleil', 'Cirque Du Soleil perform Totem at the Albert Hall. London 4th January 2011.', 'London', 2, '', 0, 1),
(9, 'cjx', 1328501634, 0, 0, '', '', './Uploads/thumb/35/20120206/s_2f329671b375532746db819be3bf46fa816a0ab1.jpg', 4, '时效图片', 'photo', 'photoer', '杭州市', 2, '', 0, 0),
(10, 'photoer', 1328502584, 0, 0, '', '', './Uploads/thumb/30/20120206/s_eae8e14410dd3cc5321d0607317514285a8bc4fa.jpg', 1, '时效图片', 'ss', 'dd', '杭州市', 2, '', 0, 0),
(11, 'xieyd', 1328503328, 0, 0, '', '', './Uploads/thumb/32/20120206/s_2acf0d7d4863019591b7e09d8cf0cda6dc5496a8.jpg', 16, '时效图片', 'Coriolanus Special Screening - C', 'James Nesbitt attends a Special Screening of Coriolanus at The Curzon, Mayfair, London, UK on 5th January 2012.', 'London', 2, '', 0, 1),
(13, 'xieyd', 1328504598, 0, 0, '', '', './Uploads/thumb/32/20120206/s_30d0e3beae52af7609d2776fbf265c63be40c7fb.jpg', 39, '时效图片', '', '', '', 2, '', 0, 1),
(15, 'xieyd', 1328506490, 0, 0, '', '', './Uploads/thumb/32/20120206/s_edfe2d45ec03930cd2590e5c99a6ea26fb02d2ec.jpg', 7, '时效图片', '', '2012年2月1日，安徽滁州市“畅游琅琊・品位山水”健身系列活动正式启动，来自各行各业的广大干部职工和部队官兵及大学生们以“全民健身，健康滁州”为主题，开展了千人登山健身走、自行车骑行、健身运动展演等，及大地丰富了新春佳节期间的群众文化体育生活。', '', 2, '', 0, 1),
(16, 'xieyd', 1328506838, 0, 0, '', '', './Uploads/thumb/32/20120206/s_45c23b83b8d21d6fb70c7afcaba7dff2854f6c30.jpg', 14, '时效图片', '南京：龙年民间艺术大展上的剪纸', '2012年01月27日，江苏省南京市，甘家大院南京市民俗博物馆，龙年春节期间举办“龙年民间艺术大展”。展出的85件民间艺术品均以“龙”为形象，出自南京市非遗传承人和民间艺术家之手，涵盖了剪纸、金银细工、灯彩、绒花、微雕、绳结、泥塑等近20个门类，让市民在“赏龙”之余领略南京传统民间工艺的绝活精粹。', '', 2, '', 0, 0),
(17, 'xieyd', 1328933045, 0, 0, '', '', './Uploads/thumb/32/20120211/s_374026772e65d9193b9fab0e7a51bf7c82f45f92.JPG', 15, '时效图片', 'Fernando Vedasco - Hopman Cup', 'Burswood Dome, Perth  Hopman Cup 2012\r 01/01/2012\rJarmila Gajdosova (AUS) Group  match\rPhoto: Frey Fotosports International / AMN\r Photo via Newscom', 'Perth', 2, '', 0, 1),
(18, 'xieyd', 1328933206, 0, 0, '', '', './Uploads/thumb/32/20120211/s_ba78c6b1b6b146993f39472e61f486ae314d57fb.jpg', 10, '时效图片', 'Celebrities arrive at a NYE part', '漏NATIONAL PHOTO GROUP \nCelebrities arrive at a NYE party at Lady Gagas fathers new restaurant Joanne in NYC.  Lady Gaga and her father originally invested in the Upper West Side restaurant originally named Vince & Eddies. The restaurants name, Joanne, is the name of Josephs sister who died when she was 19 and also the middle name of his famous daughter. It is also being reported that Art Smith, who has worked with Oprah and appeared on "Top Chef Masters" has signed on as chef.\nJob: 010112J1 Pictured: Lady G', 'New York', 1, '', 0, 1),
(19, 'xieyd', 1328933313, 0, 0, '', '', './Uploads/thumb/32/20120211/s_eaeeac72aca20cac839c5c41ebdd754728f867d8.jpg', 15, '时效图片', 'Edward Furlong Shops at Target', '漏NATIONAL PHOTO GROUP \nEdward Furlong shops at Target with a female companion.\nJob: 010112J2\nEXCLUSIVE Dec. 31st, 2012 Los Angeles, CA\nNPG.com', 'Los Angeles', 2, '', 0, 0),
(21, 'xieyd', 1328933874, 0, 0, '', '', './Uploads/thumb/32/20120211/s_20c6e280e1196af361bd8172671e35df661b931a.jpg', 23, '时效图片', '69TH ANNUAL GOLDEN GLOBE AWARDS ', 'Presenter Ashton Kutcher backstage at the 69th Annual Golden Globe Awards at the Beverly Hilton in Beverly Hills, CA on Sunday, January 15, 2012.', 'Beverly', 2, '', 0, 0),
(22, 'xieyd', 1328934065, 0, 0, '', '', './Uploads/thumb/32/20120211/s_07447704ed6c2cf2e6e03db492aabc73b1008cef.JPG', 14, '时效图片', 'NFL: JAN 01 Cowboys at Giants', '01 January 2012: Dallas Cowboys defensive end Marcus Spears (98) during the NFC East matchup between the Dallas Cowboys and the New York Giants at MetLife Stadium in East Rutherford, NJ. The Giants defeated the Cowboys 31-14 to clinch the NFC East and move on to the playoffs. Photo via Newscom', 'East Rutherford', 2, '', 0, 1),
(23, 'xieyd', 1328945209, 0, 0, '', '', './Uploads/thumb/32/20120211/s_4f0b6b66ee773407ec06590e23f0cd3e7463419d.JPG', 9, '时效图片', 'NFL: JAN 01 Cowboys at Giants', '01 January 2012: Dallas Cowboys wide receiver Laurent Robinson (81) during the NFC East matchup between the Dallas Cowboys and the New York Giants at MetLife Stadium in East Rutherford, NJ. The Giants defeated the Cowboys 31-14 to clinch the NFC East and move on to the playoffs. Photo via Newscom', 'East Rutherford', 2, '', 0, 1),
(24, 'system', 1328948137, 0, 0, '', '', './Uploads/thumb/40/20120211/s_75ddc198bad18b519d506853fb78df85f29662f4.jpg', 4, '时效图片', '上传测试', '测试图组', '沈阳市', 2, '', 0, 1),
(25, 'selectersky', 1329023132, 0, 0, '', '', './Uploads/thumb/31/20120212/s_3a81ce41e82afaf049b2a1e9f57ad877b391cebf.JPG', 2, '时效图片', '杭州西博烟花大会1111', '2011年10月20日,杭州西博烟花大会,在钱塘江和运河举行.11212', '杭州', 2, '', 0, 0),
(26, 'xieyd', 1329024207, 0, 0, '', '', './Uploads/thumb/32/20120212/s_b3827be3317d2315e69fe0b6ea6aee28b3bffcc4.JPG', 12, '时效图片', 'Fernando Vedasco - Hopman Cup-xx', 'Burswood Dome, Perth  Hopman Cup 2012/01/01/2012\r\nFernando Verdasco (ESP) Group  match Photo: Frey Fotosports International / AMN Photo via Newscom xxx', 'Perth', 2, '', 0, 1),
(27, 'xieyd', 1329213486, 0, 0, '', '', './Uploads/thumb/32/20120214/s_0b66bc5a09eab752dd5b4ce0bc4c4bada719286a.JPG', 6, '时效图片', '月全食', '月全食', '', 2, '', 0, 1),
(28, 'yangqin', 1329286870, 0, 0, '', '', './Uploads/thumb/46/20120215/s_d9174931cb641448ede4ea62f8d4bdcf43647398.jpg', 11, '时效图片', '26th Annual ASC Awards 2012', 'Charles Haid\r\n02/12/2012 26th Annual ASC Awards held at Grand Ballroom at Hollywood & Highland in Hollywood, CA Photo by Yoko Maegawa /', '', 2, '', 0, 1),
(32, 'zhangjun', 1329287287, 0, 0, '', '', './Uploads/thumb/45/20120215/s_b8dd713640f345ce1f229edad2df238043e1d4d4.JPG', 8, '时效图片', '干旱', '2012年2月15日，云南石林干旱', '昆明市', 2, '', 0, 1),
(34, 'yangqin', 1329287374, 0, 0, '', '', './Uploads/thumb/46/20120215/s_7876fe53ab5443a5aa392449c559eb6d9d596f32.JPG', 17, '时效图片', 'BAFTA Film Awards 2012 Pressroom', 'Billy Bob Thornton in the pressroom at the BAFTA Film Awards 2012, Covent Garden, London 12th February 2012', '', 2, '', 0, 1),
(37, 'yangqin', 1329287615, 0, 0, '', '', './Uploads/thumb/46/20120215/s_8ace2548e658e94a6d4cad71779b3c37f2374f60.jpg', 15, '时效图片', '"Act Of Valor" Premiere 2012', 'Jesse Metcalfe\r\n02/13/2012 "Act Of Valor" Premiere held at the Arclight Cineramadome in Hollywood, CA Photo by Izumi Hasegawa /', '', 2, '', 0, 1),
(43, 'mike', 1329288694, 0, 0, '', '', './Uploads/thumb/44/20120215/s_a7efeaf2f89b08a6e54a3e3289897d363b6fca84.jpg', 7, '时效图片', 'Orange British Academy Film Awar', 'Elizabeth McGovern arrives at the Orange British Academy Film Awards 2012 at The Royal Opera House on February 12, 2012 in London, England.', 'London', 2, '', 0, 1),
(44, 'mike', 1329289821, 0, 0, '', '', '', 0, '时效图片', 'Orange British Academy Film Awar', 'Director Martin Scorsese arrives at the Orange British Academy Film Awards 2012 at The Royal Opera House on February 12, 2012 in London, England.', 'London', 100, '', 0, 0),
(45, 'mike', 1329375902, 0, 0, '', '', '', 0, '时效图片', '', '', '', 100, '', 0, 0),
(46, 'mike', 1329376014, 0, 0, '', '', '', 0, '时效图片', 'Orange British Academy Film Awar', 'Director Martin Scorsese arrives at the Orange British Academy Film Awards 2012 at The Royal Opera House on February 12, 2012 in London, England.', 'London', 100, '', 0, 0),
(47, 'mike', 1329378693, 0, 0, '', '', '', 0, '时效图片', 'Orange British Academy Film Awar', 'Emilia Fox attends the Orange British Academy Film Awards 2012 at the Royal Opera House on February 12, 2012 in London, England.', 'London', 100, '', 0, 0),
(91, 'xieyd', 1330235340, 1330235340, 0, '', 'A6EAAE123BFDA20D5FD05F61F95C0901.jpg', './Uploads/thumb/32/2012226/s_A6EAAE123BFDA20D5FD05F61F95C0901.jpg', 16, '时效图片', 'NCIS Star Michael Weatherly Shops at Barneys NY With his Pregnant Wife', '漏NATIONAL PHOTO GROUP \nMichael Weatherly and wife pregnant wife Bojana Jankovic go shopping at Barneys New York. They are expecting a baby girl in Spring 2012.\nJob: 021912J14\nEXCLUSIVE Feb. 18th, 2012 Beverly Hills, CA\nNPG.com\n', 'Beverly Hills', 2, '', 0, 1),
(92, 'xieyd', 1330235369, 1330235369, 0, '', 'A7A3BB8FFD016D49F6C794FA0856FF7E.jpg', './Uploads/thumb/32/2012226/s_A7A3BB8FFD016D49F6C794FA0856FF7E.jpg', 5, '时效图片', 'Rainn Wilson and the Family at the Clippers Game', '漏NATIONAL PHOTO GROUP \nRainn Wilson leaves the Clippers game with his wife Holiday and son Walter.\nJob: 021912J1\nNon-Exclusive Feb. 18th, 2012 Los Angeles, CA\nNPG.com\n', 'Los Angeles', 2, '', 0, 1),
(93, 'xieyd', 1330235379, 1330235379, 0, '', '11B7AEC729293ED6E68A957A24DFC3F7.jpg', './Uploads/thumb/32/2012226/s_11B7AEC729293ED6E68A957A24DFC3F7.jpg', 18, '时效图片', 'Johnny Hallyday and Tony Parker Chat After the Clippers Game with their Women in Tow', '漏NATIONAL PHOTO GROUP \nJohnny Hallyday and wife Laeticia Boudou are seen chatting with Tony Parker and his girlfriend Axelle.\nJob: 021912J2\nNon-Exclusive Feb. 18th, 2012 Los Angeles, CA\nNPG.com\n', 'Los Angeles', 2, '', 0, 1),
(94, 'xieyd', 1330235410, 1330235410, 0, '', '3F372C524E5E9B25F2F481CF067A6813.jpg', './Uploads/thumb/32/2012226/s_3F372C524E5E9B25F2F481CF067A6813.jpg', 21, '时效图片', 'Dick Van Dyke Grocery Shops with a Younger Woman', '漏NATIONAL PHOTO GROUP \nA cheery Dick Van Dyke is seen ou grocery shopping with a young female friend.\nJob: 021912J3\nEXCLUSIVE Feb. 19th, 2012 Malibu, CA\nNPG.com\n', 'Malibu', 2, '', 0, 1),
(95, 'xieyd', 1330235456, 1330235456, 0, '', 'BA44FB5F448E0D8F7ADFB8D0FFCAF164.jpg', './Uploads/thumb/32/2012226/s_BA44FB5F448E0D8F7ADFB8D0FFCAF164.jpg', 20, '时效图片', '"The Office" Star Rainn Wilson Shares a Kiss with his Wife on the Beach', '漏NATIONAL PHOTO GROUP \nRainn Wilson and wife Holiday stroll on the beach in Malibu together where they share a kiss as son Walter plays on the beach.\nJob: 021912J4\nEXCLUSIVE Feb. 18th, 2012 Malibu, CA\nNPG.com', 'Malibu', 2, '', 0, 1),
(96, 'xieyd', 1330235475, 1330235475, 0, '', 'B43DCC40FDF418DB342A96A6D47C5A5D.jpg', './Uploads/thumb/32/2012226/s_B43DCC40FDF418DB342A96A6D47C5A5D.jpg', 11, '时效图片', 'Robert Pattinson Arrives into LAX', '漏NATIONAL PHOTO GROUP \nRobert Pattinson arrives into LAX Airport.\nJob: 021912J5\nNon-Exclusive Feb. 18th, 2012 Los Angeles, CA\nNPG.com\n', 'Los Angeles', 2, '', 0, 1),
(97, 'xieyd', 1330235493, 1330235493, 0, '', '33BF94A5707A5EC5016695195E5CBC2D.jpg', './Uploads/thumb/32/2012226/s_33BF94A5707A5EC5016695195E5CBC2D.jpg', 23, '时效图片', 'QVCs"Buzz On The Red Carpet" Cocktail Party ', 'LOS ANGELES, CA - FEBRUARY 23: Rachel Pally attends the QVCsBuzz On The Red Carpet Cocktail Party at Four Seasons Hotel Los Angeles at Beverly Hills on February 23, 2012 in Beverly Hills, California.', 'Los Angeles', 2, '', 0, 1),
(98, 'xieyd', 1330235546, 1330235546, 0, '', '2932593FA76E35CD8435FEC32417DE35.jpg', './Uploads/thumb/32/2012226/s_2932593FA76E35CD8435FEC32417DE35.jpg', 32, '时效图片', 'British Prime Minister David Cameron', '\r\n\r\nDavid Cameron\r\n\r\nopens Glyme Hall, Chipping Norton on Friday 20th January 2012. David takes part in pool, table tennis but clearly shows talent in table football by winning the match 2 - 1. David insisted on playing as the blue team and revealed the Cameron household had a table football game for Christmas.\r\n\r\nBarry Clack\r\n', 'Chipping Norton, Witney, Oxfords', 2, '', 0, 1),
(99, 'yangqin', 1330498310, 0, 0, '', '', '', 0, '时效图片', '', '', '', 100, '', 0, 0),
(100, 'chencongli', 1330506350, 0, 0, '', '', './Uploads/thumb/53/20120229/s_bb81b269d51326a5e16e0c08e5441ddb31994b88.jpg', 4, '时效图片', '云南旱区小学可能因缺水整体搬迁 部分学生转学', '云南旱区小学可能因缺水整体搬迁 部分学生转学', '', 2, '', 0, 0),
(101, 'chencongli', 1330774535, 0, 0, '', '', '', 0, '资料图片', '', '', '', 100, '', 0, 0),
(102, 'selectersky', 1330791609, 1330791609, 0, '', 'AEA62260DD978B41D077555A2DE6D0CC.jpg', './Uploads/thumb/31/201234/s_AEA62260DD978B41D077555A2DE6D0CC.jpg', 5, '时效图片', 'Beth Jeans Houghton live at Hoxton Bar & Kitchen', 'Beth Jeans Houghton live at Hoxton Bar & Kitchen on 1st March 2012 漏 Al de Perez', 'London', 2, '', 0, 1);

-- --------------------------------------------------------

--
-- 表的结构 `pc_upload_images_detail`
--

DROP TABLE IF EXISTS `pc_upload_images_detail`;
CREATE TABLE IF NOT EXISTS `pc_upload_images_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `account` varchar(32) CHARACTER SET utf8 NOT NULL,
  `createtime` int(11) NOT NULL,
  `photo_time` int(11) NOT NULL,
  `filename` varchar(64) CHARACTER SET utf8 NOT NULL,
  `cate` varchar(20) CHARACTER SET utf8 NOT NULL,
  `url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `type` varchar(64) CHARACTER SET utf8 NOT NULL,
  `title` varchar(128) CHARACTER SET utf8 NOT NULL,
  `country` varchar(50) CHARACTER SET utf8 NOT NULL,
  `province` varchar(50) CHARACTER SET utf8 NOT NULL,
  `city` varchar(32) CHARACTER SET utf8 NOT NULL,
  `remark` varchar(255) CHARACTER SET utf8 NOT NULL,
  `point` int(11) NOT NULL,
  `check_state` tinyint(4) NOT NULL,
  `big_url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `small_url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `filesize` int(11) NOT NULL,
  `author` varchar(50) CHARACTER SET utf8 NOT NULL,
  `key` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `pc_urlrule`
--

DROP TABLE IF EXISTS `pc_urlrule`;
CREATE TABLE IF NOT EXISTS `pc_urlrule` (
  `urlruleid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `ishtml` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `showurlrule` varchar(255) NOT NULL,
  `showexample` varchar(255) NOT NULL,
  `listurlrule` varchar(255) NOT NULL,
  `listexample` varchar(255) NOT NULL,
  `listorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`urlruleid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `pc_urlrule`
--

INSERT INTO `pc_urlrule` (`urlruleid`, `ishtml`, `showurlrule`, `showexample`, `listurlrule`, `listexample`, `listorder`) VALUES
(1, 0, '{$catdir}/show/{$id}.html|{$catdir}/show/{$id}_{$page}.html', 'news/show/1.html|news/show/1_1.html', '{$catdir}/|{$catdir}/{$page}.html', 'news/|news/1.html', 0),
(2, 0, 'show-{$catid}-{$id}.html|show-{$catid}-{$id}-{$page}.html', 'show-1-1.html|show-1-1-1.html', 'list-{$catid}.html|list-{$catid}-{$page}.html', 'list-1.html|list-1-1.html', 0),
(3, 0, '{$module}/show/{$id}.html|{$module}/show/{$id}-{$page}.html', 'Article/show/1.html|Article/show/1-1.html', '{$module}/list/{$catid}.html|{$module}/list/{$catid}-{$page}.html', 'Article/list/1.html|Article/list/1-1.html', 0),
(4, 1, '{$parentdir}{$catdir}/show_{$id}.html|{$parentdir}{$catdir}/show_{$id}_{$page}.html', 'news/show_1.html|news/show_1_1.html', '{$parentdir}{$catdir}/|{$parentdir}{$catdir}/{$page}.html', 'news/|news/1.html', 0);

-- --------------------------------------------------------

--
-- 表的结构 `pc_user`
--

DROP TABLE IF EXISTS `pc_user`;
CREATE TABLE IF NOT EXISTS `pc_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `realname` varchar(50) NOT NULL DEFAULT '',
  `question` varchar(50) NOT NULL DEFAULT '',
  `answer` varchar(50) NOT NULL DEFAULT '',
  `sex` tinyint(1) NOT NULL DEFAULT '0',
  `tel` varchar(50) NOT NULL DEFAULT '',
  `mobile` varchar(50) NOT NULL DEFAULT '',
  `fax` varchar(50) NOT NULL DEFAULT '',
  `web_url` varchar(100) NOT NULL DEFAULT '',
  `address` varchar(100) NOT NULL DEFAULT '',
  `login_count` int(11) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `last_logintime` int(11) unsigned NOT NULL DEFAULT '0',
  `reg_ip` char(15) NOT NULL DEFAULT '',
  `last_ip` char(15) NOT NULL DEFAULT '',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `amount` decimal(8,2) unsigned NOT NULL DEFAULT '0.00',
  `point` smallint(5) unsigned NOT NULL DEFAULT '0',
  `avatar` varchar(120) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- 转存表中的数据 `pc_user`
--

INSERT INTO `pc_user` (`id`, `groupid`, `username`, `password`, `email`, `realname`, `question`, `answer`, `sex`, `tel`, `mobile`, `fax`, `web_url`, `address`, `login_count`, `createtime`, `updatetime`, `last_logintime`, `reg_ip`, `last_ip`, `status`, `amount`, `point`, `avatar`) VALUES
(1, 1, 'admin', '3da8dc80c8e1e6c5bbcf40806903345f50b5ca2d', 'admin2@aaa.cn', '管理员', '', '', 1, '', '', '', '', '', 400, 1321685843, 1322066981, 1333813804, '127.0.0.1', '127.0.0.1', 1, '0.00', 0, ''),
(2, 2, 'wangxb', '3da8dc80c8e1e6c5bbcf40806903345f50b5ca2d', 'ncsy1983@126.com', '王小兵', '2222', '', 1, '13757190063', '13757190063', '12345678', '', '', 3, 1321860184, 1322148631, 1329121846, '127.0.0.1', '114.96.144.29', 1, '0.00', 0, ''),
(3, 1, 'test1', 'c137340e996daae5fec608dcffd7683442cf8024', '111@125.com', '1111', '11', '22', 1, '1111', '111', '111', '111', '111', 1, 1329065963, 0, 1329066000, '114.96.144.114', '114.96.144.114', 0, '0.00', 0, ''),
(4, 3, 'chencongli', 'c137340e996daae5fec608dcffd7683442cf8024', 'chencongli@photochina.com', '陈聪丽', '', '', 1, '', '', '', '', '', 36, 1329118126, 1329118668, 1330848531, '124.160.28.18', '124.160.28.18', 1, '0.00', 0, ''),
(6, 3, 'zhangjing', 'c137340e996daae5fec608dcffd7683442cf8024', 'zhangjing1@photochina.com.cn', '章静', '', '', 1, '', '', '', '', '', 7, 1329212684, 1330589423, 1330589264, '124.160.28.18', '124.160.28.18', 1, '0.00', 0, ''),
(7, 3, 'wusaihua', 'c137340e996daae5fec608dcffd7683442cf8024', 'wusaihua1@photochina.com.cn', '吴赛华', '', '', 1, '', '', '', '', '', 3, 1329212902, 1330589438, 1330589474, '124.160.28.18', '124.160.28.18', 1, '0.00', 0, ''),
(9, 4, 'honggang', 'c137340e996daae5fec608dcffd7683442cf8024', 'honggang@photochina.com.cn', '洪纲', '', '', 0, '', '', '', '', '', 3, 1329213095, 0, 1330497022, '124.160.28.18', '124.160.28.18', 1, '0.00', 0, ''),
(11, 4, 'zhangjun', 'c137340e996daae5fec608dcffd7683442cf8024', 'zhangjun@photochina.com.cn', '章俊', '', '', 0, '', '', '', '', '', 5, 1329213202, 0, 1330848423, '124.160.28.18', '124.160.28.18', 1, '0.00', 0, ''),
(12, 4, 'yeyuling', '0a5271e0f6c48ca2c0e42ef6e5ab2e4419237161', 'yeyuling@photochina.com.cn', '叶玉玲', '', '', 0, '', '', '', '', '', 1, 1329286836, 0, 1330495510, '124.160.28.18', '124.160.28.18', 1, '0.00', 0, ''),
(13, 3, 'wuang', 'c137340e996daae5fec608dcffd7683442cf8024', 'wuang@photochina.com.cn', '吴昂', '', '', 0, '', '', '', '', '', 2, 1329287982, 0, 1330240366, '124.160.28.18', '202.107.194.77', 1, '0.00', 0, ''),
(14, 3, 'yangning', 'c137340e996daae5fec608dcffd7683442cf8024', 'yangning@photochina.com.cn', '杨宁', '', '', 0, '', '', '', '', '', 2, 1329718788, 0, 1330761142, '124.160.28.18', '124.160.28.18', 1, '0.00', 0, ''),
(16, 3, 'zhaojie', 'c137340e996daae5fec608dcffd7683442cf8024', 'zhaojie@photochian.com.cn', '赵劼', '', '', 0, '', '', '', '', '', 5, 1329718959, 0, 1330673933, '124.160.28.18', '124.160.28.18', 1, '0.00', 0, ''),
(17, 2, 'ddddd', 'cb7ad6eecf07e0de50f64ce98069193e0a0ddd5e', '11@qq.com', '11111', '', '', 0, '', '', '', '', '', 0, 1329806266, 0, 0, '124.73.76.100', '', 0, '0.00', 0, '');

-- --------------------------------------------------------

--
-- 表的结构 `pc_userrequire`
--

DROP TABLE IF EXISTS `pc_userrequire`;
CREATE TABLE IF NOT EXISTS `pc_userrequire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 NOT NULL,
  `createtime` int(11) unsigned NOT NULL,
  `type` varchar(50) CHARACTER SET utf8 NOT NULL,
  `title` varchar(512) CHARACTER SET utf8 NOT NULL,
  `people` varchar(30) CHARACTER SET utf8 NOT NULL,
  `place` varchar(50) CHARACTER SET utf8 NOT NULL,
  `time` int(25) NOT NULL,
  `size` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `starttime` int(25) NOT NULL,
  `endtime` int(25) NOT NULL,
  `value` int(25) NOT NULL,
  `state` varchar(300) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- 转存表中的数据 `pc_userrequire`
--

INSERT INTO `pc_userrequire` (`id`, `username`, `createtime`, `type`, `title`, `people`, `place`, `time`, `size`, `starttime`, `endtime`, `value`, `state`) VALUES
(19, '', 0, '文娱历史', '这次测试标题', '你好', '杭州', 1324526400, '78', 1324526400, 1325304000, 900, '后说呢么 '),
(20, 'selectersky', 0, '文娱,时尚,历史', '', '人物', '地点', 1332518400, '121', 1332518400, 1332518400, 222, '备注备注备注'),
(21, 'cjx', 0, '时事,文娱,体育', '标题   ', '人物   ', '地点 ', 1332604800, '长边不小于 ', 1331654400, 1332432000, 1333, '备注'),
(22, 'cjx', 0, '时事,文娱,体育,财经,时尚,专题,历史', 'aaa', '人物', '地点', 1333814400, '222', 1333814400, 1333814400, 222, 'sadfasfsf'),
(23, 'cjx', 0, '', 'cccc1212', '人物', '地点', 0, '1', 0, 1333814400, 2, ''),
(24, 'cjx', 1333817347, '时事', 'a', 'a', 'a', 0, '0', 0, 1333814400, 0, 'a');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
